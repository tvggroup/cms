﻿loadFiles();

//@*nút bấm Home*@
$('#button-home').click(function (e) {
    e.preventDefault();
    var directory = "";
    var url = '/MediaManager/init?directory=' + directory;
    loadFiles(url);
});

//@*nút bấm Refresh*@
$('#button-refresh').click(function () {
    var directory = $('#hfDirectory').val();
    var url = '/MediaManager/init?directory=' + directory;
    loadFiles(url);
});

//@*nút bấm Parent (về thư mục cha)*@
$('#button-parent').on('click', function (e) {
    e.preventDefault();
    loadFiles($(this).attr("href"));
});

//@*nút bấm Folder (mở popup tạo thư mục mới)*@
$('#button-folder').popover({
    html: true,
    placement: 'bottom',
    trigger: 'click',
    title: 'Tạo thư mục mới',
    content: function () {
        html = '<div class="input-group">';
        html += '  <input type="text" name="folder" value="" placeholder="Tên thư mục" class="form-control">';
        html += '  <span class="input-group-btn"><button type="button" title="Tạo" id="button-create" class="btn btn-primary"><i class="fa fa-plus-circle"></i></button></span>';
        html += '</div>';

        return html;
    }
});

//@*nút bấm Folder (thực hiện tạo thư mục mới)*@
$('#button-folder').on('shown.bs.popover', function () {
    $('#button-create').on('click', function () {
        var directory = $('#hfDirectory').val();
        $.ajax({
            url: '/MediaManager/folder?directory=' + directory,
            type: 'post',
            dataType: 'json',
            data: { 'folder': encodeURIComponent($('input[name=\'folder\']').val()) },
            beforeSend: function () {
                $('#button-create').prop('disabled', true);
            },
            complete: function () {
                $('#button-create').prop('disabled', false);
                $('#button-folder').popover('hide');
            },
            success: function (json) {
                if (json.status == "error") {
                    toastr[json.status](json.notification);
                }

                if (json.status == 'success') {
                    $('#button-refresh').trigger('click');
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    });
});

$("#button-upload2").on("click", function () {
    var directory = $('#hfDirectory').val();
    $("#frmUpload").attr('action', '/MediaManager/upload?directory=' + directory);
    $("#upload-file-list").html("");
    $('#modal-upload').modal("show");
});

$("#button-file-select").on("click", function () {
    $(this).parent().find('input[type="file"]').trigger('click');
    var directory = $('#hfDirectory').val();
    $("frmUpload").attr('action', '/MediaManager/upload?directory=' + directory);
});

$("#frmUpload").fileupload({
    // This function is called when a file is added to the queue;
    // either via the browse button, or via drag/drop:
    add: function (e, data) {
        var ul = $(this).find('ul');
        var tpl = $('<li class="working"><input type="text" value="0" data-width="48" data-height="48"' +
            ' data-fgColor="#0788a5" data-readOnly="1" data-bgColor="#3e4043" /><p></p><span></span></li>');

        // Append the file name and file size
        tpl.find('p').text(data.files[0].name)
                     .append('<i>' + framework.formatFileSize(data.files[0].size) + '</i>');

        // Add the HTML to the UL element
        data.context = tpl.appendTo(ul);

        // Initialize the knob plugin
        tpl.find('input').knob();

        // Listen for clicks on the cancel icon
        tpl.find('span').click(function () {

            if (tpl.hasClass('working')) {
                jqXHR.abort();
            }

            tpl.fadeOut(function () {
                tpl.remove();
            });

        });

        // Automatically upload the file once it is added to the queue
        var jqXHR = data.submit();
    },

    done: function (e, data) {
        loadFiles();
    },

    progress: function (e, data) {

        // Calculate the completion percentage of the upload
        var progress = parseInt(data.loaded / data.total * 100, 10);

        // Update the hidden input field and trigger a change
        // so that the jQuery knob plugin knows to update the dial
        data.context.find('input').val(progress).change();

        if (progress == 100) {
            data.context.removeClass('working');
        }
    },

    fail: function (e, data) {
        // Something has gone wrong!
        data.context.addClass('error');
    }
});

//@*nút bấm Upload*@
$('#button-upload').on('click', function () {

    //@*remove form upload cũ*@
    $('#form-upload').remove();

    //@*tạo form upload mới*@
    $('body').prepend(
        '<form enctype="multipart/form-data"  id="form-upload" style="display: none;">' +
            '<input type="file" name="upload_images"  value="" />' +
        '</form>'
    );

    $('#form-upload input[name=\'upload_images\']').trigger('click');

    //if ($('#form-upload input[name=\'upload_images\']').val() != '') {
    //    var directory = $('#hfDirectory').val();
    //    $.ajax({
    //        url: '/MediaManager/upload?directory=' + directory,
    //        type: 'post',
    //        dataType: 'json',
    //        data: new FormData($('#form-upload')[0]),
    //        cache: false,
    //        traditional: true,
    //        contentType: false,
    //        processData: false,
    //        beforeSend: function () {
    //            $('#button-upload i').replaceWith('<i class="fa fa-circle-o-notch fa-spin"></i>');
    //            $('#button-upload').prop('disabled', true);
    //        },
    //        complete: function () {
    //            $('#button-upload i').replaceWith('<i class="fa fa-upload"></i>');
    //            $('#button-upload').prop('disabled', false);
    //        },
    //        success: function (json) {
    //            if (json.status == "error") {
    //                toastr[json.status](json.notification);
    //            }

    //            if (json.status == 'success') {
    //                $('#button-refresh').trigger('click');
    //            }
    //        },
    //        error: function (xhr, ajaxOptions, thrownError) {
    //            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
    //        }
    //    });
    //};

    timer = setInterval(function () {
        if ($('#form-upload input[name=\'upload_images\']').val() != '') {
            clearInterval(timer);
            var directory = $('#hfDirectory').val();
            $.ajax({
                url: '/MediaManager/upload?directory=' + directory,
                type: 'post',
                dataType: 'json',
                data: new FormData($('#form-upload')[0]),
                cache: false,
                traditional: true,
                contentType: false,
                processData: false,
                beforeSend: function () {
                    $('#button-upload i').replaceWith('<i class="fa fa-circle-o-notch fa-spin"></i>');
                    $('#button-upload').prop('disabled', true);
                },
                complete: function () {
                    $('#button-upload i').replaceWith('<i class="fa fa-upload"></i>');
                    $('#button-upload').prop('disabled', false);
                },
                success: function (json) {
                    if (json.status == "error") {
                        toastr[json.status](json.notification);
                    }

                    if (json.status == 'success') {
                        $('#button-refresh').trigger('click');
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        }
    }, 5000);
});

//@*dropdownlist chọn cách sắp xếp*@
$("#ddlOrder").change(function () {
    var sort = $(this).val();
    $.cookie('cms_media_manager_sort', sort);
    loadFiles();
});

//@*nút bấm Delete (xoá file và folder được chọn)*@
$('#list-cmd #button-delete').on('click', function (e) {
    if (confirm('Bạn có chắc chắn muốn xoá?')) {
        var paths = [];
        $('input[name^=\'filepath\']:checked').each(function () {
            paths.push($(this).val());
        });
        var dataPost = {
            filePaths: paths
        };
        $.ajax({
            url: '/MediaManager/delete',
            type: 'post',
            dataType: 'json',
            data: dataPost,
            traditional: true,
            beforeSend: function () {
                $('#button-delete').prop('disabled', true);
            },
            complete: function () {
                $('#button-delete').prop('disabled', false);
            },
            success: function (json) {
                if (json.status == "error") {
                    toastr[json.status](json.notification);
                }

                if (json.status == 'success') {
                    $('#button-refresh').trigger('click');
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }
});

//@*nút bấm Search*@
$('#button-search').on('click', function () {
    var directory = $('#hfDirectory').val();
    var url = '/MediaManager/init?directory=' + directory;

    var filter_name = $('input[name=\'search\']').val();

    if (filter_name) {
        url += '&filter_name=' + encodeURIComponent(filter_name);
    }

    loadFiles(url);
});

$("#file-search").keypress(function (event) {
    if (event.which == 13) {
        var directory = $('#hfDirectory').val();
        var url = '/MediaManager/init?directory=' + directory;
        var filter_name = $('input[name=\'search\']').val();
        if (filter_name) {
            url += '&filter_name=' + encodeURIComponent(filter_name);
        }
        loadFiles(url);
    }
});

//tạo event khi nhấn nút Select File, hàm xử lý sự kiện được thêm vào khi tạo popup
var onFileSelected = function (event, file, target) { };
$(window).on("fileSelected", function (event, file, target) { onFileSelected(event, file, target); });

//@*nút bấm select (thực hiện công việc đối với các file được chọn)*@
$('#button-select').on('click', function () {
    var files = [];
    var fileUrl = "";
    var filePath = "";
    $('input[name="filepath"]:checked').each(function () {
        var $this = $(this);
        filePath = $this.val();
        fileUrl = $this.attr("data-url");
        fileName = $this.attr("data-name");
        files.push({ path: filePath, url: fileUrl, name: fileName });
    });
    if (files.length > 0) {
        var ckEditor = $("#hfCKEditor").val();
        var ckEditorFuncNum = $("#hfCKEditorFuncNum").val();
        //console.log(ckEditorFuncNum);
        //console.log(fileUrl);
        //xu ly function file browser cua ckeditor
        if (ckEditor !== "" && ckEditorFuncNum > 0) {           
            window.opener.CKEDITOR.tools.callFunction(ckEditorFuncNum, fileUrl);
            window.close();
        } else {
            var target = $("#hfTarget").val();
            if (target.lastIndexOf("ckeditor", 0) === 0) {
                var fileType = target.split('_')[1];
                var target_name = target.split('_')[2];
                var oEditor = window.opener.CKEDITOR.instances[target_name];
                for (var i = 0; i < files.length; i++) {
                    var fileUrl = files[i].url;
                    var filePath = files[i].path;
                    var html_tag = "";
                    if (fileType == "pdf") {
                        html_tag = "<div class='pdfviewer'><object data='" + fileUrl + "' type='application/pdf' width='100%' height='700px'>" +
                            "<embed src='" + fileUrl + "'>Trình duyệt không hỗ trợ xem PDF</embed></object></div>";
                    } else {
                        html_tag = "<img src='" + fileUrl + "' style='max-width:100%' /></br>";
                    }
                    oEditor.insertHtml(html_tag);
                }
                window.close();
            } else {
                //window.opener.processFile(files, target);
                $(window).trigger("fileSelected", [files, target]);
            }
        }
        
    }
});

//nút bấm view list
$('#button-list').on('click', function () {
    $.cookie('cms_media_manager_view', 'list-view');
    $('#list-files').removeClass("grid-view").addClass("list-view");
});
//nút bấm view grid
$('#button-grid').on('click', function () {
    $.cookie('cms_media_manager_view', 'grid-view');
    $('#list-files').removeClass("list-view").addClass("grid-view");
});

//nút bấm copy
$('#button-copy').on('click', function () {
    prepareCopy('copy');
});

//nút bấm cut
$('#button-cut').on('click', function () {
    prepareCopy('cut');
});

//nút bấm paste
$('#button-paste').on('click', function () {
    var paths = [];
    var directory = $('#hfDirectory').val();
    var url = '/MediaManager/CopyOrMove';
    $('input[class^=\'copy-file-\']').each(function () {
        paths.push($(this).val());
    });
    var action = $('#hfCopyAction').val();
    var dataPost = {
        filePaths: paths,
        destinationFolder: directory,
        action: action
    };
    $.ajax({
        url: url,
        type: 'post',
        dataType: 'json',
        data: dataPost,
        traditional: true,
        async: false,
        beforeSend: function () {
            $('#button-paste').prop('disabled', true);
        },
        complete: function () {
            $('#button-paste').prop('disabled', false);
        },
        success: function (json) {
            if (json.status == "error") {
                toastr[json.status](json.notification);
            }

            if (json.status == 'success') {
                $('#button-refresh').trigger('click');
                if (action == 'cut') {
                    $('#copyFilesHolder').children('[class^="copy-file-"]').remove();
                    $('#button-cut').removeClass('btn-primary').addClass('btn-default');
                    $('#button-paste').addClass("disabled");
                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
});

//@*click vào thư mục trong danh sách --> mở thư mục đó*@
$('#list-files').on('click', '.directory-thumb', function (e) {
    e.preventDefault();
    loadFiles($(this).attr("href"));
});

//@*click vào file trong danh sách --> chọn/bỏ chọn file*@
$('#list-files').on('click', '.thumbnail', function (e) {
    e.preventDefault();
    var checkbox = $(this).parent().next("label").find("input[type='checkbox']");
    if (checkbox.is(":checked")) {
        checkbox.prop('checked', false);
        $(this).closest('.file-item').removeClass('selected');
    }
    else {
        checkbox.prop('checked', true);
        $(this).closest('.file-item').addClass('selected');
    }

});

//@*click vào checkbox ở file/thư mục trong danh sách --> chọn/bỏ chọn*@
$('#list-files').on('change', '.filepath', function (e) {
    if (this.checked)
        $(this).closest('.file-item').addClass('selected');
    else
        $(this).closest('.file-item').removeClass('selected');
});

//double click vào tên thư mục 
$('#list-files').on('dblclick', '.directory', function (e) {
    e.preventDefault();
    var link = $(this).prev().find(".directory-thumb").attr("href");
    loadFiles(link);
});

//@*double click vào file --> thực hiện công việc với file đó*@
$('#list-files').on('dblclick', '.thumbnail', function (e) {
    e.preventDefault();
    var files = [];
    var fileUrl = $(this).attr("href");
    var filePath = $(this).parent().next("label").find("input[type='checkbox']").val();
    files.push({ path: filePath, url: fileUrl });
    if (files.length > 0) {
        var target = $("#hfTarget").val();
        if (target.lastIndexOf("ckeditor", 0) === 0) {
            var fileType = target.split('_')[1];
            var target_name = target.split('_')[2];
            var oEditor = window.opener.CKEDITOR.instances[target_name];
            for (var i = 0; i < files.length; i++) {
                var fileUrl = files[i].url;
                var filePath = files[i].path;
                var html_tag = "";
                if (fileType == "pdf") {
                    html_tag = '<div class="pdfviewer"><object data="' + fileUrl + '" type="application/pdf" width="100%" height="700px">' +
                                    '<embed src="' + fileUrl + '"></embed></object></div>';
                } else {
                    html_tag = "<img src='" + fileUrl + "' style='max-width:100%' /></br>";
                }
                oEditor.insertHtml(html_tag);
            }
            window.close();
        } else {
            window.opener.processFile(files, target);
        }
    }
});

//@*phân trang*@
$('#pagination').on('click', 'a', function (e) {
    e.preventDefault();
    loadFiles($(this).attr("href"));
});

//$("#list-files").slimScroll({ height: '350px' });

//@*load danh sách file*@
function loadFiles(directory) {
    directory = typeof directory !== 'undefined' ? directory : '';
    var cookieSort = $.cookie('cms_media_manager_sort');
    var sort = 0;
    if (typeof cookieSort !== 'undefined' && cookieSort !== '')
        sort = cookieSort;

    var cookieView = $.cookie('cms_media_manager_view');
    var viewType = 'list-view';
    if (typeof cookieView !== 'undefined' && cookieView !== '')
        viewType = cookieView;

    if (directory == '') {
        cookieDir = $.cookie('cms_opened_dir');
        if (typeof cookieDir === 'undefined' || cookieDir == '')
            directory = "/MediaManager/init";
        else
            directory = cookieDir;
    }
    $.ajax({
        url: directory,
        type: 'post',
        dataType: 'json',
        data: { sort: sort },
        beforeSend: function () {
            $.blockUI();
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (json) {
            console.log(json);
            $("#list-files").html('').addClass(viewType);

            $('#button-parent').attr("href", json.parent);
            //$('#pagination').html(json.pagination);
            $('#hfDirectory').val(json.directory);
            var row = $('<div class="row"></div>');
            for (var i = 0; i < json.images.length; i++) {
                if (json.images[i].Name != "cache") {
                    $("#contactTpl").tmpl(json.images[i]).appendTo(row);
                    if (i == json.images.length - 1) {
                        row.appendTo('#list-files');
                    }
                }
            }
            $.cookie('cms_opened_dir', directory)
            //$('#list-files').find('a[rel="lightbox"]').slimbox({/* Put custom options here */ }, null, function (el) {
            //    return (this == el) || ((this.rel.length > 8) && (this.rel == el.rel));
            //});
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
}

//@*hàm xử lý file được chọn*@
function processFileEditor(files) {
    var clientDomain = $('#hfClientDomain').val();
    var currentInstance = "";
    for (var i in window.opener.CKEDITOR.instances) {
        currentInstance = i;
        break;
    }
    //alert(currentInstance);
    var oEditor = window.opener.CKEDITOR.instances[currentInstance];
    for (var i = 0; i < files.length; i++) {
        //console.log(files[i]);
        var image_tag = "<figure class='caption'><img style='max-width:100%' src='" + files[i].url + "' /><figcaption>Tiêu đề hình ảnh</figcaption></figure></br>";
        oEditor.insertHtml(image_tag);
    }
}

function prepareCopy(action)
{
    var files = [];
    var fileUrl = "";
    var filePath = "";
    var btnCopy = $('#button-copy');
    var btnCut = $('#button-cut');
    var btnPaste = $('#button-paste');
    var copyFilesHolder = $('#copyFilesHolder');

    btnPaste.removeClass("disabled");
    btnCopy.removeClass('btn-default').removeClass('btn-primary');
    btnCut.removeClass('btn-default').removeClass('btn-primary');
    if (action == "copy") {
        btnCopy.addClass('btn-primary');
        btnCut.addClass('btn-default');
    } else {
        btnCopy.addClass('btn-default');
        btnCut.addClass('btn-primary');
    }

    copyFilesHolder.children('[class^="copy-file-"]').remove();
    $('#hfCopyAction').val(action);

    $('input[name="filepath"]:checked').each(function () {
        var $this = $(this);
        filePath = $this.val();
        fileUrl = $this.attr("data-url");
        fileName = $this.attr("data-name");
        files.push({ path: filePath, url: fileUrl, name: fileName });
    });
    if (files.length > 0) {
        for (var i = 0; i < files.length; i++) {
            copyFilesHolder.append('<input type="hidden" class="copy-file-' + i + '" value="' + files[i].path + '" />');
        }
    } else {
        btnPaste.addClass("disabled");
        btnCopy.removeClass('btn-primary').removeClass('btn-default').addClass('btn-default');
        btnCut.removeClass('btn-primary').removeClass('btn-default').addClass('btn-default');
    }
}