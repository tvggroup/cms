﻿// Its good practice to wrap your JavaScript in an immediately-invoked function expression (IIFE) 
// (See https://en.wikipedia.org/wiki/Immediately-invoked_function_expression) to stop your functions being added to 
// the global scope (See https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions).

// framework - The namespace under which all your code should live if you want it to be globally available. As an 
//             example a calculator object has been created under the framework namespace below. Note that due to the
//             '||' we are using when passing in the namespace at the bottom of this file, you can repeat the pattern
//             in as many files as you want. The calculator would be better off in a calculator.js file for example.
//             (See http://elijahmanor.com/angry-birds-of-javascript-red-bird-iife/)
//         $ - The jQuery object. We can't assume that '$' sign is the main jQuery object or being used for something
//             else. It is good practice to pass in the full 'jQuery' object and assign it to the dollar sign here.
//    window - The window object could have been changed during the application lifetime, to guard against this it is 
//             good practice to hold onto a copy.
//  document - Same as above.
// undefined - We have an undefined parameter but we do not pass in a value for it. This ensures that the undefined 
//             keyword does actually mean undefined and has not been reassigned (Yes, that is possible in JavaScript).
(function (framework, $, window, document, undefined) {
    "use strict";

    // bỏ dấu tiếng việt thành không dấu
    // sử dụng: framework.Slugify(text)
    framework.Slugify = function (str) {
        str = str.toLowerCase();
        str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
        str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
        str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
        str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
        str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
        str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
        str = str.replace(/đ/g, "d");
        str = str.replace(/!|@|\$|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|;|“|”|\.|\:|\'| |\"|\&|\#|\[|\]|~/g, "-");
        str = str.replace(/-+-/g, "-"); //thay thế 2- thành 1-
        str = str.replace(/^\-+|\-+$/g, "");//cắt bỏ ký tự - ở đầu và cuối chuỗi

        return str;
    };

    framework.bindFormValidation = function (selector) {
        $(selector).validate({
            ignore: ".ignore",
            invalidHandler: function (e, validator) {
                if (validator.errorList.length) {
                    var elem = $(validator.errorList[0].element);
                    var form = elem.closest('form');
                    var tabs = form.find(".tabs")
                    if (tabs.length > 0) {
                        var tabId = elem.closest('.tab-panel').attr('id');
                        var tabNavs = tabs.find('.tabs-nav');
                        tabNavs.find('a[href="#' + tabId + '"]').trigger('click');
                    }
                }
            },
            highlight: function (element) {
                $(element).closest('.form-group').addClass("has-error");
            },
            unhighlight: function (element) {
                $(element).closest('.form-group').removeClass("has-error");
            },
        });
    };

    framework.formatFileSize = function (bytes) {
        if (typeof bytes !== 'number') {
            return '';
        }

        if (bytes >= 1000000000) {
            return (bytes / 1000000000).toFixed(2) + ' GB';
        }

        if (bytes >= 1000000) {
            return (bytes / 1000000).toFixed(2) + ' MB';
        }

        return (bytes / 1000).toFixed(2) + ' KB';
    };

    framework.truncateString = function (str, n, useWordBoundary) {
        var isTooLong = str.length > n,
            s_ = isTooLong ? str.substr(0, n - 1) : str;
        useWordBoundary = useWordBoundary ? useWordBoundary : true;
        s_ = (useWordBoundary && isTooLong) ? s_.substr(0, s_.lastIndexOf(' ')) : s_;
        return isTooLong ? s_ + '&hellip;' : s_;
    };

    $(function () {
        // Add your code here if you want to access the DOM.
    });

    // This is an example of creating a Calculator object under the framework namespace. The Calculator is using the 
    // Revealing Prototype Pattern. You can use the Calculator using the 'new' keyword e.g. var c = new Calculator();
    // (See http://weblogs.asp.net/dwahlin/techniques-strategies-and-patterns-for-structuring-javascript-code-revealing-prototype-pattern)
    framework.Calculator = function (throwOnDivideByZero) {
        // Calculator constructor. You can pass parameters to the Calculator and/or create private variables like here.
        this.throwOnDivideByZero = throwOnDivideByZero === undefined ? true : throwOnDivideByZero;
    };
    framework.Calculator.prototype = (function () {

        // The 'self' variable ensures that the 'this' keyword references the Calculator and not the calling function.
        // (See https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/this)
        var self = this;

        function add(a, b) {
            return a + b;
        }

        function subtract(a, b) {
            return a - b;
        }

        function multiply(a, b) {
            return a * b;
        }

        function divide(a, b) {
            checkDivideByZero(b);
            return a / b;
        }

        // This private function is not exposed to those who want to use the Calculator.
        function checkDivideByZero(x) {
            if ((x === 0) && self.throwOnDivideByZero) {
                throw new Error("Divide by Zero.");
            }
        }

        return {
            add: add,
            subtract: subtract,
            multiply: multiply,
            divide: divide
        };
    }());

})(window.framework = window.framework || {}, window.jQuery, window, document);

var CMS = {};
CMS.Utility = (function () {
    var obj = {};
    obj.formatDateVN = function (date) {

    };
    return obj;
})();
CMS.Common = (function () {
    var obj = {};

    obj.bindEditor = function (selectorId, height) {
        CKEDITOR.on('dialogDefinition', function (ev) {
            var dialogName = ev.data.name;
            var dialogDefinition = ev.data.definition;

            if (dialogName == 'table') {
                var info = dialogDefinition.getContents('info');

                info.get('txtWidth')['default'] = '100%';       // Set default width to 100%
                info.get('txtBorder')['default'] = '0';         // Set default border to 0
            }
        });
        height = height || '600px';
        var editor = CKEDITOR.replace(selectorId, { height: height, });
        editor.on('instanceReady', function (ev) {
            editor.pasteFilter.disallow('a[!href,target]');

        });
        editor.on('paste', function (evt) {
            var filter = new CKEDITOR.htmlParser.filter({
                elements: {
                    a: function (element) {
                        if (!element.attributes.name) {
                            var textNode = element.children[0];
                            element.replaceWith(textNode);
                        }
                    }
                }
            });
            var fragment = CKEDITOR.htmlParser.fragment.fromHtml(evt.data.dataValue),
                writer = new CKEDITOR.htmlParser.basicWriter();
            filter.applyTo(fragment);
            fragment.writeHtml(writer);
            evt.data.dataValue = writer.getHtml();
        });
    };

    obj.bindEditorWithNote = function (selectorId, notePlaceholder, height) {
        CKEDITOR.on('dialogDefinition', function (ev) {
            var dialogName = ev.data.name;
            var dialogDefinition = ev.data.definition;

            if (dialogName == 'table') {
                var info = dialogDefinition.getContents('info');

                info.get('txtWidth')['default'] = '100%';       // Set default width to 100%
                info.get('txtBorder')['default'] = '0';         // Set default border to 0
            }
        });
        height = height || '600px';
        var editor = CKEDITOR.replace(selectorId, {
            height: height,
            notePlaceholder: notePlaceholder,
            
        });
        editor.on('instanceReady', function (ev) {
            editor.pasteFilter.disallow('a[!href,target]');

        });
        editor.on('paste', function (evt) {
            var filter = new CKEDITOR.htmlParser.filter({
                elements: {
                    a: function (element) {
                        if (!element.attributes.name) {
                            var textNode = element.children[0];
                            element.replaceWith(textNode);
                        }
                    }
                }
            });
            var fragment = CKEDITOR.htmlParser.fragment.fromHtml(evt.data.dataValue),
                writer = new CKEDITOR.htmlParser.basicWriter();
            filter.applyTo(fragment);
            fragment.writeHtml(writer);
            evt.data.dataValue = writer.getHtml();
        });
    };


    obj.bindTagSelect = function (selector, seletedTags, tagType) {
        var tagType = tagType || 'post';
        $.ajax({
            url: "/Tag/GetTags?type=" + tagType,
            dataType: 'json',
            success: function (data) {
                arrTags = [];
                if (data.list.length > 0) {
                    for (var i = 0; i < data.list.length; i++) {
                        arrTags.push({ id: data.list[i].Id, text: data.list[i].Name });
                    }
                }
                //console.log(arrTags);
                $(selector).select2({ data: arrTags, placeholder: "Chọn tag" }).val(arrSelectedTags).trigger('change');
                return arrTags;
            },
        });
    };

    obj.addNewTag = function (txtTagSelector, ddlTagsSelector, tagType) {
        var tagType = tagType || 'post';
        var text = $(txtTagSelector).val();
        if (text.trim().length > 0) {
            $.post("/Tag/AddTag", { tag: text, tagType: tagType }, function (data) {
                if (data.status == "success") {
                    // Create the DOM option that is pre-selected by default
                    var newItem = new Option(text, data.tagId, true, true);
                    // Append it to the select
                    $(ddlTagsSelector).append(newItem).trigger('change');
                    $(txtTagSelector).val('');
                }
            });
        }
    };

    obj.bindOpenPopupMedia = function (opener, target, onFileSelected) {
        var self = this;
        $(opener).click(function (e) {
            e.preventDefault();
            if (typeof (self.popupMedia) == 'undefined' || self.popupMedia.closed) {
                self.popupMedia = $.popupWindow("/mediamanager/?target=" + target, {
                    height: 490,
                    width: 950,
                    center: 'screen',
                    onFileSelected: onFileSelected
                });
            } else {
                self.popupMedia.focus();
            }
        });
    };

    obj.bindDatePicker = function () {
        
        jQuery('.datepicker').datetimepicker({
            format: "DD/MM/YYYY"
        });

        
    };

    obj.autoText = function (fromSelector, toElements) {
        $(fromSelector).blur(function () {
            var text = $(this).val();

            var toSelector = '';
            var toSlugify = false;
            var toLength = 0;

            for (var i = 0; i < toElements.length; i++) {
                toSelector = toElements[i].selector || '';
                toSlugify = toElements[i].slugify || false;
                toLength = toElements[i].length || 0;

                if (toSlugify) {
                    text = framework.Slugify(text);
                }
                if (toLength > 0 && text.length > toLength) {
                    text = text.substr(0, toLength);
                }
                $(toSelector).val(text);
                $(toSelector).trigger('change');
            }
        });
    };

    obj.bindCheckLength = function (selector, maxLength, trim) {
        maxLength = maxLength || 0;
        trim = trim || false;
        var selectorLength = $(selector + "-length");

        function checkLength(selector, maxLength, trim) {
            var text = $(selector).val();            
            selectorLength.removeClass('error');
            if (maxLength > 0 && text.length > maxLength) {
                if (trim) {
                    text = text.substr(0, maxLength);
                    $this.val(text);
                } else {
                    selectorLength.addClass('error');
                }
            }
        };

        checkLength(selector, maxLength, trim);

        $(selector).change(function () {
            var $this = $(this);
            var text = $this.val();
            checkLength(selector, maxLength, trim);
            selectorLength.text(text.length);
        });
    };

    obj.bindiCheck = function () {
        $('input.icheck').iCheck({
            checkboxClass: 'icheckbox_minimal-blue',
            radioClass: 'iradio_minimal-blue',
        });
    };

    obj.stickyTableHeader = function (table) {
        var $table = $(table);
        var header = $(table).children('thead');
        var tableContent = $(table).find('tbody');
        var origOffsetY = header.offset().top;
        var headerHeight = header.height();
        var headerWidth = header.width();

        // Copy cell widths from original header
        $("th", header).each(function (index) {
            var cellWidth = $("th", header).eq(index).css('width');
            $(this).css('width', cellWidth);
        });

        $("td", tableContent).each(function (index) {
            var cellWidth = $("td", tableContent).eq(index).css('width');
            $(this).css('width', cellWidth);
        });

        $(window).bind('scroll', function () {
            if ($(window).scrollTop() >= origOffsetY) {
                header.addClass('sticky-table-header').css('width', headerWidth + "px");
                tableContent.css('padding-top', (headerHeight + 10) + "px");
            } else {
                header.removeClass('sticky-table-header');
                tableContent.css('padding-top', "15px");
            }
        });
    };

    obj.buildCheckList = function (selector) {
        var list = $(selector);
        list.find('li.has-children label').click(function () {
            var li = $(this).closest('li');
            li.children("ul").slideToggle();
            li.toggleClass('expand');
        });
        list.find('input[type="checkbox"]').change(function () {
            var $this = $(this);
            var isChecked = $this.is(":checked");
            var li = $this.closest('li');
            var parentLi = li.parent().parent();
            if (isChecked) {
                parentLi.children('.item-header').find('input[type="checkbox"]').prop('checked', isChecked).trigger('change');
            } else {
                if (li.hasClass('has-children')) {
                    li.find('input[type="checkbox"]').prop('checked', false);
                }
            }
        });
    };

    return obj;
})();
