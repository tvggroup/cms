﻿(function ($) {

    $.fn.responsiveTab = function () {
        var nav_links = this.find(".tabs-nav a");
        var tabContent = this.find(".tabs-content");
        var tabPanels = tabContent.find(".tab-panel");
        var nav_accordion = tabContent.find("h3");
        //console.log(nav_accordion);

        tabPanels.each(function () {
            var title = $(this).attr("data-title");
            var $this = $(this);
            var className = $this.hasClass('active') ? "open" : '';
            $this.before("<h3 class='" + className + "'>" + title + " <i class='fa fa-angle-left'></i></h3>");
        });

        nav_links.on("click", function (e) {
            e.preventDefault(e);
            nav_links.parent().removeClass("active");
            var href = $(this).attr("href");
            console.log(href);
            console.log(tabPanels);
            console.log(tabContent);
            tabPanels.removeClass('active');
            tabContent.find(href).addClass('active');
            $(this).parent().addClass("active");
            return false;
        });

        tabContent.on("click", "h3", function () {
            var $this = $(this);
            if (!$this.hasClass('open')) {
                $this.addClass('open');
                $this.next(".tab-panel").slideDown();
            } else {
                $this.removeClass('open');
                $this.next(".tab-panel").slideUp();
            }
            return false;
        });
        return this;
    };

}(jQuery));

/*
 * jQuery outside events - v1.1 - 3/16/2010
 * http://benalman.com/projects/jquery-outside-events-plugin/
 * 
 * Copyright (c) 2010 "Cowboy" Ben Alman
 * Dual licensed under the MIT and GPL licenses.
 * http://benalman.com/about/license/
 */
(function ($, c, b) { $.map("click dblclick mousemove mousedown mouseup mouseover mouseout change select submit keydown keypress keyup".split(" "), function (d) { a(d) }); a("focusin", "focus" + b); a("focusout", "blur" + b); $.addOutsideEvent = a; function a(g, e) { e = e || g + b; var d = $(), h = g + "." + e + "-special-event"; $.event.special[e] = { setup: function () { d = d.add(this); if (d.length === 1) { $(c).bind(h, f) } }, teardown: function () { d = d.not(this); if (d.length === 0) { $(c).unbind(h) } }, add: function (i) { var j = i.handler; i.handler = function (l, k) { l.target = k; j.apply(this, arguments) } } }; function f(i) { $(d).each(function () { var j = $(this); if (this !== i.target && !j.has(i.target).length) { j.triggerHandler(e, [i.target]) } }) } } })(jQuery, document, "outside");


/*
 * Pinebrach Autocomplete 
 * based on opencart autocomplete
 * using jquery outside events
 */ 
(function ($) {
    $.fn.autocomplete = function (option) {
        return this.each(function () {
            this.timer = null;
            this.items = new Array();
            var $this = $(this);
            $this.after('<ul class="dropdown-menu"></ul>');
            var listOptions = $this.siblings('ul.dropdown-menu');

            $.extend(this, option);

            $this.attr('autocomplete', 'off');

            // Focus
            $this.on('focus', function () {
                this.request();
            });

            // Keydown
            $this.on('keydown', function (event) {
                switch (event.keyCode) {
                    case 27: // escape
                        this.hide();
                        break;
                    default:
                        this.request();
                        break;
                }
            });

            // Click
            this.click = function (event) {
                event.preventDefault();

                value = $(event.target).parent().attr('data-value');

                var item = listOptions.children('li[data-value="' + value + '"]');
                var isRemove = item.hasClass('active');
                if (isRemove) {
                    item.removeClass('active');
                } else {
                    item.addClass('active');
                }
                if (value && this.items[value]) {
                    this.select(this.items[value], isRemove);
                }

                this.hide();
            }

            // Show
            this.show = function () {
                var pos = $this.position();

                listOptions.css({
                    top: pos.top + $this.outerHeight(),
                    left: pos.left
                });

                listOptions.show();
            }

            // Hide
            this.hide = function () {
                listOptions.hide();
            }

            // Request
            this.request = function () {
                clearTimeout(this.timer);

                this.timer = setTimeout(function (object) {
                    object.source($(object).val(), $.proxy(object.response, object));
                }, 200, this);
            }

            // Response
            this.response = function (json) {
                html = '';
                if (json.length) {
                    for (i = 0; i < json.length; i++) {
                        this.items[json[i]['value']] = json[i];
                    }

                    for (i = 0; i < json.length; i++) {
                        if (!json[i]['category']) {
                            html += '<li data-value="' + json[i]['value'] + '"><a href="#">' + json[i]['label'] + '</a></li>';
                        }
                    }

                    // Get all the ones with a categories
                    var category = new Array();

                    for (i = 0; i < json.length; i++) {
                        if (json[i]['category']) {
                            if (!category[json[i]['category']]) {
                                category[json[i]['category']] = new Array();
                                category[json[i]['category']]['name'] = json[i]['category'];
                                category[json[i]['category']]['item'] = new Array();
                            }

                            category[json[i]['category']]['item'].push(json[i]);
                        }
                    }

                    for (i in category) {
                        html += '<li class="dropdown-header">' + category[i]['name'] + '</li>';

                        for (j = 0; j < category[i]['item'].length; j++) {
                            html += '<li data-value="' + category[i]['item'][j]['value'] + '"><a href="#">&nbsp;&nbsp;&nbsp;' + category[i]['item'][j]['label'] + '</a></li>';
                        }
                    }
                }

                if (html) {
                    if (option.customHeader) {
                        html = '<li class="dropdown-header">' + option.customHeader + '</li>' + html;
                    }
                    
                    this.show();
                } else {
                    this.hide();
                }
                
                listOptions.html(html);
                
            }
            
            listOptions.delegate('a', 'click', $.proxy(this.click, this));

            listOptions.bind("clickoutside", function (event) {
                $(this).hide();
            });
        });
    }
})(window.jQuery);