﻿CKEDITOR.plugins.add('removeanchor', {
    icons: 'removeanchor',
    init: function (editor) {
        editor.ui.addButton('RemoveAnchor', {
            label: 'Xoá điểm neo',
            command: 'removeAnchor',
            toolbar: 'links'
        });
    }
});