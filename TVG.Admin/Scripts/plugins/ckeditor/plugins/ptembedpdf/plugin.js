﻿CKEDITOR.plugins.add('ptembedpdf', {
    icons: 'ptembedpdf',
    init: function (editor) {
        editor.addCommand('ptembedpdf', {
            exec: function (editor) {
                //var strWindowFeatures = "width=1000,height=420,resizable,scrollbars=yes,status=1";
                //var windowObjectReference = PopupCenter("/Template/ptfiles", "File manager", 1000, 420);
                var puMedia;
                //console.log(editor);
                if (typeof (puMedia) == 'undefined' || puMedia.closed) {
                    puMedia = $.popupWindow("/mediamanager?target=ckeditor_pdf_" + editor.name,{ 
                                height:490, 
                                width:950,
                                center:'screen',
                    });
                }
                else {
                    puMedia.focus();
                }
            }
        });
        editor.ui.addButton('ptembedpdf', {
            label: 'Chèn file pdf từ thư viện files',
            command: 'ptembedpdf',
            toolbar: 'insert'
        });
    }
});

function PopupCenter(url, title, w, h) {
    // Fixes dual-screen position                         Most browsers      Firefox
    var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
    var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

    width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
    height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

    var left = ((width / 2) - (w / 2)) + dualScreenLeft;
    var top = ((height / 2) - (h / 2)) + dualScreenTop;
    var newWindow = window.open(url, title, 'scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);

    // Puts focus on the newWindow
    if (window.focus) {
        newWindow.focus();
    }
    return newWindow;
}