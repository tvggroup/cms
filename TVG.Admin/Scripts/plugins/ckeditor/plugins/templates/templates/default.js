﻿/**
 * @license Copyright (c) 2003-2016, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

// Register a templates definition set named "default".
CKEDITOR.addTemplates('default', {
    // The name of sub folder which hold the shortcut preview images of the
    // templates.
    imagesPath: CKEDITOR.getUrl(CKEDITOR.plugins.getPath('templates') + 'templates/images/'),

    // The templates definitions.
    templates: [
        {
            title: 'CHXHCNVN',
            description: 'Bảng số hiệu đầu văn bản',
            html: '<table class="" style="width:100%" border="0" cellpadding="0" cellspacing="0">' +     
	'<tbody>' +
		'<tr>' +
			'<td valign="top" width="230">' +
			'<p align="center"><span><b>{NƠI BAN HÀNH}<br />' + 
			'-------</b></span></p>' + 
			'</td>' + 
			'<td valign="top">' + 
			'<p align="center"><span><b>CỘNG HÒA XÃ HỘI CHỦ NGHĨA VIỆT NAM<br />' +
			'Độc lập - Tự do - Hạnh phúc<br />' + 
			'---------------</b></span></p>' +
			'</td>' +
		'</tr>' + 
		'<tr>' +
			'<td valign="top">' +
			'<p align="center"><span>Số: {SỐ HIỆU}</span></p>' + 
			'</td>' + 
			'<td valign="top">' +
			'<p align="right"><span><i>{Tỉnh/TP}, ngày {00} tháng {00} năm {0000}</i></span></p>' + 
			'</td>' +
		'</tr>' +
	'</tbody>' +
'</table>'
        },
    {
        title: 'Image and Title',
        image: 'template1.gif',
        description: 'One main image with a title and text that surround the image.',
        html: '<h3>' +
			// Use src=" " so image is not filtered out by the editor as incorrect (src is required).
			'<img src=" " alt="" style="margin-right: 10px" height="100" width="100" align="left" />' +
			'Type the title here' +
			'</h3>' +
			'<p>' +
			'Type the text here' +
			'</p>'
    },
	{
	    title: 'Strange Template',
	    image: 'template2.gif',
	    description: 'A template that defines two colums, each one with a title, and some text.',
	    html: '<table cellspacing="0" cellpadding="0" style="width:100%" border="0">' +
			'<tr>' +
				'<td style="width:50%">' +
					'<h3>Title 1</h3>' +
				'</td>' +
				'<td></td>' +
				'<td style="width:50%">' +
					'<h3>Title 2</h3>' +
				'</td>' +
			'</tr>' +
			'<tr>' +
				'<td>' +
					'Text 1' +
				'</td>' +
				'<td></td>' +
				'<td>' +
					'Text 2' +
				'</td>' +
			'</tr>' +
			'</table>' +
			'<p>' +
			'More text goes here.' +
			'</p>'
	},
	{
	    title: 'Text and Table',
	    image: 'template3.gif',
	    description: 'A title with some text and a table.',
	    html: '<div style="width: 80%">' +
			'<h3>' +
				'Title goes here' +
			'</h3>' +
			'<table style="width:150px;float: right" cellspacing="0" cellpadding="0" border="1">' +
				'<caption style="border:solid 1px black">' +
					'<strong>Table title</strong>' +
				'</caption>' +
				'<tr>' +
					'<td>&nbsp;</td>' +
					'<td>&nbsp;</td>' +
					'<td>&nbsp;</td>' +
				'</tr>' +
				'<tr>' +
					'<td>&nbsp;</td>' +
					'<td>&nbsp;</td>' +
					'<td>&nbsp;</td>' +
				'</tr>' +
				'<tr>' +
					'<td>&nbsp;</td>' +
					'<td>&nbsp;</td>' +
					'<td>&nbsp;</td>' +
				'</tr>' +
			'</table>' +
			'<p>' +
				'Type the text here' +
			'</p>' +
			'</div>'
	}]
});
