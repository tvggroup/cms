﻿CKEDITOR.plugins.add('tvgnote', {
    icons: 'tvgnote',
    init: function (editor) {
        editor.addCommand('tvgnote', new CKEDITOR.dialogCommand('tvgnoteDialog'));
        editor.ui.addButton('tvgnote', {
            label: 'Thêm ghi chú',
            command: 'tvgnote',
            toolbar: 'insert'
        });
        editor.addContentsCss(this.path + 'css/style.css');
        CKEDITOR.dialog.add('tvgnoteDialog', this.path + 'dialogs/tvgnote.js');
        if (editor.contextMenu) {
            editor.addMenuGroup('tvgnote');
            editor.addMenuItem('tvgnoteItem', {
                label: "Sửa ghi chú",
                icon: this.path + 'icons/tvgnote.png',
                command: 'tvgnote',
                group: 'tvgnote'
            });

            editor.contextMenu.addListener(function (element) {
                var isTVGNote = (element.is('span') && (element.hasClass('tvg-note-anchor')))
                    || (element.hasAscendant('span') && element.getAscendant('span').hasClass('tvg-note-anchor'));
                if (isTVGNote) {
                    return { tvgnoteItem: CKEDITOR.TRISTATE_OFF };
                }

            });
        }
    },    
    resetDialog: function (dialog, editor) {
        var noteEditor = CKEDITOR.instances[dialog.editor_name];
        var selection = editor.getSelection();
        var element = selection.getStartElement();        
        ddlNoteList = dialog.getContentElement("tab-basic", "noteList");
        if (!dialog.insertMode) {
            var anchorId = element.getAttribute("data-id");
            if (anchorId) {
                dialog.editingNotes = new Array();
                
                ddlNoteList.clear();
                ddlNoteList.add('---- Thêm mới ----', '0');
                var noteList = $(editor.config.notePlaceholder).find('.' + anchorId).each(function (index, elem) {
                    var noteObj = JSON.parse($(this).val());
                    dialog.editingNotes.push(noteObj);
                    ddlNoteList.add(noteObj.title, index + 1);
                });
            }
        }
        else {
            ddlNoteList.clear();
            ddlNoteList.add('---- Thêm mới ----', '0');
        }

        dialog.getContentElement("tab-basic", "noteTitle").setValue("");
        noteEditor.setData("");
        //#endregion

    },
});