﻿CKEDITOR.dialog.add('tvgnoteDialog', function (editor) {
    //console.log(editor.config.notePlaceholder);
    return {
        title: 'GHI CHÚ',
        minWidth: 600,
        minHeight: 200,
        editor_name: false,
        editingNotes: [],
        contents: [
            {
                id: 'tab-basic',
                label: 'Basic Settings',
                elements: [
                    {
                        type: 'text',
                        id: 'noteId',
                        label: '',
                        controlStyle:"display:none",
                        setup: function (element) {
                            var dataId = element.getAttribute('data-id');
                            this.setValue(dataId);
                        }
                    },
                    {
                        type: 'html',
                        html: 'Chọn ghi chú trong khung dưới đây để chỉnh sửa hoặc xóa. <br /> Chọn <strong>"Thêm mới"</strong> nếu muốn thêm ghi chú mới vào đoạn văn bản đã có ghi chú.'
                    },
                    {
                        type: 'hbox',
                        widths: ['80%', '20%'],
                        children: [
                            {
                                type: 'select',
                                id: 'noteList',
                                controlStyle: 'width:100%',
                                items: [['---- Thêm mới ----', '0']],
                                onChange: function (api) {
                                    var dialog = this.getDialog();
                                    var index = parseInt(this.getValue() - 1);
                                    var txtTitle = dialog.getContentElement("tab-basic", "noteTitle");
                                    var txtContent = dialog.getContentElement("tab-basic", "noteContent");
                                    var txtOrder = dialog.getContentElement("tab-basic", "noteOrder");
                                    var noteEditor = CKEDITOR.instances[dialog.editor_name];
                                    if (index >= 0) {
                                        txtTitle.setValue(dialog.editingNotes[index].NoteTitle);
                                        txtOrder.setValue(dialog.editingNotes[index].NoteOrder);
                                        noteEditor.setData(dialog.editingNotes[index].NoteContent);
                                    }
                                    
                                }

                            },
                            {
                                type: 'button',
                                id: 'noteDelete',
                                label: 'Xóa ghi chú',
                                onClick: function () {
                                    var dialog = this.getDialog();
                                    var ddlNoteList = dialog.getContentElement("tab-basic", "noteList");
                                    var index = parseInt(ddlNoteList.getValue() - 1);
                                    var anchorId = dialog.getContentElement("tab-basic", "noteId").getValue();
                                   
                                    if (index >= 0) {
                                        dialog.editingNotes.splice(index, 1);
                                        $(editor.config.notePlaceholder + ' .' + anchorId + ':eq(' + index + ')').remove();
                                        editor.plugins.tvgnote.resetDialog(dialog, editor);
                                    }
                                }

                            }
                        ]
                    },                    
                    {
                        type: 'textarea',
                        id: 'noteTitle',
                        label: 'Tiêu đề',
                        rows: 2,
                        validate: CKEDITOR.dialog.validate.notEmpty("Tiêu đề không được để trống")
                    },
                    {
                        type: 'text',
                        id: 'noteOrder',
                        label: 'Thứ tự ghi chú (trong trường hợp có nhiều ghi chú cùng một chỗ)',
                        default: '0',
                        validate: CKEDITOR.dialog.validate.integer("Thứ tự phải là số")
                    },
                    {
                        type: 'textarea',
                        id: 'noteContent',
                        class: 'noteContent',
                        label: 'Nội dung'
                    }
                ]
            }
        ],

        onShow: function () {
            var dialog = this;
            var selection = editor.getSelection();
            var element = selection.getStartElement();
            ddlNoteList = dialog.getContentElement("tab-basic", "noteList");

            if (element)
                element = element.getAscendant('span', true);

            if (!element || element.getName() !== 'span' || !element.hasClass('tvg-note-anchor')) {
                element = editor.document.createElement('span');
                element.setAttribute('class', 'tvg-note-anchor');
                this.insertMode = true;
            }
            else
                this.insertMode = false;

            console.log(element);
            console.log(element.getName());
            console.log(this.insertMode);

            this.element = element;
            if (!this.insertMode)
                this.setupContent(this.element);

            //#region: tạo editor trong khung dialog
            var current_editor_id = dialog.getParentEditor().id;
            CKEDITOR.on('instanceLoaded', function (evt) {
                dialog.editor_name = evt.editor.name;
            });
            CKEDITOR.replaceAll(function (textarea, config) {
                
                // Make sure the textarea has the correct class:
                if (!textarea.className.match(/noteContent/)) {
                    return false;
                }

                // Make sure we only instantiate the relevant editor:
                var el = textarea;
                while ((el = el.parentElement) && !el.classList.contains(current_editor_id));
                if (!el) {
                    return false;
                }

                config.toolbarGroups = [
                    { name: 'editing', groups: ['undo', 'find'] },
                    { name: 'links' },
                    { name: 'basicstyles', groups: ['basicstyles', 'cleanup'] },
                    { name: 'paragraph', groups: ['list', 'indent', 'align', 'mode'] }
                    //{ name: 'styles' },
                    //{ name: 'colors' },
                ];
                
                config.enterMode = CKEDITOR.ENTER_BR;
                config.autoParagraph = false;
                config.height = 150;
                config.resize_enabled = false;
                config.autoGrow_minHeight = 80;
                config.removePlugins = 'tvgnote,elementspath';
                return true;
            });
            //#endregion

            //#region: setup nội dung note
            
            if (!this.insertMode)
            {
                var anchorId = element.getAttribute("data-id");

                if (anchorId) {
                    dialog.editingNotes = new Array();
                    ddlNoteList.clear();
                    ddlNoteList.add('---- Thêm mới ----', '0');
                    var noteList = $(editor.config.notePlaceholder).find('.' + anchorId).each(function (index, elem) {
                        var noteObj = JSON.parse($(this).val());
                        dialog.editingNotes.push(noteObj);
                        ddlNoteList.add(noteObj.NoteTitle, index + 1);
                    });
                }
            }
            else {
                ddlNoteList.clear();
                ddlNoteList.add('---- Thêm mới ----', '0');
            }
            
            //#endregion
            
        },

        onOk: function () {
            var dialog = this;
            var span = this.element;
            var selection = editor.getSelection();
            var noteEditor = CKEDITOR.instances[dialog.editor_name];
            var noteData = noteEditor.getData();
            noteEditor.destroy();

            var max = 9999;
            var min = 1;
            var randomId = Math.floor(Math.random() * (max - min + 1)) + min;
            var usedNoteIds = new Array();
            $(editor.config.notePlaceholder + " textarea").each(function () {
                usedNoteIds.push($(this).attr('data-anchor-id'));
            });
            if (usedNoteIds.length > 0) {
                while (usedNoteIds.indexOf(randomId) > -1) {
                    randomId = Math.floor(Math.random() * (max - min + 1)) + min;
                    console.log('duplicate id');
                }
            }            

            var anchorId = randomId;
            if (!this.insertMode) {
                anchorId = span.getAttribute('data-id');
            } else {
                span.setAttribute('data-id', randomId);
            }

            var noteTitle = dialog.getContentElement('tab-basic', 'noteTitle').getValue();
            var selectedNoteId = parseInt(dialog.getContentElement('tab-basic', 'noteList').getValue());
            var noteOrder = dialog.getContentElement('tab-basic', 'noteOrder').getValue();
            var body = noteData;

            var noteObj = {
                DocumentId: 0,
                AnchorId: anchorId,
                NoteTitle: noteTitle,
                NoteOrder: noteOrder,
                NoteContent: body
            };            

            this.commitContent(span);
            if (this.insertMode) {
                if (selection.getSelectedText() === "") {
                    span.setHtml(noteTitle);
                } else {
                    span.setHtml(selection.getSelectedText());
                }
            }
            

            if (selectedNoteId === 0) {
                $(editor.config.notePlaceholder).append('<textarea name="tvgNotes" data-anchor-id="' + anchorId + '" class="' + anchorId + '">' + JSON.stringify(noteObj) + '</textarea>');
            } else {
                $(editor.config.notePlaceholder + ' .' + anchorId + ':eq(' + (selectedNoteId - 1) + ')').val(JSON.stringify(noteObj));
            }
            

            if (this.insertMode) {
                editor.insertElement(span);
                if (!span.hasNext()) {
                    editor.insertText(' ');
                }
            }
            
        },

        onCancel: function () {
            var dialog = this;
            var noteEditor = CKEDITOR.instances[dialog.editor_name];
            if (noteEditor) {
                noteEditor.destroy();
            }
            
            var span = this.element;
            console.log(dialog.editingNotes);
            if (dialog.editingNotes.length <= 0) {
                span.remove(true);
            }
        }

    };
});