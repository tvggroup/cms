﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TVG.Data.Services;
using TVG.Identity;
using Microsoft.AspNet.Identity.Owin;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using TVG.Core.Helper;
using System.Web.Routing;

namespace TVG.Admin.Extensions
{
    public class TVGAuthorize : AuthorizeAttribute
    {
        public IPermissionService permissionService { get; set; }

        public string PermissionName { get; set; }

        public string[] PermissionNames { get; set; }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            var isAuthorized = base.AuthorizeCore(httpContext);
            if (!isAuthorized)
            {
                return false;
            }
            var result = false;
            var userManager = httpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            var roles = userManager.GetRoles(httpContext.User.Identity.GetUserId().TryConvert<int>(0));
            var count = 0;

            if (PermissionNames == null || PermissionNames.Count() <= 0)
            {
                if (!string.IsNullOrEmpty(PermissionName))
                {
                    PermissionNames = new string[] { PermissionName };
                }
            }

            if (PermissionNames != null && PermissionNames.Count() > 0)
            {
                for (int r = 0; r < roles.Count(); r++)
                {
                    for (int i = 0; i < PermissionNames.Count(); i++)
                    {
                        count = permissionService.CheckPermissionRole(PermissionNames[i], roles[r]);
                        if (count > 0)
                        {
                            result = true;
                            break;
                        }
                    }
                    if (result)
                    {
                        break;
                    }
                }
            }
            return result;
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            filterContext.Result = new HttpUnauthorizedResult();
        }
    }

    public class TVGAuthorizeAction : AuthorizeAttribute
    {
        public IPermissionService permissionService { get; set; }

        public string PermissionName { get; set; }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            var result = false;
            var userManager = httpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            var roles = userManager.GetRoles(httpContext.User.Identity.GetUserId().TryConvert<int>(0));
            var count = 0;
            var permissionNames = PermissionName.Split(',');
            

            if(permissionNames.Count() > 0)
            {
                for(int r = 0; r < roles.Count(); r++)
                {
                    for(int i = 0; i < permissionNames.Count(); i++)
                    {
                        count = permissionService.CheckPermissionRole(permissionNames[i].Trim(), roles[r]);
                        if (count > 0)
                        {
                            result = true;
                            break;
                        }
                    }
                    if (result)
                    {
                        break;
                    }
                }
            }
            
            return result;
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            filterContext.Result = new RedirectToRouteResult(
                        new RouteValueDictionary(new { controller = "Error", action = "Forbidden" }));
        }
    }
}