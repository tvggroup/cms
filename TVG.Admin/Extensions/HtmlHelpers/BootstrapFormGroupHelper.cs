﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace TVG.Admin.Extensions
{
    public static class BootstrapFormGroupHelper
    {
        public static BootstrapFormGroup BeginBootstrapFormGroup(this HtmlHelper html, string label, int labelSize, string customClass = "")
        {
            var htmlText = new StringBuilder("");
            htmlText.AppendFormat("<div class=\"form-group {0}\">", customClass);
            if(labelSize > 0)
            {
                htmlText.AppendFormat("<label class=\"control-label col-md-{0}\">{1}</label>", labelSize, label);
                htmlText.AppendFormat("<div class=\"col-md-{0}\">", (12 - labelSize));
            }
            else
            {
                htmlText.AppendFormat("<label class=\"control-label\">{0}</label>", label);
                htmlText.AppendFormat("<div class=\"\">");
            }
            

            html.ViewContext.Writer.Write(htmlText.ToString());
            return new BootstrapFormGroup(html);
        }

        public static BootstrapFormGroup BeginBootstrapFormGroupNoLabel(this HtmlHelper html, string customClass = "")
        {
            var htmlText = new StringBuilder("");
            htmlText.AppendFormat("<div class=\"form-group {0}\">", customClass);
            htmlText.AppendFormat("<div class='col-md-12'>");

            html.ViewContext.Writer.Write(htmlText.ToString());
            return new BootstrapFormGroup(html);
        }

        public static void EndBootstrapFormGroup(this HtmlHelper html)
        {
            html.ViewContext.Writer.Write("</div></div>");
        }
    }

    public class BootstrapFormGroup : IDisposable
    {
        private readonly HtmlHelper _html;

        public BootstrapFormGroup(HtmlHelper html)
        {
            _html = html;
        }

        public void Dispose()
        {
            _html.EndBootstrapFormGroup();
        }
    }
}