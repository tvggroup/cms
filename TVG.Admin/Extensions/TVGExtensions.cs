﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using TVG.Core.Helper;

namespace TVG.Admin.Extensions
{
    public static class TVGExtensions
    {
        public static string SafeGet(this TempDataDictionary tempData, string key)
        {
            object value;
            if (!tempData.TryGetValue(key, out value))
                return string.Empty;
            return value.ToString();
        }

        public static MvcHtmlString SetPageNotification(this HtmlHelper html, TempDataDictionary tempData)
        {
            string result = "";
            var status = tempData.SafeGet("status");
            if (!string.IsNullOrEmpty(status))
            {
                var message = tempData.SafeGet("message");
                var title = tempData.SafeGet("title");
                result = string.Format("toastr['{0}']('{1}', '{2}')", status, message, title);
            }
                
            return MvcHtmlString.Create(result);
        }

        public static string SetSelect(this HtmlHelper html, string value, string selectedValue)
        {
            string result = "";
            if(string.Compare(value, selectedValue, false) == 0)
            {
                result = "selected";
            }
            return result;
        }

        public static string SetSelect(this HtmlHelper html, int value, int selectedValue)
        {
            string result = "";
            if (value == selectedValue) 
            {
                result = "selected";
            }
            return result;
        }

        public static MvcHtmlString Pager(this HtmlHelper html, long totalPages, long pageIndex, long pageSize, string url)
        {
            StringBuilder sb = new StringBuilder();
            url = MyUrlHelper.RemoveQueryStringByKey(url, "pid");
            url = MyUrlHelper.RemoveQueryStringByKey(url, "psize");
            // only show paging if we have more items than the page size
            long displayedPages = 20;

            if (totalPages > 1)
            {
                long currentGroup = (pageIndex - 1) / displayedPages;
                long startPage = (currentGroup * displayedPages) + 1;
                long endPage = (currentGroup + 1) * displayedPages > totalPages ? totalPages : (currentGroup + 1) * displayedPages;
                long prevPage = startPage - 1;
                long nextPage = endPage < totalPages ? endPage + 1 : totalPages;

                sb.Append("<ul class=\"pagination\">");

                //link first page
                sb.Append("<li class=''>");
                sb.Append("<a href='");
                if (url.IndexOf('?') > 0)
                {
                    sb.Append(url).Append("&pid=");
                }
                else
                {
                    sb.Append(url).Append("?pid=");
                }
                sb.Append("1").Append("&pSize=").Append(pageSize.ToString());
                sb.Append("' title=\"Go to First Page");
                sb.Append("\">").Append("«").Append("</a>");
                sb.Append("</li>");

                //link to previous group, chỉ hiển thị khi ở group 2 trở lên
                if (currentGroup > 0)
                {
                    sb.Append("<li class=''>");
                    sb.Append("<a href='");
                    if (url.IndexOf('?') > 0)
                    {
                        sb.Append(url).Append("&pid=");
                    }
                    else
                    {
                        sb.Append(url).Append("?pid=");
                    }
                    sb.Append(prevPage.ToString()).Append("&pSize=").Append(pageSize.ToString());
                    sb.Append("' title=\"Go to Page ").Append(prevPage.ToString());
                    sb.Append("\">").Append("❮").Append("</a>");
                    sb.Append("</li>");
                }

                //link các trang trong group
                for (var i = startPage; i <= endPage; i++)
                {
                    sb.Append("<li class='");
                    if (i == pageIndex)
                    {
                        sb.Append(" active'>");
                        sb.Append("<a href='#' data-dt-idx='").Append(i.ToString()).Append("'>").Append(i.ToString()).Append("</a>");
                    }
                    else
                    {
                        sb.Append("'>");
                        sb.Append("<a href='");
                        if (url.IndexOf('?') > 0)
                        {
                            sb.Append(url).Append("&pid=");
                        }
                        else
                        {
                            sb.Append(url).Append("?pid=");
                        }
                        sb.Append(i.ToString()).Append("&pSize=").Append(pageSize.ToString());
                        sb.Append("' title=\"Go to Page ").Append(i.ToString());
                        sb.Append("\">").Append(i.ToString()).Append("</a>");
                    }
                    sb.Append("</li>");
                }

                //link to next group, chỉ hiển thị khi không phải group cuối 
                if ((totalPages / displayedPages > 1) && (currentGroup < (totalPages / displayedPages)))
                {
                    sb.Append("<li class=''>");
                    sb.Append("<a href='");
                    if (url.IndexOf('?') > 0)
                    {
                        sb.Append(url).Append("&pid=");
                    }
                    else
                    {
                        sb.Append(url).Append("?pid=");
                    }
                    sb.Append(nextPage.ToString()).Append("&pSize=").Append(pageSize.ToString());
                    sb.Append("' title=\"Go to Page ").Append(nextPage.ToString());
                    sb.Append("\">").Append("❯").Append("</a>");
                    sb.Append("</li>");
                }

                //link to last page
                sb.Append("<li class=''>");
                sb.Append("<a href='");
                if (url.IndexOf('?') > 0)
                {
                    sb.Append(url).Append("&pid=");
                }
                else
                {
                    sb.Append(url).Append("?pid=");
                }
                sb.Append(totalPages.ToString()).Append("&pSize=").Append(pageSize.ToString());
                sb.Append("' title=\"Go to Last Page");
                sb.Append("\">").Append("»").Append("</a>");
                sb.Append("</li>");

                sb.Append("</ul>");
            }

            return MvcHtmlString.Create(sb.ToString());
        }

        public static string RelativePath(this HttpServerUtility srv, string path)
        {
            return path.Replace(HttpContext.Current.Server.MapPath("~/"), "~/").Replace(@"\", "/");
        }
    }
}