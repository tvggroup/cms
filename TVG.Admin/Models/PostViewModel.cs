﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TVG.Data.Domain;

namespace TVG.Admin.Models
{
    public class PostCreateModel
    {
        [Required(ErrorMessageResourceType = typeof(Localizations.Models), ErrorMessageResourceName = "Validation_Required")]
        [Display(ResourceType = typeof(Localizations.Models), Name = "Post_Title")]
        public string Title { get; set; }
        [Required(ErrorMessage = "URL tĩnh không được bỏ trống")]
        public string Slug { get; set; }
        [Required(ErrorMessage = "Tóm tắt không được bỏ trống")]
        public string Description { get; set; }
        [Required(ErrorMessage = "Nội dung không được bỏ trống")]
        [AllowHtml]
        public string Content { get; set; }
        public string MetaDescription { get; set; }
        public string MetaKeyword { get; set; }
        public string Image { get; set; }
        public string PostType { get; set; }
    }

    public class PostEditModel
    {
        [Required]
        public int Id { get; set; }
        [Required(ErrorMessageResourceType = typeof(Localizations.Models), ErrorMessageResourceName = "Validation_Required")]
        [Display(ResourceType = typeof(Localizations.Models), Name = "Post_Title")]
        public string Title { get; set; }
        [Required(ErrorMessage = "URL tĩnh không được bỏ trống")]
        public string Slug { get; set; }
        [Required(ErrorMessage = "Tóm tắt không được bỏ trống")]
        public string Description { get; set; }
        [Required(ErrorMessage = "Nội dung không được bỏ trống")]
        [AllowHtml]
        public string Content { get; set; }
        public string MetaDescription { get; set; }
        public string MetaKeyword { get; set; }
        public string Image { get; set; }
        public string PostType { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime PublishedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
    }

    public class PostFormModel
    {
        public PostFormModel()
        {
            this.Post = new Post();
        }
        public Post Post { get; set; }
        [Required]
        public int Id { get; set; }
        [Required(ErrorMessageResourceType = typeof(Localizations.Models), ErrorMessageResourceName = "Validation_Required")]
        [Display(ResourceType = typeof(Localizations.Models), Name = "Post_Title")]
        public string Title { get; set; }
        [Required(ErrorMessage = "URL tĩnh không được bỏ trống")]
        public string Slug { get; set; }
        [Required(ErrorMessage = "Tóm tắt không được bỏ trống")]
        public string Description { get; set; }
        [Required(ErrorMessage = "Nội dung không được bỏ trống")]
        [AllowHtml]
        public string Content { get; set; }
        public string MetaDescription { get; set; }
        public string MetaKeyword { get; set; }
        public string Image { get; set; }
        public string PostType { get; set; }
        public int PostStatus { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime PublishedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public int CreatedBy { get; set; }
        public int UpdatedBy { get; set; }
        public int PublishedBy { get; set; }
        public string CreatedByName { get; set; }
        public string UpdatedByName { get; set; }
        public string Note { get; set; }
        public int DisplayOrder { get; set; }

        public List<int> PostTermIds { get; set; }
        public List<TaxonomyTerm> Categories { get; set; }
        public List<PostMeta> PostMetas { get; set; }
        public List<PostTag> PostTags { get; set; }
        public PostExt PostExt { get; set; }

        public string FormAction { get; set; }
    }

    public class ForKidFormModel : PostFormModel
    {
        public List<TaxonomyTerm> Categories_chu_de { get; set; }
        public List<TaxonomyTerm> Categories_nguyen_lieu { get; set; }
        public List<TaxonomyTerm> Categories_linh_vuc { get; set; }
        public List<TaxonomyTerm> Categories_su_kien { get; set; }
    }

    public class ForTeacherFormModel : PostFormModel
    {
        public List<TaxonomyTerm> Categories_thong_tin_nganh { get; set; }
        public List<TaxonomyTerm> Categories_giao_an_mau { get; set; }
        public List<TaxonomyTerm> Categories_ke_hoach { get; set; }
    }

    public class ForMomFormModel : PostFormModel
    {
        public List<TaxonomyTerm> Categories_album { get; set; }
        public List<TaxonomyTerm> Categories_nguyen_lieu { get; set; }
    }
}