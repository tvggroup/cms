﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TVG.Admin.Models
{
    public class MenuCreateModel
    {
        [Required(ErrorMessage = "Tên menu không được bỏ trống")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Mã menu không được bỏ trống")]
        public string Slug { get; set; }
    }

    public class MenuEditModel
    {
        [Required]
        public int Id { get; set; }
        [Required(ErrorMessage = "Tên menu không được bỏ trống")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Mã menu không được bỏ trống")]
        public string Slug { get; set; }
    }
}