﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TVG.Admin.Models
{
    public class TagCreateModel
    {
        public string TagType { get; set; }
        [Required(ErrorMessage = "Tên không được bỏ trống")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Tên SEO không được bỏ trống")]
        public string Slug { get; set; }
    }

    public class TagEditModel
    {
        public int ID { get; set; }
        public string TagType { get; set; }
        [Required(ErrorMessage = "Tên không được bỏ trống")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Tên SEO không được bỏ trống")]
        public string Slug { get; set; }
    }
}