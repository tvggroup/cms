﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TVG.Admin.Models
{
    public class BannerCreateModel
    {
        [Required(ErrorMessage = "Tên không được bỏ trống")]
        public string Title { get; set; }
        [Required(ErrorMessage = "Đường dẫn tĩnh không được bỏ trống")]
        public string Slug { get; set; }
    }

    public class BannerEditModel
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Tên không được bỏ trống")]
        public string Title { get; set; }
        [Required(ErrorMessage = "Đường dẫn tĩnh không được bỏ trống")]
        public string Slug { get; set; }
    }
}