﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TVG.Data.Domain;

namespace TVG.Admin.Models
{
    public class UserPostsViewModel
    {
        public int UserId { get; set; }
        public string Username { get; set; }
        public int PostCount { get; set; }
        public int LawDocumentCount { get; set; }
        public int DocumentCount { get; set; }
        public int DocumentPostCount { get; set; }
        public int DocumentCaseCount { get; set; }
        public int TotalCount { get; set; }
    }

    public class UserControlTagsViewModel
    {
        public int UserId { get; set; }
        public string Username { get; set; }
        public int LawDocumentControlTagCount { get; set; }
        public int DocumentControlTagCount { get; set; }
        public int DocumentPostControlTagCount { get; set; }
        public int DocumentCaseControlTagCount { get; set; }
        public int TotalCount { get; set; }
        
    }
}