﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TVG.Admin.Models
{
    public class ControlTagCreateModel
    {
        [Required(ErrorMessage = "Tên không được bỏ trống")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Tên SEO không được bỏ trống")]
        public string Slug { get; set; }
        public int DisplayOrder { get; set; }
        public string Color { get; set; }
    }

    public class ControlTagEditModel
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Tên không được bỏ trống")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Tên SEO không được bỏ trống")]
        public string Slug { get; set; }
        public int DisplayOrder { get; set; }
        public string Color { get; set; }
    }
}