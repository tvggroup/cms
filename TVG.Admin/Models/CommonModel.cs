﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TVG.Admin.Models
{
    public class SelectItemModel
    {
        public string Id { get; set; }
        public string Text { get; set; }
    }

    public class UserOnlineModel
    {
        public string Username { get; set; }
        public string PageTitle { get; set; }
    }
}