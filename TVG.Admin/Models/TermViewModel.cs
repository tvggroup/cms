﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TVG.Data.Domain;

namespace TVG.Admin.Models
{
    public class TermViewModel
    {
        public TaxonomyTerm Term { get; set; }       
    }

    public class TermFormModel
    {
        public TaxonomyTerm Term { get; set; }
        public Taxonomy Taxonomy { get; set; }
        public List<TaxonomyTermMeta> MetaList { get; set; }
        public List<TaxonomyTerm> TermTree { get; set; }
    }

    public class TermsChecklistModel
    {
        public List<TaxonomyTerm> Terms { get; set; }
        public string CssClass { get; set; }
        public List<int> SelectedIds { get; set; }
        public string CheckboxName { get; set; }
        public string ListId { get; set; }
    }
}