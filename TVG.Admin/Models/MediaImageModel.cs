﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TVG.Admin.Models
{
    public class MediaImageModel
    {
        public string Thumb { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public string Path { get; set; }
        public string Url { get; set; }
        public string Extension { get; set; }
        //public string FullPath { get; set; }
    }
}