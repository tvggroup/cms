﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TVG.Admin.Models
{
    public class AutocompleteModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}