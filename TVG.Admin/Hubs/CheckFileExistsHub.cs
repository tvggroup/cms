﻿using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using NPoco;
using TVG.Data.Services;
using TVG.Core.Caching;
using System.IO;
using System.Configuration;
using TVG.Core.Helper;

namespace TVG.Admin.Hubs
{
    public class CheckFileExistsHub : Hub
    {
        public string msg = "Initializing and Preparing...";
        public int count = 100;

        public void CheckAttachment(int type, string fDate, string tDate)
        {
            DateTime fromDate = fDate.TryConvert<DateTime>(DateTime.Now.AddDays(-7));
            DateTime toDate = tDate.TryConvert<DateTime>(DateTime.Now);
            Dictionary<string, object> args = new Dictionary<string, object>();
            args["fromDate"] = fromDate;
            args["toDate"] = toDate;
            Database db = new Database("DefaultConnection");
            MemoryCacheManager cache = new MemoryCacheManager();
            UtilityService utilityService = new UtilityService(db, cache);
            List<Dictionary<string, object>> attachments = new List<Dictionary<string, object>>();
            switch(type)
            {
                case 0:
                    attachments = utilityService.GetLawDocumentAttachments(args);
                    break;
                case 1:
                    attachments = utilityService.GetDocumentAttachments(args);
                    break;
                case 2:
                    attachments = utilityService.GetDocumentCaseAttachments(args);
                    break;
                case 3:
                    attachments = utilityService.GetDocumentPostAttachments(args);
                    break;
            }            
            count = attachments.Count();
            int percent = 0;
            bool fileExists = true;
            string uploadPath = System.Web.HttpContext.Current.Server.MapPath("~/Uploads"); //ConfigurationManager.AppSettings["ClientFolderUpload"];
            for (int i = 0; i < count; i++)
            {
                percent = (i + 1) * 100 / count;
                fileExists = File.Exists(Path.Combine(uploadPath, attachments[i]["FilePath"].ToString()));                

                // call client-side SendMethod method
                Clients.Caller.sendMessage(attachments[i], percent, fileExists);
            }
        }
    }
}