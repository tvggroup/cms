﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;
using System.Threading.Tasks;
using TVG.Core.Caching;
using Microsoft.AspNet.SignalR.Owin;
using TVG.Admin.Models;

namespace TVG.Admin.Hubs
{
    public class OnlineUserHub : Hub
    {
        public override Task OnConnected()
        {
            var location = Context.QueryString["loc"];
            MemoryCacheManager cache = new MemoryCacheManager();
            var users = cache.Get<List<UserOnlineModel>>("online_users");
            if(users == null)
            {
                users = new List<UserOnlineModel>();
            }
            var user = Context.User.Identity.Name;
            
            if(!users.Any(x => x.Username == user))
            {
                users.Add(new UserOnlineModel { Username = user, PageTitle = location });
                cache.AddOrUpdate("online_users", users, TimeSpan.FromMinutes(60));
            }
            else
            {
                var currentUser = users.Where(x => x.Username == user).FirstOrDefault();
                currentUser.PageTitle = location;
                cache.AddOrUpdate("online_users", users, TimeSpan.FromMinutes(60));
            }
            
            Clients.All.GetOnlineUsers(users);
            return base.OnConnected();
	    }
        public override Task OnDisconnected(bool stopCalled)
        {
            MemoryCacheManager cache = new MemoryCacheManager();
            var users = cache.Get<List<UserOnlineModel>>("online_users");
            if (users == null)
            {
                users = new List<UserOnlineModel>();
            }
            var user = Context.User.Identity.Name;
            var currentUser = users.Where(x => x.Username == user).FirstOrDefault();
            if (currentUser != null)
            {
                users.Remove(currentUser);
            }
            cache.AddOrUpdate("online_users", users, TimeSpan.FromMinutes(60));
            
            Clients.All.GetOnlineUsers(users);
            return base.OnDisconnected(stopCalled);
	    }
    }
}