﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using NPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using TVG.Admin.Extensions;
using TVG.Admin.Models;
using TVG.Core.Helper;
using TVG.Core.Logging;
using TVG.Data;
using TVG.Data.Domain;
using TVG.Data.Services;
using TVG.Identity;

namespace TVG.Admin.Controllers
{
    public class PostController : BaseController
    {
        private const string ACTION_EDIT = "edit";
        private const string ACTION_CREATE = "create";
        private const string POST_TYPE = "post";

        private const string PERMISSION_VIEW = "cms_post_view";
        private const string PERMISSION_EDIT = "cms_post_edit";
        private const string PERMISSION_ADD = "cms_post_add";
        private const string PERMISSION_DELETE = "cms_post_delete";
        private const string PERMISSION_EDIT_OWN = "cms_post_edit_own";
        private const string PERMISSION_EDIT_RULES = "cms_post_edit, cms_post_edit_own";
        private const string PERMISSION_APPROVE = "cms_post_approve";
        private const string PERMISSION_REJECT = "cms_post_reject";

        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        IPostService postService;
        ITaxonomyService taxonomyService;
        private readonly ILogManager logger;
        public PostController(
            IPostService postService, 
            ITaxonomyService taxonomyService,
            ILogManager logger,
            IPermissionService permissionService)
        {
            this.postService = postService;
            this.taxonomyService = taxonomyService;
            this.logger = logger;
            this.permissionService = permissionService;
        }

        [TVGAuthorizeAction(PermissionName = PERMISSION_VIEW)]
        public ActionResult Index()
        {
            Dictionary<string, object> args = new Dictionary<string, object>();
            args["postType"] = ValueHelper.TryGet(Request.QueryString["type"], "post");
            args["termId"] = ValueHelper.TryGet(Request.QueryString["catId"], 0);
            args["keyword"] = ValueHelper.TryGet(Request.QueryString["kw"], "");
            args["status"] = ValueHelper.TryGet(Request.QueryString["status"], -1);
            args["userId"] = ValueHelper.TryGet(Request.QueryString["userId"], 0);
            args["pageIndex"] = ValueHelper.TryGet(Request.QueryString["pId"], 1);
            args["pageSize"] = ValueHelper.TryGet(Request.QueryString["pSize"], 20);
            args["isDeleted"] = ValueHelper.TryGet(Request.QueryString["deleted"], false);

            var posts = postService.GetPosts(args);

            for (int i = 0; i < posts.Items.Count; i++)
            {
                posts.Items[i].Terms = postService.GetPostTermsByPostId(posts.Items[i].Id);
            }

            //Lấy danh sách category
            Dictionary<string, object> argsCat = new Dictionary<string, object>();
            argsCat["type"] = "category";
            argsCat["getTree"] = true;
            Page<TaxonomyTerm> terms = taxonomyService.GetTerms(argsCat);
            ViewBag.Categories = terms;

            //lấy danh sách user
            ViewBag.Users = UserManager.Users.ToList();

            ViewBag.PostStatuses = ConstantDatas.PostStatus;

            ViewBag.FilterCategoryId = ValueHelper.TryGet(Request.QueryString["catId"], 0);
            ViewBag.FilterPostType = ValueHelper.TryGet(Request.QueryString["type"], 0);
            ViewBag.FilterKeyword = ValueHelper.TryGet(Request.QueryString["kw"], "");
            ViewBag.FilterUserId = ValueHelper.TryGet(Request.QueryString["userId"], 0);
            ViewBag.FilterStatus = Request.QueryString["status"].TryConvert<int>(-1);
            ViewBag.FilterDeleted = Request.QueryString["deleted"].TryConvert<bool>(false);

            Dictionary<string, bool> permissions = HasPermissions(new List<string>{ PERMISSION_ADD, PERMISSION_DELETE, PERMISSION_EDIT });
            ViewBag.CanInsert = permissions[PERMISSION_ADD];
            ViewBag.CanEdit = permissions[PERMISSION_EDIT];
            ViewBag.CanDelete = permissions[PERMISSION_DELETE];

            return View(posts);
        }

        [TVGAuthorizeAction(PermissionName = PERMISSION_ADD)]
        public ActionResult Create()
        {
            var model = new PostFormModel();
            PrepareFormModel(model);
            return View("Form", model);
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        [TVGAuthorizeAction(PermissionName = PERMISSION_ADD)]
        public ActionResult Create(PostFormModel model, params int[] category)
        {
            if (ModelState.IsValid)
            {
                var dataPost = PrepareSaveModel(model, null);
                var result = postService.InsertPost(dataPost);

                if (result > 0)
                {
                    TempData["message"] = "Tạo mới thành công";
                    TempData["status"] = "success";
                    return RedirectToAction("Index");
                }
            }

            PrepareFormModel(model);
            return View("Form", model);
        }

        [TVGAuthorizeAction(PermissionName = PERMISSION_EDIT_RULES)]
        public ActionResult Edit(int id)
        {
            if (id <= 0) return RedirectToAction("Index");
            Post post = postService.GetPostById(id);
            if (post == null) return RedirectToAction("Index");

            //tạo view model
            PostFormModel model = new PostFormModel();
            PrepareFormModel(model, post, ACTION_EDIT);

            return View("Form", model);
        }

        //POST: Post/Edit/
        [HttpPost]
        [ValidateAntiForgeryToken]
        [TVGAuthorizeAction(PermissionName = PERMISSION_EDIT_RULES)]
        [HttpParamAction("Edit")]
        public ActionResult Save(PostFormModel model, params int[] category)
        {
            if (ModelState.IsValid)
            {
                var status = model.PostStatus;
                if (status == PostStatus.GetPostStatusIdByCode(PostStatus.BI_TRA_VE))
                {
                    status = PostStatus.GetPostStatusIdByCode(PostStatus.CHO_DUYET);
                }
                var result = SavePost(model, status);

                if (result > 0)
                {
                    TempData["message"] = "Cập nhật thành công";
                    TempData["status"] = "success";
                    return RedirectToAction("Edit", new { id = model.Id });
                }
            }

            PrepareFormModel(model, null, ACTION_EDIT);
            return View("Form", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [TVGAuthorizeAction(PermissionName = PERMISSION_EDIT_RULES)]
        [HttpParamAction("Edit")]
        public ActionResult Approve(PostFormModel model, params int[] category)
        {
            if (ModelState.IsValid)
            {
                var status = PostStatus.GetPostStatusIdByCode(PostStatus.DA_DUYET);
                var result = SavePost(model, status);

                if (result > 0)
                {
                    TempData["message"] = "Cập nhật thành công";
                    TempData["status"] = "success";
                    return RedirectToAction("Edit", new { id = model.Id });
                }
            }

            PrepareFormModel(model, null, ACTION_EDIT);
            return View("Form", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [TVGAuthorizeAction(PermissionName = PERMISSION_EDIT_RULES)]
        [HttpParamAction("Edit")]
        public ActionResult Reject(PostFormModel model, params int[] category)
        {
            if (ModelState.IsValid)
            {
                var status = PostStatus.GetPostStatusIdByCode(PostStatus.BI_TRA_VE);
                var result = SavePost(model, status);

                if (result > 0)
                {
                    TempData["message"] = "Cập nhật thành công";
                    TempData["status"] = "success";
                    return RedirectToAction("Edit", new { id = model.Id });
                }
            }

            PrepareFormModel(model, null, ACTION_EDIT);
            return View("Form", model);
        }

        private int SavePost(PostFormModel model, int statusId)
        {
            var result = 0;
            var post = postService.GetPostById(model.Id);

            //chỉ thực hiện nếu bài viết có tồn tại và chưa bị xoá (IsDeleted=false)
            if (post != null && !post.IsDeleted)
            {
                var dataPost = PrepareSaveModel(model, post);
                dataPost.Post.PostStatus = statusId;
                result = postService.UpdatePost(dataPost);
            }
            return result;
        }

        //POST: Post/Delete
        [HttpPost]
        [TVGAuthorizeAction(PermissionName = PERMISSION_DELETE)]
        public ActionResult Delete(FormCollection collection)
        {
            try
            {
                int id = ValueHelper.TryGet(collection["deletedId"], 0);
                if (id > 0)
                {
                    postService.DeletePost(id);
                }
                TempData["message"] = "Xoá thành công";
                TempData["status"] = "success";
                var queryStrings = MyUrlHelper.QueryStringToDictionary(Request.Url.Query);
                return RedirectToAction("Index", new RouteValueDictionary(queryStrings));
            }
            catch(Exception ex)
            {
                TempData["message"] = "Lỗi: " + ex.Message;
                TempData["status"] = "error";
                logger.Error(ex);
                return RedirectToAction("Index");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [TVGAuthorizeAction(PermissionName = PERMISSION_EDIT)]
        public ActionResult Restore(FormCollection collection)
        {
            try
            {
                int id = ValueHelper.TryGet(collection["deletedId"], 0);
                if (id > 0)
                {
                    var post = postService.GetPostById(id);
                    post.IsDeleted = false;
                    postService.UpdatePost(post);
                }
                TempData["message"] = "Khôi phục thành công";
                TempData["status"] = "success";
                return RedirectToAction("Edit", new { id = id });
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                return RedirectToAction("Index");
            }
        }

        private void PrepareFormModel(PostFormModel model, Post post = null, string action = ACTION_CREATE)
        {
            ViewBag.Title = (action == ACTION_CREATE) ? "Tạo bài viết mới" : "Sửa thông tin bài viết";

            var userId = ValueHelper.TryGet(User.Identity.GetUserId(), 0);

            //lấy danh sách terms category trong hệ thống
            Dictionary<string, object> args = new Dictionary<string, object>();
            args["type"] = "category";
            args["getTree"] = true;
            model.Categories = taxonomyService.GetTerms(args).Items;

            model.Id = 0;
            model.FormAction = action;
            model.CreatedDate = DateTime.Now;
            model.UpdatedDate = DateTime.Now;
            model.PublishedDate = DateTime.Now;
            model.CreatedByName = "";
            model.UpdatedByName = "";
            model.PostStatus = 0;
            model.Post = new Post();
            model.Post.PostStatus = 0;
            model.PostExt = new PostExt();

            if (post != null)
            {
                model.Post = post;
                model.Id = post.Id;
                model.Title = post.Title;
                model.Slug = post.Slug.TryConvert<string>("");
                model.Description = post.Description;
                model.Content = post.Content;
                model.MetaDescription = post.MetaDescription.TryConvert<string>("");
                model.MetaKeyword = post.MetaKeyword.TryConvert<string>("");
                model.Image = post.Image;
                model.PostType = POST_TYPE;
                model.CreatedDate = post.CreatedDate;
                model.UpdatedDate = post.UpdatedDate;
                model.PublishedDate = post.PublishedDate;
                model.CreatedBy = post.CreatedBy;
                model.UpdatedBy = post.UpdatedBy;
                model.PublishedBy = post.PublishedBy;
                model.CreatedByName = UserManager.FindById(post.CreatedBy).UserName;
                model.UpdatedByName = UserManager.FindById(post.UpdatedBy).UserName;
                model.Note = post.Note;
                model.PostStatus = post.PostStatus;
            }

            if (model.Id > 0)
            {
                //lấy danh sách các terms ids 
                model.PostTermIds = postService.GetPostTermsByPostId(model.Id).Select(x => x.TermId).ToList();

                //Lấy danh sách meta
                model.PostMetas = postService.GetMetasByPostId(model.Id);

                //Lấy dánh sách tag
                model.PostTags = postService.GetTags(model.Id);

                model.PostExt = postService.GetPostExtById(model.Id);
            }

            Dictionary<string, bool> permissions = HasPermissions(new List<string> { PERMISSION_ADD, PERMISSION_DELETE, PERMISSION_EDIT, PERMISSION_EDIT_OWN, PERMISSION_APPROVE, PERMISSION_REJECT });
            ViewBag.CanInsert = permissions[PERMISSION_ADD];
            ViewBag.CanEdit = permissions[PERMISSION_EDIT] || (permissions[PERMISSION_EDIT_OWN] && userId == model.CreatedBy);
            ViewBag.CanDelete = permissions[PERMISSION_DELETE];
            ViewBag.CanApprove = permissions[PERMISSION_APPROVE];
            ViewBag.CanReject = permissions[PERMISSION_REJECT];
        }

        private PostSaveModel PrepareSaveModel(PostFormModel editModel, Post post)
        {
            if (post == null)
            {
                post = new Post();
                post.Id = 0;
                post.CreatedBy = User.Identity.GetUserId<int>();
                post.PublishedBy = post.CreatedBy;
                post.CreatedDate = DateTime.Now;
                post.PublishedDate = DateTime.Now;
                post.PostStatus = 0;
                post.CommentStatus = false;
                post.IsDeleted = false;
                post.PostType = POST_TYPE;
            }

            post.Title = editModel.Title;
            post.Slug = editModel.Slug;
            post.Description = editModel.Description;
            post.Content = editModel.Content;
            post.MetaDescription = editModel.MetaDescription.TryConvert<string>("");
            post.MetaKeyword = editModel.MetaKeyword.TryConvert<string>("");
            post.Image = editModel.Image;
            post.UpdatedBy = User.Identity.GetUserId<int>();
            post.UpdatedDate = DateTime.Now;
            post.Note = editModel.Note;

            //lay danh sach selected categories
            List<string> categoryIds = Request.Form.GetValues("category").ConvertToList<string>();
            List<int> categoryList = categoryIds.Select(int.Parse).ToList();
            List<PostTerm> postTerms = new List<PostTerm>();
            foreach (var termId in categoryList)
            {
                postTerms.Add(new PostTerm { PostId = post.Id, TermId = termId, DisplayOrder = 0 });
            }

            //lấy danh sách tags
            List<string> tagIds = Request.Form.GetValues("tagIds").ConvertToList<string>();
            var tags = new List<PostTag>();
            foreach (var str in tagIds)
            {
                tags.Add(new PostTag { PostId = post.Id, TagId = ValueHelper.TryGet(str, 0) });
            }

            PostSaveModel dataPost = new PostSaveModel();
            dataPost.Post = post;
            dataPost.Tags = tags;
            dataPost.Terms = postTerms;

            return dataPost;
        }
    }
}