﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TVG.Admin.Extensions;
using TVG.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.Identity;
using TVG.Core.Helper;
using TVG.Data.Services;

namespace TVG.Admin.Controllers
{
    [TVGAuthorize(PermissionName = "AccessAdminPanel")]
    public class BaseController : Controller
    {
        protected IPermissionService permissionService;

        public BaseController()
        {
            
        }

        public bool HasPermission(string permission)
        {
            var result = false;
            var userManager = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            var roles = userManager.GetRoles(HttpContext.User.Identity.GetUserId().TryConvert<int>(0));
            var count = 0;
            foreach (var role in roles)
            {
                count = permissionService.CheckPermissionRole(permission, role);
                if (count > 0)
                {
                    result = true;
                    break;
                }
            }
            return result;
        }

        public Dictionary<string, bool> HasPermissions(List<string> permissions)
        {
            var result = new Dictionary<string, bool>();
            foreach(var str in permissions)
            {
                result.Add(str, false);
            }
            var userManager = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            var roles = userManager.GetRoles(HttpContext.User.Identity.GetUserId().TryConvert<int>(0));
            var count = 0;
            foreach(var permission in permissions)
            {
                foreach (var role in roles)
                {
                    count = permissionService.CheckPermissionRole(permission, role);
                    if (count > 0)
                    {
                        result[permission] = true;
                        break;
                    }
                }
            }
            
            return result;
        }
    }
}