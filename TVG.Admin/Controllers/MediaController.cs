﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TVG.Data.Services;

namespace TVG.Admin.Controllers
{
    public class MediaController : BaseController
    {
        private const string PERMISSION_VIEW = "media_view";
        private const string PERMISSION_EDIT = "media_edit";
        private const string PERMISSION_ADD = "media_add";
        private const string PERMISSION_DELETE = "media_delete";

        public MediaController(IPermissionService permissionService)
        {
            this.permissionService = permissionService;
        }

        // GET: Media
        public ActionResult Index()
        {
            Dictionary<string, bool> permissions = HasPermissions(new List<string> { PERMISSION_VIEW, PERMISSION_ADD, PERMISSION_DELETE, PERMISSION_EDIT });
            ViewBag.CanView = permissions[PERMISSION_VIEW];
            ViewBag.CanInsert = permissions[PERMISSION_ADD];
            ViewBag.CanEdit = permissions[PERMISSION_EDIT];
            ViewBag.CanDelete = permissions[PERMISSION_DELETE];
            return View();
        }
    }
}