﻿using NPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TVG.Admin.Extensions;
using TVG.Admin.Models;
using TVG.Core.Helper;
using TVG.Core.Logging;
using TVG.Data.Domain;
using TVG.Data.Extensions;
using TVG.Data.Services;

namespace TVG.Admin.Controllers
{
    public class TermController : BaseController
    {
        private const string PERMISSION_VIEW = "term_view";
        private const string PERMISSION_EDIT = "term_edit";
        private const string PERMISSION_ADD = "term_add";
        private const string PERMISSION_DELETE = "term_delete";

        private const string ACTION_CREATE = "create";
        private const string ACTION_EDIT = "edit";

        ITaxonomyService taxonomyService;
        private readonly ILogManager logger;
        public TermController(ITaxonomyService taxonomyService, ILogManager logger, IPermissionService permissionService)
        {
            this.taxonomyService = taxonomyService;
            this.logger = logger;
            this.permissionService = permissionService;
        }

        [TVGAuthorizeAction(PermissionName = PERMISSION_VIEW)]
        public ActionResult Index(string type)
        {
            type = string.IsNullOrEmpty(type) ? "category" : type;
            Taxonomy taxonomy = taxonomyService.GetTaxonomyByName(type);

            //nếu không có kiểu taxonomy, báo lỗi
            if (taxonomy == null)
                return RedirectToAction("BadRequest", "Error");

            ViewBag.Taxonomy = taxonomy;

            int pageIndex = ValueHelper.TryGet(Request["pid"], 1);
            int pageSize = ValueHelper.TryGet(Request["pSize"], 20);

            Dictionary<string, object> args = new Dictionary<string, object>();
            args["type"] = type;
            args["pId"] = 0;
            args["pageIndex"] = pageIndex;
            args["pageSize"] = pageSize;
            args["getTree"] = taxonomy.IsHierarchy;
            Page<TaxonomyTerm> terms = taxonomyService.GetTerms(args);

            Dictionary<string, bool> permissions = HasPermissions(new List<string>{ PERMISSION_ADD, PERMISSION_DELETE, PERMISSION_EDIT });
            ViewBag.CanInsert = permissions[PERMISSION_ADD];
            ViewBag.CanEdit = permissions[PERMISSION_EDIT];
            ViewBag.CanDelete = permissions[PERMISSION_DELETE];

            return View(terms);
        }

        [TVGAuthorizeAction(PermissionName = PERMISSION_ADD)]
        public ActionResult Create()
        {
            string type = ValueHelper.TryGet(Request["type"], "category");
            Taxonomy taxonomy = taxonomyService.GetTaxonomyByName(type);

            //nếu không có kiểu taxonomy, báo lỗi
            if (taxonomy == null)
                return RedirectToAction("BadRequest", "Error");

            var model = new TermFormModel();
            var term = new TaxonomyTerm();
            term.Taxonomy = type;
            PrepareFormModel(model, term, ACTION_CREATE);

            return View(model);
        }

        // POST: Term/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        [TVGAuthorizeAction(PermissionName = PERMISSION_ADD)]
        public ActionResult Create(TermFormModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var metaList = TermHelper.GetMetaList(model.Term.Taxonomy);
                    var metaValue = "";
                    foreach(var meta in metaList)
                    {
                        metaValue = Request[meta.MetaName].TryConvert<string>("");
                        if(!string.IsNullOrEmpty(metaValue))
                        {
                            meta.MetaValue = metaValue;
                        }
                    }
                    model.Term.Metas = metaList;
                    taxonomyService.InsertTerm(model.Term);
                    TempData["message"] = "Tạo mới thành công";
                    TempData["status"] = "success";
                    return RedirectToAction("Index", new { type = model.Term.Taxonomy });
                }
                catch(Exception ex)
                {
                    ModelState.AddModelError("", "Something failed.");
                    logger.Error(ex);
                    return View(model);
                }
            }
            ModelState.AddModelError("", "Something failed.");
            
            return View(model);
        }

        [TVGAuthorizeAction(PermissionName = PERMISSION_EDIT)]
        public ActionResult Edit(int id)
        {
            if (id <= 0)
                return RedirectToAction("Index");

            var term = taxonomyService.GetTermById(id);
            if(term == null)
                return RedirectToAction("Index");

            var model = new TermFormModel();
            PrepareFormModel(model, term, ACTION_EDIT);

            return View(model);
        }

        // POST: Term/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        [TVGAuthorizeAction(PermissionName = PERMISSION_EDIT)]
        public ActionResult Edit(TermFormModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var metaList = TermHelper.GetMetaList(model.Term.Taxonomy);
                    var metaValue = "";
                    foreach (var meta in metaList)
                    {
                        metaValue = Request[meta.MetaName].TryConvert<string>("");
                        if (!string.IsNullOrEmpty(metaValue))
                        {
                            meta.MetaValue = metaValue;
                        }
                        meta.TermId = model.Term.Id;
                    }
                    model.Term.Metas = metaList;

                    taxonomyService.UpdateTerm(model.Term);
                    TempData["message"] = "Cập nhật thành công";
                    TempData["status"] = "success";
                    return RedirectToAction("Index", new { type = model.Term.Taxonomy });
                }
                catch
                {
                    ModelState.AddModelError("", "Something failed.");
                    return View(model);
                }
            }
            ModelState.AddModelError("", "Something failed.");
            return View(model);
        }

        // POST: Term/Delete/5
        [HttpPost]
        [TVGAuthorizeAction(PermissionName = PERMISSION_DELETE)]
        public ActionResult Delete(FormCollection collection)
        {
            try
            {
                int id = ValueHelper.TryGet(collection["deletedId"], 0);
                string type = ValueHelper.TryGet(collection["taxonomyType"], "category");
                if(id > 0)
                {
                    taxonomyService.DeleteTerm(id);
                }
                TempData["message"] = "Xoá thành công";
                TempData["status"] = "success";
                return RedirectToAction("Index", new { type = type });
            }
            catch
            {
                return RedirectToAction("Index");
            }
        }

        public void PrepareFormModel(TermFormModel model, TaxonomyTerm term, string action)
        {
            Taxonomy taxonomy = taxonomyService.GetTaxonomyByName(term.Taxonomy);
            ViewBag.Title = action == ACTION_CREATE ? "Tạo " + taxonomy.DisplayName : "Sửa thông tin " + taxonomy.DisplayName;

            List<TaxonomyTerm> termTree = new List<TaxonomyTerm>();
            if (taxonomy.IsHierarchy)
            {
                //lấy danh sách terms cho dropdown list parent terms
                Dictionary<string, object> args = new Dictionary<string, object>();
                args["type"] = taxonomy.Name;
                args["getTree"] = taxonomy.IsHierarchy;

                Page<TaxonomyTerm> terms = taxonomyService.GetTerms(args);
                termTree = terms.Items;
            }

            var metaList = TermHelper.GetMetaList(taxonomy.Name);
            if(term.Id > 0)
            {
                term.Metas = taxonomyService.GetMetasByTermId(term.Id);
            }

            model.Term = term;
            model.Taxonomy = taxonomy;
            model.MetaList = metaList;
            model.TermTree = termTree;
        }
    }
}
