﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NPoco;
using TVG.Data.Domain;
using TVG.Data.Services;
using TVG.Core.Logging;
using TVG.Admin.Models;
using TVG.Core.Helper;

namespace TVG.Admin.Controllers
{
    public class TagController : BaseController
    {
        ITagService tagService;
        ILogManager logger;

        public TagController(ITagService tagService, ILogManager logger)
        {
            this.tagService = tagService;
            this.logger = logger;
        }

        // GET: Tag
        public ActionResult Index(string type = "")
        {
            type = string.IsNullOrEmpty(type) ? "post" : type;
            int pageIndex = ValueHelper.TryGet(Request["pid"], 1);
            int pageSize = ValueHelper.TryGet(Request["pSize"], 20);

            Dictionary<string, object> args = new Dictionary<string, object>();
            args["type"] = type;
            args["pId"] = 0;
            args["pageIndex"] = pageIndex;
            args["pageSize"] = pageSize;

            var list = tagService.GetTags(args);
            ViewBag.TagType = type;
            return View(list);
        }

        public ActionResult Create()
        {
            string type = ValueHelper.TryGet(Request["type"], "post");
            ViewBag.TagType = type;
            return View();
        }

        [HttpPost]
        public ActionResult Create(TagCreateModel model)
        {
            if (ModelState.IsValid)
            {
                Tag item = new Tag
                {
                    Name = model.Name,
                    ParentId = 0,
                    TagType = model.TagType,
                    MetaDescription = model.Name,
                    MetaKeyword = model.Name,
                    Slug = model.Slug,
                    IsDeleted = false
                };
                var result = tagService.Insert(item);
                if (result > 0)
                {
                    TempData["message"] = "Tạo mới thành công";
                    TempData["status"] = "success";
                    return RedirectToAction("Index", new { type = model.TagType });
                }
            }
            return View(model);
        }

        public ActionResult Edit(int id)
        {
            if (id <= 0) return RedirectToAction("Index");
            var item = tagService.GetById(id);
            if (item == null) return RedirectToAction("Index");
            var model = new TagEditModel { ID = item.Id, Name = item.Name, Slug = item.Slug, TagType = item.TagType };
            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(TagEditModel model)
        {
            if (ModelState.IsValid)
            {
                var item = tagService.GetById(model.ID);
                item.Name = model.Name;
                item.Slug = model.Slug;
                item.MetaDescription = model.Name;
                item.MetaKeyword = model.Name;
                var result = tagService.Update(item);
                if (result > 0)
                {
                    TempData["message"] = "Cập nhật thành công";
                    TempData["status"] = "success";
                    return RedirectToAction("Index", new { type = model.TagType });
                }
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult Delete(FormCollection collection)
        {
            try
            {
                int id = ValueHelper.TryGet(collection["deletedId"], 0);
                var item = tagService.GetById(id);
                if (item != null)
                {                    
                    tagService.Delete(id);
                }
                TempData["message"] = "Xoá thành công";
                TempData["status"] = "success";
                return RedirectToAction("Index", new { type = item.TagType });
            }
            catch (Exception ex)
            {
                TempData["message"] = "Lỗi: " + ex.Message;
                TempData["status"] = "error";
                logger.Error(ex);
                return RedirectToAction("Index");
            }
        }

        public JsonResult GetTags()
        {
            var type = ValueHelper.TryGet(Request["type"], "post");
            Dictionary<string, object> args = new Dictionary<string, object>();
            args["type"] = type;
            var list = tagService.GetTags(args);
            return Json(new { list = list.Items.Select(x => new { Id = x.Id, Name = x.Name }).ToList() }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult AddTag(string tag, string tagType = "post")
        {
            string status = "error";
            int tagId = 0;
            if(!string.IsNullOrEmpty(tag))
            {
                string slug = StringHelper.Slugify(tag);
                var item = new Tag { Name = tag, Slug = slug, MetaDescription = tag, MetaKeyword = tag, TagType = tagType, IsDeleted = false };
                var result = tagService.Insert(item);
                if(result > 0)
                {
                    status = "success";
                    tagId = result;
                }
            }
            return Json(new { status = status, tagId = tagId });
        }

        public JsonResult AjaxFindTag()
        {
            string name = ValueHelper.TryGet(Request.QueryString["filter_name"], "");
            string tagType = ValueHelper.TryGet(Request.QueryString["type"], "");
            List<AutocompleteModel> list = new List<AutocompleteModel>();
            if (!string.IsNullOrEmpty(name) && name.Length > 2)
            {
                Dictionary<string, object> args = new Dictionary<string, object>();
                args["keyword"] = name;
                args["pageIndex"] = 1;
                args["pageSize"] = 10;
                args["type"] = tagType;
                args["orderBy"] = "Rank DESC";
                var tags = tagService.GetTags(args);
                foreach (var tag in tags.Items)
                {
                    list.Add(new AutocompleteModel
                    {
                        Id = tag.Id.ToString(),
                        Name = tag.Name
                    });
                }
            }
            return Json(new { status = "success", list = list }, JsonRequestBehavior.AllowGet);
        }
    }
}