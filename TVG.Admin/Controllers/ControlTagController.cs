﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NPoco;
using TVG.Data.Domain;
using TVG.Data.Services;
using TVG.Core.Logging;
using TVG.Admin.Models;
using TVG.Core.Helper;

namespace TVG.Admin.Controllers
{
    public class ControlTagController : BaseController
    {
        IControlTagService controlTagService;
        ILogManager logger;

        public ControlTagController(IControlTagService controlTagService, ILogManager logger)
        {
            this.controlTagService = controlTagService;
            this.logger = logger;
        }

        // GET: Tag
        public ActionResult Index()
        {
            Dictionary<string, object> args = new Dictionary<string, object>();
            var list = controlTagService.GetControlTags(args);
            return View(list);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(ControlTagCreateModel model)
        {
            if (ModelState.IsValid)
            {
                ControlTag item = new ControlTag
                {
                    Name = model.Name,
                    Slug = model.Slug,
                    DisplayOrder = model.DisplayOrder,
                    Color = model.Color,
                    IsDeleted = false
                };
                var result = controlTagService.Insert(item);
                if (result > 0)
                {
                    TempData["message"] = "Tạo mới thành công";
                    TempData["status"] = "success";
                    return RedirectToAction("Index");
                }
            }
            return View(model);
        }

        public ActionResult Edit(int id)
        {
            if (id <= 0) return RedirectToAction("Index");
            var item = controlTagService.GetById(id);
            if (item == null) return RedirectToAction("Index");
            var model = new ControlTagEditModel
            {
                Id = item.Id,
                Name = item.Name,
                Slug = item.Slug,
                DisplayOrder = item.DisplayOrder,
                Color = item.Color
            };
            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(ControlTagEditModel model)
        {
            if (ModelState.IsValid)
            {
                var item = controlTagService.GetById(model.Id);
                item.Name = model.Name;
                item.Slug = model.Slug;
                item.DisplayOrder = model.DisplayOrder;
                item.Color = model.Color;
                var result = controlTagService.Update(item);
                if (result > 0)
                {
                    TempData["message"] = "Tạo mới thành công";
                    TempData["status"] = "success";
                    return RedirectToAction("Index");
                }
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult Delete(FormCollection collection)
        {
            try
            {
                int id = ValueHelper.TryGet(collection["deletedId"], 0);
                if (id > 0)
                {
                    controlTagService.Delete(id);
                }
                TempData["message"] = "Xoá thành công";
                TempData["status"] = "success";
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {

                TempData["message"] = "Lỗi: " + ex.Message;
                TempData["status"] = "error";
                logger.Error(ex);
                return RedirectToAction("Index");
            }
        }
    }
}