﻿using Microsoft.AspNet.Identity;
using NPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TVG.Admin.Extensions;
using TVG.Admin.Models;
using TVG.Core.Helper;
using TVG.Core.Logging;
using TVG.Data.Domain;
using TVG.Data.Services;

namespace TVG.Admin.Controllers
{
    public class ContactMessageController : BaseController
    {
        const string ACTION_CREATE = "create";
        const string ACTION_EDIT = "edit";
        const string PERMISSION = "contact_manager";
        IContactMessageService contactMessageService;
        ILogManager logger;
        ITaxonomyService taxonomyService;

        public ContactMessageController(
            IContactMessageService contactMessageService,
            ILogManager logger,
            ITaxonomyService taxonomyService)
        {
            this.contactMessageService = contactMessageService;
            this.logger = logger;
            this.taxonomyService = taxonomyService;
        }
        // GET: ContactMessage
        public ActionResult Index(string type)
        {
            Dictionary<string, object> args = new Dictionary<string, object>();
            args["pageIndex"] = ValueHelper.TryGet(Request.QueryString["pId"], 1);
            args["pageSize"] = ValueHelper.TryGet(Request.QueryString["pSize"], 20);
            var typeId = ValueHelper.TryGet(Request.QueryString["type"], 0);
            if (typeId > 0)
            {
                args["termIds"] = typeId.ToString();
            }
            Page<ContactMessage> msgs = contactMessageService.GetMessages(args);
            foreach (var item in msgs.Items)
            {
                item.Terms = contactMessageService.GetTermsByContactMessageId(item.Id);
            }
            return View(msgs);
        }

        public ActionResult Detail(int id)
        {
            ContactMessage contact = contactMessageService.GetMessageById(id);
            if (contact != null)
            {
                contact.Metas = contactMessageService.GetMetasByMessageId(contact.Id);
                contact.Terms = contactMessageService.GetTermsByContactMessageId(contact.Id);
                return View(contact);
            }
            else
            {
                return RedirectToAction("Index");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(FormCollection collection)
        {
            try
            {
                int id = ValueHelper.TryGet(collection["deletedId"], 0);
                if (id > 0)
                {
                    contactMessageService.DeleteMessage(id);
                }
                TempData["message"] = "Xoá thành công";
                TempData["status"] = "success";
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                TempData["message"] = "Lỗi: " + ex.Message;
                TempData["status"] = "error";
                logger.Error(ex);
                return RedirectToAction("Index");
            }
        }

        public ActionResult InboxSidebar()
        {
            Dictionary<string, object> argsCat = new Dictionary<string, object>();
            argsCat["type"] = "contact_type";
            argsCat["getTree"] = true;
            Page<TaxonomyTerm> terms = taxonomyService.GetTerms(argsCat);
            ViewBag.ContactTypes = terms.Items;

            return PartialView();
        }
    }
}