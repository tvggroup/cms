﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Google.Apis.Analytics.v3;
using Google.Apis.Auth.OAuth2;
using System.Threading;
using Google.Apis.Util.Store;
using Google.Apis.Services;
using System.Security.Cryptography.X509Certificates;
using System.IO;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Security.Cryptography;
using System.Net;
using System.Collections.Specialized;
using System.Configuration;
using TVG.Data.Services;
using TVG.Core.Helper;
using TVG.Admin.Models;
using System.Threading.Tasks;
using TVG.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.Identity;
using System.Web;
using TVG.Core.Logging;

namespace TVG.Admin.Controllers
{
    public class StatisticController : BaseController
    {
        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        IStatisticService statisticService;
        IControlTagService controlTagService;
        ILogManager logger;

        public StatisticController(IStatisticService statisticService, IControlTagService controlTagService, ILogManager logger)
        {
            this.statisticService = statisticService;
            this.controlTagService = controlTagService;
            this.logger = logger;
        }

        // GET: Statistic
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult UserPosts()
        {
            DateTime fromDate = Request.QueryString["fromdate"].TryConvert<DateTime>(DateTime.Now.AddDays(-30));
            DateTime toDate = Request.QueryString["todate"].TryConvert<DateTime>(DateTime.Now);

            Dictionary<string, object> args = new Dictionary<string, object>();
            args["fromDate"] = fromDate;
            args["toDate"] = toDate.AddDays(1);//+1 vì so sánh tới 23:59

            var userPostCount = statisticService.GetUserPostCount(args);
            var userLawDocumentCount = statisticService.GetUserLawDocumentCount(args);
            var userDocumentCount = statisticService.GetUserDocumentCount(args);
            var userDocumentPostCount = statisticService.GetUserDocumentPostCount(args);
            var userDocumentCaseCount = statisticService.GetUserDocumentCaseCount(args);

            var users = UserManager.Users.ToList();

            List<UserPostsViewModel> model = new List<UserPostsViewModel>();
            var userPosts = new UserPostsViewModel();
            foreach(var u in users)
            {
                userPosts = new UserPostsViewModel();
                userPosts.UserId = u.Id;
                userPosts.Username = u.UserName;
                userPosts.PostCount = userPostCount.Where(x => x["CreatedBy"] != null && x["CreatedBy"].TryConvert<int>(0) == u.Id)
                                        .Select(x => x["UserPosts"].TryConvert<int>(0))
                                        .FirstOrDefault();
                userPosts.DocumentCount = userDocumentCount.Where(x => x["CreatedBy"] != null && x["CreatedBy"].TryConvert<int>(0) == u.Id)
                                        .Select(x => x["UserPosts"].TryConvert<int>(0))
                                        .FirstOrDefault();
                userPosts.DocumentPostCount = userDocumentPostCount.Where(x => x["CreatedBy"] != null && x["CreatedBy"].TryConvert<int>(0) == u.Id)
                                        .Select(x => x["UserPosts"].TryConvert<int>(0))
                                        .FirstOrDefault();
                userPosts.DocumentCaseCount = userDocumentCaseCount.Where(x => x["CreatedBy"] != null && x["CreatedBy"].TryConvert<int>(0) == u.Id)
                                        .Select(x => x["UserPosts"].TryConvert<int>(0))
                                        .FirstOrDefault();
                userPosts.LawDocumentCount = userLawDocumentCount.Where(x => x["CreatedBy"] != null && x["CreatedBy"].TryConvert<int>(0) == u.Id)
                                        .Select(x => x["UserPosts"].TryConvert<int>(0))
                                        .FirstOrDefault();
                userPosts.TotalCount = userPosts.PostCount + userPosts.DocumentCount + userPosts.DocumentPostCount + userPosts.DocumentCaseCount + userPosts.LawDocumentCount;
                model.Add(userPosts);
            }

            ViewBag.FromDate = fromDate;
            ViewBag.ToDate = toDate;
            return View(model);
        }

        public ActionResult UserControlTags()
        {
            DateTime fromDate = Request.QueryString["fromdate"].TryConvert<DateTime>(DateTime.Now.AddDays(-30));
            DateTime toDate = Request.QueryString["todate"].TryConvert<DateTime>(DateTime.Now);
            int controlTagId = Request.QueryString["controlTagId"].TryConvert<int>(0);

            Dictionary<string, object> args = new Dictionary<string, object>();
            args["fromDate"] = fromDate;
            args["toDate"] = toDate.AddDays(1);//+1 vì so sánh tới 23:59
            args["controlTagId"] = controlTagId;
            
            var userLawDocumentControlTagCount = statisticService.GetUserLawDocumentControlTagCount(args);
            var userDocumentControlTagCount = statisticService.GetUserDocumentControlTagCount(args);
            var userDocumentPostControlTagCount = statisticService.GetUserDocumentPostControlTagCount(args);
            var userDocumentCaseControlTagCount = statisticService.GetUserDocumentCaseControlTagCount(args);

            var users = UserManager.Users.ToList();

            List<UserControlTagsViewModel> model = new List<UserControlTagsViewModel>();            
            var userPosts = new UserControlTagsViewModel();
            foreach (var u in users)
            {
                userPosts = new UserControlTagsViewModel();
                userPosts.UserId = u.Id;
                userPosts.Username = u.UserName;                
                userPosts.DocumentControlTagCount = userDocumentControlTagCount.Where(x => x["CreatedBy"] != null && x["CreatedBy"].TryConvert<int>(0) == u.Id)
                                        .Select(x => x["UserPosts"].TryConvert<int>(0))
                                        .FirstOrDefault();
                userPosts.DocumentPostControlTagCount = userDocumentPostControlTagCount.Where(x => x["CreatedBy"] != null && x["CreatedBy"].TryConvert<int>(0) == u.Id)
                                        .Select(x => x["UserPosts"].TryConvert<int>(0))
                                        .FirstOrDefault();
                userPosts.DocumentCaseControlTagCount = userDocumentCaseControlTagCount.Where(x => x["CreatedBy"] != null && x["CreatedBy"].TryConvert<int>(0) == u.Id)
                                        .Select(x => x["UserPosts"].TryConvert<int>(0))
                                        .FirstOrDefault();
                userPosts.LawDocumentControlTagCount = userLawDocumentControlTagCount.Where(x => x["CreatedBy"] != null && x["CreatedBy"].TryConvert<int>(0) == u.Id)
                                        .Select(x => x["UserPosts"].TryConvert<int>(0))
                                        .FirstOrDefault();
                userPosts.TotalCount = userPosts.DocumentControlTagCount + userPosts.DocumentPostControlTagCount + userPosts.DocumentCaseControlTagCount + userPosts.LawDocumentControlTagCount;
                model.Add(userPosts);
            }

            ViewBag.FromDate = fromDate;
            ViewBag.ToDate = toDate;
            ViewBag.ControlTagId = controlTagId;

            args = new Dictionary<string, object>();
            ViewBag.ControlTags = controlTagService.GetControlTags(args);
            return View(model);
        }

        public ActionResult HomeGoogleAnalytics()
        {
            string keyFilePath = ConfigurationManager.AppSettings["GoogleAnalyticsAPIKeyFile"];
            string serviceAccountEmail = ConfigurationManager.AppSettings["GoogleAnalyticsServiceAccount"];
            string profileId = ConfigurationManager.AppSettings["GoogleAnalyticsProfileId"];

            //lay Token cách 1, tạm thời không sử dụng do phải gửi request 1 lần
            //loading the Key file
            //var certificate = new X509Certificate2(keyFilePath, "notasecret", X509KeyStorageFlags.Exportable);
            //var credential = new ServiceAccountCredential(new ServiceAccountCredential.Initializer(serviceAccountEmail)
            //{
            //    Scopes = scopes
            //}.FromCertificate(certificate));
            //var service = new AnalyticsService(new BaseClientService.Initializer()
            //{
            //    HttpClientInitializer = credential,
            //    ApplicationName = "Habisu",
            //});
            //DataResource.GaResource.GetRequest request = service.Data.Ga.Get(
            //   "ga:50143694",
            //   DateTime.Today.AddDays(-15).ToString("yyyy-MM-dd"),
            //   DateTime.Today.ToString("yyyy-MM-dd"),
            //   "ga:sessions");
            //request.Dimensions = "ga:year,ga:month,ga:day";
            //var data = request.Execute();

            string SCOPE_ANALYTICS_READONLY = AnalyticsService.Scope.AnalyticsReadonly;
            string token = Convert.ToString(GetAccessToken(serviceAccountEmail, keyFilePath, SCOPE_ANALYTICS_READONLY));

            ViewBag.Token = token;
            ViewBag.ProfileId = profileId;
          
            return PartialView();
        }

        public dynamic GetAccessToken(string clientIdEMail, string keyFilePath, string scope)
        {
            // certificate
            var certificate = new X509Certificate2(keyFilePath, "notasecret");

            // header
            var header = new { typ = "JWT", alg = "RS256" };

            // claimset
            var times = GetExpiryAndIssueDate();
            var claimset = new
            {
                iss = clientIdEMail,
                scope = scope,
                aud = "https://accounts.google.com/o/oauth2/token",
                iat = times[0],
                exp = times[1],
            };

            JavaScriptSerializer ser = new JavaScriptSerializer();

            // encoded header
            var headerSerialized = ser.Serialize(header);
            var headerBytes = Encoding.UTF8.GetBytes(headerSerialized);
            var headerEncoded = Convert.ToBase64String(headerBytes);

            // encoded claimset
            var claimsetSerialized = ser.Serialize(claimset);
            var claimsetBytes = Encoding.UTF8.GetBytes(claimsetSerialized);
            var claimsetEncoded = Convert.ToBase64String(claimsetBytes);

            // input
            var input = headerEncoded + "." + claimsetEncoded;
            var inputBytes = Encoding.UTF8.GetBytes(input);

            // signature
            var rsa = certificate.PrivateKey as RSACryptoServiceProvider;
            var cspParam = new CspParameters
            {
                KeyContainerName = rsa.CspKeyContainerInfo.KeyContainerName,
                KeyNumber = rsa.CspKeyContainerInfo.KeyNumber == KeyNumber.Exchange ? 1 : 2
            };
            var aescsp = new RSACryptoServiceProvider(cspParam) { PersistKeyInCsp = false };
            var signatureBytes = aescsp.SignData(inputBytes, "SHA256");
            var signatureEncoded = Convert.ToBase64String(signatureBytes);

            // jwt
            var jwt = headerEncoded + "." + claimsetEncoded + "." + signatureEncoded;

            var client = new WebClient();
            client.Encoding = Encoding.UTF8;
            var uri = "https://accounts.google.com/o/oauth2/token";
            var content = new NameValueCollection();

            content["assertion"] = jwt;
            content["grant_type"] = "urn:ietf:params:oauth:grant-type:jwt-bearer";

            string response = "";
            try
            {
                response = Encoding.UTF8.GetString(client.UploadValues(uri, "POST", content));
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
            
            var result = ser.Deserialize<dynamic>(response);

            object pulledObject = null;

            string token = "access_token";
            if (result != null && result.ContainsKey(token))
            {
                pulledObject = result[token];
            }

            //return result;
            return pulledObject;
        }

        private static int[] GetExpiryAndIssueDate()
        {
            var utc0 = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            var issueTime = DateTime.UtcNow;

            var iat = (int)issueTime.Subtract(utc0).TotalSeconds;
            var exp = (int)issueTime.AddMinutes(55).Subtract(utc0).TotalSeconds;

            return new[] { iat, exp };
        }

    }
}