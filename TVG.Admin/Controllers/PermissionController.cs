﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TVG.Core.Logging;
using TVG.Data.Domain;
using TVG.Data.Services;
using TVG.Identity;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using TVG.Core.Helper;
using TVG.Admin.Extensions;

namespace TVG.Admin.Controllers
{
    public class PermissionController : BaseController
    {
        ILogManager logger;

        const string PERMISSION = "permission_edit";

        public PermissionController(ILogManager logger, IPermissionService permissionService)
        {
            this.logger = logger;
            this.permissionService = permissionService;
        }

        [TVGAuthorizeAction(PermissionName = PERMISSION)]
        public ActionResult Index()
        {
            //get all permission
            Dictionary<string, object> args = new Dictionary<string, object>();
            ViewBag.Permissions = permissionService.GetPermissions(args).Items;

            //get all roles
            var roleManager = HttpContext.GetOwinContext().Get<ApplicationRoleManager>();
            ViewBag.Roles = roleManager.Roles.ToList();

            //get all permission roles
            ViewBag.PermissionRoles = permissionService.GetRolePemissions(0);

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [TVGAuthorizeAction(PermissionName = PERMISSION)]
        public ActionResult Index(FormCollection form)
        {
            List<string> selected = Request.Form.GetValues("permission_role").ConvertToList<string>();
            List<PermissionRole> permissionRoles = new List<PermissionRole>();
            string[] permission_role;
            foreach(var s in selected)
            {
                permission_role = s.Split('_');
                permissionRoles.Add(new PermissionRole {
                    PermissionId = permission_role[0].TryConvert<int>(0),
                    RoleId = permission_role[1].TryConvert<int>(0)
                });
            }
            permissionService.SavePermissionRoles(permissionRoles);
            return RedirectToAction("Index");
        }
        
    }
}