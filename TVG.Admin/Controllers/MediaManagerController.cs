﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TVG.Admin.Models;
using TVG.Core.Helper;
using System.Drawing;
using ImageProcessor;
using ImageProcessor.Imaging.Formats;
using ImageProcessor.Imaging;
using TVG.Data.Services;

namespace TVG.Admin.Controllers
{
    public class MediaManagerController : BaseController
    {
        private const string PERMISSION_VIEW = "media_view";
        private const string PERMISSION_EDIT = "media_edit";
        private const string PERMISSION_ADD = "media_add";
        private const string PERMISSION_DELETE = "media_delete";
        private const string UPLOAD_FOLDER = "~/Uploads";

        public MediaManagerController(IPermissionService permissionService)
        {
            this.permissionService = permissionService;
        }

        public ActionResult Index()
        {
            string target = ValueHelper.TryGet(Request.QueryString["target"], "");
            string ckEditor = ValueHelper.TryGet(Request.QueryString["ckeditor"], "");
            string ckEditorFuncNum = ValueHelper.TryGet(Request.QueryString["ckeditorfuncnum"], "0");

            ViewBag.Target = target;
            ViewBag.CKEditor = ckEditor;
            ViewBag.CKEditorFuncNum = ckEditorFuncNum;

            Dictionary<string, bool> permissions = HasPermissions(new List<string> {PERMISSION_VIEW, PERMISSION_ADD, PERMISSION_DELETE, PERMISSION_EDIT });
            ViewBag.CanView = permissions[PERMISSION_VIEW];
            ViewBag.CanInsert = permissions[PERMISSION_ADD];
            ViewBag.CanEdit = permissions[PERMISSION_EDIT];
            ViewBag.CanDelete = permissions[PERMISSION_DELETE];
            return View();
        }

        public ActionResult UploadImage()
        {
            string target = ValueHelper.TryGet(Request.QueryString["target"], "");
            string crop = ValueHelper.TryGet(Request.QueryString["crop"], "0");
            string ratio = ValueHelper.TryGet(Request.QueryString["ratio"], "0");
            string ckEditor = ValueHelper.TryGet(Request.QueryString["ckeditor"], "");
            string ckEditorFuncNum = ValueHelper.TryGet(Request.QueryString["ckeditorfuncnum"], "0");

            ViewBag.Target = target;
            ViewBag.IsCrop = crop;
            ViewBag.Ratio = ratio;
            ViewBag.CKEditor = ckEditor;
            ViewBag.CKEditorFuncNum = ckEditorFuncNum;
            return View();
        }

        [HttpPost]
        public JsonResult UploadImageAjax()
        {
            int x1 = Request.Form["hfX1"].TryConvert<int>(0);
            int y1 = Request.Form["hfY1"].TryConvert<int>(0);
            int x2 = Request.Form["hfX2"].TryConvert<int>(0);
            int y2 = Request.Form["hfY2"].TryConvert<int>(0);
            int cropHeight = Request.Form["hfCropHeight"].TryConvert<int>(0);
            int cropWidth = Request.Form["hfCropWidth"].TryConvert<int>(0);
            int thumbWidth = Request.Form["hfThumbWidth"].TryConvert<int>(0);
            int thumbHeight = Request.Form["hfThumbHeight"].TryConvert<int>(0);


            string clientUrl = ConfigurationManager.AppSettings["ClientDomain"];
            string uploadPath = Server.MapPath(UPLOAD_FOLDER);
            string directory = ValueHelper.TryGet(Request.QueryString["directory"], "images");
            directory = directory.Trim('\\').Trim('/');
            string directoryPath = "";
            if (string.IsNullOrEmpty(directory))
            {
                directoryPath = Server.MapPath(UPLOAD_FOLDER) + "\\";
            }
            else
            {
                directoryPath = Server.MapPath(UPLOAD_FOLDER) + "\\" + directory.TrimEnd('\\') + "\\";
            }

            DateTime today = DateTime.Today;
            directoryPath = Path.Combine(directoryPath, today.Year.ToString(), today.Month.ToString("00"), today.Day.ToString("00"));
            directory = Path.Combine(directory, today.Year.ToString(), today.Month.ToString("00"), today.Day.ToString("00")).Replace("\\", "/");
            if(!Directory.Exists(directoryPath))
            {
                Directory.CreateDirectory(directoryPath);
            }

            List<object> paths = new List<object>();
            List<string> uploadError = new List<string>();
            string notification = "";
            for (int i = 0; i < Request.Files.Count; i++)
            {
                var file = Request.Files[i];
                string ext = Path.GetExtension(file.FileName);
                if (!string.IsNullOrEmpty(ext))
                    ext = ext.Substring(1);
                if (IsAllowedFileType(ext))
                {
                    string newPath = Path.Combine(directoryPath, file.FileName);
                    string newFileName = Path.GetFileNameWithoutExtension(file.FileName);
                    while (System.IO.File.Exists(newPath))
                    {
                        //string newFileName = Path.GetFileNameWithoutExtension(file.FileName);
                        newFileName += "_1";
                        newPath = Path.Combine(directoryPath, newFileName + "." + ext);
                    }

                    //xu ly hinh anh (resize, crop)
                    // Format is automatically detected though can be changed.
                    ISupportedImageFormat format;
                    if (string.Compare(ext, "gif", true) == 0)
                    {
                        format = new GifFormat();
                    }
                    else if (string.Compare(ext, "png", true) == 0)
                    {
                        format = new PngFormat();
                    }
                    else
                    {
                        format = new JpegFormat { Quality = 90 };
                    }
                    byte[] data;
                    using (Stream inputStream = file.InputStream)
                    {
                        MemoryStream memoryStream = inputStream as MemoryStream;
                        if (memoryStream == null)
                        {
                            memoryStream = new MemoryStream();
                            inputStream.CopyTo(memoryStream);
                        }
                        data = memoryStream.ToArray();

                        Size size = new Size(800, 0);
                        using (MemoryStream inStream = new MemoryStream(data))
                        {
                            
                            using (MemoryStream outStream = new MemoryStream())
                            {
                                // Initialize the ImageFactory using the overload to preserve EXIF metadata.
                                using (ImageFactory imageFactory = new ImageFactory(preserveExifData: false))
                                {
                                    var resizeLayer = new ResizeLayer(size);
                                    resizeLayer.Upscale = false;

                                    //tinh crop (mode percentage)
                                    //left = % tinh tu canh trai 
                                    //top = % tinh tu canh tren
                                    //right = % tinh tu canh phai
                                    //bottom = % tinh tu canh day
                                    //x1 = khoang cach tinh tu canh trai
                                    //y1 = khoang cach tu tu canh tren
                                    //x2 = khoang cach tinh tu canh trai
                                    //y2 = khoang cach tinh tu canh tren

                                    float left =  ((float) x1 / thumbWidth) ;
                                    float top = ((float) y1 / thumbHeight) ;
                                    float right = ((float) (thumbWidth - x2) / thumbWidth) ;
                                    float bottom = ((float) (thumbHeight - y2) / thumbHeight) ;
                                    var cropLayer = new CropLayer(left, top, right, bottom, CropMode.Percentage);

                                    // Load, resize, set the format and quality and save an image.
                                    imageFactory.Load(inStream);
                                    imageFactory.Resize(resizeLayer);
                                    imageFactory.Crop(cropLayer);
                                    imageFactory.Format(format);
                                    imageFactory.Save(outStream);
                                }
                                // Do something with the stream.
                                using (FileStream img = new FileStream(newPath, FileMode.Create, FileAccess.Write))
                                {
                                    outStream.WriteTo(img);
                                }
                            }
                        }
                    }


                    //file.SaveAs(newPath);
                    paths.Add(new {
                        url = clientUrl + string.Format("/uploads/images/{0}/{1:00}/{2:00}/{3}.{4}", today.Year, today.Month, today.Day, newFileName, ext),
                        path = directory + "/" + newFileName + "." + ext
                    });
                    notification = file.FileName;
                }
                else
                {
                    return Json(new { status = "error", files = "", notification = "File type is not allowed" });
                }

            }
            return Json(new { status = "success", files = paths, notification = notification });
        }

        [HttpPost]
        public ActionResult UploadImageCK(string CKEditorFuncNum, string CKEditor, string langCod)
        {            
            string clientUrl = ConfigurationManager.AppSettings["ClientDomain"];
            string uploadPath = Server.MapPath(UPLOAD_FOLDER);
            string directory = ValueHelper.TryGet(Request.QueryString["directory"], "images");
            directory = directory.Trim('\\').Trim('/');
            string directoryPath = "";
            if (string.IsNullOrEmpty(directory))
            {
                directoryPath = Server.MapPath(UPLOAD_FOLDER) + "\\";
            }
            else
            {
                directoryPath = Server.MapPath(UPLOAD_FOLDER) + "\\" + directory.TrimEnd('\\') + "\\";
            }

            DateTime today = DateTime.Today;
            directoryPath = Path.Combine(directoryPath, today.Year.ToString(), today.Month.ToString("00"), today.Day.ToString("00"));
            directory = Path.Combine(directory, today.Year.ToString(), today.Month.ToString("00"), today.Day.ToString("00")).Replace("\\", "/");
            if (!Directory.Exists(directoryPath))
            {
                Directory.CreateDirectory(directoryPath);
            }

            List<object> paths = new List<object>();
            List<string> uploadError = new List<string>();
            string notification = "";
            string imagePath = String.Empty;
            string filePath = String.Empty;

            if(Request.Files.Count > 0)
            {
                var file = Request.Files[0];
                string ext = Path.GetExtension(file.FileName);
                if (!string.IsNullOrEmpty(ext))
                    ext = ext.Substring(1);
                if (IsAllowedFileType(ext))
                {
                    string newPath = Path.Combine(directoryPath, file.FileName);
                    string newFileName = Path.GetFileNameWithoutExtension(file.FileName);
                    while (System.IO.File.Exists(newPath))
                    {
                        //string newFileName = Path.GetFileNameWithoutExtension(file.FileName);
                        newFileName += "_1";
                        newPath = Path.Combine(directoryPath, newFileName + "." + ext);
                    }

                    //xu ly hinh anh (resize, crop)
                    // Format is automatically detected though can be changed.
                    ISupportedImageFormat format;
                    if (string.Compare(ext, "gif", true) == 0)
                    {
                        format = new GifFormat();
                    }
                    else if (string.Compare(ext, "png", true) == 0)
                    {
                        format = new PngFormat();
                    }
                    else
                    {
                        format = new JpegFormat { Quality = 90 };
                    }
                    byte[] data;
                    using (Stream inputStream = file.InputStream)
                    {
                        MemoryStream memoryStream = inputStream as MemoryStream;
                        if (memoryStream == null)
                        {
                            memoryStream = new MemoryStream();
                            inputStream.CopyTo(memoryStream);
                        }
                        data = memoryStream.ToArray();

                        Size size = new Size(800, 0);
                        using (MemoryStream inStream = new MemoryStream(data))
                        {

                            using (MemoryStream outStream = new MemoryStream())
                            {
                                // Initialize the ImageFactory using the overload to preserve EXIF metadata.
                                using (ImageFactory imageFactory = new ImageFactory(preserveExifData: false))
                                {
                                    var resizeLayer = new ResizeLayer(size);
                                    resizeLayer.Upscale = false;

                                    // Load, resize, set the format and quality and save an image.
                                    imageFactory.Load(inStream);
                                    imageFactory.Resize(resizeLayer);
                                    imageFactory.Format(format);
                                    imageFactory.Save(outStream);
                                }
                                // Do something with the stream.
                                using (FileStream img = new FileStream(newPath, FileMode.Create, FileAccess.Write))
                                {
                                    outStream.WriteTo(img);
                                }
                            }
                        }
                    }


                    //file.SaveAs(newPath);
                    imagePath = clientUrl + string.Format("/uploads/images/{0}/{1:00}/{2:00}/{3}.{4}", today.Year, today.Month, today.Day, newFileName, ext);
                    filePath = directory + "/" + newFileName + "." + ext;
                    notification = "Hình ảnh đã được upload.";
                }
                else
                {
                    notification = "Có lỗi xảy ra khi upload hình ảnh";
                }
            }
            

           
            //return Json(new { status = "success", files = paths, notification = notification });

            var vOutput = @"<html><body><script>window.parent.CKEDITOR.tools.callFunction(" + CKEditorFuncNum + ", \"" + imagePath + "\", \"" + notification + "\");</script></body></html>";

            return Content(vOutput);
        }

        public JsonResult Init(int sort)
        {
            string clientUrl = ConfigurationManager.AppSettings["ClientDomain"];
            string uploadPath = Server.MapPath(UPLOAD_FOLDER);
            string allowedFileType = ConfigurationManager.AppSettings["AllowedFileType"];
            string filterName = ValueHelper.TryGet(Request.QueryString["filter_name"], "");
            string directory = ValueHelper.TryGet(Request.QueryString["directory"], "");
            //string target = ValueHelper.TryGet(Request.QueryString["target"], "");
            directory = directory.Trim('\\').Trim('/');
            string directoryPath = "";
            if (string.IsNullOrEmpty(directory))
            {
                directoryPath = Server.MapPath(UPLOAD_FOLDER) + "\\";
            }
            else
            {
                directoryPath = Server.MapPath(UPLOAD_FOLDER) + "\\" + directory.TrimEnd('\\') + "\\";
            }

            int pageIndex = ValueHelper.TryGet(Request.QueryString["pageIndex"], 1);

            //data images
            var images = new List<MediaImageModel>();
            var directories = Directory.GetDirectories(directoryPath, filterName + "*", SearchOption.TopDirectoryOnly).Where(obj => new DirectoryInfo(obj).Name != "cache").ToList();
            DirectoryInfo dirInfo = new DirectoryInfo(directoryPath);
            string[] searchPatterns = allowedFileType.Split(',');
            //var filesQuery = dirInfo.EnumerateFiles(filterName + "*", SearchOption.TopDirectoryOnly)
            //            .Where(s => s.Name.EndsWith(".jpg", StringComparison.OrdinalIgnoreCase)
            //                || s.Name.EndsWith(".png", StringComparison.OrdinalIgnoreCase)
            //                || s.Name.EndsWith(".jpeg", StringComparison.OrdinalIgnoreCase)
            //                || s.Name.EndsWith(".gif", StringComparison.OrdinalIgnoreCase)
            //                || s.Name.EndsWith(".pdf", StringComparison.OrdinalIgnoreCase)
            //                || s.Name.EndsWith(".doc", StringComparison.OrdinalIgnoreCase)
            //                || s.Name.EndsWith(".docx", StringComparison.OrdinalIgnoreCase)
            //                || s.Name.EndsWith(".xls", StringComparison.OrdinalIgnoreCase)
            //                || s.Name.EndsWith(".xlsx", StringComparison.OrdinalIgnoreCase)
            //                || s.Name.EndsWith(".ppt", StringComparison.OrdinalIgnoreCase)
            //                || s.Name.EndsWith(".pptx", StringComparison.OrdinalIgnoreCase)
            //            );
            if (!string.IsNullOrEmpty(filterName.Trim()))
            {
                filterName = "*" + filterName;
            }
            var filesQuery = searchPatterns.AsParallel()
             .SelectMany(searchPattern =>
                    dirInfo.EnumerateFiles(filterName + searchPattern, SearchOption.TopDirectoryOnly));

            switch (sort)
            {
                case 1:
                    filesQuery = filesQuery.OrderBy(s => s.CreationTime);
                    break;
                case 2:
                    filesQuery = filesQuery.OrderByDescending(s => s.CreationTime);
                    break;
                case 3:
                    filesQuery = filesQuery.OrderBy(s => s.Name);
                    break;
                case 4:
                    filesQuery = filesQuery.OrderByDescending(s => s.Name);
                    break;
            }

            var files = filesQuery.Select(s => s.FullName);

            var strImages = new List<string>();
            strImages.AddRange(directories);
            strImages.AddRange(files);

            int totalImages = strImages.Count;
            //int pageSize = 16;
            //int startIndex = (pageIndex - 1) * pageSize;
            //int endIndex = startIndex + pageSize - 1;
            int startIndex = 0;
            int endIndex = totalImages - 1;

            string strImg = "";
            string ext = "";
            MediaImageModel model = new MediaImageModel();
            for (int i = startIndex; i <= endIndex; i++)
            {
                strImg = strImages[i];
                FileAttributes attr = System.IO.File.GetAttributes(strImages[i]);
                if (attr == FileAttributes.Directory)
                {
                    DirectoryInfo di = new DirectoryInfo(strImg);
                    model = new MediaImageModel();
                    model.Name = di.Name;
                    model.Thumb = "";
                    model.Type = "directory";
                    if (string.IsNullOrEmpty(directory))
                    {
                        model.Path = di.FullName.Substring(directoryPath.Length).Replace('\\', '/');
                    }
                    else
                    {
                        model.Path = directory + "/" + di.FullName.Substring(directoryPath.Length).Replace('\\', '/');
                    }
                    model.Url = "/MediaManager/init?directory=" + model.Path;
                    images.Add(model);
                }
                else
                {
                    FileInfo fi = new FileInfo(strImg);

                    model = new MediaImageModel();
                    model.Name = fi.Name;
                    model.Thumb = GetThumbnail(fi.FullName);
                    model.Type = "file";
                    ext = Path.GetExtension(fi.FullName);
                    if (!string.IsNullOrEmpty(ext))
                        ext = ext.Substring(1);
                    model.Extension = ext;
                    if (string.IsNullOrEmpty(directory))
                    {
                        model.Path = fi.FullName.Substring(directoryPath.Length).Replace('\\', '/');
                        model.Url = clientUrl + "/uploads/" + fi.FullName.Substring(directoryPath.Length).Replace('\\', '/');
                    }
                    else
                    {
                        model.Path = directory + "/" + fi.FullName.Substring(directoryPath.Length).Replace('\\', '/');
                        model.Url = clientUrl + "/uploads/" + directory + "/" + fi.FullName.Substring(directoryPath.Length).Replace('\\', '/');
                    }
                    images.Add(model);
                }
            }

            //parent
            var parent = "/MediaManager/init?directory=";
            var pIndex = directory.LastIndexOf('/');
            if (pIndex > 0)
                parent = parent + directory.Substring(0, pIndex);

            return Json(new { images = images, parent = parent, directory = directory }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Folder()
        {
            string clientUrl = ConfigurationManager.AppSettings["ClientDomain"];
            string uploadPath = Server.MapPath(UPLOAD_FOLDER);
            string filterName = ValueHelper.TryGet(Request.QueryString["filter_name"], "");
            string directory = ValueHelper.TryGet(Request.QueryString["directory"], "");
            directory = directory.Trim('\\').Trim('/');
            //directory = HttpUtility.HtmlDecode(directory);
            string directoryPath = "";
            if (string.IsNullOrEmpty(directory))
            {
                directoryPath = Server.MapPath(UPLOAD_FOLDER) + "\\";
            }
            else
            {
                directoryPath = Server.MapPath(UPLOAD_FOLDER) + "\\" + directory.TrimEnd('\\') + "\\";
            }

            string folderName = ValueHelper.TryGet(Request.Form["folder"], "");
            folderName = Uri.UnescapeDataString(folderName);
            if (string.IsNullOrEmpty(folderName.Trim()))
            {
                return Json(new { status = "error", notification = "Vui lòng nhập tên thư mục" });
            }
            else
            {
                string newPath = Path.Combine(directoryPath, folderName);
                if (Directory.Exists(newPath))
                {
                    return Json(new { status = "error", notification = "Thư mục đã tồn tại" });
                }
                else
                {
                    Directory.CreateDirectory(newPath);
                }
            }

            return Json(new { status = "success", notification = "Tạo thư mục thành công" });
        }

        [HttpPost]
        public JsonResult Delete(List<string> filePaths)
        {
            string clientUrl = ConfigurationManager.AppSettings["ClientDomain"];
            string uploadPath = Server.MapPath(UPLOAD_FOLDER);
            string directory = ValueHelper.TryGet(Request.QueryString["directory"], "");
            directory = directory.Trim('\\').Trim('/');
            string directoryPath = "";
            if (string.IsNullOrEmpty(directory))
            {
                directoryPath = Server.MapPath(UPLOAD_FOLDER) + "\\";
            }
            else
            {
                directoryPath = Server.MapPath(UPLOAD_FOLDER) + "\\" + directory.TrimEnd('\\') + "\\";
            }
            string path = "";
            List<string> paths = new List<string>();

            foreach (string file in filePaths)
            {
                path = Path.Combine(directoryPath, file);
                if (string.Compare(path, uploadPath, true) != 0)
                {
                    FileAttributes attr = System.IO.File.GetAttributes(path);
                    if (attr != FileAttributes.Directory)
                    {
                        System.IO.File.Delete(path);
                    }
                    else
                    {
                        Directory.Delete(path, true);
                    }
                }
            }

            return Json(new { status = "success", notification = "File đã được xoá" });
        }

        [HttpPost]
        public JsonResult Upload()
        {
            string clientUrl = ConfigurationManager.AppSettings["ClientDomain"];
            string uploadPath = Server.MapPath(UPLOAD_FOLDER);
            string directory = ValueHelper.TryGet(Request.QueryString["directory"], "");
            directory = directory.Trim('\\').Trim('/');
            string directoryPath = "";
            if (string.IsNullOrEmpty(directory))
            {
                directoryPath = Server.MapPath(UPLOAD_FOLDER) + "\\";
            }
            else
            {
                directoryPath = Server.MapPath(UPLOAD_FOLDER) + "\\" + directory.TrimEnd('\\') + "\\";
            }

            List<string> paths = new List<string>();
            List<string> uploadError = new List<string>();
            string notification = "";
            for (int i = 0; i < Request.Files.Count; i++)
            {
                var file = Request.Files[i];
                string ext = Path.GetExtension(file.FileName);
                if (!string.IsNullOrEmpty(ext))
                    ext = ext.Substring(1);
                if (IsAllowedFileType(ext))
                {
                    string newPath = Path.Combine(directoryPath, file.FileName);
                    string newFileName = Path.GetFileNameWithoutExtension(file.FileName);
                    while (System.IO.File.Exists(newPath))
                    {
                        //string newFileName = Path.GetFileNameWithoutExtension(file.FileName);
                        newFileName += "_1";
                        newPath = Path.Combine(directoryPath, newFileName + "." + ext);
                    }

                    file.SaveAs(newPath);

                    notification = file.FileName;
                }
                else
                {
                    return Json(new { status = "error", files = "", notification = "File type is not allowed" });
                }

            }
            return Json(new { status = "success", files = paths, notification = notification });
        }

        [HttpPost]
        public JsonResult CopyOrMove(List<string> filePaths, string destinationFolder, string action)
        {
            var sourceFiles = new List<string>();
            var rootDirectory = Server.MapPath(UPLOAD_FOLDER) + "\\";
            destinationFolder = Server.MapPath(UPLOAD_FOLDER) + "\\" + destinationFolder.TrimEnd('\\') + "\\";
            for (int i = 0; i < filePaths.Count; i++)
            {
                sourceFiles.Add(Path.Combine(rootDirectory, filePaths[i]));
            }
            var destFile = "";
            if (action == "copy")
            {
                foreach (var file in sourceFiles)
                {
                    destFile = Path.Combine(destinationFolder, Path.GetFileName(file));
                    System.IO.File.Copy(file, destFile, true);
                }
            }
            else if (action == "cut")
            {
                foreach (var file in sourceFiles)
                {
                    destFile = Path.Combine(destinationFolder, Path.GetFileName(file));
                    System.IO.File.Move(file, destFile);
                }
            }

            return Json(new { status = "success" });
        }

        private bool IsAllowedFileType(string ext)
        {
            string allowedFileType = ConfigurationManager.AppSettings["AllowedFileType"];
            List<string> allowedList = allowedFileType.Split(',').ToList();
            return allowedList.Contains("*." + ext.ToLower());
        }

        private string GetThumbnail(string filePath)
        {
            string ext = Path.GetExtension(filePath);
            if (!string.IsNullOrEmpty(ext))
                ext = ext.Substring(1);
            string clientUrl = ConfigurationManager.AppSettings["ClientDomain"];
            string uploadPath = Server.MapPath(UPLOAD_FOLDER);
            string thumb = "";
            try
            {
                if (ImageHelper.IsImage(ext))
                {
                    thumb = clientUrl + "/Content/Images/icon-file-img.png";
                    FileInfo fi = new FileInfo(filePath);

                    //tao thu muc moi
                    string fileFolder = "";
                    if (fi.DirectoryName.Length > uploadPath.Length)
                    {
                        fileFolder = fi.DirectoryName.Substring(uploadPath.Length);
                    }
                    string folderPath = uploadPath + "\\cache" + fileFolder;
                    if (!Directory.Exists(folderPath))
                        Directory.CreateDirectory(folderPath);

                    //tao filename moi
                    string newFileName = "";
                    string fileName = Path.GetFileNameWithoutExtension(filePath);
                    fileName += "-80x80." + ext;
                    newFileName = folderPath + "\\" + fileName;

                    //tao thumbnail neu chua co
                    if (!System.IO.File.Exists(newFileName))
                    {
                        ImageHelper.ResizeImage(filePath, newFileName, 80, 0);
                        //Image img;
                        //using (var bmpTemp = new Bitmap(filePath))
                        //{
                        //    img = new Bitmap(bmpTemp);

                        //    Image newImage = ImageHelper.Resize(img, 80, 80, RotateFlipType.RotateNoneFlipNone);
                        //    newImage.Save(newFileName);
                        //    newImage.Dispose();
                        //}
                    }
                    //thumb = clientUrl + "/uploads" + newFileName.Substring(uploadPath.Length).Replace('\\', '/');
                    thumb = "/uploads" + newFileName.Substring(uploadPath.Length).Replace('\\', '/');
                }
            }
            catch (Exception ex)
            {
                NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
                logger.Error(ex);
            }

            return thumb;
        }
    }
}