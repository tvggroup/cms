﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TVG.Admin.Extensions;

namespace TVG.Admin.Controllers
{
    public class SystemController : BaseController
    {
        const string PERMISSION = "system_file_maintenance";

        [TVGAuthorizeAction(PermissionName = PERMISSION)]
        public ActionResult CheckBrokenAttachments()
        {
            return View();
        }
    }
}