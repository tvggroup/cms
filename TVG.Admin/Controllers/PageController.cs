﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using NPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TVG.Admin.Extensions;
using TVG.Admin.Models;
using TVG.Core.Helper;
using TVG.Core.Logging;
using TVG.Data.Domain;
using TVG.Data.Services;
using TVG.Identity;

namespace TVG.Admin.Controllers
{
    /*
    Page về bản chất cũng là Post (postType = 'page')
    TO DO: Gom lại vào PostController
    /**/
    public class PageController : BaseController
    {
        private const string ACTION_EDIT = "edit";
        private const string ACTION_CREATE = "create";
        private const string POST_TYPE = "page";

        private const string PERMISSION_VIEW = "cms_post_view";
        private const string PERMISSION_EDIT = "cms_post_edit";
        private const string PERMISSION_ADD = "cms_post_add";
        private const string PERMISSION_DELETE = "cms_post_delete";

        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        IPostService postService;
        ITaxonomyService taxonomyService;
        private readonly ILogManager logger;
        public PageController(
            IPostService postService, 
            ITaxonomyService taxonomyService,
            ILogManager logger,
            IPermissionService permissionService)
        {
            this.postService = postService;
            this.taxonomyService = taxonomyService;
            this.logger = logger;
            this.permissionService = permissionService;
        }

        [TVGAuthorizeAction(PermissionName = PERMISSION_VIEW)]
        public ActionResult Index()
        {
            Dictionary<string, object> args = new Dictionary<string, object>();
            //lấy kiểu là 'page'
            args["postType"] = "page";
            args["pageIndex"] = ValueHelper.TryGet(Request.QueryString["pId"], 1);
            args["pageSize"] = ValueHelper.TryGet(Request.QueryString["pSize"], 20);
            var posts = postService.GetPosts(args);
            for (int i = 0; i < posts.Items.Count; i++)
            {
                posts.Items[i].Terms = postService.GetPostTermsByPostId(posts.Items[i].Id);
            }

            Dictionary<string, bool> permissions = HasPermissions(new List<string>{ PERMISSION_ADD, PERMISSION_DELETE, PERMISSION_EDIT });
            ViewBag.CanInsert = permissions[PERMISSION_ADD];
            ViewBag.CanEdit = permissions[PERMISSION_EDIT];
            ViewBag.CanDelete = permissions[PERMISSION_DELETE];

            return View(posts);
        }

        [TVGAuthorizeAction(PermissionName = PERMISSION_ADD)]
        public ActionResult Create()
        {
            var model = new PostFormModel();
            PrepareFormModel(model);
            return View("Form", model);
        }

        //POST Page/Create
        [HttpPost]
        [TVGAuthorizeAction(PermissionName = PERMISSION_ADD)]
        public ActionResult Create(PostFormModel model)
        {
            if (ModelState.IsValid)
            {
                var dataPost = PrepareSaveModel(model, null);
                var result = postService.InsertPost(dataPost);

                if (result > 0)
                {
                    TempData["message"] = "Tạo mới thành công";
                    TempData["status"] = "success";
                    return RedirectToAction("Index");
                }
            }

            PrepareFormModel(model);
            return View("Form", model);
        }

        [TVGAuthorizeAction(PermissionName = PERMISSION_EDIT)]
        public ActionResult Edit(int id)
        {
            if (id <= 0) return RedirectToAction("Index");
            Post post = postService.GetPostById(id);
            if (post == null) return RedirectToAction("Index");

            //tạo view model
            PostFormModel model = new PostFormModel();
            PrepareFormModel(model, post, ACTION_EDIT);

            return View("Form", model);
        }

        //POST: Post/Edit/
        [HttpPost]
        [TVGAuthorizeAction(PermissionName = PERMISSION_EDIT)]
        public ActionResult Edit(PostFormModel model)
        {
            if (ModelState.IsValid)
            {
                var result = SavePost(model, model.PostStatus);

                if (result > 0)
                {
                    TempData["message"] = "Cập nhật thành công";
                    TempData["status"] = "success";
                    return RedirectToAction("Edit", new { id = model.Id });
                }
            }

            PrepareFormModel(model, null, ACTION_EDIT);
            return View("Form", model);
        }

        private int SavePost(PostFormModel model, int statusId)
        {
            var result = 0;
            var post = postService.GetPostById(model.Id);

            //chỉ thực hiện nếu bài viết có tồn tại và chưa bị xoá (IsDeleted=false)
            if (post != null && !post.IsDeleted)
            {
                var dataPost = PrepareSaveModel(model, post);
                dataPost.Post.PostStatus = statusId;
                result = postService.UpdatePost(dataPost);
            }
            return result;
        }

        //POST: Post/Delete
        [HttpPost]
        [TVGAuthorizeAction(PermissionName = PERMISSION_DELETE)]
        public ActionResult Delete(FormCollection collection)
        {
            try
            {
                int id = ValueHelper.TryGet(collection["deletedId"], 0);
                if (id > 0)
                {
                    postService.DeletePost(id);
                }
                TempData["message"] = "Xoá thành công";
                TempData["status"] = "success";
                return RedirectToAction("Index");
            }
            catch(Exception ex)
            {
                TempData["message"] = "Lỗi: " + ex.Message;
                TempData["status"] = "error";
                logger.Error(ex);
                return RedirectToAction("Index");
            }
        }

        private void PrepareFormModel(PostFormModel model, Post post = null, string action = ACTION_CREATE)
        {
            ViewBag.Title = (action == ACTION_CREATE) ? "Tạo trang mới" : "Sửa thông tin trang";

            var userId = ValueHelper.TryGet(User.Identity.GetUserId(), 0);

            //lấy danh sách terms category trong hệ thống
            Dictionary<string, object> args = new Dictionary<string, object>();
            args["type"] = "page_category";
            args["getTree"] = true;
            model.Categories = taxonomyService.GetTerms(args).Items;

            model.Id = 0;
            model.FormAction = action;
            model.CreatedDate = DateTime.Now;
            model.UpdatedDate = DateTime.Now;
            model.PublishedDate = DateTime.Now;
            model.PostStatus = 0;

            if (post != null)
            {
                model.Post = post;
                model.Id = post.Id;
                model.Title = post.Title;
                model.Slug = post.Slug.TryConvert<string>("");
                model.Description = post.Description;
                model.Content = post.Content;
                model.MetaDescription = post.MetaDescription.TryConvert<string>("");
                model.MetaKeyword = post.MetaKeyword.TryConvert<string>("");
                model.Image = post.Image;
                model.PostType = POST_TYPE;
                model.CreatedDate = post.CreatedDate;
                model.UpdatedDate = post.UpdatedDate;
                model.PublishedDate = post.PublishedDate;
                model.CreatedBy = post.CreatedBy;
                model.UpdatedBy = post.UpdatedBy;
                model.PublishedBy = post.PublishedBy;
                model.CreatedByName = UserManager.FindById(post.CreatedBy).UserName;
                model.UpdatedByName = UserManager.FindById(post.UpdatedBy).UserName;
                model.Note = post.Note;
                model.PostStatus = post.PostStatus;
            }

            if (model.Id > 0)
            {
                var postTerms = postService.GetPostTermsByPostId(model.Id);
                //lấy danh sách các terms ids 
                model.PostTermIds = postTerms.Select(x => x.TermId).ToList();
                if(postTerms != null && postTerms.Count() > 0)
                {
                    model.DisplayOrder = postTerms[0].DisplayOrder;
                }

                //Lấy danh sách meta
                model.PostMetas = postService.GetMetasByPostId(model.Id);

                //Lấy dánh sách tag
                model.PostTags = postService.GetTags(model.Id);
            }

            Dictionary<string, bool> permissions = HasPermissions(new List<string> { PERMISSION_ADD, PERMISSION_DELETE, PERMISSION_EDIT });
            ViewBag.CanInsert = permissions[PERMISSION_ADD];
            ViewBag.CanEdit = permissions[PERMISSION_EDIT];
            ViewBag.CanDelete = permissions[PERMISSION_DELETE];
        }

        private PostSaveModel PrepareSaveModel(PostFormModel editModel, Post post)
        {
            if (post == null)
            {
                post = new Post();
                post.Id = 0;
                post.CreatedBy = User.Identity.GetUserId<int>();
                post.PublishedBy = post.CreatedBy;
                post.CreatedDate = DateTime.Now;
                post.PublishedDate = DateTime.Now;
                post.PostStatus = 1;
                post.CommentStatus = false;
                post.IsDeleted = false;
                post.PostType = POST_TYPE;
            }

            post.Title = editModel.Title;
            post.Slug = editModel.Slug;
            post.Description = editModel.Description;
            post.Content = editModel.Content;
            post.MetaDescription = editModel.MetaDescription.TryConvert<string>("");
            post.MetaKeyword = editModel.MetaKeyword.TryConvert<string>("");
            post.Image = editModel.Image;
            post.UpdatedBy = User.Identity.GetUserId<int>();
            post.UpdatedDate = DateTime.Now;
            post.Note = editModel.Note;

            //lay danh sach selected categories
            List<string> categoryIds = Request.Form.GetValues("category").ConvertToList<string>();
            List<string> categoryDisplayOrders = Request.Form.GetValues("category_display_order").ConvertToList<string>();
            List<int> categoryList = categoryIds.Select(int.Parse).ToList();
            List<int> categoryDisplayOrderList = categoryDisplayOrders.Select(int.Parse).ToList();
            List<PostTerm> postTerms = new List<PostTerm>();
            for(int i = 0; i < categoryIds.Count(); i ++)
            {
                if(categoryList[i] > 0)
                {
                    postTerms.Add(new PostTerm { PostId = post.Id, TermId = categoryList[i], DisplayOrder = categoryDisplayOrderList[i] });
                }
            }

            PostSaveModel dataPost = new PostSaveModel();
            dataPost.Post = post;
            dataPost.Terms = postTerms;

            return dataPost;
        }
    }
}