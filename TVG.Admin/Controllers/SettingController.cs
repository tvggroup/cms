﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TVG.Admin.Extensions;
using TVG.Core.Helper;
using TVG.Core.Logging;
using TVG.Data.Domain;
using TVG.Data.Services;

namespace TVG.Admin.Controllers
{
    public class SettingController : BaseController
    {
        const string PERMISSION = "system_setting";
        ISettingService optionService;
        IBannerService bannerService;
        ILogManager logger;
        ITaxonomyService taxonomyService;

        public SettingController(
            ISettingService optionService,
            IBannerService bannerService,
            ILogManager logger,
            ITaxonomyService taxonomyService)
        {
            this.optionService = optionService;
            this.bannerService = bannerService;
            this.logger = logger;
            this.taxonomyService = taxonomyService;
        }

        [TVGAuthorizeAction(PermissionName = PERMISSION)]
        public ActionResult Index()
        {
            List<Setting> options = optionService.GetSettings("site");
            Dictionary<string, object> opts = new Dictionary<string, object>();
            foreach(var opt in options)
            {
                opts.Add(opt.SettingName, opt.SettingValue);
            }
            Dictionary<string, object> args = new Dictionary<string, object>();
            ViewBag.Banners = bannerService.GetBanners(args); 
            return View(opts);
        }

        [HttpPost, ValidateInput(false)]
        [TVGAuthorizeAction(PermissionName = PERMISSION)]
        public ActionResult Index(FormCollection form)
        {
            string siteName = ValueHelper.TryGet(form["site_name"], "");
            string siteTitle = ValueHelper.TryGet(form["site_title"], "");
            string siteMetaDescription = ValueHelper.TryGet(form["site_meta_description"], "");
            string siteMetaKeyword = ValueHelper.TryGet(form["site_meta_keyword"], "");
            string sitePhone = ValueHelper.TryGet(form["site_phone"], "");
            string siteEmail = ValueHelper.TryGet(form["site_email"], "");
            string siteGA = ValueHelper.TryGet(form["site_google_analytics"], "");
            string siteFooterText = ValueHelper.TryGet(form["site_footer_text"], "");
            string siteHomeBanner = ValueHelper.TryGet(form["site_home_banner"], "");
            string siteChatBox = ValueHelper.TryGet(form["site_chat_box"], "");

            List<Setting> opts = new List<Setting>();
            opts.Add(new Setting { SettingGroup = "site", SettingName = "site_name", SettingValue = siteName });
            opts.Add(new Setting { SettingGroup = "site", SettingName = "site_title", SettingValue = siteTitle });
            opts.Add(new Setting { SettingGroup = "site", SettingName = "site_meta_description", SettingValue = siteMetaDescription });
            opts.Add(new Setting { SettingGroup = "site", SettingName = "site_meta_keyword", SettingValue = siteMetaKeyword });
            opts.Add(new Setting { SettingGroup = "site", SettingName = "site_phone", SettingValue = sitePhone });
            opts.Add(new Setting { SettingGroup = "site", SettingName = "site_email", SettingValue = siteEmail });
            opts.Add(new Setting { SettingGroup = "site", SettingName = "site_google_analytics", SettingValue = siteGA });
            opts.Add(new Setting { SettingGroup = "site", SettingName = "site_footer_text", SettingValue = siteFooterText });
            opts.Add(new Setting { SettingGroup = "site", SettingName = "site_home_banner", SettingValue = siteHomeBanner });
            opts.Add(new Setting { SettingGroup = "site", SettingName = "site_chat_box", SettingValue = siteChatBox });

            optionService.DeleteSettings("site");
            optionService.InsertSettings(opts);

            return RedirectToAction("Index");
        }

        public ActionResult HomepageSettings()
        {
            List<Setting> options = optionService.GetSettings("homepage");
            Dictionary<string, object> opts = new Dictionary<string, object>();
            foreach (var opt in options)
            {
                opts.Add(opt.SettingName, opt.SettingValue);
            }

            string homeDocumentSubjects = ValueHelper.TryGet(opts, "home_doc_subjects", "");
            string homeDocumentCaseSubjects = ValueHelper.TryGet(opts, "home_doc_case_subjects", "");
            string homeDocumentPostSubjects = ValueHelper.TryGet(opts, "home_doc_post_subjects", "");
            string homeNewsBlocks = ValueHelper.TryGet(opts, "home_news_blocks", "");
            string homeNewsTagBlocks = ValueHelper.TryGet(opts, "home_news_tag_blocks", "");

            Dictionary<string, object> args = new Dictionary<string, object>();
            args["type"] = "doc_subject";
            args["getTree"] = true;
            var documentSubjects = taxonomyService.GetTerms(args).Items;

            args = new Dictionary<string, object>();
            args["type"] = "doc_post_subject";
            args["getTree"] = true;
            var documentPostSubjects = taxonomyService.GetTerms(args).Items;

            args = new Dictionary<string, object>();
            args["type"] = "doc_case_subject";
            args["getTree"] = true;
            var documentCaseSubjects = taxonomyService.GetTerms(args).Items;

            args = new Dictionary<string, object>();
            args["type"] = "category";
            args["getTree"] = true;
            var newsCategories = taxonomyService.GetTerms(args).Items;

            int sId = 0;
            List<TaxonomyTerm> homeDocSubjectIds = new List<TaxonomyTerm>();
            string[] strHomeDocSubjectIds = homeDocumentSubjects.Split(',');
            foreach(var str in strHomeDocSubjectIds)
            {
                if(!string.IsNullOrEmpty(str))
                {
                    sId = str.TryConvert<int>(0);
                    if(documentSubjects.Any(x => x.Id == sId))
                    {
                        homeDocSubjectIds.Add(documentSubjects.Single(x => x.Id == sId));
                    }
                }
            }

            sId = 0;
            List<TaxonomyTerm> homeDocPostSubjectIds = new List<TaxonomyTerm>();
            string[] strHomeDocPostSubjectIds = homeDocumentPostSubjects.Split(',');
            foreach (var str in strHomeDocPostSubjectIds)
            {
                if (!string.IsNullOrEmpty(str))
                {
                    sId = str.TryConvert<int>(0);
                    if (documentPostSubjects.Any(x => x.Id == sId))
                    {
                        homeDocPostSubjectIds.Add(documentPostSubjects.Single(x => x.Id == sId));
                    }
                }
            }

            sId = 0;
            List<TaxonomyTerm> homeDocCaseSubjectIds = new List<TaxonomyTerm>();
            string[] strHomeDocCaseSubjectIds = homeDocumentCaseSubjects.Split(',');
            foreach (var str in strHomeDocCaseSubjectIds)
            {
                if (!string.IsNullOrEmpty(str))
                {
                    sId = str.TryConvert<int>(0);
                    if (documentCaseSubjects.Any(x => x.Id == sId))
                    {
                        homeDocCaseSubjectIds.Add(documentCaseSubjects.Single(x => x.Id == sId));
                    }
                }
            }

            sId = 0;
            List<TaxonomyTerm> homeNewsBlockIds = new List<TaxonomyTerm>();



            ViewBag.DocumentSubjects = documentSubjects;
            ViewBag.HomeDocumentSubjects = homeDocSubjectIds;

            ViewBag.DocumentPostSubjects = documentPostSubjects;
            ViewBag.HomeDocumentPostSubjects = homeDocPostSubjectIds;

            ViewBag.DocumentCaseSubjects = documentCaseSubjects;
            ViewBag.HomeDocumentCaseSubjects = homeDocCaseSubjectIds;

            ViewBag.NewsCategories = newsCategories;
           

            return View(opts);
        }

        [HttpPost]
        public ActionResult HomepageSettings(FormCollection form)
        {
            string homeDocumentSubjects = ValueHelper.TryGet(form["hfHomeDocumentSubjects"], "");
            string homeDocumentPostSubjects = ValueHelper.TryGet(form["hfHomeDocumentPostSubjects"], "");
            string homeDocumentCaseSubjects = ValueHelper.TryGet(form["hfHomeDocumentCaseSubjects"], "");
            string homeNewsBlocks = ValueHelper.TryGet(form["hfHomeNewsBlocks"], "");
            string homeNewsTagBlocks = ValueHelper.TryGet(form["hfHomeNewsTagBlocks"], "");

            List<Setting> opts = new List<Setting>();
            opts.Add(new Setting { SettingGroup = "homepage", SettingName = "home_doc_subjects", SettingValue = homeDocumentSubjects });
            opts.Add(new Setting { SettingGroup = "homepage", SettingName = "home_doc_post_subjects", SettingValue = homeDocumentPostSubjects });
            opts.Add(new Setting { SettingGroup = "homepage", SettingName = "home_doc_case_subjects", SettingValue = homeDocumentCaseSubjects });
            opts.Add(new Setting { SettingGroup = "homepage", SettingName = "home_news_blocks", SettingValue = homeNewsBlocks });
            opts.Add(new Setting { SettingGroup = "homepage", SettingName = "home_news_tag_blocks", SettingValue = homeNewsTagBlocks });

            optionService.DeleteSettings("homepage");
            optionService.InsertSettings(opts);

            return RedirectToAction("HomepageSettings");
        }
    }
}