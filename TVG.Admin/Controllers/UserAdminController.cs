﻿using TVG.Admin.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using TVG.Identity;
using TVG.Admin.Extensions;
using TVG.Data.Services;
using TVG.Core.Helper;
using NPoco;
using TVG.Data.Domain;

namespace TVG.Admin.Controllers
{
    public class UsersAdminController : BaseController
    {
        private const string PERMISSION_VIEW = "user_view";
        private const string PERMISSION_EDIT = "user_edit";
        private const string PERMISSION_ADD = "user_add";
        private const string PERMISSION_DELETE = "user_delete";

        IUserService userService;
        IRoleService roleService;

        public UsersAdminController(IPermissionService permissionService, IUserService userService, IRoleService roleService)
        {
            this.permissionService = permissionService;
            this.userService = userService;
            this.roleService = roleService;
        }

        public UsersAdminController(ApplicationUserManager userManager, 
            ApplicationRoleManager roleManager, 
            IPermissionService permissionService,
            IUserService userService,
            IRoleService roleService
        )
        {
            UserManager = userManager;
            RoleManager = roleManager;
            this.permissionService = permissionService;
            this.userService = userService;
            this.roleService = roleService;
        }

        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        private ApplicationRoleManager _roleManager;
        public ApplicationRoleManager RoleManager
        {
            get
            {
                return _roleManager ?? HttpContext.GetOwinContext().Get<ApplicationRoleManager>();
            }
            private set
            {
                _roleManager = value;
            }
        }

        [TVGAuthorizeAction(PermissionName = PERMISSION_VIEW)]
        public ActionResult Index()
        {
            string username = ValueHelper.TryGet(Request.QueryString["username"], "");
            string email = ValueHelper.TryGet(Request.QueryString["email"], "");
            string roleId = ValueHelper.TryGet(Request.QueryString["roleId"], "");

            Dictionary<string, object> args = new Dictionary<string, object>();
            args["username"] = username;
            args["email"] = email;
            args["roles"] = roleId;
            args["pageIndex"] = ValueHelper.TryGet(Request.QueryString["pId"], 1);
            args["pageSize"] = ValueHelper.TryGet(Request.QueryString["pSize"], 20);
            Page<User> users = userService.GetUsers(args);

            args = new Dictionary<string, object>();
            ViewBag.Roles = roleService.GetRoles(args).Items;

            ViewBag.FilterUsername = username;
            ViewBag.FilterEmail = email;
            ViewBag.FilterRoleId = roleId;

            Dictionary<string, bool> permissions = HasPermissions(new List<string> { PERMISSION_ADD, PERMISSION_DELETE, PERMISSION_EDIT });
            ViewBag.CanInsert = permissions[PERMISSION_ADD];
            ViewBag.CanEdit = permissions[PERMISSION_EDIT];
            ViewBag.CanDelete = permissions[PERMISSION_DELETE];
            return View(users);
            //return View(UserManager.Users.ToList());
        }

        //
        // GET: /Users/Details/5
        public async Task<ActionResult> Details(int id)
        {
            if (id <= 0)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var user = await UserManager.FindByIdAsync(id);

            ViewBag.RoleNames = await UserManager.GetRolesAsync(user.Id);

            return View(user);
        }

        [TVGAuthorizeAction(PermissionName = PERMISSION_ADD)]
        public ActionResult Create()
        {
            //Get the list of Roles
            ViewBag.RoleId = new SelectList(RoleManager.Roles.ToList(), "Name", "Name");
            return View();
        }

        //
        // POST: /Users/Create
        [HttpPost]
        [TVGAuthorizeAction(PermissionName = PERMISSION_ADD)]
        public ActionResult Create(RegisterViewModel userViewModel, params string[] selectedRoles)
        {
            if (ModelState.IsValid)
            {
                var user = new User
                {
                    UserName = userViewModel.UserName,
                    Email = userViewModel.Email,
                    FirstName = userViewModel.FirstName,
                    LastName = userViewModel.LastName,
                    EmailConfirmed = true,
                    CreatedDate = DateTime.Now,
                    UpdatedDate = DateTime.Now,
                    LastActiveDate = DateTime.Now
                };

                selectedRoles = selectedRoles ?? new string[] { };
                var userRoles = new List<UserRole>();
                var selectedRoleId = 0;
                foreach (var role in selectedRoles)
                {
                    selectedRoleId = ValueHelper.TryGet(role, 0);
                    if (selectedRoleId > 0)
                    {
                        userRoles.Add(new UserRole { UserId = user.Id, RoleId = selectedRoleId });
                    }
                }
                var saveModel = new UserSaveModel();
                saveModel.User = user;
                saveModel.Roles = userRoles;
                var result = userService.InsertUser(saveModel);
                if(result > 0)
                {
                    return RedirectToAction("Edit", new { id = result });
                }

                #region Backup code identity 2.0
                //var user = new ApplicationUser
                //{
                //    UserName = userViewModel.UserName,
                //    Email = userViewModel.Email,
                //    FirstName = userViewModel.FirstName,
                //    LastName = userViewModel.LastName,
                //    EmailConfirmed = true

                //};
                //var adminresult = await UserManager.CreateAsync(user, userViewModel.Password);

                ////Add User to the selected Roles 
                //if (adminresult.Succeeded)
                //{
                //    if (selectedRoles != null)
                //    {
                //        var result = await UserManager.AddToRolesAsync(user.Id, selectedRoles);
                //        if (!result.Succeeded)
                //        {
                //            ModelState.AddModelError("", result.Errors.First());
                //            ViewBag.RoleId = new SelectList(await RoleManager.Roles.ToListAsync(), "Name", "Name");
                //            return View();
                //        }
                //    }
                //}
                //else
                //{
                //    ModelState.AddModelError("", adminresult.Errors.First());
                //    ViewBag.RoleId = new SelectList(RoleManager.Roles, "Name", "Name");
                //    return View();

                //}
                //return RedirectToAction("Index"); 
                #endregion
            }
            ViewBag.RoleId = new SelectList(RoleManager.Roles, "Name", "Name");
            return View();
        }

        //
        [TVGAuthorizeAction(PermissionName = PERMISSION_EDIT)]
        public ActionResult Edit(int id)
        {
            if (id <= 0)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var user = UserManager.FindById(id);
            if (user == null)
            {
                return HttpNotFound();
            }

            var userRoles = UserManager.GetRoles(user.Id);

            var roles = RoleManager.Roles.ToList();

            return View(new EditUserViewModel()
            {
                Id = user.Id,
                Email = user.Email,
                UserName = user.UserName,
                FirstName = user.FirstName,
                LastName = user.LastName,
                IsLocked = user.LockoutEnabled && user.LockoutEndDateUtc >= DateTime.UtcNow,
                EmailConfirmed = user.EmailConfirmed,
                RolesList = roles.Select(x => new SelectListItem()
                {
                    Selected = userRoles.Contains(x.Name),
                    Text = x.Name,
                    Value = x.Id.ToString()
                })
            });
        }

        //
        // POST: /Users/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        [TVGAuthorizeAction(PermissionName = PERMISSION_EDIT)]
        public ActionResult Edit(EditUserViewModel editUser, params string[] selectedRole)
        {
            if (ModelState.IsValid)
            {
                User user = userService.GetUserById(editUser.Id);
                user.UserName = editUser.UserName;
                user.Email = editUser.Email;
                user.FirstName = editUser.FirstName;
                user.LastName = editUser.LastName;
                user.EmailConfirmed = editUser.EmailConfirmed;
                user.UpdatedDate = DateTime.Now;
                user.UpdatedBy = ValueHelper.TryGet(User.Identity.GetUserId(), 0);

                selectedRole = selectedRole ?? new string[] { };
                var userRoles = new List<UserRole>();
                var selectedRoleId = 0;
                foreach(var role in selectedRole)
                {
                    selectedRoleId = ValueHelper.TryGet(role, 0);
                    if (selectedRoleId > 0)
                    {
                        userRoles.Add(new UserRole { UserId = user.Id, RoleId = selectedRoleId });
                    }                        
                }
                var saveModel = new UserSaveModel();
                saveModel.User = user;
                saveModel.Roles = userRoles;
                userService.UpdateUser(saveModel);

                #region Backup code theo identity 2.0
                //var user = await UserManager.FindByIdAsync(editUser.Id);
                //if (user == null)
                //{
                //    return HttpNotFound();
                //}
                //user.UserName = editUser.UserName;
                //user.Email = editUser.Email;
                //user.FirstName = editUser.FirstName;
                //user.LastName = editUser.LastName;
                //user.EmailConfirmed = editUser.EmailConfirmed;
                //user.UpdatedDate = DateTime.Now;
                //user.UpdatedBy = ValueHelper.TryGet(User.Identity.GetUserId(), 0);
                //var result = await UserManager.UpdateAsync(user);

                //var userRoles = await UserManager.GetRolesAsync(user.Id);

                //selectedRole = selectedRole ?? new string[] { };

                //result = await UserManager.AddToRolesAsync(user.Id, selectedRole.Except(userRoles).ToArray<string>());

                //if (!result.Succeeded)
                //{
                //    ModelState.AddModelError("", result.Errors.First());
                //    return View();
                //}
                //result = await UserManager.RemoveFromRolesAsync(user.Id, userRoles.Except(selectedRole).ToArray<string>());

                //if (!result.Succeeded)
                //{
                //    ModelState.AddModelError("", result.Errors.First());
                //    return View();
                //} 
                #endregion
                return RedirectToAction("Edit", new { id = editUser.Id });
            }
            ModelState.AddModelError("", "Something failed.");
            return View();
        }

        [TVGAuthorizeAction(PermissionName = PERMISSION_DELETE)]
        public async Task<ActionResult> Delete(int id)
        {
            if (id <= 0)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var user = await UserManager.FindByIdAsync(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        //
        // POST: /Users/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [TVGAuthorizeAction(PermissionName = PERMISSION_DELETE)]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            if (ModelState.IsValid)
            {
                if (id <= 0)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }

                var user = await UserManager.FindByIdAsync(id);
                if (user == null)
                {
                    return HttpNotFound();
                }
                var result = await UserManager.DeleteAsync(user);
                if (!result.Succeeded)
                {
                    ModelState.AddModelError("", result.Errors.First());
                    return View();
                }
                return RedirectToAction("Index");
            }
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ResetPassword()
        {
            var id = Request.Form["Id"].TryConvert<int>(0);
            var password = Request.Form["Password"].TryConvert<string>("");
            if(id > 0)
            {
                var user = userService.GetUserById(id);
                if(string.IsNullOrEmpty(password))
                {
                    password = "123@456";
                }
                if(user != null)
                {
                    var code = UserManager.GeneratePasswordResetToken(user.Id);                   
                    UserManager.ResetPassword(user.Id, code, password);
                    TempData["message"] = "Đổi mật khẩu thành công";
                    TempData["status"] = "success";
                    return RedirectToAction("Edit", new { id = id });
                }
            }
            return RedirectToAction("Index");
        }


        #region Locked users manager

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LockUser()
        {
            var id = Request.Form["LockUserId"].TryConvert<int>(0);
            if (id > 0)
            {
                var user = userService.GetUserById(id);
                if (user != null)
                {
                    userService.LockUser(user);
                }
                return RedirectToAction("Edit", new { id = id });
            }

            return RedirectToAction("Index");
        }

        [TVGAuthorizeAction(PermissionName = PERMISSION_VIEW)]
        public ActionResult LockedOutUsers()
        {
            Dictionary<string, bool> permissions = HasPermissions(new List<string> { PERMISSION_ADD, PERMISSION_DELETE, PERMISSION_EDIT });
            ViewBag.CanInsert = permissions[PERMISSION_ADD];
            ViewBag.CanEdit = permissions[PERMISSION_EDIT];
            ViewBag.CanDelete = permissions[PERMISSION_DELETE];
            var users = UserManager.Users.Where(x => x.LockoutEnabled == true && x.LockoutEndDateUtc >= DateTime.UtcNow).ToList();
            return View(users);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [TVGAuthorizeAction(PermissionName = PERMISSION_EDIT)]
        public ActionResult Unlock(int id)
        {
            if(id > 0)
            {
                var user = userService.GetUserById(id);
                UserManager.ResetAccessFailedCount(id);
                userService.UnlockUser(user);
                TempData["message"] = "Tài khoản đã được unlock";
                TempData["status"] = "success";
                return RedirectToAction("Edit", new { id = id });
            }
            
            return RedirectToAction("LockedOutUsers");
        }
        #endregion

        #region Ajax
        [HttpPost]
        public JsonResult AjaxGetUsers()
        {
            bool simpleList = Request.Form["simpleList"].TryConvert<bool>(true);
            bool canManage = Request.Form["canManage"].TryConvert<bool>(true);

            List<ApplicationUser> users = new List<ApplicationUser>();
            var allUsers = UserManager.Users.OrderBy(x => x.UserName).ToList();
            if(canManage)
            {
                var permissionRoles = permissionService.GetPermissionRoleAccessAdmin();
                bool isInRole = false;
                foreach(var user in allUsers)
                {
                    isInRole = false;
                    foreach(var pr in permissionRoles)
                    {
                        if(UserManager.IsInRole(user.Id, pr.RoleName))
                        {
                            isInRole = true;
                            break;
                        }
                    }
                    if (isInRole)
                    {
                        users.Add(user);
                    }
                }
            }
            else
            {
                users = allUsers;
            }
            if(!simpleList)
            {
                return Json(new { list = users });
            }
            else
            {
                var list = users.Select(x => new SelectItemModel { Id = x.Id.ToString(), Text = x.UserName }).ToList();
                return Json(new { list = list });
            }
            
        }
        #endregion
    }
}
