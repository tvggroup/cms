﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TVG.Admin.Models;
using TVG.Core.Helper;
using TVG.Core.Logging;
using TVG.Data.Domain;
using TVG.Data.Services;

namespace TVG.Admin.Controllers
{
    public class MenuManagerController : BaseController
    {
        IMenuService menuService;
        private readonly ILogManager logger;
        public MenuManagerController(IMenuService menuService, ILogManager logger)
        {
            this.menuService = menuService;
            this.logger = logger;
        }

        //GET /menumanager
        public ActionResult Index()
        {
            Dictionary<string, object> args = new Dictionary<string, object>();
            args["isDeleted"] = false;
            var list = menuService.GetMenus(args);
            return View(list);
        }

        //GET /menumanager/create
        public ActionResult Create()
        {
            return View();
        }

        //POST /menumanager/create
        [HttpPost]
        public ActionResult Create(MenuCreateModel item)
        {
            if(ModelState.IsValid)
            {
                Menu menu = new Menu {
                    Name = item.Name,
                    Slug = item.Slug,
                    IsDeleted = false
                };

                int result = menuService.InsertMenu(menu);
                if(result > 0)
                {
                    TempData["message"] = "Tạo mới thành công";
                    TempData["status"] = "success";
                    return RedirectToAction("Index");
                }
            }

            return View(item);
        }

        public ActionResult Edit(int id)
        {
            if (id <= 0) return RedirectToAction("Index");
            Menu menu = menuService.GetMenuById(id);
            if (menu == null) return RedirectToAction("Index");
            MenuEditModel model = new MenuEditModel {
                Id = menu.Id,
                Name = menu.Name,
                Slug = menu.Slug
            };

            Dictionary<string, object> args = new Dictionary<string, object>();
            args["menuId"] = id;
            args["parentId"] = 0;
            List<MenuItem> menuItems = menuService.GetMenuItems(args);
            ViewBag.MenuItems = menuItems;

            return View(model);
        }

        public JsonResult SaveMenu()
        {
            int menuId = ValueHelper.TryGet(Request.Form["id"], 0);
            string menuName = ValueHelper.TryGet(Request.Form["menu_name"], "");
            string menuSlug = ValueHelper.TryGet(Request.Form["menu_slug"], "");

            if (menuId > 0)
            {
                Menu menu = menuService.GetMenuById(menuId);
                if (menu != null)
                {
                    menu.Name = menuName;
                    menu.Slug = menuSlug;
                    if (menuService.UpdateMenu(menu) > 0)
                    {
                        return Json(new { success = "Cập nhật thành công" });
                    }
                }
            }
            return Json(new { error = "Cập nhật thất bại" });
        }

        public JsonResult AddMenuItem()
        {
            int menuId = ValueHelper.TryGet(Request.Form["menu_id"], 0);
            string text = ValueHelper.TryGet(Request.Form["item_text"], "");
            string url = ValueHelper.TryGet(Request.Form["item_url"], "");
            string customClass = ValueHelper.TryGet(Request.Form["item_class"], "");
            if(menuId > 0 && !string.IsNullOrEmpty(text) && !string.IsNullOrEmpty(url))
            {
                MenuItem item = new MenuItem {
                    Text = text,
                    Url = url,
                    Slug = "",
                    CustomClass = customClass,
                    MenuId = menuId,
                    ParentId = 0,
                    Enabled = false
                };
                var itemId = menuService.InsertMenuItem(item);
                if (itemId > 0)
                {
                    return Json(new { success = "Thêm mục menu thành công", item_id = itemId });
                }
            }

            return Json(new { error = "Thêm mục menu thất bại" });
        }

        public JsonResult SaveMenuTree(MenuPost data)
        {
            MenuItem item;
            foreach(MenuItemPost postItem in data.menu_tree)
            {
                item = menuService.GetMenuItemById(postItem.id);
                item.ParentId = postItem.parent;
                item.Text = postItem.text;
                item.Url = postItem.url;
                item.DisplayOrder = postItem.order;
                item.CustomClass = postItem.css;
                item.Enabled = postItem.enabled;
                menuService.UpdateMenuItem(item);
            }

            if(!string.IsNullOrEmpty(data.deleted_ids))
            {
                var ids = data.deleted_ids.TrimEnd(',');
                menuService.DeleteMenuItems(ids);
            }

            return Json(new { success = "Danh sách menu đã được cập nhật" });            
        }


        #region Internal Class dùng để lấy dữ liệu post ajax khi save danh sách menu items
        public class MenuItemPost
        {
            public int id { get; set; }
            public int parent { get; set; }
            public int order { get; set; }
            public string text { get; set; }
            public string url { get; set; }
            public string css { get; set; }
            public bool enabled { get; set; }
        }

        public class MenuPost
        {
            public List<MenuItemPost> menu_tree { get; set; }
            public string deleted_ids { get; set; }
        } 
        #endregion
    }
}