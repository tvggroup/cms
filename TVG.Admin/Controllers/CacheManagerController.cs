﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TVG.Core.Caching;

namespace TVG.Admin.Controllers
{
    public class CacheManagerController : Controller
    {
        private ICacheManager cacheManager;

        public CacheManagerController(ICacheManager cacheManager)
        {
            this.cacheManager = cacheManager;
        }
        // GET: CacheManager
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ClearAllCache()
        {
            cacheManager.Clear();
            return RedirectToAction("Index");
        }
    }
}