﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TVG.Admin.Extensions;
using TVG.Admin.Models;
using TVG.Core.Helper;
using TVG.Core.Logging;
using TVG.Data.Domain;
using TVG.Data.Services;

namespace TVG.Admin.Controllers
{
    public class BannerController : BaseController
    {
        const string PERMISSION = "banner_manager";
        IBannerService bannerService;
        ILogManager logger;

        public BannerController(
            IBannerService bannerService,
            ILogManager logger)
        {
            this.bannerService = bannerService;
            this.logger = logger;
        }

        [TVGAuthorizeAction(PermissionName = PERMISSION)]
        public ActionResult Index()
        {
            Dictionary<string, object> args = new Dictionary<string, object>();
            List<Banner> banners = bannerService.GetBanners(args);
            //ViewBag.Banners = banners;
            return View(banners);
        }

        [TVGAuthorizeAction(PermissionName = PERMISSION)]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [TVGAuthorizeAction(PermissionName = PERMISSION)]
        public ActionResult Create(BannerCreateModel model)
        {
            if(ModelState.IsValid)
            {
                Banner banner = new Banner {
                    Title = model.Title,
                    Slug = model.Slug,
                    IsDeleted = false
                };

                List<string> imgImages = new List<string>();
                if (Request.Form.GetValues("images") != null)
                {
                    imgImages = Request.Form.GetValues("images").ToList();
                }

                List<string> imgLinks = new List<string>();
                if (Request.Form.GetValues("links") != null)
                {
                    imgLinks = Request.Form.GetValues("links").ToList();
                }

                List<string> imgDisplayOrders = new List<string>();
                if (Request.Form.GetValues("display_orders") != null)
                {
                    imgDisplayOrders = Request.Form.GetValues("display_orders").ToList();
                }

                List<BannerImage> bannerImages = new List<BannerImage>();
                string itemImage = "";
                string itemLink = "";
                int itemDisplayOrder = 0;
                for(int i = 0; i < imgImages.Count; i++)
                {
                    itemImage = ValueHelper.TryGet(imgImages[i], "");
                    itemLink = ValueHelper.TryGet(imgLinks[i], "");
                    itemDisplayOrder = ValueHelper.TryGet(imgDisplayOrders[i], 0);
                    bannerImages.Add(new BannerImage { BannerId = 0, Image = itemImage, Link = itemLink, DisplayOrder = itemDisplayOrder });
                }

                var result = bannerService.InsertBanner(banner, bannerImages);
                if (result > 0)
                {
                    TempData["message"] = "Tạo mới thành công";
                    TempData["status"] = "success";
                    return RedirectToAction("Index");
                }
            }
            return View(model);
        }

        [TVGAuthorizeAction(PermissionName = PERMISSION)]
        public ActionResult Edit(int id)
        {
            if (id <= 0) return RedirectToAction("Index");
            var banner = bannerService.GetBannerById(id);
            if (banner == null) return RedirectToAction("Index");

            BannerEditModel model = new BannerEditModel
            {
                Id = banner.Id,
                Title = banner.Title,
                Slug = banner.Slug
            };

            List<BannerImage> images = bannerService.GetBannerImages(id);
            ViewBag.Images = images;

            return View(model);
        }

        [HttpPost]
        [TVGAuthorizeAction(PermissionName = PERMISSION)]
        public ActionResult Edit(BannerEditModel model)
        {
            if(ModelState.IsValid)
            {
                var banner = bannerService.GetBannerById(model.Id);

                if(banner != null && banner.IsDeleted == false)
                {
                    banner.Title = model.Title;
                    banner.Slug = model.Slug;

                    List<string> imgImages = new List<string>();
                    if (Request.Form.GetValues("images") != null)
                    {
                        imgImages = Request.Form.GetValues("images").ToList();
                    }

                    List<string> imgLinks = new List<string>();
                    if (Request.Form.GetValues("links") != null)
                    {
                        imgLinks = Request.Form.GetValues("links").ToList();
                    }

                    List<string> imgDisplayOrders = new List<string>();
                    if (Request.Form.GetValues("display_orders") != null)
                    {
                        imgDisplayOrders = Request.Form.GetValues("display_orders").ToList();
                    }

                    List<BannerImage> bannerImages = new List<BannerImage>();
                    string itemImage = "";
                    string itemLink = "";
                    int itemDisplayOrder = 0;
                    for (int i = 0; i < imgImages.Count; i++)
                    {
                        itemImage = ValueHelper.TryGet(imgImages[i], "");
                        itemLink = ValueHelper.TryGet(imgLinks[i], "");
                        itemDisplayOrder = ValueHelper.TryGet(imgDisplayOrders[i], 0);
                        bannerImages.Add(new BannerImage { BannerId = banner.Id, Image = itemImage, Link = itemLink, DisplayOrder = itemDisplayOrder });
                    }

                    var result = bannerService.UpdateBanner(banner, bannerImages);
                    if (result > 0)
                    {
                        TempData["message"] = "Cập nhật thành công";
                        TempData["status"] = "success";
                        return RedirectToAction("Index");
                    }

                }
            }

            List<BannerImage> images = bannerService.GetBannerImages(model.Id);
            ViewBag.Images = images;
            return View(model);
        }

        [HttpPost]
        [TVGAuthorizeAction(PermissionName = PERMISSION)]
        public ActionResult Delete(FormCollection collection)
        {
            try
            {
                int id = ValueHelper.TryGet(collection["deletedId"], 0);
                if (id > 0)
                {
                    bannerService.DeleteBanner(id);
                }
                TempData["message"] = "Xoá thành công";
                TempData["status"] = "success";
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                TempData["message"] = "Lỗi: " + ex.Message;
                TempData["status"] = "error";
                logger.Error(ex);
                return RedirectToAction("Index");
            }
        }
    }
}