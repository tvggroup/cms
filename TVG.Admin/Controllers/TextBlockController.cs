﻿using Microsoft.AspNet.Identity;
using NPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TVG.Admin.Extensions;
using TVG.Admin.Models;
using TVG.Core.Helper;
using TVG.Core.Logging;
using TVG.Data.Domain;
using TVG.Data.Services;

namespace TVG.Admin.Controllers
{
    /*
    Page về bản chất cũng là Post (postType = 'page')
    TO DO: Gom lại vào PostController
    /**/
    public class TextBlockController : BaseController
    {
        private const string PERMISSION_VIEW = "cms_post_view";
        private const string PERMISSION_EDIT = "cms_post_edit";
        private const string PERMISSION_ADD = "cms_post_add";
        private const string PERMISSION_DELETE = "cms_post_delete";

        IPostService postService;
        ITaxonomyService taxonomyService;
        private readonly ILogManager logger;
        public TextBlockController(
            IPostService postService,
            ITaxonomyService taxonomyService,
            ILogManager logger,
            IPermissionService permissionService)
        {
            this.postService = postService;
            this.taxonomyService = taxonomyService;
            this.logger = logger;
            this.permissionService = permissionService;
        }

        [TVGAuthorizeAction(PermissionName = PERMISSION_VIEW)]
        public ActionResult Index()
        {
            Dictionary<string, object> args = new Dictionary<string, object>();
            //lấy kiểu là 'page'
            args["postType"] = "text-block";
            //chỉ lấy các cột cần thiết trong bảng Posts
            args["columns"] = "Id, Title, Slug, Description, CreatedDate, CreatedBy";
            args["pageIndex"] = ValueHelper.TryGet(Request.QueryString["pId"], 1);
            args["pageSize"] = ValueHelper.TryGet(Request.QueryString["pSize"], 20);
            var posts = postService.GetPosts(args);

            Dictionary<string, bool> permissions = HasPermissions(new List<string> { PERMISSION_ADD, PERMISSION_DELETE, PERMISSION_EDIT });
            ViewBag.CanInsert = permissions[PERMISSION_ADD];
            ViewBag.CanEdit = permissions[PERMISSION_EDIT];
            ViewBag.CanDelete = permissions[PERMISSION_DELETE];

            return View(posts);
        }

        [TVGAuthorizeAction(PermissionName = PERMISSION_ADD)]
        public ActionResult Create()
        {
            //page không có category, tag ---> không cần lấy list terms như post
            return View();
        }

        //POST Page/Create
        [HttpPost]
        [TVGAuthorizeAction(PermissionName = PERMISSION_ADD)]
        public ActionResult Create(PostCreateModel model)
        {
            if (ModelState.IsValid)
            {
                var post = new Post
                {
                    Title = model.Title,
                    Slug = model.Slug,
                    Description = model.Description,
                    Content = model.Content,
                    MetaDescription = "",
                    MetaKeyword = "",
                    Image = "",
                    PostType = "text-block",
                    CreatedBy = ValueHelper.TryGet(User.Identity.GetUserId(), 0),
                    CreatedDate = DateTime.Now,
                    UpdatedDate = DateTime.Now,
                    PublishedDate = DateTime.Now,
                    PostStatus = 1,
                    CommentStatus = false,
                    IsDeleted = false
                };

                var result = postService.InsertPost(post);
                if (result > 0)
                {
                    TempData["message"] = "Tạo mới thành công";
                    TempData["status"] = "success";
                    return RedirectToAction("Index");
                }
            }

            //tạo view khi có lỗi xảy ra            
            return View(model);
        }

        [TVGAuthorizeAction(PermissionName = PERMISSION_EDIT)]
        public ActionResult Edit(int id)
        {
            if (id <= 0) return RedirectToAction("Index");
            Post post = postService.GetPostById(id);
            if (post == null) return RedirectToAction("Index");

            //tạo view model
            PostEditModel model = new PostEditModel
            {
                Id = post.Id,
                Title = post.Title,
                Slug = post.Slug,
                Description = post.Description,
                Content = post.Content,
                MetaDescription = post.MetaDescription.TryConvert<string>(""),
                MetaKeyword = post.MetaKeyword.TryConvert<string>(""),
                Image = post.Image,
                PostType = post.PostType,
                CreatedDate = post.CreatedDate,
                UpdatedDate = post.UpdatedDate,
                PublishedDate = post.PublishedDate
            };

            return View(model);
        }

        //POST: Post/Edit/
        [HttpPost]
        [TVGAuthorizeAction(PermissionName = PERMISSION_EDIT)]
        public ActionResult Edit(PostEditModel model)
        {
            if (ModelState.IsValid)
            {
                var post = postService.GetPostById(model.Id);
                //chỉ thực hiện nếu bài viết có tồn tại và chưa bị xoá (IsDeleted=false)
                if (post != null && !post.IsDeleted)
                {
                    post.Title = model.Title;
                    post.Slug = model.Slug;
                    post.Description = model.Description;
                    post.Content = model.Content;
                    post.MetaDescription = "";
                    post.MetaKeyword = "";
                    post.Image = model.Image;
                    post.PostType = "text-block";
                    post.CreatedBy = ValueHelper.TryGet(User.Identity.GetUserId(), 0);
                    post.UpdatedDate = DateTime.Now;

                    var result = postService.UpdatePost(post);
                    if (result > 0)
                    {
                        TempData["message"] = "Cập nhật thành công";
                        TempData["status"] = "success";
                        return RedirectToAction("Index");
                    }
                }
            }

            //tạo view nếu có lỗi xảy ra
            return View(model);
        }

        //POST: Post/Delete
        [HttpPost]
        [TVGAuthorizeAction(PermissionName = PERMISSION_DELETE)]
        public ActionResult Delete(FormCollection collection)
        {
            try
            {
                int id = ValueHelper.TryGet(collection["deletedId"], 0);
                if (id > 0)
                {
                    postService.DeletePost(id);
                }
                TempData["message"] = "Xoá thành công";
                TempData["status"] = "success";
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                TempData["message"] = "Lỗi: " + ex.Message;
                TempData["status"] = "error";
                logger.Error(ex);
                return RedirectToAction("Index");
            }
        }
    }
}