﻿using NPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVG.Identity
{
    public class UserTable<TUser> where TUser : IdentityUser
    {
        private Database db;
        private const string tableUser = "AspNetUsers";

        public UserTable(Database db)
        {
            this.db = db;
        }

        public IEnumerable<TUser> GetUsers()
        {
            return db.Fetch<TUser>(string.Format("SELECT * FROM {0}", tableUser));
        }

        public string GetUserName(int userId)
        {
            return db.ExecuteScalar<string>(string.Format("SELECT UserName FROM {0} WHERE Id = @0", tableUser), userId);
        }

        public int GetUserId(string userName)
        {
            return db.ExecuteScalar<int>(string.Format("SELECT Id FROM {0} WHERE UserName = @0", tableUser), userName);
        }

        public TUser GetUserById(int userId)
        {
            return db.FirstOrDefault<TUser>(string.Format("SELECT * FROM {0} WHERE Id = @0", tableUser), userId);
        }

        public List<TUser> GetUserByName(string userName)
        {
            string sql = string.Format("SELECT * FROM {0} WHERE UserName = @0", tableUser);
            return db.Fetch<TUser>(sql, userName);
        }

        public List<TUser> GetUserByEmail(string email)
        {
            string sql = string.Format("SELECT * FROM {0} WHERE Email = @0", tableUser);
            return db.Fetch<TUser>(sql, email);
        }

        public string GetPasswordHash(int userId)
        {
            string sql = string.Format("SELECT PasswordHash FROM {0} WHERE Id=@0", tableUser);
            var passHash = db.ExecuteScalar<string>(sql, userId);

            return string.IsNullOrEmpty(passHash) ? null : passHash;
        }

        public int SetPasswordHash(int userId, string passwordHash)
        {
            string sql = string.Format("UPDATE {0} SET PasswordHash = @0 WHERE Id = @1", tableUser);
            return db.Execute(sql, passwordHash, userId);
        }

        public string GetSecurityStamp(int userId)
        {
            string commandText = string.Format("Select SecurityStamp from {0} where Id = @id", tableUser);
            return db.ExecuteScalar<string>(commandText, userId);
        }

        public int Insert(TUser user)
        {
            return db.Insert(tableUser, "Id", user) != null ? 1 : 0;
        }

        private int Delete(int userId)
        {
            string sql = string.Format("DELETE FROM {0} WHERE Id = @0", tableUser);
            return db.Execute(sql, userId);
        }

        public int Delete(TUser user)
        {
            return Delete(user.Id);
        }

        public int Update(TUser user)
        {
            return db.Update(tableUser, "Id", user);
        }
    }
}
