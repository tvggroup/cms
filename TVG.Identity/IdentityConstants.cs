﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVG.Identity
{
    public static class IdentityConstants
    {
        public const string ConnecionName = "DefaultConnection";

        public const string DefaultMemberRole = "member";
        public static readonly List<string> ReservedRoles = new List<string>{ "admin", "member" };
    }

    public static class MyIdentityMessages
    {
        public const string ConfirmAccountEmailHeader = "[TheGioiLuat.vn] Thư xác nhận đăng ký";
        public const string ConfirmAccountEmailBody = @"Cám ơn bạn đã đăng ký thành viên. Vui lòng nhấn <a href='{0}'>vào đây</a> để hoàn tất việc đăng ký.";

        public const string EmailResetPasswordHeader = "[TheGioiLuat.vn] Thay đổi mật khẩu";
        public const string EmailResetPasswordBody = "Vui lòng truy cập <a href='{0}'>vào đây</a> để thay đổi mật khẩu của bạn. <br /> Nếu bạn không yêu cầu đổi mật khẩu thì có thể bỏ qua email này.";

    }
}
