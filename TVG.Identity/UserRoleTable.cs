﻿using NPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVG.Identity
{
    public class UserRoleTable
    {
        private Database db;
        private const string tableUser = "AspNetUsers";
        private const string tableRole = "AspNetRoles";
        private const string tableUserRole = "AspNetUserRoles";

        public UserRoleTable(Database db)
        {
            this.db = db;
        }

        public List<string> FindByUserId(int userId)
        {
            string sql = string.Format("SELECT r.Name FROM {0} ur JOIN {1} r ON ur.RoleId = r.Id WHERE ur.UserId = @0", 
                tableUserRole, tableRole);
            return db.Fetch<string>(sql, userId);
        }

        public int Delete(int userId)
        {
            string sql = string.Format("DELETE FROM {0} WHERE UserId = @0", tableUserRole);
            return db.Execute(sql, userId);
        }

        public int Delete(int roleId, int userId)
        {
            string sql = string.Format("DELETE FROM {0} WHERE RoleId = @0 AND UserId = @1", tableUserRole);
            return db.Execute(sql, roleId, userId);
        }

        public int Insert(IdentityUser user, int roleId)
        {
            string sql = string.Format("INSERT INTO {0}(UserId, RoleId) VALUES(@0, @1)", tableUserRole);
            return db.Execute(sql, user.Id, roleId);
        }
    }
}
