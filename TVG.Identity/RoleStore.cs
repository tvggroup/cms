﻿using Microsoft.AspNet.Identity;
using NPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVG.Identity
{
    public class RoleStore<TRole> : IQueryableRoleStore<TRole, int> where TRole : IdentityRole
    {
        private RoleTable<TRole> roleTable;
        public Database Database { get; private set; }

        public RoleStore()
        {
            Database = new Database(IdentityConstants.ConnecionName);
            roleTable = new RoleTable<TRole>(Database);
        }

        public RoleStore(Database database)
        {
            Database = database;
            roleTable = new RoleTable<TRole>(database);
        }

        public IQueryable<TRole> Roles
        {
            get { return roleTable.GetRoles() as IQueryable<TRole>; }

        }

        public Task CreateAsync(TRole role)
        {
            if (role == null)
            {
                throw new ArgumentNullException("role");
            }

            roleTable.Insert(role);

            return Task.FromResult<object>(null);
        }

        public Task DeleteAsync(TRole role)
        {
            if (role == null)
            {
                throw new ArgumentNullException("user");
            }

            roleTable.Delete(role.Id);

            return Task.FromResult<Object>(null);
        }

        public Task<TRole> FindByIdAsync(int roleId)
        {
            TRole result = roleTable.GetRoleById(roleId) as TRole;

            return Task.FromResult<TRole>(result);
        }

        public Task<TRole> FindByNameAsync(string roleName)
        {
            TRole result = roleTable.GetRoleByName(roleName) as TRole;

            return Task.FromResult<TRole>(result);
        }

        public Task UpdateAsync(TRole role)
        {
            if (role == null)
            {
                throw new ArgumentNullException("user");
            }

            roleTable.Update(role);

            return Task.FromResult<Object>(null);
        }

        public void Dispose()
        {
            if (Database != null)
            {
                Database.Dispose();
                Database = null;
            }
        }

    }
}
