﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NPoco;
using Microsoft.AspNet.Identity;

namespace TVG.Identity
{
    public class UserLoginTable
    {
        private Database db;
        private const string tableUserLogin = "AspNetUserLogins";

        public UserLoginTable(Database db)
        {
            this.db = db;
        }

        public int Delete(IdentityUser user, UserLoginInfo login)
        {
            string sql = string.Format("DELETE FROM {0} WHERE UserId=@0 AND LoginProvider=@1 AND ProviderKey=@2", tableUserLogin);
            return db.Execute(sql, user.Id, login.LoginProvider, login.ProviderKey);
        }

        public int Delete(int userId)
        {
            string sql = string.Format("DELETE FROM {0} WHERE UserId=@0", tableUserLogin);
            return db.Execute(sql, userId);
        }
        
        public int Insert(IdentityUser user, UserLoginInfo login)
        {
            string sql = string.Format("INSERT INTO {0} (LoginProvider, ProviderKey, UserId) VALUES (@0, @1, @2)", tableUserLogin);
            return db.Execute(sql, login.LoginProvider, login.ProviderKey, user.Id);
        }

        public int FindUserIdByLogin(UserLoginInfo userLogin)
        {
            string sql = string.Format("SELECT UserId FROM {0} WHERE LoginProvider = @0 and ProviderKey = @1", tableUserLogin);
            return db.ExecuteScalar<int>(sql, userLogin.LoginProvider, userLogin.ProviderKey);
        }

        public List<UserLoginInfo> FindByUserId(int userId)
        {
            string sql = string.Format("SELECT * FROM {0} WHERE UserId=@0", tableUserLogin);
            return db.Fetch<IdentityUserLogin>(sql, userId)
                .Select(userLogin => new UserLoginInfo(userLogin.LoginProvider, userLogin.ProviderKey))
                .ToList();
        }
    }
}
