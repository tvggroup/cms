﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NPoco;
using System.Security.Claims;

namespace TVG.Identity
{
    public class UserClaimTable
    {
        private Database db;
        private const string tableUserClaim = "AspNetUserClaims";

        public UserClaimTable(Database db)
        {
            this.db = db;
        }

        public ClaimsIdentity FindByUserId(int userId)
        {
            string sql = string.Format("SELECT * FROM {0} WHERE UserId = @0", tableUserClaim);
            var userClaims = db.Fetch<IdentityUserClaim>(sql, userId);

            var claims = new ClaimsIdentity();
            var claim = new Claim("", "");
            foreach (var userClaim in userClaims)
            {
                claim = new Claim(userClaim.ClaimType, userClaim.ClaimValue);
                claims.AddClaim(claim);
            }

            return claims;
        }

        public int Delete(int userId)
        {
            string sql = string.Format("DELETE FROM {0} WHERE UserId = @0", tableUserClaim);
            return db.Execute(sql, userId);
        }

        public int Insert(Claim userClaim, int userId)
        {
            string sql = string.Format("INSERT INTO {0}(UserId, ClaimType, ClaimValue) VALUES(@0, @1, @2)", tableUserClaim);
            return db.Execute(sql, userId, userClaim.Type, userClaim.Value);
        }

        public int Delete(IdentityUser user, Claim claim)
        {
            string sql = string.Format("DELETE FROM {0} WHERE UserId=@0 AND ClaimValue=@1 AND ClaimType=@2", tableUserClaim);
            return db.Execute(sql, user.Id, claim.Value, claim.Type);
        }

    }
}
