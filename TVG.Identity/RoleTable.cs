﻿using NPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVG.Identity
{
    public class RoleTable<TRole> where TRole : IdentityRole
    {
        private Database db;
        private const string tableRole = "AspNetRoles";

        public RoleTable(Database db)
        {
            this.db = db;
        }

        public int Delete(int roleId)
        {
            string commandText = string.Format("DELETE FROM {0} WHERE Id = @0", tableRole);
            return db.Execute(commandText, roleId);
        }

        public int Insert(TRole role)
        {
            return db.Insert(tableRole, "Id", role) != null ? 1 : 0;
        }

        public IQueryable<TRole> GetRoles()
        {
            string sql = "SELECT * FROM " + tableRole;
            return db.Query<TRole>(sql).AsQueryable();
        }

        public string GetRoleName(int roleId)
        {
            string sql = string.Format("SELECT Name FROM {0} WHERE Id = @0", tableRole);
            return db.ExecuteScalar<string>(sql, roleId);
        }

        public int GetRoleId(string roleName)
        {
            string sql = string.Format("SELECT Id FROM {0} WHERE Name = @0", tableRole);
            return db.ExecuteScalar<int>(sql, roleName);
        }

        public TRole GetRoleById(int roleId)
        {
            string sql = string.Format("SELECT * FROM {0} WHERE Id = @0", tableRole);
            return db.FirstOrDefault<TRole>(sql, roleId);
        }

        public TRole GetRoleByName(string roleName)
        {
            string sql = string.Format("SELECT * FROM {0} WHERE Name = @0", tableRole);
            return db.FirstOrDefault<TRole>(sql, roleName);
        }

        public int Update(TRole role)
        {
            return db.Update(tableRole, "Id", role);
        }
    }
}
