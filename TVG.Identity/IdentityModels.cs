﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;

namespace TVG.Identity
{
    public class ApplicationDbContext : NPoco.Database
    {
        public ApplicationDbContext() : base("DefaultConnection")
        {
            
        }

        public ApplicationDbContext(string connectionName) : base(connectionName)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext("DefaultConnection");
        }
    }

    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser, int> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }
    public class ApplicationRole : IdentityRole
    {
        public ApplicationRole() : base() { }

        public ApplicationRole(string name) : this()
        {
            this.Name = name;
        }

        public ApplicationRole(string name, string description)
        : this(name)
        {
            this.Description = description;
        }
    }
    public class ApplicationUserLogin : IdentityUserLogin { }
    public class ApplicationUserClaim : IdentityUserClaim { }
    public class ApplicationUserRole : IdentityUserRole { }

    public class ApplicationUserStore :
        UserStore<ApplicationUser, ApplicationRole>, IUserStore<ApplicationUser, int>,
        IDisposable
    {
        public ApplicationUserStore(ApplicationDbContext context)
            : base(context)
        {
        }
    }


    public class ApplicationRoleStore
        : RoleStore<ApplicationRole>, IRoleStore<ApplicationRole, int>, IDisposable
    {
        public ApplicationRoleStore(ApplicationDbContext context)
            : base(context)
        {
        }
    }
}
