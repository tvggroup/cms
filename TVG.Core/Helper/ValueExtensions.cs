﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVG.Core.Helper
{
    public static class ValueExtensions
    {
        /// <summary>
        /// Extension chuyển giá trị cho trước sang kiểu nào đó, trả về default nếu null
        /// Áp dụng cho các kiểu đơn giản: int, double, string, bool, DateTime
        /// </summary>
        /// <typeparam name="T">Kiểu cần chuyển</typeparam>
        /// <param name="obj">Giá trị cần chuyển</param>
        /// <param name="defaultValue">Giá trị mặc định khi null</param>
        /// <returns></returns>
        public static T TryConvert<T>(this object obj, T defaultValue) where T : IConvertible
        {
            T result = defaultValue;
            if (obj != null)
            {
                TypeCode typeCode = defaultValue.GetTypeCode();
                switch (typeCode)
                {
                    case TypeCode.Int16:
                    case TypeCode.Int32:
                        int i = int.TryParse(obj.ToString(), out i) ? i : int.Parse(defaultValue.ToString());
                        result = (T)Convert.ChangeType(i, typeof(T));
                        break;
                    case TypeCode.Double:
                        double d = double.TryParse(obj.ToString(), out d) ? d : double.Parse(defaultValue.ToString());
                        result = (T)Convert.ChangeType(d, typeof(T));
                        break;
                    case TypeCode.Boolean:
                        bool b = bool.TryParse(obj.ToString(), out b) ? b : bool.Parse(defaultValue.ToString());
                        result = (T)Convert.ChangeType(b, typeof(T));
                        break;
                    case TypeCode.String:
                        result = (T)Convert.ChangeType(obj.ToString(), typeof(T));
                        break;
                    case TypeCode.DateTime:
                        DateTime dt = DateTime.TryParse(obj.ToString(), out dt) ? dt : DateTime.Parse(defaultValue.ToString());
                        result = (T)Convert.ChangeType(dt, typeof(T));
                        break;
                }
            }

            return result;
        }        

        //Kiểm tra đối tượng có phải kiểu mảng T (int, bool, string...) hay không
        public static bool IsArrayOf<T>(this object array)
        {
            return array.GetType() == typeof(T[]);
        }

        /// <summary>
        /// Chuyển mảng kiểu T sang dạng list, nếu mảng null thì trả về list mới
        /// </summary>
        /// <typeparam name="T">kiểu mảng</typeparam>
        /// <param name="obj">đối tượng mảng cần chuyển đổi</param>
        /// <returns></returns>
        public static List<T> ConvertToList<T>(this object obj)
        {
            if (obj != null && obj.IsArrayOf<T>())
            {
                T[] arr = obj as T[];
                return arr.ToList<T>();
            }
            return new List<T>();
        }

    }
}
