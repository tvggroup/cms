﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVG.Core.Helper
{

    public class ValueHelper
    {
        /**
           * Các hàm giúp chuyển đổi giá trị cho trước sang kiểu dữ liệu nào đó, 
           * nếu giá trị cho trước là null hoặc không đúng kiểu 
           * thì trả về giá trị mặc định của kiểu đó (vd chuyển "a" sang int --> 0)
           * 
           * Sử dụng:
           * int a = ValueHelper.TryGet("1", 0) ---> 1
           * int b = ValueHelper.TryGet("a", 0) ---> 0
           * int c = ValueHelper.TryGet(null, 0) ---> 0
           * 
           * Có thể bỏ qua tham số thứ 2, ví dụ:
           * int d = ValueHelper.TryGet("1") ---> 1
           * int e = ValueHelper.TryGet("a") ---> 0
           * 
        */

        #region Các kiểu đơn trị
        /// <summary>
        /// Chuyển giá trị sang dạng chuỗi
        /// </summary>
        /// <param name="obj">Giá trị cần chuyển</param>
        /// <param name="defaultValue">Giá trị mặc định</param>
        /// <returns></returns>
        public static string TryGet(object obj, string defaultValue)
        {
            if (obj == null)
                return defaultValue;
            else
                return obj.ToString();
        }


        /// <summary>
        /// Chuyển giá trị sang dạng integer
        /// </summary>
        /// <param name="obj">Giá trị cần chuyển</param>
        /// <param name="defaultValue">Giá trị mặc định</param>
        /// <returns></returns>
        public static int TryGet(object obj, int defaultValue)
        {
            if (obj != null)
            {
                int i = int.TryParse(obj.ToString(), out i) ? i : defaultValue;
                return i;
            }
            return defaultValue;
        }

        /// <summary>
        /// Chuyển giá trị sang dạng boolean
        /// </summary>
        /// <param name="obj">Giá trị cần chuyển</param>
        /// <param name="defaultValue">Giá trị mặc định</param>
        /// <returns></returns>
        public static bool TryGet(object obj, bool defaultValue)
        {
            if (obj != null)
            {
                bool b = bool.TryParse(obj.ToString(), out b) ? b : defaultValue;
                return b;
            }
            return defaultValue;
        }

        /// <summary>
        /// Chuyển giá trị sang dạng DateTime
        /// </summary>
        /// <param name="obj">Giá trị cần chuyển</param>
        /// <param name="defaultValue">Giá trị mặc định</param>
        /// <returns></returns>
        public static DateTime TryGet(object obj, DateTime defaultValue)
        {
            if (obj != null)
            {
                DateTime b = DateTime.TryParse(obj.ToString(), out b) ? b : defaultValue;
                return b;
            }
            return defaultValue;
        }
        
        #endregion

        #region List

        /// <summary>
        /// [Deprecated] - Sẽ bỏ, nên dùng TryGetList (tên rõ nghĩa hơn)
        /// Chuyển đổi giá trị sang dạng List<T>
        /// </summary>
        /// <typeparam name="T">Kiểu dữ liệu</typeparam>
        /// <param name="obj">Giá trị cần chuyển</param>
        /// <param name="defaultValue">Giá trị mặc định</param>
        /// <returns></returns>
        public static List<T> TryGet<T>(object obj, List<T> defaultValue)
        {
            if (obj != null)
            {
                List<T> value = obj as List<T>;
                if (value != null)
                {
                    return value;
                }
            }
            return defaultValue;
        }

        /// <summary>
        /// Chuyển đổi giá trị sang dạng List<T>
        /// </summary>
        /// <typeparam name="T">Kiểu dữ liệu</typeparam>
        /// <param name="obj">Giá trị cần chuyển</param>
        /// <param name="defaultValue">Giá trị mặc định</param>
        /// <returns></returns>
        public static List<T> TryGetList<T>(object obj, List<T> defaultValue)
        {
            if (obj != null)
            {
                List<T> value = obj as List<T>;
                if (value != null)
                {
                    return value;
                }
            }
            return defaultValue;
        }

        /// <summary>
        /// Chuyển đổi giá trị sang dạng List<T>
        /// </summary>
        /// <typeparam name="T">Kiểu dữ liệu</typeparam>
        /// <param name="obj">Giá trị cần chuyển</param>
        /// <returns></returns>
        public static List<T> TryGetList<T>(object obj)
        {
            return TryGetList<T>(obj, new List<T>());
        }
        #endregion

        #region Dictionary
        /*
         * Các hàm giúp lấy dữ liệu từ dictionary theo key, 
         * nếu dictionary là null hoặc key không tồn tại
         * thì lấy giá trị mặc định
         */ 
        
        /// <summary>
        /// Lấy dữ liệu dạng chuỗi từ dictionary
        /// </summary>
        public static string TryGet(IDictionary<string, object> dictionary, string key, string defaultValue = "")
        {
            if (dictionary == null) return defaultValue;
            return dictionary.ContainsKey(key) ? ValueHelper.TryGet(dictionary[key], defaultValue) : defaultValue;
        }

        /// <summary>
        /// Lấy dữ liệu dạng integer từ dictionary
        /// </summary>
        public static int TryGet(IDictionary<string, object> dictionary, string key, int defaultValue = 0)
        {
            if (dictionary == null) return defaultValue;
            return dictionary.ContainsKey(key) ? ValueHelper.TryGet(dictionary[key], defaultValue) : defaultValue;
        }

        /// <summary>
        /// Lấy dữ liệu dạng boolean từ dictionary
        /// </summary>
        public static bool TryGet(IDictionary<string, object> dictionary, string key, bool defaultValue = false)
        {
            if (dictionary == null) return defaultValue;
            return dictionary.ContainsKey(key) ? ValueHelper.TryGet(dictionary[key], defaultValue) : defaultValue;
        }


        /// <summary>
        /// Lấy dữ liệu dạng DateTime từ dictionary
        /// </summary>
        public static DateTime TryGet(IDictionary<string, object> dictionary, string key, DateTime defaultValue)
        {
            if (dictionary == null) return defaultValue;
            return dictionary.ContainsKey(key) ? ValueHelper.TryGet(dictionary[key], defaultValue) : defaultValue;
        }

        /// <summary>
        /// Lấy dữ liệu dạng DateTime từ dictionary, trả về ngày tháng hiện tại nếu null
        /// </summary>
        public static DateTime TryGet(IDictionary<string, object> dictionary, string key)
        {
            return TryGet(dictionary, key, DateTime.Now);
        }

        /// <summary>
        /// [Deprecated] Sẽ bỏ, nên dùng TryGetList (rõ nghĩa hơn)
        /// Lấy dữ liệu dạng List<T> từ dictionary
        /// </summary>
        public static List<T> TryGet<T>(IDictionary<string, object> dictionary, string key, List<T> defaultValue)
        {
            if (dictionary == null) return defaultValue;
            return dictionary.ContainsKey(key) ? ValueHelper.TryGet(dictionary[key], defaultValue) : defaultValue;
        }

        /// <summary>
        /// Lấy dữ liệu dạng List<T> từ dictionary
        /// </summary>
        public static List<T> TryGetList<T>(IDictionary<string, object> dictionary, string key, List<T> defaultValue)
        {
            if (dictionary == null) return defaultValue;
            return dictionary.ContainsKey(key) ? ValueHelper.TryGet(dictionary[key], defaultValue) : defaultValue;
        }


        /// <summary>
        /// Lấy dữ liệu dạng List<T> từ dictionary, trả về list mới nếu null
        /// </summary>
        public static List<T> TryGetList<T>(IDictionary<string, object> dictionary, string key)
        {
            return TryGetList<T>(dictionary, key, new List<T>());
        }
        #endregion
    }
}
