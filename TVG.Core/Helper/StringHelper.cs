﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace TVG.Core.Helper
{
	public static class StringExtensions
	{
		/// <summary>
		/// Thêm n ký tự vào bên trái chuỗi
		/// </summary>
		/// <param name="str">Chuỗi cần thêm ký tự</param>
		/// <param name="character">Ký tự thêm vào</param>
		/// <param name="number">Số ký tự thêm vào</param>
		/// <returns></returns>
		public static string AppendLeft(this String str, char character, int number)
		{
			StringBuilder sb = new StringBuilder();
			if (number > 0)
			{
				sb.Append(character, number);
			}
			sb.Append(str);
			return sb.ToString();
		}  
		

		/// <summary>
		/// Thêm giá trị chuỗi không rỗng vào list dạng chuỗi
		/// </summary>
		/// <param name="list">List chuỗi</param>
		/// <param name="value">Giá trị thêm vào</param>
		public static void AddNonEmpty(this List<string> list, string value)
		{
			if(!string.IsNullOrEmpty(value))
			{
				list.Add(value);
			}
		}

		/// <summary>
		/// Thêm số nguyên khác 0 vào list dạng chuỗi
		/// </summary>
		/// <param name="list">List chuỗi</param>
		/// <param name="value">Giá trị thêm vào</param>
		public static void AddNonEmpty(this List<string> list, int value)
		{
			if (value != 0)
			{
				list.Add(value.ToString());
			}
		}

		public static string TruncateAtWord(this string input, int length)
		{
			if (input == null || input.Length < length)
				return input;
			int iNextSpace = input.LastIndexOf(" ", length);
			return string.Format("{0}...", input.Substring(0, (iNextSpace > 0) ? iNextSpace : length).Trim());
		}

		public static bool StartsWithAny(this string source, IEnumerable<string> strings)
		{
			foreach (var valueToCheck in strings)
			{
				if (source.StartsWith(valueToCheck, StringComparison.CurrentCultureIgnoreCase))
				{
					return true;
				}
			}

			return false;
		}
	}

	public class StringHelper
	{
		/// <summary>
		/// Tạo từ khoá fulltext search
		/// </summary>
		/// <param name="keyword">Từ khoá</param>
		/// <returns></returns>
		public static string FTSKeyword(string keyword)
		{
			StringBuilder sb = new StringBuilder("");
			string sep = "";

			sb.Append("ISABOUT (\"" + keyword + "\" weight (0.8)");
			keyword = keyword.Replace('/', ' ');
			if(keyword.IndexOf(' ') > 0)
			{
				string[] kws = keyword.Split(' ');
				for (int i = 0; i < kws.Length; i++)
				{
					sep = " , ";
					if (!string.IsNullOrEmpty(kws[i]))
					{
						sb.Append(sep + "\"" + kws[i] + "*\" weight(0.1)");
					}
				}
			}
			sb.Append(")");

			//if (keyword.IndexOf(' ') > 0)
			//{
			//    string[] kws = keyword.Split(' ');                

			//    for(int i = 0; i< kws.Length; i++)
			//    {
			//        if(!string.IsNullOrEmpty(kws[i]))
			//        {
			//            //kws[i] = kws[i] + "*";
			//            //sb.Append(sep + "\"" + kws[i] + "*\"");                    
			//            sb.Append(sep + "" + kws[i] + "*");//bo dau nhay kep
			//            sep = " NEAR ";
			//        }

			//    }
			//}
			//else
			//{
			//    //sb.Append("\"" + keyword + "*\"");
			//    sb.Append("ISABOUT (" + keyword + "* weight (0.8)");//bo dau nhay kep
			//    if (keyword.IndexOf('/') > 0)
			//    {
			//        sep = " , ";
			//        subKw = keyword.Split('/');
			//        foreach (var skw in subKw)
			//        {
			//            if(!string.IsNullOrEmpty(skw))
			//            {
			//                sb.Append(sep + "" + skw + " weight(0.1)");//bo dau nhay kep
			//                sep = " , ";
			//            }

			//        }

			//        //foreach (var skw in subKw)
			//        //{
			//        //    sb.Append(sep + "" + skw + "");//bo dau nhay kep
			//        //    sep = " NEAR ";
			//        //}
			//        //sb.Append(" weight(0.2))");
			//    }
			//    sb.Append(")");
			//}
			return sb.ToString();
		}

		public static string CreateFTSTable(string keyword, string tableName, string columns)
		{
			/* cau lenh tao bang co dang nhu sau
				(SELECT [Key], Rank FROM (
					SELECT [KEY], RANK FROM CONTAINSTABLE(TenBang, (TenCot), 
					N'"tu khoa"'
					, 1000)
					UNION ALL
					-- SELECT * FROM FREETEXTTABLE(TenBang, (TenCot), N'tu khoa', 1000)
					select * from containstable(Lawdocument, (Title, Content), 
						N'ISABOUT (
							"15/2017/nd-cp" weight (1),
							"15 NEAR 2017 NEAR nd-cp" weight(0.5),
							"15*" weight(0.1),
							"2017*" weight(0.1),
							"ND-CP" weight (0.1)
						)'
						, 500)
				) AS ft1)
			 */
			keyword = keyword.Trim();
			if (string.IsNullOrEmpty(keyword)) return "";
			keyword = StringHelper.CompactWhitespaces(keyword);
			keyword = keyword.Replace("\"", "").Replace("'", "");
			string result = "";
			StringBuilder sb = new StringBuilder();
			sb.Append("(");
			sb.Append("SELECT [Key], MAX([Rank]) AS Rank FROM");
			sb.Append("(");
			sb.Append(@"SELECT [KEY], ([RANK]*2)/(SUM([RANK]) OVER( PARTITION BY 1)/ CAST(COUNT([RANK]) OVER( PARTITION BY 1) AS FLOAT)) AS [RANK] ");
			sb.Append(string.Format(" FROM CONTAINSTABLE({0}, ({1}), N'\"{2}\"', 1000)", tableName, columns, keyword));
			sb.Append(" UNION ALL ");
			sb.Append(@"SELECT [KEY], ([RANK]*1)/(SUM([RANK]) OVER( PARTITION BY 1)/ CAST(COUNT([RANK]) OVER( PARTITION BY 1) AS FLOAT)) AS [RANK] ");
			sb.Append(string.Format(" FROM FREETEXTTABLE({0}, ({1}), N'{2}', 1000)", tableName, columns, keyword));
			
			sb.Append(") as ft1 GROUP BY [Key]");
			sb.Append(") AS ft");
			result = sb.ToString();
			return result;
		}


		/// <summary>
		/// Tạo slug từ chuỗi
		/// </summary>
		/// <param name="text">Chuỗi cần tạo slug</param>
		/// <returns></returns>
		public static string Slugify(string text)
		{
			text = text.Replace('"', ' ').Replace('“', ' ').Replace('”', ' ').Replace('\'', ' ');
			text = text.Trim();
			for (int i = 33; i < 48; i++)
			{
				text = text.Replace(((char)i).ToString(), "");
			}

			for (int i = 58; i < 65; i++)
			{
				text = text.Replace(((char)i).ToString(), "");
			}

			for (int i = 91; i < 97; i++)
			{
				text = text.Replace(((char)i).ToString(), "");
			}

			for (int i = 123; i < 127; i++)
			{
				text = text.Replace(((char)i).ToString(), "");
			}

			text = text.Replace(" ", "-");

			Regex regex = new Regex(@"\p{IsCombiningDiacriticalMarks}+");

			string strFormD = text.Normalize(System.Text.NormalizationForm.FormD);

			return regex.Replace(strFormD, String.Empty).Replace('\u0111', 'd').Replace('\u0110', 'D');
		}


		public static string StripHtml(string html)
		{
			if (String.IsNullOrEmpty(html)) return "";
			HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
			doc.LoadHtml(html);
			return  CompactWhitespaces(WebUtility.HtmlDecode(doc.DocumentNode.InnerText));
		}

		public static String CompactWhitespaces(String s)
		{
			s = s.Trim();
			if (string.IsNullOrEmpty(s)) return "";
			StringBuilder sb = new StringBuilder(s);
			
			int start = 0;
			int end = sb.Length - 1;

			// compact string
			int dest = 0;
			bool previousIsWhitespace = false;

			for (int i = start; i <= end; i++)
			{
				if (Char.IsWhiteSpace(sb[i]))
				{
					if (!previousIsWhitespace)
					{
						previousIsWhitespace = true;
						sb[dest] = ' ';
						dest++;
					}
				}
				else
				{
					previousIsWhitespace = false;
					sb[dest] = sb[i];
					dest++;
				}
			}

			sb.Length = dest;

			return sb.ToString();
		}

		public static string ExtractByKeyword(string source, string keyword, int numberOfCharacters)
		{
			source = source.Trim();
			if (source.Length <= numberOfCharacters)
				return source;

			var result = "";
			var start = 0;
			var end = 0;
			var max = numberOfCharacters;
			var sourceText = source.ToLower();
			CultureInfo culture = new CultureInfo("en-US");//khong hieu sao en-US thi nhan chu tieng Viet khong dau, con vn thi khong
			var fullKwIdx = culture.CompareInfo.IndexOf(sourceText, keyword, CompareOptions.IgnoreNonSpace);

			if (fullKwIdx > -1)
			{
				start = (fullKwIdx - 20) >= 0 ? fullKwIdx - 20 : 0;
				while (!char.IsWhiteSpace(sourceText[start]) && start > 0)
				{
					start--;
				}

				if (keyword.Length >= max)
				{
					end = fullKwIdx + 20;
				}
				else
				{
					end = start + max;
				}

				while (!char.IsWhiteSpace(sourceText[end]) && end < sourceText.Length - 1)
				{
					end++;
				}
			}
			else
			{
				var subKw = "";
				var arrayKeyword = keyword.Split(' ').
					  Select(x => x.Trim()).
					  Where(x => !string.IsNullOrEmpty(x)).ToList();
				var arraySubkw = new List<string>();
				List<int> idxWord = new List<int>();
				int idx = -1;
				int idxSubKw = -1;

				foreach (var s in arrayKeyword)
				{
					idx = culture.CompareInfo.IndexOf(sourceText, s, CompareOptions.IgnoreNonSpace);
					if (idx > -1)
					{
						idxWord.Add(idx);
					}
					if (s.Length > 1 && s.IndexOf('/') > 0)
					{
						subKw = s.Replace('/', ' ');
						arraySubkw = subKw.Split(' ').
							Select(x => x.Trim()).
							Where(x => !string.IsNullOrEmpty(x)).ToList();
						foreach (var s2 in arraySubkw)
						{
							idxSubKw = culture.CompareInfo.IndexOf(sourceText, s2, CompareOptions.IgnoreNonSpace);
							if (idxSubKw > -1)
							{
								idxWord.Add(idxSubKw);
							}
						}
					}
				}

				if (fullKwIdx > -1)
				{
					idxWord.Add(fullKwIdx);
				}

				max = numberOfCharacters - 20;
				var q = from p1 in idxWord
						from p2 in idxWord
						let distance = Math.Abs(p1 - p2)
						where !p1.Equals(p2)
						select new { Point1 = p1, Point2 = p2, Distance = distance };

				var maximum = q.Where(x => x.Distance < max).OrderByDescending(r => r.Distance).FirstOrDefault();
				if (maximum == null)
				{
					if (fullKwIdx > -1)
					{
						maximum = new { Point1 = fullKwIdx, Point2 = fullKwIdx, Distance = 0 };
					}
					else
					{
						maximum = new { Point1 = idxWord.Min(), Point2 = idxWord.Min(), Distance = 0 };
					}
				}

				start = Math.Min(maximum.Point1, maximum.Point2);
				if (maximum.Distance < max)
				{
					start = (start - max) > 0 ? (start - max) : 0;
					while (!char.IsWhiteSpace(sourceText[start]) && start > 0)
					{
						start--;
					}
				}

				end = Math.Max(maximum.Point1, maximum.Point2);
				if (fullKwIdx == end)
				{
					end = end + keyword.Length;
				}
				end += 20;
				if (end > sourceText.Length - 1)
				{
					end = sourceText.Length - 1;
				}
				while (!char.IsWhiteSpace(sourceText[end]) && end < sourceText.Length - 1)
				{
					end++;
				}
			}


			result = "[...] " + source.Substring(start, end - start + 1) + " [...]";
			return result;
		}
	}
}
