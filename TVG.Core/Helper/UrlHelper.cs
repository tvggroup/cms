﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;

namespace TVG.Core.Helper
{
    public class MyUrlHelper
    {
        public static string SiteUrl
        {
            get { return HttpContext.Current.Request.Url.Scheme + System.Uri.SchemeDelimiter + HttpContext.Current.Request.Url.Host; }
        }
        public static string RemoveQueryStringByKey(string url, string key)
        {
            var indexOfQuestionMark = url.IndexOf("?");
            if (indexOfQuestionMark == -1)
            {
                return url;
            }

            var result = url.Substring(0, indexOfQuestionMark);
            var queryStrings = url.Substring(indexOfQuestionMark + 1);
            var queryStringParts = queryStrings.Split(new[] { '&' });
            var isFirstAdded = false;

            for (int index = 0; index < queryStringParts.Length; index++)
            {
                var keyValue = queryStringParts[index].Split(new char[] { '=' });
                if (keyValue[0] == key)
                {
                    continue;
                }

                if (!isFirstAdded)
                {
                    result += "?";
                    isFirstAdded = true;
                }
                else
                {
                    result += "&";
                }

                result += queryStringParts[index];
            }

            return result;
        }   
        
        public static Dictionary<string, object> QueryStringToDictionary(string queryString)
        {
            var parsed = HttpUtility.ParseQueryString(queryString);
            Dictionary<string, object> querystringDic = parsed.AllKeys.ToDictionary(k => k, k => (object)parsed[k]);
            return querystringDic;
        }
    }
}
