﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVG.Core.Helper
{
    public static class DateExtensions
    {
        public static string ToShortDateStringVN(this DateTime date)
        {
            return date.ToString("dd/MM/yyyy");
        }

        public static string ToLongDateStringVN(this DateTime date)
        {
            return date.ToString("dd/MM/yyyy HH:mm:ss");
        }
    }
}
