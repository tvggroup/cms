﻿using Newtonsoft.Json;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Runtime.Caching;
using System.Text;
using System.Threading.Tasks;

namespace TVG.Core.Caching
{
    public class RedisCacheManager : ICacheManager
    {
        private readonly ConnectionMultiplexer redisConnections;

        public int DatabaseNumber { get; set; }
        
        private static Lazy<ConfigurationOptions> configOptions = new Lazy<ConfigurationOptions>(() =>
        {
            var configOptions = ConfigurationOptions.Parse(ConfigurationManager.ConnectionStrings["RedisCache"].ConnectionString);
            configOptions.ConnectTimeout = 100000;
            configOptions.SyncTimeout = 100000;
            configOptions.AbortOnConnectFail = false;
            configOptions.ClientName = "RedisConnection";
            return configOptions;
        });

        private static Lazy<ConnectionMultiplexer> conn = new Lazy<ConnectionMultiplexer>(
                () => ConnectionMultiplexer.Connect(configOptions.Value));

        private static ConnectionMultiplexer Connection
        {
            get
            {
                return conn.Value;
            }
        }

        public RedisCacheManager()
        {
            var connectionString = ConfigurationManager.ConnectionStrings["RedisCache"].ConnectionString;
            //this.redisConnections = ConnectionMultiplexer.Connect(connectionString);
            this.redisConnections = Connection;
        }

        public RedisCacheManager(string connectionName)
        {
            var connectionString = ConfigurationManager.ConnectionStrings[connectionName].ConnectionString;
            this.redisConnections = Connection;
        }

        public long Count
        {
            get
            {
                var endpoints = redisConnections.GetEndPoints();
                var server = redisConnections.GetServer(endpoints.First());
                var keys = server.Keys();
                return keys.Count();
            }
        }

        public void AddOrUpdate<T>(string key, T value) 
        {
            this.AddOrUpdate(key, value, null, null);
        }

        public void AddOrUpdate<T>(string key, T value, TimeSpan slidingExpiration) 
        {
            this.AddOrUpdate(key, value, null, slidingExpiration);
        }

        public void AddOrUpdate<T>(string key, T value, DateTimeOffset absoluteExpiration) 
        {
            this.AddOrUpdate(key, value, absoluteExpiration, null);
        }

        public void Clear()
        {
            var endpoints = redisConnections.GetEndPoints();
            var server = redisConnections.GetServer(endpoints.First());
            server.FlushAllDatabases();
        }

        public bool Contains(string key)
        {
            var db = redisConnections.GetDatabase(DatabaseNumber);
            return db.KeyExists(key);
        }

        public T Get<T>(string key) 
        {
            var db = redisConnections.GetDatabase(DatabaseNumber);
            var serializedObject = db.StringGet(key);

            T value = JsonConvert.DeserializeObject<T>(serializedObject);
            return value;
        }

        #region GetOrAdd
        public T GetOrAdd<T>(string key, Func<T> value) 
        {
            return this.GetOrAdd(key, value, null, null);
        }

        public T GetOrAdd<T>(string key, Func<T> value, TimeSpan slidingExpiration) 
        {
            return this.GetOrAdd(key, value, null, slidingExpiration);
        }

        public T GetOrAdd<T>(string key, Func<T> value, DateTimeOffset absoluteExpiration) 
        {
            return this.GetOrAdd(key, value, absoluteExpiration, null);
        }

        public Task<T> GetOrAddAsync<T>(string key, Func<Task<T>> value) 
        {
            return this.GetOrAddAsync(key, value, null, null);
        }

        public Task<T> GetOrAddAsync<T>(string key, Func<Task<T>> value, TimeSpan slidingExpiration) 
        {
            return this.GetOrAddAsync(key, value, null, slidingExpiration);
        }

        public Task<T> GetOrAddAsync<T>(string key, Func<Task<T>> value, DateTimeOffset absoluteExpiration) 
        {
            return this.GetOrAddAsync(key, value, absoluteExpiration, null);
        }
        #endregion

        #region GetOrAdd with group
        public T GetOrAdd<T>(string group, string key, Func<T> value) 
        {
            return this.GetOrAdd(group, key, value, null, null);
        }

        public T GetOrAdd<T>(string group, string key, Func<T> value, TimeSpan slidingExpiration) 
        {
            return this.GetOrAdd(group, key, value, null, slidingExpiration);
        }

        public T GetOrAdd<T>(string group, string key, Func<T> value, DateTimeOffset absoluteExpiration) 
        {
            return this.GetOrAdd(group, key, value, absoluteExpiration, null);
        }

        public Task<T> GetOrAddAsync<T>(string group, string key, Func<Task<T>> value) 
        {
            return this.GetOrAddAsync(group, key, value, null, null);
        }

        public Task<T> GetOrAddAsync<T>(string group, string key, Func<Task<T>> value, TimeSpan slidingExpiration) 
        {
            return this.GetOrAddAsync(group, key, value, null, slidingExpiration);
        }

        public Task<T> GetOrAddAsync<T>(string group, string key, Func<Task<T>> value, DateTimeOffset absoluteExpiration) 
        {
            return this.GetOrAddAsync(group, key, value, absoluteExpiration, null);
        }
        #endregion

        public void Remove(string key)
        {
            var db = redisConnections.GetDatabase(DatabaseNumber);
            db.KeyDelete(key);
        }

        public void RemoveByPattern(string pattern)
        {
            var endpoints = redisConnections.GetEndPoints();
            var server = redisConnections.GetServer(endpoints.First());
            var keys = server.Keys(pattern: pattern + "*", database: DatabaseNumber);
            var db = redisConnections.GetDatabase(DatabaseNumber);
            db.KeyDelete(keys.ToArray());
        }

        public void RemoveByGroup(string group)
        {
            var db = redisConnections.GetDatabase(DatabaseNumber);
            var items = db.SetPop(group);
            while (items.HasValue)
            {
                db.KeyDelete(items.ToString());
                items = db.SetPop(group);
            }
            db.KeyDelete(group);
        }

        #region Private
        private T GetOrAdd<T>(string key, Func<T> getValue, DateTimeOffset? absoluteExpiration, TimeSpan? slidingExpiration) 
        {
            var db = redisConnections.GetDatabase(DatabaseNumber);
            var serializedObject = db.StringGet(key);
            T value = default(T);

            if (!serializedObject.IsNullOrEmpty)
            {
                value = JsonConvert.DeserializeObject<T>(serializedObject);
            }
            else
            {
                value = getValue();
                this.AddOrUpdate(key, value, absoluteExpiration, slidingExpiration);
            }

            return value;
        }

        private async Task<T> GetOrAddAsync<T>(string key, Func<Task<T>> getValue, DateTimeOffset? absoluteExpiration, TimeSpan? slidingExpiration) 
        {
            var db = redisConnections.GetDatabase(DatabaseNumber);
            var serializedObject = db.StringGet(key);

            T value = default(T);

            if (!serializedObject.IsNullOrEmpty)
            {
                value = JsonConvert.DeserializeObject<T>(serializedObject);
            }
            else
            {
                value = await getValue();
                this.AddOrUpdate(key, value, absoluteExpiration, slidingExpiration);
            }

            return value;
        }

        private T GetOrAdd<T>(string group, string key, Func<T> getValue, DateTimeOffset? absoluteExpiration, TimeSpan? slidingExpiration) 
        {
            var db = redisConnections.GetDatabase(DatabaseNumber);
            var serializedObject = db.StringGet(key);
            T value = default(T);

            if (!serializedObject.IsNullOrEmpty)
            {
                value = JsonConvert.DeserializeObject<T>(serializedObject);
            }
            else
            {
                value = getValue();
                this.AddOrUpdate(group, key, value, absoluteExpiration, slidingExpiration);
            }

            return value;
        }

        private async Task<T> GetOrAddAsync<T>(string group, string key, Func<Task<T>> getValue, DateTimeOffset? absoluteExpiration, TimeSpan? slidingExpiration) 
        {
            var db = redisConnections.GetDatabase(DatabaseNumber);
            var serializedObject = db.StringGet(key);

            T value = default(T);

            if (!serializedObject.IsNullOrEmpty)
            {
                value = JsonConvert.DeserializeObject<T>(serializedObject);
            }
            else
            {
                value = await getValue();
                this.AddOrUpdate(group, key, value, absoluteExpiration, slidingExpiration);
            }

            return value;
        }

        private void AddOrUpdate<T>(string key, T value, DateTimeOffset? absoluteExpiration, TimeSpan? slidingExpiration) 
        {
            if (value == null) return;

            var expiration = slidingExpiration;

            if (absoluteExpiration.HasValue)
            {
                expiration = absoluteExpiration.Value.Subtract(DateTimeOffset.Now);
            }

            var db = redisConnections.GetDatabase(DatabaseNumber);
            var serializedObject = JsonConvert.SerializeObject(value);

            db.StringSet(key, serializedObject, slidingExpiration);
        }

        private void AddOrUpdate<T>(string group, string key, T value, DateTimeOffset? absoluteExpiration, TimeSpan? slidingExpiration) 
        {
            if (value == null) return;

            var expiration = slidingExpiration;

            if (absoluteExpiration.HasValue)
            {
                expiration = absoluteExpiration.Value.Subtract(DateTimeOffset.Now);
            }

            var db = redisConnections.GetDatabase(DatabaseNumber);
            var serializedObject = JsonConvert.SerializeObject(value);
            db.StringSet(key, serializedObject, slidingExpiration);
            db.SetAdd(group, key);
            db.KeyExpire(group, slidingExpiration);
        }
        #endregion 
        
        public void AddKeyToGroup(string key, string group, TimeSpan? slidingExpiration)
        {
            var db = redisConnections.GetDatabase(DatabaseNumber);
            db.SetAdd(group, key);
            db.KeyExpire(group, slidingExpiration);
        }
    }
}
