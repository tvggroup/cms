﻿namespace TVG.Core.Caching
{
    using Helper;
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Linq;
    using System.Runtime.Caching;
    using System.Text.RegularExpressions;
    using System.Threading.Tasks;

    /// <summary>
    /// A cached collection of key value pairs. This can be used to cache objects internally on the server.
    /// </summary>
    public class MemoryCacheManager : ICacheManager
    {
        private readonly MemoryCache memoryCache;

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="MemoryCacheManager"/> class using the default <see cref="MemoryCache"/>.
        /// </summary>
        public MemoryCacheManager()
        {
            this.memoryCache = MemoryCache.Default;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MemoryCacheManager"/> class.
        /// </summary>
        /// <param name="memoryCache">The memory cache.</param>
        public MemoryCacheManager(MemoryCache memoryCache)
        {
            this.memoryCache = memoryCache;
        }

        #endregion

        #region Public Properties

        public int DatabaseNumber { get; set; }

        /// <summary>
        /// Gets the total number of cache entries in the cache.
        /// </summary>
        public long Count
        {
            get { return this.memoryCache.GetCount(); }
        }

        #endregion

        #region Public Methods

        public void AddOrUpdate<T>(string key, T value) 
        {
            this.AddOrUpdate(key, value, null, null);
        }

        public void AddOrUpdate<T>(string key, T value, DateTimeOffset absoluteExpiration) 
        {
            this.AddOrUpdate(key, value, absoluteExpiration, null);
        }

        public void AddOrUpdate<T>(string key, T value, TimeSpan slidingExpiration) 
        {
            this.AddOrUpdate(key, value, null, slidingExpiration);
        }

        public void Clear()
        {
            foreach (KeyValuePair<string, object> item in this.memoryCache.ToArray())
            {
                this.memoryCache.Remove(item.Key);
            }
        }

        public bool Contains(string key)
        {
            return this.memoryCache.Contains(key);
        }

        public T Get<T>(string key) 
        {
            return (T)this.memoryCache.Get(key);
        }

        public T GetOrAdd<T>(string key, Func<T> value) 
        {
            return this.GetOrAdd(key, value, null, null);
        }

        public T GetOrAdd<T>(string key, Func<T> value, DateTimeOffset absoluteExpiration) 
        {
            return this.GetOrAdd(key, value, absoluteExpiration, null);
        }

        public T GetOrAdd<T>(string key, Func<T> value, TimeSpan slidingExpiration) 
        {
            return this.GetOrAdd(key, value, null, slidingExpiration);
        }

        public Task<T> GetOrAddAsync<T>(string key, Func<Task<T>> value) 
        {
            return this.GetOrAddAsync(key, value, null, null);
        }

        public Task<T> GetOrAddAsync<T>(string key, Func<Task<T>> value, DateTimeOffset absoluteExpiration) 
        {
            return this.GetOrAddAsync(key, value, absoluteExpiration, null);
        }

        public Task<T> GetOrAddAsync<T>(string key, Func<Task<T>> value, TimeSpan slidingExpiration) 
        {
            return this.GetOrAddAsync(key, value, null, slidingExpiration);
        }

        /// <summary>
        /// Removes a cache entry from the cache with the specified key.
        /// </summary>
        /// <param name="key">A unique identifier for the cache entry to remove.</param>
        /// <exception cref="System.ArgumentNullException">key is null.</exception>
        public void Remove(string key)
        {
            this.memoryCache.Remove(key);
        }

        /// <summary>
        /// Removes items by pattern
        /// </summary>
        /// <param name="pattern">pattern</param>
        public void RemoveByPattern(string pattern)
        {
            var regex = new Regex(pattern, RegexOptions.Singleline | RegexOptions.Compiled | RegexOptions.IgnoreCase);
            var keysToRemove = new List<String>();

            foreach (var item in this.memoryCache)
                if (regex.IsMatch(item.Key))
                    keysToRemove.Add(item.Key);

            foreach (string key in keysToRemove)
            {
                Remove(key);
            }
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Gets an entry from the cache with the specified key, or if the entry does not exist, inserts a
        /// cache entry into the cache by using a key, a value, a type of expiration and returns it.
        /// </summary>
        /// <typeparam name="T">The type of the cache entry value.</typeparam>
        /// <param name="key">A unique identifier for the cache entry to insert.</param>
        /// <param name="getValue">A function that gets the data for the cache entry.</param>
        /// <param name="absoluteExpiration">The absolute expiration time, after which the cache entry will expire.</param>
        /// <param name="slidingExpiration">The sliding expiration time, after which the cache entry will expire.</param>
        /// <returns>A reference to the cache entry that is identified by key.</returns>
        private T GetOrAdd<T>(string key, Func<T> getValue, DateTimeOffset? absoluteExpiration, TimeSpan? slidingExpiration) 
        {
            T value = this.Get<T>(key);

            if (value == null)
            {
                value = getValue();
                this.AddOrUpdate(key, value, absoluteExpiration, slidingExpiration);
            }

            return value;
        }

        /// <summary>
        /// Gets an entry from the cache with the specified key, or if the entry does not exist, inserts a
        /// cache entry into the cache by using a key, a value, a type of expiration and returns it.
        /// </summary>
        /// <typeparam name="T">The type of the cache entry value.</typeparam>
        /// <param name="key">A unique identifier for the cache entry to insert.</param>
        /// <param name="getValue">A function that asynchronously gets the data for the cache entry.</param>
        /// <param name="absoluteExpiration">The absolute expiration time, after which the cache entry will expire.</param>
        /// <param name="slidingExpiration">The sliding expiration time, after which the cache entry will expire.</param>
        /// <returns>A reference to the cache entry that is identified by key.</returns>
        private async Task<T> GetOrAddAsync<T>(string key, Func<Task<T>> getValue, DateTimeOffset? absoluteExpiration, TimeSpan? slidingExpiration) 
        {
            T value = this.Get<T>(key);

            if (value == null)
            {
                value = await getValue();
                this.AddOrUpdate(key, value, absoluteExpiration, slidingExpiration);
            }

            return value;
        }

        /// <summary>
        /// Inserts a cache entry into the cache by using a key, a value and an expiration type.
        /// </summary>
        /// <typeparam name="T">The type of the cache entry value.</typeparam>
        /// <param name="key">A unique identifier for the cache entry to insert.</param>
        /// <param name="value">The data for the cache entry.</param>
        /// <param name="absoluteExpiration">The absolute expiration time, after which the cache entry will expire.</param>
        /// <param name="slidingExpiration">The sliding expiration time, after which the cache entry will expire.</param>
        private void AddOrUpdate<T>(string key, T value, DateTimeOffset? absoluteExpiration, TimeSpan? slidingExpiration) 
        {
            if (value == null) return;
            CacheItemPolicy cacheItemPolicy = new CacheItemPolicy();

            if (absoluteExpiration.HasValue)
            {
                cacheItemPolicy.AbsoluteExpiration = absoluteExpiration.Value;
            }

            if (slidingExpiration.HasValue)
            {
                cacheItemPolicy.SlidingExpiration = slidingExpiration.Value;
            }

            //if (afterItemRemoved != null)
            //{
            //    cacheItemPolicy.RemovedCallback = x => afterItemRemoved(
            //        x.CacheItem.Key,
            //        (T)x.CacheItem.Value);
            //}

            //if (beforeItemRemoved != null)
            //{
            //    cacheItemPolicy.UpdateCallback = x => beforeItemRemoved(
            //        x.UpdatedCacheItem.Key,
            //        (T)x.UpdatedCacheItem.Value);
            //}

            this.memoryCache.Set(key, value, cacheItemPolicy);
        }

        public T GetOrAdd<T>(string group, string key, Func<T> value) 
        {
            throw new NotImplementedException();
        }

        public T GetOrAdd<T>(string group, string key, Func<T> value, DateTimeOffset absoluteExpiration) 
        {
            throw new NotImplementedException();
        }

        public T GetOrAdd<T>(string group, string key, Func<T> value, TimeSpan slidingExpiration) 
        {
            throw new NotImplementedException();
        }

        public Task<T> GetOrAddAsync<T>(string group, string key, Func<Task<T>> value) 
        {
            throw new NotImplementedException();
        }

        public Task<T> GetOrAddAsync<T>(string group, string key, Func<Task<T>> value, DateTimeOffset absoluteExpiration) 
        {
            throw new NotImplementedException();
        }

        public Task<T> GetOrAddAsync<T>(string group, string key, Func<Task<T>> value, TimeSpan slidingExpiration) 
        {
            throw new NotImplementedException();
        }

        public void RemoveByGroup(string group)
        {

        }

        #endregion

        public void AddKeyToGroup(string key, string group, TimeSpan? slidingExpiration)
        {
            return;
        }
    }
}