﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVG.Core.Caching
{
    public class DummyCacheManager : ICacheManager
    {
        public long Count { get { return 0; } }

        public int DatabaseNumber { get; set; }

        public void AddOrUpdate<T>(string key, T value) where T : class
        {
            this.AddOrUpdate(key, value, null, null);
        }

        public void AddOrUpdate<T>(string key, T value, TimeSpan slidingExpiration) where T : class
        {
            this.AddOrUpdate(key, value, null, slidingExpiration);
        }

        public void AddOrUpdate<T>(string key, T value, DateTimeOffset absoluteExpiration) where T : class
        {
            this.AddOrUpdate(key, value, absoluteExpiration, null);
        }

        public void Clear()
        {
            return;
        }

        public bool Contains(string key)
        {
            return false;
        }

        public T Get<T>(string key) where T : class
        {
            return default(T);
        }

        public T GetOrAdd<T>(string key, Func<T> value) where T : class
        {
            return this.GetOrAdd(key, value, null, null);
        }

        public T GetOrAdd<T>(string key, Func<T> value, TimeSpan slidingExpiration) where T : class
        {
            return this.GetOrAdd(key, value, null, null);
        }

        public T GetOrAdd<T>(string group, string key, Func<T> value) where T : class
        {
            return this.GetOrAdd(key, value, null, null);
        }

        public T GetOrAdd<T>(string key, Func<T> value, DateTimeOffset absoluteExpiration) where T : class
        {
            return this.GetOrAdd(key, value, null, null);
        }

        public T GetOrAdd<T>(string group, string key, Func<T> value, TimeSpan slidingExpiration) where T : class
        {
            return this.GetOrAdd(key, value, null, null);
        }

        public T GetOrAdd<T>(string group, string key, Func<T> value, DateTimeOffset absoluteExpiration) where T : class
        {
            return this.GetOrAdd(key, value, null, null);
        }

        public Task<T> GetOrAddAsync<T>(string key, Func<Task<T>> value) where T : class
        {
            return this.GetOrAddAsync(key, value, null, null);
        }

        public Task<T> GetOrAddAsync<T>(string group, string key, Func<Task<T>> value) where T : class
        {
            return this.GetOrAddAsync(key, value, null, null);
        }

        public Task<T> GetOrAddAsync<T>(string key, Func<Task<T>> value, TimeSpan slidingExpiration) where T : class
        {
            return this.GetOrAddAsync(key, value, null, null);
        }

        public Task<T> GetOrAddAsync<T>(string key, Func<Task<T>> value, DateTimeOffset absoluteExpiration) where T : class
        {
            return this.GetOrAddAsync(key, value, null, null);
        }

        public Task<T> GetOrAddAsync<T>(string group, string key, Func<Task<T>> value, TimeSpan slidingExpiration) where T : class
        {
            return this.GetOrAddAsync(key, value, null, null);
        }

        public Task<T> GetOrAddAsync<T>(string group, string key, Func<Task<T>> value, DateTimeOffset absoluteExpiration) where T : class
        {
            return this.GetOrAddAsync(key, value, null, null);
        }

        public void Remove(string key)
        {
            return;
        }

        public void RemoveByGroup(string group)
        {
            return;
        }

        public void RemoveByPattern(string pattern)
        {
            return;
        }

        private T GetOrAdd<T>(string key, Func<T> getValue, DateTimeOffset? absoluteExpiration, TimeSpan? slidingExpiration) where T : class
        {
            return getValue();
        }

        private async Task<T> GetOrAddAsync<T>(string key, Func<Task<T>> getValue, DateTimeOffset? absoluteExpiration, TimeSpan? slidingExpiration) where T : class
        {
            return await getValue();
        }

        private void AddOrUpdate<T>(string key, T value, DateTimeOffset? absoluteExpiration, TimeSpan? slidingExpiration) where T : class
        {
            return;
        }
    }
}
