﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TVG.Core.Helper;

namespace TVG.Core.Caching
{
    public class CacheHelper
    {
        public static TimeSpan CacheDurationShort
        {
            get {
                var minutes = ConfigurationManager.AppSettings["CacheDurationShort"].TryConvert<int>(5);
                return TimeSpan.FromMinutes(minutes);
            }
        }

        public static TimeSpan CacheDurationMedium
        {
            get
            {
                var minutes = ConfigurationManager.AppSettings["CacheDurationMedium"].TryConvert<int>(30);
                return TimeSpan.FromMinutes(minutes);
            }
        }

        public static TimeSpan CacheDurationLong
        {
            get
            {
                var minutes = ConfigurationManager.AppSettings["CacheDurationLong"].TryConvert<int>(60);
                return TimeSpan.FromMinutes(minutes);
            }
        }

        /// <summary>
        /// Đặt tên cache
        /// </summary>
        /// <param name="prefix">Tiền tố để phân biệt module</param>
        /// <param name="values">Các giá trị gắn kèm để phân biệt cache</param>
        /// <returns></returns>
        public static string SetCacheName(string prefix, params object[] values)
        {
            var cacheName = new StringBuilder();
            cacheName.Append(prefix);
            if(values.Length > 0)
            {
                for(int i = 0; i < values.Length; i++)
                {
                    cacheName.Append(values[i].ToString());
                    if (i < values.Length - 1)
                    {
                        cacheName.Append(";");
                    }
                }
            }
            return cacheName.ToString();
        }
    }
}
