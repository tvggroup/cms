﻿namespace TVG.Core.Caching
{
    using System;
    using System.Threading.Tasks;

    /// <summary>
    /// A cached collection of key value pairs.
    /// </summary>
    public interface ICacheManager
    {
        /// <summary>
        /// Get the number (id) of database (Redis)
        /// </summary>
        int DatabaseNumber { get; set; }

        /// <summary>
        /// Gets the total number of cache entries in the cache.
        /// </summary>
        long Count { get; }

        /// <summary>
        /// Inserts a cache entry into the cache by using a key, a value and no expiration.
        /// </summary>
        /// <typeparam name="T">The type of the cache entry value.</typeparam>
        /// <param name="key">A unique identifier for the cache entry to insert.</param>
        /// <param name="value">The data for the cache entry.</param>
        void AddOrUpdate<T>(string key, T value);

        /// <summary>
        /// Inserts a cache entry into the cache by using a key, a value and absolute expiration.
        /// </summary>
        /// <typeparam name="T">The type of the cache entry value.</typeparam>
        /// <param name="key">A unique identifier for the cache entry to insert.</param>
        /// <param name="value">The data for the cache entry.</param>
        /// <param name="absoluteExpiration">The absolute expiration time, after which the cache entry will expire.</param>
        void AddOrUpdate<T>(string key, T value, DateTimeOffset absoluteExpiration);

        /// <summary>
        /// Inserts a cache entry into the cache by using a key, a value and sliding expiration.
        /// </summary>
        /// <typeparam name="T">The type of the cache entry value.</typeparam>
        /// <param name="key">A unique identifier for the cache entry to insert.</param>
        /// <param name="value">The data for the cache entry.</param>
        /// <param name="slidingExpiration">The sliding expiration time, after which the cache entry will expire.</param>
        void AddOrUpdate<T>(string key, T value, TimeSpan slidingExpiration);

        /// <summary>
        /// Gets an entry from the cache with the specified key.
        /// </summary>
        /// <typeparam name="T">The type of the cache entry to get.</typeparam>
        /// <param name="key">A unique identifier for the cache entry to get.</param>
        /// <returns>A reference to the cache entry that is identified by key, if the entry exists; otherwise, <c>null</c>.</returns>
        /// <exception cref="System.ArgumentNullException">key is null.</exception>
        T Get<T>(string key);

        /// <summary>
        /// Gets an entry from the cache with the specified key, or if the entry does not exist, inserts a
        /// cache entry into the cache by using a key, a value and no expiration and returns it.
        /// </summary>
        /// <typeparam name="T">The type of the cache entry value.</typeparam>
        /// <param name="key">A unique identifier for the cache entry to insert.</param>
        /// <param name="value">The data for the cache entry.</param>
        /// <returns>A reference to the cache entry that is identified by key.</returns>
        T GetOrAdd<T>(string key, Func<T> value);

        /// <summary>
        /// Gets an entry from the cache with the specified key, or if the entry does not exist, inserts a
        /// cache entry into the cache by using a key, a value and absolute expiration and returns it.
        /// </summary>
        /// <typeparam name="T">The type of the cache entry value.</typeparam>
        /// <param name="key">A unique identifier for the cache entry to insert.</param>
        /// <param name="value">The data for the cache entry.</param>
        /// <param name="absoluteExpiration">The absolute expiration time, after which the cache entry will expire.</param>
        /// <returns>A reference to the cache entry that is identified by key.</returns>
        T GetOrAdd<T>(string key, Func<T> value, DateTimeOffset absoluteExpiration);

        /// <summary>
        /// Gets an entry from the cache with the specified key, or if the entry does not exist, inserts a
        /// cache entry into the cache by using a key, a value and sliding expiration and returns it.
        /// </summary>
        /// <typeparam name="T">The type of the cache entry value.</typeparam>
        /// <param name="key">A unique identifier for the cache entry to insert.</param>
        /// <param name="value">The data for the cache entry.</param>
        /// <param name="slidingExpiration">The sliding expiration time, after which the cache entry will expire.</param>
        /// <returns>A reference to the cache entry that is identified by key.</returns>
        T GetOrAdd<T>(string key,  Func<T> value, TimeSpan slidingExpiration);

        /// <summary>
        /// Gets an entry from the cache with the specified key, or if the entry does not exist, inserts a
        /// cache entry into the cache by using a key, a value and no expiration and returns it.
        /// </summary>
        /// <typeparam name="T">The type of the cache entry value.</typeparam>
        /// <param name="key">A unique identifier for the cache entry to insert.</param>
        /// <param name="value">A function that asynchronously gets the data for the cache entry.</param>
        /// <returns>A reference to the cache entry that is identified by key.</returns>
        Task<T> GetOrAddAsync<T>(string key, Func<Task<T>> value);

        /// <summary>
        /// Gets an entry from the cache with the specified key, or if the entry does not exist, inserts a
        /// cache entry into the cache by using a key, a value and absolute expiration and returns it.
        /// </summary>
        /// <typeparam name="T">The type of the cache entry value.</typeparam>
        /// <param name="key">A unique identifier for the cache entry to insert.</param>
        /// <param name="value">A function that asynchronously gets the data for the cache entry.</param>
        /// <param name="absoluteExpiration">The absolute expiration time, after which the cache entry will expire.</param>
        /// <returns>A reference to the cache entry that is identified by key.</returns>
        Task<T> GetOrAddAsync<T>(string key, Func<Task<T>> value, DateTimeOffset absoluteExpiration);

        /// <summary>
        /// Gets an entry from the cache with the specified key, or if the entry does not exist, inserts a
        /// cache entry into the cache by using a key, a value and sliding expiration and returns it.
        /// </summary>
        /// <typeparam name="T">The type of the cache entry value.</typeparam>
        /// <param name="key">A unique identifier for the cache entry to insert.</param>
        /// <param name="value">A function that asynchronously gets the data for the cache entry.</param>
        /// <param name="slidingExpiration">The sliding expiration time, after which the cache entry will expire.</param>
        /// <returns>A reference to the cache entry that is identified by key.</returns>
        Task<T> GetOrAddAsync<T>(string key, Func<Task<T>> value, TimeSpan slidingExpiration);

        #region GetOrAdd with group
        
        T GetOrAdd<T>(string group, string key, Func<T> value);        
        T GetOrAdd<T>(string group, string key, Func<T> value, DateTimeOffset absoluteExpiration);
        T GetOrAdd<T>(string group, string key, Func<T> value, TimeSpan slidingExpiration) ;
        Task<T> GetOrAddAsync<T>(string group, string key, Func<Task<T>> value) ;
        Task<T> GetOrAddAsync<T>(string group, string key, Func<Task<T>> value, DateTimeOffset absoluteExpiration) ;
        Task<T> GetOrAddAsync<T>(string group, string key, Func<Task<T>> value, TimeSpan slidingExpiration) ;
        #endregion

        /// <summary>
        /// Clears all items from the cache.
        /// </summary>
        void Clear();

        /// <summary>
        /// Determines whether a cache entry exists in the cache with the specified key.
        /// </summary>
        /// <param name="key">A unique identifier for the cache entry.</param>
        /// <returns><c>true</c> if a cache entry exists, otherwise <c>false</c>.</returns>
        /// <exception cref="System.ArgumentNullException">key is null.</exception>
        bool Contains(string key);

        /// <summary>
        /// Removes a cache entry from the cache with the specified key.
        /// </summary>
        /// <param name="key">A unique identifier for the cache entry to remove.</param>
        /// <exception cref="System.ArgumentNullException">key is null.</exception>
        void Remove(string key);

        /// <summary>
        /// Removes items by pattern
        /// </summary>
        /// <param name="pattern">pattern</param>
        void RemoveByPattern(string pattern);

        /// <summary>
        /// Remove items in group
        /// </summary>
        /// <param name="group"></param>
        void RemoveByGroup(string group);

        /// <summary>
        /// Add key to group
        /// </summary>
        /// <param name="key">cache key</param>
        /// <param name="group">cache group</param>
        /// <param name="slidingExpiration"></param>
        void AddKeyToGroup(string key, string group, TimeSpan? slidingExpiration);
    }
}
