﻿; (function ($) {
    $.fn.columnlist = function (options) {
        options = $.extend({}, $.fn.columnlist.defaults, options);

        return this.each(function () {
            var
              $list = $(this),
              size = options.size || $list.data('columnList') || 1,
              $children = $list.children('li'),
              perColumn = Math.ceil($children.length / size),
              $column;
            for (var i = 0; i < size; i++) {
                $column = $('<ul />').appendTo(returnColumn(i));
                for (var j = 0; j < perColumn; j++) {
                    if ($children.length > i * perColumn + j) {
                        $column.append($children[i * perColumn + j]);
                    }
                }
                $list.append($column.parent());
            }
        });

        function returnColumn(inc) {
            return $('<li class="' + options.incrementClass + inc + ' ' + options['class'] + '"></li>');
        }
    };

    $.fn.columnlist.defaults = {
        'class': 'column-list',
        incrementClass: 'column-list-'
    };

})(jQuery);

(function ($) {

    $.fn.responsiveTab = function () {
        return this.each(function () {
            var nav_links = $(this).find(".tabs-nav a");
            var tabContent = $(this).find(".tabs-content");
            var tabPanels = tabContent.find(".tab-panel");
            var nav_accordion = tabContent.find("h3");
            //console.log(nav_accordion);

            tabPanels.each(function () {
                var title = $(this).attr("data-title");
                var $this = $(this);
                var className = $this.hasClass('active') ? "open" : '';
                $this.before("<h3 class='" + className + "'>" + title + " <i class='fa fa-angle-left'></i></h3>");
            });

            nav_links.on("click", function (e) {
                e.preventDefault(e);
                nav_links.parent().removeClass("active");
                var href = $(this).attr("href");
                //console.log(href);
                //console.log(tabPanels);
                //console.log(tabContent);
                tabPanels.removeClass('active');
                tabContent.find(href).addClass('active');
                $(this).parent().addClass("active");
                return false;
            });

            tabContent.on("click", "h3", function () {
                var $this = $(this);
                if (!$this.hasClass('open')) {
                    $this.addClass('open');
                    $this.next(".tab-panel").slideDown();
                } else {
                    $this.removeClass('open');
                    $this.next(".tab-panel").slideUp();
                }
                return false;
            });
        });
    };

}(jQuery));
; (function ($) {
    $.fn.fontResizer = function (options) {
        options = $.extend({}, $.fn.fontResizer.defaults, options);

        return this.each(function () {
            var $content = $(this);
            var wrapper = $content.wrapInner('<div class="font-resizer-wrapper"></div>');
            var resizer = $("<div class='font-resizer'><div>");
            var ul = $("<ul></ul>");
            ul.append('<li><a href="#" class="smaller" data-change="-1" data-toggle="tooltip" title="Giảm cỡ chữ"><i class="fa fa-search-minus"></i></a></li>');
            ul.append('<li><a href="#" class="normal" data-change="0" data-toggle="tooltip" title="Cỡ chữ mặc định"><i class="fa fa-font"></i></a></li>');
            ul.append('<li><a href="#" class="bigger" data-change="1" data-toggle="tooltip" title="Tăng cỡ chữ"><i class="fa fa-search-plus"></i></a></li>');
            resizer.append(ul);
            resizer.on('click', 'a', function (e) {
                e.preventDefault();
                var change = parseInt($(this).attr('data-change'));
                var size = $content.css('font-size');
                size = parseInt(size.replace("px", ""));
                console.log(size);
                if (change == 0) {
                    size = options.defaultSize;
                } else {
                    size += change;
                }
                wrapper.css('font-size', size + "px");
            });
            $content.append(resizer);

        });
        
    };

    $.fn.fontResizer.defaults = {
        defaultSize: 13
    };

})(jQuery);
