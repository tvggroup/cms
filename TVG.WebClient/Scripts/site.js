﻿$(document).ready(function () {
    $("object[type='application/pdf']").each(function () {
        var $this = $(this);
        var wrapper = $this.parent();
        var fileUrl = $this.attr('data');
        var frame = "<iframe src='/pdfviewer/viewer.html?file=" + fileUrl + "' width='100%' height='800px'/>";
        wrapper.html(frame);
    });
    
    $('#header-search-form').submit(function () {
        var type = $('#search-type').val();
        var frm = $(this);
        frm.attr("action", "/" + type + "/tim-kiem/");
    });

    var pathArray = window.location.pathname.split('/');
    if (pathArray.length > 1) {
        $('#search-type option[value="' + pathArray[1] + '"]').attr('selected', 'selected');
        $('#search-type').change();
    }

    $(".image-carousel").owlCarousel({
        singleItem: true,
        autoPlay: true,
        stopOnHover: true,
        navigation: true,
        navigationText: false,
        responsiveBaseWidth: ".image-carousel-slide"
        //responsiveBaseWidth: ".author"
    });
    $('.selectized').selectize();

    $('.related-docs .box-header').on('click', function () {
        $(this).toggleClass('collapsed');
        $(this).parent().children('.box-body').slideToggle();
    });

    //BACK TO TOP
    $('.bottom-nav').hide();

    $(window).bind('scroll', function () {
        var $this = $(this);
        if ($this.scrollTop() > 400) {
            $('.bottom-nav').fadeIn();
        } else {
            $('.bottom-nav').fadeOut();
        }
    });

    $(".gototop").click(function () {
        $("html, body").animate({ scrollTop: 0 }, 500);
    });

    $('#puLinkRegister').click(function (e) {
        e.preventDefault();
        $('#puLogin').modal('hide');
        $('#puRegister').modal('show');
    });
    $('#puLinkLogin').click(function (e) {
        e.preventDefault();
        $('#puLogin').modal('show');
        $('#puRegister').modal('hide');
    });


    $('body').tooltip({
        selector: '[data-toggle=tooltip]'
    });

    $('.list-cols').columnlist({
        size: 3,
        'class': 'menu-col',
        incrementClass: 'menu-col-'
    });

    $('#tab_content table:first').css("width", "100%");

    CMS.Common.makeButtonRippleEffect();

    
});

var CMS = {};
CMS.Common = (function () {
    var obj = {};

    obj.stickyTabHeader = function (tabWrapper) {
        var $tabs = $(tabWrapper);
        var header = $(tabWrapper).children('.nav-tabs');
        var tabContent = $(tabWrapper).find('.tab-content');
        var origOffsetY = header.offset().top;
        var headerHeight = header.height();
        var headerWidth = header.width();

        $(window).bind('scroll', function () {
            if ($(window).scrollTop() >= origOffsetY) {
                header.addClass('sticky-tab-header').css('width', headerWidth + "px");
                tabContent.css('padding-top', (headerHeight + 10) + "px");
            } else {
                header.removeClass('sticky-tab-header');
                tabContent.css('padding-top', "15px");
            }
        });

        //header.children('li').on('click', function () {
        //    if ($(window).scrollTop() >= origOffsetY) {
        //        $("html, body").animate({ scrollTop: origOffsetY }, 500);
        //    }
        //});
    };

    obj.truncateString = function (n, useWordBoundary) {
        var isTooLong = this.length > n,
            s_ = isTooLong ? this.substr(0, n - 1) : this;
        s_ = (useWordBoundary && isTooLong) ? s_.substr(0, s_.lastIndexOf(' ')) : s_;
        return isTooLong ? s_ + '&hellip;' : s_;
    };

    obj.makeButtonRippleEffect = function () {
        var ink, d, x, y;
        jQuery(".btn").click(function (e) {
            if ($(this).find(".ink").length === 0) {
                $(this).prepend("<span class='ink'></span>");
            }

            ink = $(this).find(".ink");
            ink.removeClass("animate");

            if (!ink.height() && !ink.width()) {
                d = Math.max($(this).outerWidth(), $(this).outerHeight());
                ink.css({ height: d, width: d });
            }

            x = e.pageX - $(this).offset().left - ink.width() / 2;
            y = e.pageY - $(this).offset().top - ink.height() / 2;

            ink.css({ top: y + 'px', left: x + 'px' }).addClass("animate");
        });
    };

    obj.pagingList = function (options) {
        var defaults = {
            listId: "",
            pagerId: "",
            pageSize: 5,
            pagingStyle: "number" //number or nav
        };
        options = $.extend({}, defaults, options);
        var listElem = $('#' + options.listId),
            pagerElem = $('#' + options.pagerId),
            totalItems = listElem.children().length,
            totalPages = Math.ceil(totalItems / options.pageSize);
        if (totalPages <= 1) return;
        //listElem.wrap("<div class='pagedListWrapper'></div>");
        listElem.attr('data-page-index', 1);

        //add page data to each child
        var pageIndex = 1;
        listElem.children().each(function (i) {
            if (i < pageIndex * options.pageSize && i >= (pageIndex - 1) * options.pageSize) {
                $(this).attr("data-page", pageIndex);
            }
            else {
                $(this).attr("data-page", (pageIndex + 1));
                pageIndex++;
            }
        });

        listElem.children().hide();
        listElem.children('[data-page="1"]').show();

        //create pager
        //console.log(options);
        var pageNav = $("<div class='simplePageNav' data-list='" + options.listId + "'></div>");
        if (options.pagingStyle == "nav") {
            var prev = $("<div class='prev'></div>");
            var next = $("<div class='next'></div>");
            prev.click(function () {
                var listId = $(this).parent().attr('data-list');
                var list = $('#' + listId)
                var pIndex = list.attr('data-page-index');
                if (pIndex == 1) return;
                pIndex--;
                list.attr('data-page-index', pIndex);
                list.children().hide();
                list.children('[data-page="' + pIndex + '"]').show();
            });
            next.click(function () {
                var listId = $(this).parent().attr('data-list');
                var list = $('#' + listId)
                var pIndex = list.attr('data-page-index');
                if (pIndex == totalPages) return;
                pIndex++;
                list.attr('data-page-index', pIndex);
                list.children().hide();
                list.children('[data-page="' + pIndex + '"]').show();
            });
            pageNav.append(prev).append(next);
        } else if (options.pagingStyle == "number") {            
            var navUl = $("<ul class='pagination'></ul>");
            for (var i = 0; i < totalPages; i++) {
                navUl.append("<li><a href='#' data-pid='" + (i + 1) + "'>" + (i + 1) + "</a></li>");
            }
            navUl.children("li:first").addClass('active');
            navUl.find('a').click(function (e) {
                e.preventDefault();
                var $this = $(this);
                $this.closest('ul').find('li').removeClass('active');
                $this.parent().addClass('active');
                var pIndex = $this.attr('data-pid');
                listElem.attr('data-page-index', pIndex);
                listElem.children().hide();
                listElem.children('[data-page="' + pIndex + '"]').show();

            });
            pageNav.append(navUl);
        }
        
        pagerElem.append(pageNav);
    };

    return obj;
})();

CMS.Document = (function () {
    var obj = {};
    obj.createLawDocumentLink = function (containerId, token) {
        var reg = /(\d+[\/-]?\d+[\/-][-A-Za-zĐđ]+)/g;
        var str = document.getElementById(containerId).innerHTML;
        var matches = str.match(reg);
        if (matches != null) {
            //matches = jQuery.unique(matches);
            matches = CMS.Helper.uniqueArray(matches);
            var nre = new RegExp('', "g");
            var dataPost = {};
            for (var i = 0; i < matches.length; i++) {
                dataPost = {
                    '__RequestVerificationToken': token,
                    'codeNum': matches[i],
                };
                $.ajax({
                    method: "POST",
                    url: "/lawdocument/GetDocumentByCodeNum",
                    data: dataPost,
                    async: false,
                    success: function (data) {
                        if (data.Status == 'success') {
                            nre = new RegExp(matches[i], "g");
                            str = str.replace(nre, "<a target='_blank' class='link-codenum' data-toggle='tooltip' href='/phap-luat/" + data.Slug + "-" + data.Id + "' title='" + data.Title + "'>" + matches[i] + "</a>");
                        }
                    }
                });
            }
            document.getElementById(containerId).innerHTML = str;
        }
    };

    obj.bindSearchDateBox = function (selector, valueSelector, date, autoUpdateInput) {
        var $this = $(selector),
            valueContainer = $(valueSelector);
        var opt = {
            autoUpdateInput: autoUpdateInput,
            singleDatePicker: true,
            showDropdowns: true,            
            minDate: "01/01/1990",
            maxDate: moment(),
            locale: { format: 'DD/MM/YYYY' }
        };
        if (date != '') {
            opt.startDate = date;
        }
        $this.daterangepicker(opt, function (start, end, label) {
            if (start != '') {
                valueContainer.val(start.format('YYYY-MM-DD'));
            }            
        });
        $this.on('apply.daterangepicker', function (ev, picker) {
            $this.val(picker.startDate.format('DD/MM/YYYY'));
        });
    };

    return obj;
})();

CMS.Helper = (function () {
    var obj = {};

    obj.uniqueArray = function(a) {
        var prims = { "boolean": {}, "number": {}, "string": {} }, objs = [];

        return a.filter(function (item) {
            var type = typeof item;
            if (type in prims)
                return prims[type].hasOwnProperty(item) ? false : (prims[type][item] = true);
            else
                return objs.indexOf(item) >= 0 ? false : objs.push(item);
        });
    };

    return obj;
})();

(function ($) {
    $.fn.truncateString = function (n, useWordBoundary) {
        var text = this.text();
        var isTooLong = text.length > n,
            s_ = isTooLong ? text.substr(0, n - 1) : text;
        s_ = (useWordBoundary && isTooLong) ? s_.substr(0, s_.lastIndexOf(' ')) : s_;
        text = isTooLong ? s_ + ' ...' : s_;
        this.text(text);
        return this;
    };
})(jQuery);