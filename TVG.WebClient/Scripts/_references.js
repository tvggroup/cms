/// <autosync enabled="true" />
/// <reference path="../pdfviewer/compatibility.js" />
/// <reference path="../pdfviewer/debugger.js" />
/// <reference path="../pdfviewer/l10n.js" />
/// <reference path="../pdfviewer/pdf.js" />
/// <reference path="../pdfviewer/pdf.worker.js" />
/// <reference path="../pdfviewer/viewer.js" />
/// <reference path="bootstrap.min.js" />
/// <reference path="fallback/scripts.js" />
/// <reference path="fallback/styles.js" />
/// <reference path="jquery.validate.min.js" />
/// <reference path="jquery.validate.unobtrusive.min.js" />
/// <reference path="jquery-2.2.0.min.js" />
/// <reference path="modernizr-2.8.3.js" />
/// <reference path="owl.carousel.min.js" />
/// <reference path="plugins/jpaging/jquery.quick.pagination.js" />
/// <reference path="plugins/jplugins.js" />
/// <reference path="plugins/jquery.mark.min.js" />
/// <reference path="plugins/jquery.sticky.js" />
/// <reference path="plugins/jquery.sticky-kit.min.js" />
/// <reference path="plugins/jquery-highlight1.js" />
/// <reference path="plugins/jquery-slimscroll/examples/libs/prettify/prettify.js" />
/// <reference path="plugins/jquery-slimscroll/jquery.slimscroll.js" />
/// <reference path="plugins/jquerytmpl/jquery.tmpl.min.js" />
/// <reference path="plugins/selectize/selectize.min.js" />
/// <reference path="plugins/slidepanel/main.js" />
/// <reference path="plugins/tipso/tipso.js" />
/// <reference path="plugins/trumbowyg/langs/vi.min.js" />
/// <reference path="plugins/trumbowyg/plugins/base64/trumbowyg.base64.js" />
/// <reference path="plugins/trumbowyg/plugins/cleanpaste/trumbowyg.cleanpaste.js" />
/// <reference path="plugins/trumbowyg/plugins/colors/trumbowyg.colors.js" />
/// <reference path="plugins/trumbowyg/plugins/emoji/trumbowyg.emoji.js" />
/// <reference path="plugins/trumbowyg/plugins/insertaudio/trumbowyg.insertaudio.js" />
/// <reference path="plugins/trumbowyg/plugins/pasteimage/trumbowyg.pasteimage.js" />
/// <reference path="plugins/trumbowyg/plugins/table/trumbowyg.table.js" />
/// <reference path="plugins/trumbowyg/plugins/template/trumbowyg.template.js" />
/// <reference path="plugins/trumbowyg/plugins/upload/trumbowyg.upload.js" />
/// <reference path="plugins/trumbowyg/trumbowyg.js" />
/// <reference path="site.js" />
