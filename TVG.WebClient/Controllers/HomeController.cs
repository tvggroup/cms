﻿namespace TVG.WebClient.Controllers
{
    using System.Diagnostics;
    using System.Net;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Web.Mvc;
    using Boilerplate.Web.Mvc;
    using Boilerplate.Web.Mvc.Filters;
    using TVG.WebClient.Constants;
    using TVG.WebClient.Services;
    using Data.Services;
    using Core.Helper;
    using System.Collections.Generic;
    using Data.Domain;
    using NPoco;
    using Microsoft.AspNet.Identity;
    using Core.Caching;
    using System;
    using System.Linq;
    using TVG.WebClient.Models;

    public class HomeController : BaseController
    {
        #region Fields

        private readonly IBrowserConfigService browserConfigService;
        private readonly IFeedService feedService;
        private readonly IManifestService manifestService;
        private readonly IOpenSearchService openSearchService;
        private readonly IRobotsService robotsService;
        private readonly ISitemapService sitemapService;

        ISettingService settingService;
        IMenuService menuService;
        IBannerService bannerService;
        ITaxonomyService taxonomyService;
        IPostService postService;
        ITagService tagService;

        #endregion

        #region Constructors

        public HomeController(
            IBrowserConfigService browserConfigService,
            IFeedService feedService,
            IManifestService manifestService,
            IOpenSearchService openSearchService,
            IRobotsService robotsService,
            ISitemapService sitemapService,
            ISettingService settingService,
            IMenuService menuService,
            IBannerService bannerService,
            ITaxonomyService taxonomyService,
            IPostService postService,
            IPermissionService permissionService,
            ITagService tagService)
        {
            this.browserConfigService = browserConfigService;
            this.feedService = feedService;
            this.manifestService = manifestService;
            this.openSearchService = openSearchService;
            this.robotsService = robotsService;
            this.sitemapService = sitemapService;
            this.settingService = settingService;
            this.menuService = menuService;
            this.bannerService = bannerService;
            this.taxonomyService = taxonomyService;
            this.postService = postService;
            this.permissionService = permissionService;
            this.tagService = tagService;
        }

        #endregion

        [Route("", Name = HomeControllerRoute.GetIndex)]
        public ActionResult Index()
        {
            ViewBag.Title = ValueHelper.TryGet(settingService.GetSettingValue("site_title"), "");
            ViewBag.MetaDescription = ValueHelper.TryGet(settingService.GetSettingValue("site_meta_description"), "");
            ViewBag.MetaKeyword = ValueHelper.TryGet(settingService.GetSettingValue("site_meta_keyword"), "");
            return this.View(HomeControllerAction.Index);
        }

        public ActionResult Header()
        {
            ViewBag.SitePhone = ValueHelper.TryGet(settingService.GetSettingValue("site_phone"), "");
            ViewBag.SiteEmail = ValueHelper.TryGet(settingService.GetSettingValue("site_email"), "");
            ViewBag.SiteGA = ValueHelper.TryGet(settingService.GetSettingValue("site_google_analytics"), "");

            bool isLoggedIn = (System.Web.HttpContext.Current.User != null) && (System.Web.HttpContext.Current.User.Identity.IsAuthenticated);
            ViewBag.IsLoggedIn = isLoggedIn;

            return PartialView();
        }

        public ActionResult MainMenu()
        {
            //Lấy danh sách law category
            Dictionary<string, object> args = new Dictionary<string, object>();
            args["type"] = "law_category";
            args["getTree"] = true;
            var terms = taxonomyService.GetTerms(args);
            ViewBag.LawDocumentCategories = terms.Items;

            //Lấy danh sách category
            args = new Dictionary<string, object>();
            args["type"] = "doc_category";
            args["getTree"] = true;
            Page<TaxonomyTerm> dterms = taxonomyService.GetTerms(args);
            ViewBag.DocumentCategories = dterms.Items;

            return PartialView();
        }

        public ActionResult HeaderAdminMenu()
        {
            bool isLoggedIn = (System.Web.HttpContext.Current.User != null) && (System.Web.HttpContext.Current.User.Identity.IsAuthenticated);
            bool accessAdmin = false;
            if(isLoggedIn)
            {
                accessAdmin = HasPermission("AccessAdminPanel");
            }
            ViewBag.AccessAdminPanel = accessAdmin;
            return PartialView();
        }

        public ActionResult Footer()
        {
            ViewBag.SiteFooter = ValueHelper.TryGet(settingService.GetSettingValue("site_footer_text"), "");
            return PartialView();
        }

        public ActionResult HomeBanner()
        {
            int bannerId = ValueHelper.TryGet(settingService.GetSettingValue("site_home_banner"), 0);
            var bannerImages = bannerService.GetBannerImages(bannerId);
            return PartialView(bannerImages);
        }        

        public ActionResult HeaderSearchBox()
        {
            ViewBag.SearchType = ValueHelper.TryGet(Request.QueryString["type"], 0);
            ViewBag.Keyword = ValueHelper.TryGet(Request.QueryString["kw"], "");
            return PartialView();
        }

        

        public ActionResult HomeTopPosts()
        {
            //lấy top 5 bài viết
            Dictionary<string, object> args = new Dictionary<string, object>();
            args["postType"] = "post";
            args["status"] = 1;
            args["pageIndex"] = 1;
            args["pageSize"] = 5;
            var posts = postService.GetPosts(args);
            return PartialView(posts.Items);
        }

        public ActionResult HomePostCategories()
        {
            Dictionary<string, object> args = new Dictionary<string, object>();
            var settingHomeNewsBlock = settingService.GetSettingValue("home_news_blocks");
            List<HomeNewsBlockModel> homeNewsBlocks = Newtonsoft.Json.JsonConvert.DeserializeObject<List<HomeNewsBlockModel>>(settingHomeNewsBlock);
            foreach(var item in homeNewsBlocks)
            {
                item.Category = taxonomyService.GetTermById(item.CategoryId);
                if(item.GetChildren)
                {
                    args = new Dictionary<string, object>();
                    args["type"] = "category";
                    args["pId"] = item.CategoryId;
                    args["getTree"] = true;
                    item.Category.Children = taxonomyService.GetTerms(args).Items;
                }
            }
            return PartialView(homeNewsBlocks);
        }

        public ActionResult HomePostTagBlocks()
        {
            Dictionary<string, object> args = new Dictionary<string, object>();
            var settingHomeNewsTagBlock = settingService.GetSettingValue("home_news_tag_blocks");
            List<HomeNewsTagBlockModel> homeNewsTagBlocks = Newtonsoft.Json.JsonConvert.DeserializeObject<List<HomeNewsTagBlockModel>>(settingHomeNewsTagBlock);
            foreach (var item in homeNewsTagBlocks)
            {
                item.Tag = tagService.GetById(item.TagId);
            }
            return PartialView(homeNewsTagBlocks);
        }

        [Route("about", Name = HomeControllerRoute.GetAbout)]
        public ActionResult About()
        {
            return this.View(HomeControllerAction.About);
        }

        [Route("contact", Name = HomeControllerRoute.GetContact)]
        public ActionResult Contact()
        {
            return this.View(HomeControllerAction.Contact);
        }

        /// <summary>
        /// Gets the Atom 1.0 feed for the current site. Note that Atom 1.0 is used over RSS 2.0 because Atom 1.0 is a 
        /// newer and more well defined format. Atom 1.0 is a standard and RSS is not. See
        /// http://rehansaeed.com/building-rssatom-feeds-for-asp-net-mvc/
        /// </summary>
        /// <returns>The Atom 1.0 feed for the current site.</returns>
        [OutputCache(CacheProfile = CacheProfileName.Feed)]
        [Route("feed", Name = HomeControllerRoute.GetFeed)]
        public async Task<ActionResult> Feed()
        {
            // A CancellationToken signifying if the request is cancelled. See
            // http://www.davepaquette.com/archive/2015/07/19/cancelling-long-running-queries-in-asp-net-mvc-and-web-api.aspx
            CancellationToken cancellationToken = this.Response.ClientDisconnectedToken;
            return new AtomActionResult(await this.feedService.GetFeed(cancellationToken));
        }

        //[Route("search", Name = HomeControllerRoute.GetSearch)]
        //public ActionResult Search(string query)
        //{
        //    // You can implement a proper search function here and add a Search.cshtml page.
        //    // return this.View(HomeControllerAction.Search);

        //    // Or you could use Google Custom Search (https://cse.google.co.uk/cse) to index your site and display your 
        //    // search results in your own page.

        //    // For simplicity we are just assuming your site is indexed on Google and redirecting to it.
        //    return this.Redirect(string.Format(
        //        "https://www.google.co.uk/?q=site:{0} {1}", 
        //        this.Url.AbsoluteRouteUrl(HomeControllerRoute.GetIndex),
        //        query));
        //}

        /// <summary>
        /// Gets the browserconfig XML for the current site. This allows you to customize the tile, when a user pins 
        /// the site to their Windows 8/10 start screen. See http://www.buildmypinnedsite.com and 
        /// https://msdn.microsoft.com/en-us/library/dn320426%28v=vs.85%29.aspx
        /// </summary>
        /// <returns>The browserconfig XML for the current site.</returns>
        [NoTrailingSlash]
        [OutputCache(CacheProfile = CacheProfileName.BrowserConfigXml)]
        [Route("browserconfig.xml", Name = HomeControllerRoute.GetBrowserConfigXml)]
        public ContentResult BrowserConfigXml()
        {
            Trace.WriteLine(string.Format(
                "browserconfig.xml requested. User Agent:<{0}>.",
                this.Request.Headers.Get("User-Agent")));
            string content = this.browserConfigService.GetBrowserConfigXml();
            return this.Content(content, ContentType.Xml, Encoding.UTF8);
        }

        /// <summary>
        /// Gets the manifest JSON for the current site. This allows you to customize the icon and other browser 
        /// settings for Chrome/Android and FireFox (FireFox support is coming). See https://w3c.github.io/manifest/
        /// for the official W3C specification. See http://html5doctor.com/web-manifest-specification/ for more 
        /// information. See https://developer.chrome.com/multidevice/android/installtohomescreen for Chrome's 
        /// implementation.
        /// </summary>
        /// <returns>The manifest JSON for the current site.</returns>
        [NoTrailingSlash]
        [OutputCache(CacheProfile = CacheProfileName.ManifestJson)]
        [Route("manifest.json", Name = HomeControllerRoute.GetManifestJson)]
        public ContentResult ManifestJson()
        {
            Trace.WriteLine(string.Format(
                "manifest.jsonrequested. User Agent:<{0}>.",
                this.Request.Headers.Get("User-Agent")));
            string content = this.manifestService.GetManifestJson();
            return this.Content(content, ContentType.Json, Encoding.UTF8);
        }

        /// <summary>
        /// Gets the Open Search XML for the current site. You can customize the contents of this XML here. The open 
        /// search action is cached for one day, adjust this time to whatever you require. See
        /// http://www.hanselman.com/blog/CommentView.aspx?guid=50cc95b1-c043-451f-9bc2-696dc564766d
        /// http://www.opensearch.org
        /// </summary>
        /// <returns>The Open Search XML for the current site.</returns>
        [NoTrailingSlash]
        [OutputCache(CacheProfile = CacheProfileName.OpenSearchXml)]
        [Route("opensearch.xml", Name = HomeControllerRoute.GetOpenSearchXml)]
        public ContentResult OpenSearchXml()
        {
            Trace.WriteLine(string.Format(
                "opensearch.xml requested. User Agent:<{0}>.", 
                this.Request.Headers.Get("User-Agent")));
            string content = this.openSearchService.GetOpenSearchXml();
            return this.Content(content, ContentType.Xml, Encoding.UTF8);
        }

        /// <summary>
        /// Tells search engines (or robots) how to index your site. 
        /// The reason for dynamically generating this code is to enable generation of the full absolute sitemap URL
        /// and also to give you added flexibility in case you want to disallow search engines from certain paths. The 
        /// sitemap is cached for one day, adjust this time to whatever you require. See
        /// http://rehansaeed.com/dynamically-generating-robots-txt-using-asp-net-mvc/
        /// </summary>
        /// <returns>The robots text for the current site.</returns>
        //[NoTrailingSlash]
        //[OutputCache(CacheProfile = CacheProfileName.RobotsText)]
        //[Route("robots.txt", Name = HomeControllerRoute.GetRobotsText)]
        //public ContentResult RobotsText()
        //{
        //    Trace.WriteLine(string.Format(
        //        "robots.txt requested. User Agent:<{0}>.", 
        //        this.Request.Headers.Get("User-Agent")));
        //    string content = this.robotsService.GetRobotsText();
        //    return this.Content(content, ContentType.Text, Encoding.UTF8);
        //}

        /// <summary>
        /// Gets the sitemap XML for the current site. You can customize the contents of this XML from the 
        /// <see cref="SitemapService"/>. The sitemap is cached for one day, adjust this time to whatever you require.
        /// http://www.sitemaps.org/protocol.html
        /// </summary>
        /// <param name="index">The index of the sitemap to retrieve. <c>null</c> if you want to retrieve the root 
        /// sitemap file, which may be a sitemap index file.</param>
        /// <returns>The sitemap XML for the current site.</returns>
        [NoTrailingSlash]
        [Route("sitemap.xml", Name = HomeControllerRoute.GetSitemapXml)]
        public ActionResult SitemapXml(int? index = null)
        {
            string content = this.sitemapService.GetSitemapXml(index);

            if (content == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Sitemap index is out of range.");
            }

            return this.Content(content, ContentType.Xml, Encoding.UTF8);
        }  
        
    }
}