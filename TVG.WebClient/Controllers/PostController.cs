﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TVG.Data.Services;
using TVG.Data.Domain;
using TVG.Core.Helper;
using NPoco;

namespace TVG.WebClient.Controllers
{
    public class PostController : Controller
    {
        ITaxonomyService taxonomyService;
        IPostService postService;
        public PostController(
            ITaxonomyService taxonomyService,
            IPostService postService
        )
        {
            this.taxonomyService = taxonomyService;
            this.postService = postService;
        }

        // GET: Danh sách bài viết
        public ActionResult Index(string slug = "", int id = 0)
        {
            Dictionary<string, object> args = new Dictionary<string, object>();
            args["type"] = "category";
            args["pId"] = id;
            args["pageIndex"] = 1;
            args["pageSize"] = int.MaxValue;
            args["getTree"] = true;
            Page<TaxonomyTerm> terms = taxonomyService.GetTerms(args);
            string strTermIds = "";
            if (terms.Items.Count > 0)
            {
                List<string> strTerms = new List<string>();
                strTerms.Add(id.ToString());
                foreach (var item in terms.Items)
                {
                    strTerms.Add(item.Id.ToString());
                }
                strTermIds = string.Join(",", strTerms.ToArray()) ;
            }
            else
            {
                strTermIds = id.ToString();
            }

            args = new Dictionary<string, object>();
            args["postType"] = "post";
            args["termId"] = strTermIds;
            args["status"] = 1;
            args["pageIndex"] = ValueHelper.TryGet(Request.QueryString["pId"], 1);
            args["pageSize"] = ValueHelper.TryGet(Request.QueryString["pSize"], 10);
            var posts = postService.GetPosts(args);

            ViewBag.Term = taxonomyService.GetTermById(id);            

            return View(posts);
        }

        //chi tiết bài viết
        public ActionResult Detail(string slug = "", int id = 0)
        {
            if (id <= 0) return RedirectToAction("NotFound", "Error");
            var post = postService.GetPostById(id);
            if (post == null) return RedirectToAction("NotFound", "Error");
            postService.UpdatePostExt(post.Id, "PageView", 1);

            var postTerms = postService.GetPostTermsByPostId(id);
            if (postTerms.Count > 0)
            {
                ViewBag.Term = taxonomyService.GetTermById(postTerms[0].TermId);
            }

            return View(post);
        }

        //widget - danh sách chuyên mục (category)
        public ActionResult TermsList(string type)
        {
            Taxonomy taxonomy = taxonomyService.GetTaxonomyByName(type);
            List<TaxonomyTerm> terms = new List<TaxonomyTerm>();
            if (taxonomy != null)
            {
                Dictionary<string, object> args = new Dictionary<string, object>();
                args["type"] = type;
                args["pId"] = 0;
                args["pageIndex"] = 1;
                args["pageSize"] = int.MaxValue;
                args["getTree"] = taxonomy.IsHierarchy;
                Page<TaxonomyTerm> pterms = taxonomyService.GetTerms(args);
                terms = pterms.Items;
            }
            return PartialView(terms);
        }

        //partial - Related documents
        public ActionResult RelatedPosts(int categoryId, int numberOfPost = 5)
        {
            Dictionary<string, object> args = new Dictionary<string, object>();
            args["termId"] = categoryId;
            args["status"] = 1;
            args["pageIndex"] = ValueHelper.TryGet(Request.QueryString["pId"], 1);
            args["pageSize"] = ValueHelper.TryGet(Request.QueryString["pSize"], numberOfPost);
            var docs = postService.GetPosts(args).Items;

            return PartialView(docs);
        }
    }
}