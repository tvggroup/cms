﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TVG.Data.Domain;
using TVG.Data.Services;

namespace TVG.WebClient.Controllers
{
    public class PageController : Controller
    {
        IPostService postService;
        public PageController(IPostService postService)
        {
            this.postService = postService;
        }

        // GET: Page
        public ActionResult Index(string slug = "")
        {
            if (string.IsNullOrEmpty(slug)) return RedirectToAction("NotFound", "Error");
            Post post = postService.GetPostBySlug(slug);
            if(post == null) return RedirectToAction("NotFound", "Error");
            return View(post);
        }

        [Route("lien-he")]
        public ActionResult Contact()
        {
            return View();
        }
    }
}