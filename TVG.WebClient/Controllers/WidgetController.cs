﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TVG.Data.Services;
using TVG.Data.Domain;
using TVG.WebClient.Models;
using NPoco;

namespace TVG.WebClient.Controllers
{
    public class WidgetController : Controller
    {
        private IPostService postService;
        private ITaxonomyService taxonomyService;
        ITagService tagService;

        public WidgetController(
            IPostService postService ,
            ITaxonomyService taxonomyService,
            ITagService tagService
        )
        {
            this.postService = postService;
            this.taxonomyService = taxonomyService;
            this.tagService = tagService;
        }
        

        public ActionResult TopPosts()
        {
            //lấy top 5 bài viết
            Dictionary<string, object> args = new Dictionary<string, object>();
            args["postType"] = "post";
            args["status"] = 1;
            args["pageIndex"] = 1;
            args["pageSize"] = 5;
            var posts = postService.GetPosts(args);
            return PartialView(posts.Items);
        }

        public ActionResult TopPostsBlock(HomeNewsBlockModel model)
        {
            Dictionary<string, object> args = new Dictionary<string, object>();
            string strTermIds = "";
            if (model.CategoryId > 0)
            {
                model.Category = taxonomyService.GetTermById(model.CategoryId);
                args["type"] = "category";
                args["pId"] = model.CategoryId;
                args["pageIndex"] = 1;
                args["pageSize"] = int.MaxValue;
                args["getTree"] = true;
                Page<TaxonomyTerm> terms = taxonomyService.GetTerms(args);
                model.Category.Children = terms.Items;
                if (terms.Items.Count > 0)
                {
                    List<string> strTerms = new List<string>();
                    strTerms.Add(model.CategoryId.ToString());
                    foreach (var item in terms.Items)
                    {
                        strTerms.Add(item.Id.ToString());
                    }
                    strTermIds = string.Join(",", strTerms.ToArray());
                }
                else
                {
                    strTermIds = model.CategoryId.ToString();
                }
            }
            args = new Dictionary<string, object>();
            args["postType"] = "post";
            args["termId"] = strTermIds;
            args["status"] = 1;
            args["pageIndex"] = 1;
            args["pageSize"] = model.PostsCount;
            var posts = postService.GetPosts(args);

            model.Posts = posts.Items;
            return PartialView(model);
        }

        public ActionResult TopTagPostsBlock(HomeNewsTagBlockModel model)
        {
            Dictionary<string, object> args = new Dictionary<string, object>();
            string strTermIds = "";
            if (model.TagId > 0)
            {
                model.Tag = tagService.GetById(model.TagId);
                strTermIds = model.TagId.ToString();
            }
            args = new Dictionary<string, object>();
            args["postType"] = "post";
            args["tagIds"] = strTermIds;
            args["status"] = 1;
            args["pageIndex"] = 1;
            args["pageSize"] = model.PostsCount;
            var posts = postService.GetPostsByTag(args);

            model.Posts = posts.Items;
            return PartialView(model);
        }

        public ActionResult TextBlock(string slug)
        {
            Post post = null;
            if (!string.IsNullOrEmpty(slug))
            {
                post = postService.GetPostBySlug(slug);
            }
            var model = new WidgetModel();
            model.Title = post.Title;
            model.Content = post;
            return PartialView(model);
        }
    }
}