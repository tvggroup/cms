﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TVG.Core.Helper;
using TVG.Data.Domain;
using TVG.Data.Services;

namespace TVG.WebClient.Controllers
{
    public class BannerController : BaseController
    {
        private IBannerService bannerService;

        public BannerController(IBannerService bannerService)
        {
            this.bannerService = bannerService;
        }

        // GET: Banner
        public ActionResult Slider(string slug)
        {
            var banner = bannerService.GetBannerBySlug(slug);
            var bannerImages = new List<BannerImage>();
            if (banner != null)
            {
                bannerImages = bannerService.GetBannerImages(banner.Id);
            }
            return PartialView(bannerImages);
        }
    }
}