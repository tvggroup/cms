﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TVG.Core.Helper;
using TVG.Data;
using TVG.Data.Domain;
using TVG.Data.Services;
using NPoco;
using TVG.WebClient.Models;

namespace TVG.WebClient.Controllers
{
    public class SearchController : Controller
    {
        private ITaxonomyService taxonomyService;

        public SearchController(
            ITaxonomyService taxonomyService
            )
        {
            this.taxonomyService = taxonomyService;
        }

        // GET: Search
        public ActionResult Index()
        {
            int type = ValueHelper.TryGet(Request.QueryString["type"], 0);
            string kw = ValueHelper.TryGet(Request.QueryString["kw"], "");
            int pageIndex = ValueHelper.TryGet(Request.QueryString["pId"], 1);
            int pageSize = ValueHelper.TryGet(Request.QueryString["pSize"], 20);
            string returnUrl = Request.Url.PathAndQuery;
            returnUrl = MyUrlHelper.RemoveQueryStringByKey(returnUrl, "type");

            if (string.IsNullOrEmpty(kw)) return RedirectToAction("Index", "Home");

            SearchViewModel model = new SearchViewModel();

            
            
            model.Keyword = kw;
            model.SearchType = type;
            model.ReturnUrlDocs = returnUrl + "&type=1";
            model.ReturnUrlLawDocs = returnUrl + "&type=2";

            return View(model);
        }
    }
}