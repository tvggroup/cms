﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TVG.Data.Domain;
using TVG.Data.Services;
using TVG.Core.Helper;
using System.Text;
using Microsoft.AspNet.Identity;
using TVG.WebClient.Models;
using TVG.Identity;
using Microsoft.AspNet.Identity.Owin;
using System.IO;

namespace TVG.WebClient.Controllers
{
    public class ContactController : BaseController
    {
        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        IContactMessageService contactMessageService;
        ITaxonomyService taxonomyService;
        public ContactController(
            IContactMessageService contactMessageService,
            ITaxonomyService taxonomyService
        )
        {
            this.contactMessageService = contactMessageService;
            this.taxonomyService = taxonomyService;
        }

        public ActionResult SendQuestion()
        {
            return View();
        }

        [Route("contact/gui-bai-viet-hoc-thuat")]
        public ActionResult SendDocumentPost()
        {
            var model = new ContactDocumentPostModel();
            model.UserId = User.Identity.GetUserId().TryConvert<int>(0);
            ApplicationUser currentUser = UserManager.FindById(model.UserId);
            if (currentUser != null)
            {
                model.Email = currentUser.Email;
                model.Name = currentUser.LastName + " " + currentUser.FirstName;
                model.Phone = currentUser.PhoneNumber;
            }
            return View(model);
        }

        [Route("contact/gui-bai-viet-hoc-thuat")]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult SendDocumentPost(ContactDocumentPostModel model)
        {
            //return View(model);
            var validTypes = new string[]
            {
                "application/pdf"
            };

            if (model.Attachment != null && model.Attachment.ContentLength > 0)
            {
                if (!validTypes.Contains(model.Attachment.ContentType))
                {
                    ModelState.AddModelError("Attachment", "File đính kèm không hợp lệ, vui lòng chọn file PDF");
                }
            }
            if (ModelState.IsValid)
            {
                var contact = new ContactMessage
                {
                    UserId = model.UserId,
                    ContactName = model.Name,
                    ContactEmail = model.Email,
                    ContactPhone = model.Phone,
                    Subject = model.Subject,
                    Message = model.Message,
                    CreatedDate = DateTime.Now
                };
                var attachmentUrl = "";
                if (model.Attachment != null && model.Attachment.ContentLength > 0)
                {
                    var uploadFolder = "~/uploads";
                    var uploadPath = "users";                    
                    var today = DateTime.Today;
                    uploadPath = Path.Combine(uploadPath, today.Year.ToString(), today.Month.ToString("00"));
                    if(model.UserId > 0)
                    {
                        uploadPath = Path.Combine(uploadPath, model.UserId.ToString());
                    }
                    var uploadDir = Server.MapPath(Path.Combine(uploadFolder, uploadPath));
                    if(!Directory.Exists(uploadDir))
                    {
                        Directory.CreateDirectory(uploadDir);
                    }
                    var filePath = Path.Combine(uploadDir, model.Attachment.FileName);
                    var fileUrl = Path.Combine(uploadPath, model.Attachment.FileName);
                    model.Attachment.SaveAs(filePath);
                    attachmentUrl = fileUrl.Replace("\\", "/");
                }

                var term = taxonomyService.GetTermBySlug(model.ContactType);
                List<int> termIds = new List<int>();
                if (term != null)
                {
                    termIds.Add(term.Id);
                }

                var meta = new List<ContactMessageMeta>();
                if (!string.IsNullOrEmpty(attachmentUrl))
                {
                    meta.Add(new ContactMessageMeta { ContactMessageId = contact.Id, MetaName = "attachment", MetaValue = attachmentUrl });
                }
                if(!string.IsNullOrEmpty(model.Author))
                {
                    meta.Add(new ContactMessageMeta { ContactMessageId = contact.Id, MetaName = "author", MetaValue = model.Author });
                }
                if (!string.IsNullOrEmpty(model.Journal))
                {
                    meta.Add(new ContactMessageMeta { ContactMessageId = contact.Id, MetaName = "journal", MetaValue = model.Journal });
                }
                if (!string.IsNullOrEmpty(model.Year))
                {
                    meta.Add(new ContactMessageMeta { ContactMessageId = contact.Id, MetaName = "published_year", MetaValue = model.Year });
                }

                var saveModel = new ContactMessageSaveModel();
                saveModel.Message = contact;
                saveModel.Metas = meta;
                saveModel.TermIds = termIds;
                int result = contactMessageService.InsertMessage(saveModel);
                if (result > 0)
                {
                    TempData["message"] = "Gửi thành công";
                    TempData["status"] = "success";
                }
            }

            return View(model);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult SendQuestion(string type, string name, string email, string phone, string subject, string message)
        {
            string status = "error";
            if (!string.IsNullOrEmpty(name) &&
                !string.IsNullOrEmpty(email) &&
                !string.IsNullOrEmpty(phone) &&
                !string.IsNullOrEmpty(subject) &&
                !string.IsNullOrEmpty(message))
            {
                var messageBody = "<p style='white-space: pre-line'>" + message + "</p>";
                var contact = new ContactMessage
                {
                    ContactName = name,
                    ContactEmail = email,
                    ContactPhone = phone,
                    Subject = subject,
                    Message = messageBody,
                    CreatedDate = DateTime.Now
                };
                var term = taxonomyService.GetTermBySlug("contact_type",type);
                List<int> termIds = new List<int>();
                if (term != null)
                {
                    termIds.Add(term.Id);
                }
                var model = new ContactMessageSaveModel();
                model.Message = contact;
                model.TermIds = termIds;

                int result = contactMessageService.InsertMessage(model);
                if (result > 0)
                {
                    status = "success";
                }
            }
            return Json(new { status = status });
        }

        [HttpPost]
        [ValidateInput(false)]
        public JsonResult SendDocumentPostAjax(string type, string name, string email, string phone, string subject, string message, string author, string journal, string year)
        {
            string status = "error";
            if (!string.IsNullOrEmpty(name) &&
                !string.IsNullOrEmpty(email) &&
                !string.IsNullOrEmpty(phone) &&
                !string.IsNullOrEmpty(subject) &&
                !string.IsNullOrEmpty(message))
            {
                var messageBody = new StringBuilder("");
                messageBody.Append("<p>Tác giả: " + author + "</p>");
                messageBody.Append("<p>Nơi đăng: " + journal + "</p>");
                messageBody.Append("<p>Năm đăng: " + year + "</p>");
                messageBody.Append("<p>Nội dung:</p>");
                messageBody.Append(message);
                var contact = new ContactMessage
                {
                    ContactName = name,
                    ContactEmail = email,
                    ContactPhone = phone,
                    Subject = subject,
                    Message = messageBody.ToString(),
                    CreatedDate = DateTime.Now
                };
                var term = taxonomyService.GetTermBySlug(type);
                List<int> termIds = new List<int>();
                if (term != null)
                {
                    termIds.Add(term.Id);
                }
                var model = new ContactMessageSaveModel();
                model.Message = contact;
                model.TermIds = termIds;

                int result = contactMessageService.InsertMessage(model);
                if (result > 0)
                {
                    status = "success";
                }
            }
            return Json(new { status = status });
        }
    }
}