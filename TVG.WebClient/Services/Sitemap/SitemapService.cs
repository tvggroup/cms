﻿namespace TVG.WebClient.Services
{
    using System;
    using System.Collections.Generic;
    using System.Web.Mvc;
    using Boilerplate.Web.Mvc;
    using Boilerplate.Web.Mvc.Sitemap;
    using TVG.WebClient.Constants;
    using Data.Services;
    using NPoco;
    using Data.Domain;

    /// <summary>
    /// Generates sitemap XML for the current site.
    /// </summary>
    public class SitemapService : SitemapGenerator, ISitemapService
    {
        #region Fields
        
        private readonly ICacheService cacheService;
        private readonly ILoggingService loggingService;
        private readonly UrlHelper urlHelper;

        private ITaxonomyService taxonomyService;
        private IPostService postService;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="SitemapService" /> class.
        /// </summary>
        /// <param name="cacheService">The cache service.</param>
        /// <param name="loggingService">The logging service.</param>
        /// <param name="urlHelper">The URL helper.</param>
        public SitemapService(
            ICacheService cacheService,
            ILoggingService loggingService,
            UrlHelper urlHelper,
            ITaxonomyService taxonomyService,
            IPostService postService)
        {
            this.cacheService = cacheService;
            this.loggingService = loggingService;
            this.urlHelper = urlHelper;
            this.taxonomyService = taxonomyService;
            this.postService = postService;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Gets the sitemap XML for the current site. If an index of null is passed and there are more than 25,000 
        /// sitemap nodes, a sitemap index file is returned (A sitemap index file contains links to other sitemap files 
        /// and is a way of splitting up your sitemap into separate files). If an index is specified, a standard 
        /// sitemap is returned for the specified index parameter. See http://www.sitemaps.org/protocol.html
        /// </summary>
        /// <param name="index">The index of the sitemap to retrieve. <c>null</c> if you want to retrieve the root 
        /// sitemap or sitemap index document, depending on the number of sitemap nodes.</param>
        /// <returns>The sitemap XML for the current site or <c>null</c> if the sitemap index is out of range.</returns>
        public string GetSitemapXml(int? index = null)
        {
            // Here we are caching the entire set of sitemap documents. We cannot use OutputCacheAttribute because 
            // cache expiry could get out of sync if the number of sitemaps changes.
            List<string> sitemapDocuments = this.cacheService.GetOrAdd(
                CacheSetting.SitemapNodes.Key,
                () =>
                {
                    IReadOnlyCollection<SitemapNode> sitemapNodes = this.GetSitemapNodes();
                    return this.GetSitemapDocuments(sitemapNodes);
                },
                CacheSetting.SitemapNodes.SlidingExpiration);

            if (index.HasValue && ((index < 1) || (index.Value >= sitemapDocuments.Count)))
            {
                return null;
            }

            return sitemapDocuments[index.HasValue ? index.Value : 0];
        }

        #endregion

        #region Protected Methods

        /// <summary>
        /// Gets a collection of sitemap nodes for the current site.
        /// TODO: Add code here to create nodes to all your important sitemap URL's.
        /// You may want to do this from a database or in code.
        /// </summary>
        /// <returns>A collection of sitemap nodes for the current site.</returns>
        protected virtual IReadOnlyCollection<SitemapNode> GetSitemapNodes()
        {
            List<SitemapNode> nodes = new List<SitemapNode>();

            nodes.Add(
                new SitemapNode(this.urlHelper.AbsoluteRouteUrl(HomeControllerRoute.GetIndex))
                {
                    Priority = 1
                });
            //nodes.Add(
            //   new SitemapNode(this.urlHelper.AbsoluteRouteUrl(HomeControllerRoute.GetAbout))
            //   {
            //       Priority = 0.9
            //   });
            //nodes.Add(
            //   new SitemapNode(this.urlHelper.AbsoluteRouteUrl(HomeControllerRoute.GetContact))
            //   {
            //       Priority = 0.9
            //   });

            //get taxonomy
            Dictionary<string, object> args = new Dictionary<string, object>();
            //Lấy danh sách law category
            args["type"] = "law_category";
            args["getTree"] = true;
            var terms = taxonomyService.GetTerms(args);

            foreach(TaxonomyTerm item in terms.Items)
            {
                nodes.Add(
                    new SitemapNode(this.urlHelper.AbsoluteRouteUrl("LawDocuments", new { id = item.Id, slug = item.Slug }))
                    {
                        Frequency = SitemapFrequency.Daily,
                        LastModified = DateTime.Now,
                        Priority = 0.8
                    });                   
            }

            //Lấy danh sách category
            args = new Dictionary<string, object>();
            args["type"] = "doc_category";
            args["getTree"] = true;
            Page<TaxonomyTerm> dterms = taxonomyService.GetTerms(args);

            foreach (TaxonomyTerm item in dterms.Items)
            {
                nodes.Add(
                    new SitemapNode(this.urlHelper.AbsoluteRouteUrl("Documents", new { id = item.Id, slug = item.Slug }))
                    {
                        Frequency = SitemapFrequency.Daily,
                        LastModified = DateTime.Now,
                        Priority = 0.8
                    });
                nodes.Add(
                    new SitemapNode(this.urlHelper.AbsoluteRouteUrl("DocumentPosts", new { id = item.Id, slug = item.Slug }))
                    {
                        Frequency = SitemapFrequency.Daily,
                        LastModified = DateTime.Now,
                        Priority = 0.8
                    });
                nodes.Add(
                    new SitemapNode(this.urlHelper.AbsoluteRouteUrl("DocumentCases", new { id = item.Id, slug = item.Slug }))
                    {
                        Frequency = SitemapFrequency.Daily,
                        LastModified = DateTime.Now,
                        Priority = 0.8
                    });
            }

            //bai viet tin tuc
            args["postType"] = "post";
            args["columns"] = "Id, Title, Slug, UpdatedDate";
            args["pageIndex"] = 1;
            args["pageSize"] = int.MaxValue;
            var posts = postService.GetPosts(args);
            foreach (Post item in posts.Items)
            {
                nodes.Add(
                    new SitemapNode(this.urlHelper.AbsoluteRouteUrl("PostDetails", new { id = item.Id, slug = item.Slug }))
                    {
                        Frequency = SitemapFrequency.Monthly,
                        LastModified = item.UpdatedDate,
                        Priority = 0.5
                    });
            }

            return nodes;
        }

        protected override string GetSitemapUrl(int index)
        {
            return this.urlHelper.AbsoluteRouteUrl(HomeControllerRoute.GetSitemapXml).TrimEnd('/') + "?index=" + index;
        }

        protected override void LogWarning(Exception exception)
        {
            this.loggingService.Log(exception);
        }

        #endregion
    }
}