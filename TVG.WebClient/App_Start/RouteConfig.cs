﻿namespace TVG.WebClient
{
    using System.Web.Mvc;
    using System.Web.Routing;

    public static class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            // Improve SEO by stopping duplicate URL's due to case differences or trailing slashes.
            // See http://googlewebmastercentral.blogspot.co.uk/2010/04/to-slash-or-not-to-slash.html
            routes.AppendTrailingSlash = true;
            routes.LowercaseUrls = false;

            // IgnoreRoute - Tell the routing system to ignore certain routes for better performance.
            // Ignore .axd files.
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            // Ignore everything in the Content folder.
            routes.IgnoreRoute("Content/{*pathInfo}");
            // Ignore everything in the Scripts folder.
            routes.IgnoreRoute("Scripts/{*pathInfo}");
            // Ignore the Forbidden.html file.
            routes.IgnoreRoute("Error/Forbidden.html");
            // Ignore the GatewayTimeout.html file.
            routes.IgnoreRoute("Error/GatewayTimeout.html");
            // Ignore the ServiceUnavailable.html file.
            routes.IgnoreRoute("Error/ServiceUnavailable.html");
            // Ignore the humans.txt file.
            routes.IgnoreRoute("humans.txt");

            // Enable attribute routing.
            routes.MapMvcAttributeRoutes();

            routes.MapRoute(
                name: "Page",
                url: "page/{slug}",
                defaults: new {controller = "Page", Action = "Index", slug = UrlParameter.Optional}                
            );

            routes.MapRoute(
               name: "PageService",
               url: "dich-vu/{slug}",
               defaults: new { controller = "PageService", Action = "Index", slug = UrlParameter.Optional }
           );

            routes.MapRoute(
                name: "DocumentDefault",
                url: "van-ban",
                defaults: new { controller = "Document", Action = "Index" }
            );

            routes.MapRoute(
                name: "Documents",
                url: "van-ban/linh-vuc/{slug}-{id}",
                defaults: new { controller = "Document", Action = "Index", slug = UrlParameter.Optional, id = UrlParameter.Optional }
            );
            routes.MapRoute(
                name: "DocumentTypes",
                url: "van-ban/loai-van-ban/{slug}-{id}",
                defaults: new { controller = "Document", Action = "IndexType", slug = UrlParameter.Optional, id = UrlParameter.Optional }
            );
            routes.MapRoute(
                name: "DocumentSubjects",
                url: "van-ban/chu-de/{slug}-{id}",
                defaults: new { controller = "Document", Action = "Subject", slug = UrlParameter.Optional, id = UrlParameter.Optional }
            );
            routes.MapRoute(
                name: "DocumentSearch",
                url: "van-ban/tim-kiem/",
                defaults: new { controller = "Document", Action = "Search"}
            );

            routes.MapRoute(
                name: "DocumentDetails",
                url: "van-ban/{slug}-{id}",
                defaults: new { controller = "Document", Action = "Detail", slug = UrlParameter.Optional, id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "LawDocumentDefault",
                url: "phap-luat",
                defaults: new { controller = "LawDocument", Action = "Index" }
            );

            routes.MapRoute(
                name: "LawDocuments",
                url: "phap-luat/linh-vuc/{slug}-{id}",
                defaults: new { controller = "LawDocument", Action = "Index", slug = UrlParameter.Optional, id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "LawDocumentSearch",
                url: "phap-luat/tim-kiem/",
                defaults: new { controller = "LawDocument", Action = "Search" }
            );

            routes.MapRoute(
                name: "LawDocumentDetails",
                url: "phap-luat/{slug}-{id}",
                defaults: new { controller = "LawDocument", Action = "Detail", slug = UrlParameter.Optional, id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "PostsDefault",
                url: "bai-viet",
                defaults: new { controller = "Post", Action = "Index" }
            );

            routes.MapRoute(
                name: "Posts",
                url: "bai-viet/chuyen-muc/{slug}-{id}",
                defaults: new { controller = "Post", Action = "Index", slug = UrlParameter.Optional, id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "PostDetails",
                url: "bai-viet/{slug}-{id}",
                defaults: new { controller = "Post", Action = "Detail", slug = UrlParameter.Optional, id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "DocumentPostsDefault",
                url: "bai-viet-hoc-thuat",
                defaults: new { controller = "DocumentPost", Action = "Index" }
            );

            routes.MapRoute(
                name: "DocumentPosts",
                url: "bai-viet-hoc-thuat/linh-vuc/{slug}-{id}",
                defaults: new { controller = "DocumentPost", Action = "Index", slug = UrlParameter.Optional, id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "DocumentPostSubjects",
                url: "bai-viet-hoc-thuat/chu-de/{slug}-{id}",
                defaults: new { controller = "DocumentPost", Action = "Subject", slug = UrlParameter.Optional, id = UrlParameter.Optional }
            );

            routes.MapRoute(
               name: "DocumentPostSearch",
               url: "bai-viet-hoc-thuat/tim-kiem/",
               defaults: new { controller = "DocumentPost", Action = "Search" }
           );

            routes.MapRoute(
                name: "DocumentPostDetails",
                url: "bai-viet-hoc-thuat/{slug}-{id}",
                defaults: new { controller = "DocumentPost", Action = "Detail", slug = UrlParameter.Optional, id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "DocumentCasesDefault",
                url: "an-le",
                defaults: new { controller = "DocumentCase", Action = "Index" }
            );

            routes.MapRoute(
                name: "DocumentCases",
                url: "an-le/linh-vuc/{slug}-{id}",
                defaults: new { controller = "DocumentCase", Action = "Index", slug = UrlParameter.Optional, id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "DocumentCaseSubjects",
                url: "an-le/chu-de/{slug}-{id}",
                defaults: new { controller = "DocumentCase", Action = "Subject", slug = UrlParameter.Optional, id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "DocumentCaseSearch",
                url: "an-le/tim-kiem/",
                defaults: new { controller = "DocumentCase", Action = "Search" }
            );

            routes.MapRoute(
                name: "DocumentCaseDetails",
                url: "an-le/{slug}-{id}",
                defaults: new { controller = "DocumentCase", Action = "Detail", slug = UrlParameter.Optional, id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "QuestionsDefault",
                url: "hoi-dap",
                defaults: new { controller = "Question", Action = "Index" }
            );

            routes.MapRoute(
                name: "Questions",
                url: "hoi-dap/chuyen-muc/{slug}-{id}",
                defaults: new { controller = "Question", Action = "Index", slug = UrlParameter.Optional, id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "QuestionDetails",
                url: "hoi-dap/{slug}-{id}",
                defaults: new { controller = "Question", Action = "Detail", slug = UrlParameter.Optional, id = UrlParameter.Optional }
            );

            //routes.MapRoute(
            //    name: "ContactSendDocumentPosts",
            //    url: "contact/gui-bai-viet-hoc-thuat",
            //    defaults: new { controller = "Contact", Action = "SendDocumentPost" }
            //);

            routes.MapRoute(
                name: "ContactSendQuestion",
                url: "contact/gui-cau-hoi",
                defaults: new { controller = "Contact", Action = "SendQuestion" }
            );

            // Normal routes are not required because we are using attribute routing. So we don't need this MapRoute 
            // statement. Unfortunately, Elmah.MVC has a bug in which some 404 and 500 errors are not logged without 
            // this route in place. So we include this but look out on these pages for a fix:
            // https://github.com/alexbeletsky/elmah-mvc/issues/60
            // https://github.com/RehanSaeed/ASP.NET-MVC-Boilerplate/issues/8
            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional });

            

        }
    }
}
