﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TVG.Data.Domain;
using Newtonsoft.Json;

namespace TVG.WebClient.Models
{
    public class HomeNewsBlockModel
    {
        [JsonProperty("id")]
        public int CategoryId { get; set; }
        [JsonProperty("name")]
        public string DisplayName { get; set; }
        [JsonProperty("style")]
        public string Style { get; set; }
        [JsonProperty("count")]
        public int PostsCount { get; set; }
        [JsonProperty("css")]
        public string CssClass { get; set; }
        [JsonProperty("children")]
        public bool GetChildren { get; set; }
        public TaxonomyTerm Category { get; set; }
        public List<Post> Posts { get; set; }
    }

    public class HomeNewsTagBlockModel
    {
        [JsonProperty("id")]
        public int TagId { get; set; }
        [JsonProperty("title")]
        public string DisplayName { get; set; }
        [JsonProperty("count")]
        public int PostsCount { get; set; }
        [JsonProperty("css")]
        public string CssClass { get; set; }
        public Tag Tag { get; set; }
        public List<Post> Posts { get; set; }
    }
}