﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace TVG.WebClient.Models
{
    public class RoleViewModel
    {
        public int Id { get; set; }

        [Required(AllowEmptyStrings = false)]
        [Display(Name = "RoleName")]
        public string Name { get; set; }

        [Required(AllowEmptyStrings = false)]
        [Display(Name = "CodeName")]
        public string CodeName { get; set; }
    }

    public class EditUserViewModel
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Email không được bỏ trống")]
        [Display(Name = "Email")]
        [EmailAddress]
        public string Email { get; set; }

        [Required (ErrorMessage = "Tên đăng nhập không được bỏ trống")]
        [Display(Name = "Tên đăng nhập")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "Tên và chữ lót không được bỏ trống")]
        [Display(Name = "Tên và chữ lót")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Họ không được bỏ trống")]
        [Display(Name = "Họ")]
        public string LastName { get; set; }

        //[Required(ErrorMessage = "Điện thoại không được bỏ trống")]
        [Display(Name = "Điện thoại")]
        [DataType(DataType.PhoneNumber)]
        public string PhoneNumber { get; set; }

        public IEnumerable<SelectListItem> RolesList { get; set; }
    }
}