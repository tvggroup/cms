﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TVG.WebClient.Models
{
    public class WidgetModel
    {
        public string Title { get; set; }
        public string CssClass { get; set; }
        public object Content { get; set; }
    }
}