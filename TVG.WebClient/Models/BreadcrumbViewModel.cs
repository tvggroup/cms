﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TVG.WebClient.Models
{
    public class BreadcrumbViewModel
    {
        public string Text { get; set; }
        public string Url { get; set; }
    }
}