﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TVG.WebClient.Models
{
    public class ContactViewModel
    {
        public int UserId { get; set; }
        public string ContactType { get; set; }
        [Required(ErrorMessage = "Vui lòng nhập họ tên")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Vui lòng nhập email")]
        public string Email { get; set; }
        public string Phone { get; set; }
        [Required(ErrorMessage = "Vui lòng nhập tiêu đề")]
        public string Subject { get; set; }
        [Required(ErrorMessage = "Vui lòng nhập nội dung")]
        [AllowHtml]
        public string Message { get; set; }
    }

    public class ContactDocumentPostModel : ContactViewModel
    {
        [AllowHtml]
        public new string Message { get; set; }
        public string Author { get; set; }
        public string Journal { get; set; }
        public string Year { get; set; }
        public HttpPostedFileBase Attachment { get; set; }
    }
}