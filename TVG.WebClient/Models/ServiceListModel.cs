﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TVG.Data.Domain;

namespace TVG.WebClient.Models
{
    public class PageServiceListModel
    {
        public TaxonomyTerm Term { get; set; }
        public List<Post> Pages { get; set; }
    }
}