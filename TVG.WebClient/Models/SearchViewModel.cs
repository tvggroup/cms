﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NPoco;
using TVG.Data.Domain;

namespace TVG.WebClient.Models
{
    public class SearchViewModel
    {
        public string ReturnUrlDocs { get; set; }
        public string ReturnUrlLawDocs { get; set; }
        public string PagerUrl { get; set; }
        public string Keyword { get; set; }
        public int SearchType { get; set; }       
        public List<TaxonomyTerm> LawTypes { get; set; }
    }
}