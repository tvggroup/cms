﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace TVG.WebClient.Extensions
{
    public static class TVGRouteExtension
    {
        public static string GetRouteParam(this RouteData routeData, string name)
        {
            string result = "";
            if(routeData.Values[name] != null)
            {
                result = routeData.Values[name].ToString();
            }
            return result;
        }
    }

    public class TVGRouteHelper
    {
        public static string GetEditUrl(string controller, string itemId)
        {
            string result = "";
            if (string.IsNullOrEmpty(itemId)) return "";
            controller = controller.ToLower();
            string adminDomain = ConfigurationManager.AppSettings["AdminDomain"].ToString();
            switch(controller)
            {
                case "lawdocument":
                    result = string.Format("{0}/lawdocument/edit/{1}", adminDomain, itemId);
                    break;
                case "document":
                    result = string.Format("{0}/document/edit/{1}", adminDomain, itemId);
                    break;
                case "documentcase":
                    result = string.Format("{0}/documentcase/edit/{1}", adminDomain, itemId);
                    break;
                case "documentpost":
                    result = string.Format("{0}/documentpost/edit/{1}", adminDomain, itemId);
                    break;
                case "post":
                    result = string.Format("{0}/post/edit/{1}", adminDomain, itemId);
                    break;
                case "page":
                    result = string.Format("{0}/page/edit/{1}", adminDomain, itemId);
                    break;
            }
            return result;
        }

        public static string RouteUrl(UrlHelper urlHelper, string routeName, object routeValues, string domain = "")
        {
            if(string.IsNullOrEmpty(domain))
            {
                domain = ConfigurationManager.AppSettings["ClientDomain"];
            }

            var uri = new Uri(domain);
            var routeValueDictionary = new RouteValueDictionary(routeValues);
            var url = "";
            url = urlHelper.RouteUrl(routeName, routeValueDictionary, "", uri.Host);
            return url;
        }
    }
}