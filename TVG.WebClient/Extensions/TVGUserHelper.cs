﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TVG.WebClient.Extensions
{
    public class TVGUserHelper
    {
        public static bool IsLoggedIn()
        {
            bool isLoggedIn = (System.Web.HttpContext.Current.User != null) && (System.Web.HttpContext.Current.User.Identity.IsAuthenticated);
            return isLoggedIn;
        }

    }
}