﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TVG.Core.Helper;
using System.Drawing;
using TVG.Core.Logging;
using NLog;
using Autofac.Integration.Mvc;
using Autofac.Core;

namespace TVG.WebClient.Extensions
{
    public class TVGHelper
    {
        public static string GetThumbnail(string filePath, int width, int height)
        {
            ILogManager logger = DependencyResolver.Current.GetService<ILogManager>();
            
            if (string.IsNullOrEmpty(filePath)) return "/Content/Images/noimage.png";
            string ext = Path.GetExtension(filePath);
            if (!string.IsNullOrEmpty(ext))
                ext = ext.Substring(1);
            string clientUrl = "";
            string uploadPath = HttpContext.Current.Server.MapPath("~/uploads/");
            filePath = uploadPath + filePath;
            string thumb = "";
            try
            {
                if (ImageHelper.IsImage(ext))
                {
                    //thumb = clientUrl + "/Content/Images/noimage.png";
                    FileInfo fi = new FileInfo(filePath);

                    //tao thu muc moi
                    string fileFolder = "";
                    if(fi.DirectoryName.Length > uploadPath.Length)
                    {
                        fileFolder = fi.DirectoryName.Substring(uploadPath.Length);
                    }
                    string folderPath = uploadPath + "\\cache\\" + fileFolder;
                    if (!Directory.Exists(folderPath))
                        Directory.CreateDirectory(folderPath);

                    //tao filename moi
                    string newFileName = "";
                    string fileName = Path.GetFileNameWithoutExtension(filePath);
                    fileName += string.Format("-{0}x{1}.{2}", width, height, ext);
                    newFileName = folderPath + "\\" + fileName;

                    //tao thumbnail neu chua co
                    if (!System.IO.File.Exists(newFileName))
                    {
                        logger.Info("Xử lý hình {0}", filePath);
                        ImageHelper.ResizeImage(filePath, newFileName, width, 0);
                        //Image img;
                        //using (var bmpTemp = new Bitmap(filePath))
                        //{
                        //    img = new Bitmap(bmpTemp);

                        //    Image newImage = ImageHelper.Resize(img, width, height, RotateFlipType.RotateNoneFlipNone);
                        //    newImage.Save(newFileName);
                        //    newImage.Dispose();
                        //}
                    }
                    thumb = clientUrl + "/uploads" + newFileName.Substring(uploadPath.Length).Replace('\\', '/');
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Lỗi khi xử lý hình {0}", filePath );
            }

            return thumb;
        }
    }
}