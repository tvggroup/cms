﻿using HashidsNet;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using TVG.Core.Helper;
using TVG.Data.Domain;

namespace TVG.WebClient.Extensions
{
    public static class TVGExtensions
    {
        public static string SafeGet(this TempDataDictionary tempData, string key)
        {
            object value;
            if (!tempData.TryGetValue(key, out value))
                return string.Empty;
            return value.ToString();
        }

        public static MvcHtmlString Pager(this HtmlHelper html, long totalPages, long pageIndex, long pageSize, string url)
        {
            StringBuilder sb = new StringBuilder();
            url = MyUrlHelper.RemoveQueryStringByKey(url, "pid");
            url = MyUrlHelper.RemoveQueryStringByKey(url, "psize");
            // only show paging if we have more items than the page size
            long displayedPages = 5;

            if (totalPages > 1)
            {
                long currentGroup = (pageIndex - 1) / displayedPages;
                long startPage = (currentGroup * displayedPages) + 1;
                long endPage = (currentGroup + 1) * displayedPages > totalPages ? totalPages : (currentGroup + 1) * 5;
                long prevPage = startPage - 1;
                long nextPage = endPage < totalPages ? endPage + 1 : totalPages;

                sb.Append("<ul class=\"pagination\">");

                //link first page
                sb.Append("<li class=''>");
                sb.Append("<a href='");
                if (url.IndexOf('?') > 0)
                {
                    sb.Append(url).Append("&pid=");
                }
                else
                {
                    sb.Append(url).Append("?pid=");
                }
                sb.Append("1").Append("&psize=").Append(pageSize.ToString());
                sb.Append("' title=\"Go to First Page");
                sb.Append("\">").Append("«").Append("</a>");
                sb.Append("</li>");

                //link to previous group, chỉ hiển thị khi ở group 2 trở lên
                if(currentGroup > 0)
                {
                    sb.Append("<li class=''>");
                    sb.Append("<a href='");
                    if (url.IndexOf('?') > 0)
                    {
                        sb.Append(url).Append("&pid=");
                    }
                    else
                    {
                        sb.Append(url).Append("?pid=");
                    }
                    sb.Append(prevPage.ToString()).Append("&psize=").Append(pageSize.ToString());
                    sb.Append("' title=\"Go to Page ").Append(prevPage.ToString());
                    sb.Append("\">").Append("❮").Append("</a>");
                    sb.Append("</li>");
                }

                //link các trang trong group
                for (var i = startPage; i <= endPage; i++)
                {
                    sb.Append("<li class='");
                    if (i == pageIndex)
                    {
                        sb.Append(" active'>");
                        sb.Append("<a href='#' data-dt-idx='").Append(i.ToString()).Append("'>").Append(i.ToString()).Append("</a>");
                    }
                    else
                    {
                        sb.Append("'>");
                        sb.Append("<a href='");
                        if (url.IndexOf('?') > 0)
                        {
                            sb.Append(url).Append("&pid=");
                        }
                        else
                        {
                            sb.Append(url).Append("?pid=");
                        }
                        sb.Append(i.ToString()).Append("&psize=").Append(pageSize.ToString());
                        sb.Append("' title=\"Go to Page ").Append(i.ToString());
                        sb.Append("\">").Append(i.ToString()).Append("</a>");
                    }
                    sb.Append("</li>");
                }

                //link to next group, chỉ hiển thị khi không phải group cuối 
                if((totalPages /displayedPages > 1) && (currentGroup < (totalPages / displayedPages)))
                {
                    sb.Append("<li class=''>");
                    sb.Append("<a href='");
                    if (url.IndexOf('?') > 0)
                    {
                        sb.Append(url).Append("&pid=");
                    }
                    else
                    {
                        sb.Append(url).Append("?pid=");
                    }
                    sb.Append(nextPage.ToString()).Append("&psize=").Append(pageSize.ToString());
                    sb.Append("' title=\"Go to Page ").Append(nextPage.ToString());
                    sb.Append("\">").Append("❯").Append("</a>");
                    sb.Append("</li>");
                }

                //link to last page
                sb.Append("<li class=''>");
                sb.Append("<a href='");
                if (url.IndexOf('?') > 0)
                {
                    sb.Append(url).Append("&pid=");
                }
                else
                {
                    sb.Append(url).Append("?pid=");
                }
                sb.Append(totalPages.ToString()).Append("&psize=").Append(pageSize.ToString());
                sb.Append("' title=\"Go to Last Page");
                sb.Append("\">").Append("»").Append("</a>");
                sb.Append("</li>");

                sb.Append("</ul>");
            }

            return MvcHtmlString.Create(sb.ToString());
        }

        public static string RelativePath(this HttpServerUtility srv, string path)
        {
            return path.Replace(HttpContext.Current.Server.MapPath("~/"), "~/").Replace(@"\", "/");
        }

        public static string HighlightKeywords(this string input, string keywords)
        {
            if (input == string.Empty || keywords == string.Empty)
            {
                return input;
            }

            string[] sKeywords = keywords.Split(' ');
            foreach (string sKeyword in sKeywords)
            {
                try
                {
                    input = Regex.Replace(input, sKeyword, string.Format("<span class=\"kw-hit\">{0}</span>", "$0"), RegexOptions.IgnoreCase);
                }
                catch
                {
                    //
                }
            }
            return input;
        }

        //public static string TruncateAtWord(this string input, int length)
        //{
        //    if (input == null || input.Length < length)
        //        return input;
        //    int iNextSpace = input.LastIndexOf(" ", length);
        //    return string.Format("{0}...", input.Substring(0, (iNextSpace > 0) ? iNextSpace : length).Trim());
        //}

        public static string GetExtensionFAIcon(this string filePath)
        {
            string icon = "fa-file-text-o";
            var ext = Path.GetExtension(filePath);
            if (!string.IsNullOrEmpty(ext))
            {
                ext = ext.Substring(1);
                switch(ext)
                {
                    case "txt":
                        icon = "fa-file-text-o";
                        break;
                    case "pdf":
                        icon = "fa-file-pdf-o";
                        break;
                    case "doc":
                    case "docx":
                        icon = "fa-file-word-o";
                        break;
                    case "xls":
                    case "xlsx":
                        icon = "fa-file-excel-o";
                        break;
                    case "ppt":
                    case "pptx":
                        icon = "fa-file-powerpoint-o";
                        break;
                    case "zip":
                    case "rar":
                    case "7z":
                        icon = "fa-file-zip-o";
                        break;
                    default:
                        icon = "fa-file-photo-o";
                        break;
                }
            }

            return icon;
        }

        public static string GetExtensionFileType(this string filePath)
        {
            string icon = "";
            var ext = Path.GetExtension(filePath);
            if (!string.IsNullOrEmpty(ext))
            {
                ext = ext.Substring(1);
                switch (ext)
                {
                    case "txt":
                        icon = "Text";
                        break;
                    case "pdf":
                        icon = "PDF";
                        break;
                    case "doc":
                    case "docx":
                        icon = "Word";
                        break;
                    case "xls":
                    case "xlsx":
                        icon = "Excel";
                        break;
                    case "ppt":
                    case "pptx":
                        icon = "Powerpoint";
                        break;
                    case "zip":
                    case "rar":
                    case "7z":
                        icon = "nén";
                        break;
                    default:
                        icon = "hình ảnh";
                        break;
                }
            }

            return icon;
        }

        public static string GetFileName(this string filePath)
        {
            return Path.GetFileName(filePath);
        }

        public static string HashId(this int i)
        {
            var hashids = new Hashids(ConfigurationManager.AppSettings["HashIdSalt"], ConfigurationManager.AppSettings["HashIdMinLength"].TryConvert<int>(0));
            var result = hashids.Encode(i);
            return result;
        }
    }
}