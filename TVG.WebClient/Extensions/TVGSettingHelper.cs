﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TVG.Core.Helper;
using TVG.Data.Services;

namespace TVG.WebClient.Extensions
{
    public class TVGSettingHelper
    {
        public static string GetSettingValue(string settingName)
        {
            ISettingService settingService = DependencyResolver.Current.GetService<ISettingService>();
            var result = ValueHelper.TryGet(settingService.GetSettingValue(settingName), "");
            return result;
        }
    }
}