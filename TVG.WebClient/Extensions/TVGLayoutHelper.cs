﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TVG.WebClient.Extensions
{
    public class LayoutConfig
    {
        public string Layout { get; set; }
        public string Name { get; set; }
        public bool HasLeftSidebar { get; set; }
        public bool HasRightSidebar { get; set; }
    }

    public class TVGLayoutHelper
    {
        public static LayoutConfig GetLayout(string name)
        {
            var layout = new LayoutConfig();
            layout.Name = name;
            layout.Layout = "~/Views/Shared/Layout/" + name + ".cshtml";
            switch(name.ToLower())
            {
                case "_sidebarleft":
                    layout.HasLeftSidebar = true;
                    break;
                case "_sidebarright":
                    layout.HasRightSidebar = true;
                    break;
                case "_sidebarboth":
                    layout.HasLeftSidebar = true;
                    layout.HasRightSidebar = true;
                    break;
            }

            return layout;
        }
    }
}