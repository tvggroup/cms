﻿using NPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TVG.Core.Caching;
using TVG.Core.Helper;
using TVG.Data.Domain;

namespace TVG.Data.Services
{
    public class ContactMessageService : IContactMessageService
    {
        private IDatabase db;
        private ICacheManager cache;

        public ContactMessageService(IDatabase database, ICacheManager cache)
        {
            this.db = database;
            this.cache = cache;
        }

        public ContactMessage GetMessageById(int id)
        {
            return db.SingleOrDefaultById<ContactMessage>(id);
        }

        public Page<ContactMessage> GetMessages(Dictionary<string, object> parameters)
        {
            int pageIndex = ValueHelper.TryGet(parameters, "pageIndex", 1);
            int pageSize = ValueHelper.TryGet(parameters, "pageSize", int.MaxValue);
            string termIds = ValueHelper.TryGet(parameters, "termIds", "");
            int status = ValueHelper.TryGet(parameters, "status", -1);
            bool isDeleted = ValueHelper.TryGet(parameters, "isDeleted", false);

            Sql sql = new Sql();
            sql.Select("m.Id, m.UserId, m.Subject, m.ContactName, m.ContactEmail, m.ContactPhone, m.Status, m.CreatedDate, m.IsDeleted");
            sql.From("ContactMessage m");
            sql.LeftJoin("ContactMessageTerm mt").On("m.Id = mt.ContactMessageId");

            sql.Where("m.IsDeleted=@0", isDeleted);
            if(status >= 0)
            {
                sql.Where("m.Status=@0", status);
            }
            if (!string.IsNullOrEmpty(termIds))
            {
                int[] arrTermIds = Array.ConvertAll(termIds.Split(','), int.Parse);
                if (arrTermIds.Length > 1 || (arrTermIds.Length == 1 && arrTermIds[0] != 0))
                {
                    sql.Where("mt.TermId IN (@0) ", arrTermIds);
                }
            }

            sql.OrderBy("Id DESC");
            Page<ContactMessage> messages = db.Page<ContactMessage>(pageIndex, pageSize, sql);
            return messages;
        }

        public int InsertMessage(ContactMessageSaveModel model)
        {
            int id = 0;
            using (var transaction = db.GetTransaction())
            {
                db.Insert<ContactMessage>(model.Message);
                id = model.Message.Id;

                if (id > 0)
                {
                    if (model.TermIds != null && model.TermIds.Count > 0)
                    {
                        InsertContactMessageTerm(id, model.TermIds);
                    }

                    if (model.Metas != null && model.Metas.Count > 0)
                    {
                        foreach (var meta in model.Metas)
                        {
                            meta.ContactMessageId = id;
                        }
                        InsertMetas(id, model.Metas);
                    }
                }
                transaction.Complete();
            }

            return id;
        }

        public int UpdateMessage(ContactMessageSaveModel model)
        {
            int result = 0;
            using (var transaction = db.GetTransaction())
            {
                result = db.Update(model.Message);

                if (result > 0)
                {
                    InsertContactMessageTerm(model.Message.Id, model.TermIds);
                    InsertMetas(model.Message.Id, model.Metas);
                }
                transaction.Complete();
            }

            return result;
        }

        public void DeleteMessage(int id)
        {
            using (var transaction = db.GetTransaction())
            {
                var msg = GetMessageById(id);
                if (msg != null)
                {
                    msg.IsDeleted = true;
                    db.Update(msg);
                }

                transaction.Complete();
            }
        }

        #region DocumentTerm

        public void InsertContactMessageTerm(int messageId, List<int> termIds)
        {
            db.Delete<ContactMessageTerm>("WHERE ContactMessageId=@0", messageId);
            
            List<ContactMessageTerm> contactMessageTerms = new List<ContactMessageTerm>();
            foreach (var item in termIds)
            {
                contactMessageTerms.Add(new ContactMessageTerm { ContactMessageId = messageId, TermId = item });
            }
            //sử dụng InsertBatch thay vì làm từng câu Insert
            db.InsertBatch<ContactMessageTerm>(contactMessageTerms);
        }

        public List<ContactMessageTerm> GetTermsByContactMessageId(int messageId)
        {
            var sql = new Sql();
            sql.Select("cmt.ContactMessageId, cmt.TermId, t.Slug AS TermSlug, t.Name AS TermName, t.Taxonomy AS TaxonomyType");
            sql.From("ContactMessageTerm cmt");
            sql.LeftJoin("TaxonomyTerms t").On("cmt.TermId = t.Id");
            sql.Where("ContactMessageId = @0", messageId);
            return db.Fetch<ContactMessageTerm>(sql);
        }

        #endregion

        #region Metas

        public List<ContactMessageMeta> GetMetasByMessageId(int messageId)
        {
            return db.Fetch<ContactMessageMeta>("WHERE ContactMessageId=@0", messageId);
        }

        public void InsertMetas(int messageId, List<ContactMessageMeta> metas)
        {
            if (messageId > 0)
            {
                db.Delete<ContactMessageMeta>("WHERE ContactMessageId=@0", messageId);
                if (metas != null && metas.Count > 0)
                {
                    db.InsertBatch<ContactMessageMeta>(metas);
                }
            }
        }
        #endregion


    }
}
