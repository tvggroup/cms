﻿using NPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVG.Data.Domain
{
    [TableName("ContactMessage")]
    [PrimaryKey("Id", AutoIncrement = true)]
    public class ContactMessage
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string Subject { get; set; }
        public string ContactType { get; set; }
        public string ContactName { get; set; }
        public string ContactEmail { get; set; }
        public string ContactPhone { get; set; }
        public string Message { get; set; }
        public int Status { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime CreatedDate { get; set; }

        [Ignore]
        public List<ContactMessageTerm> Terms { get; set; }

        [Ignore]
        public List<ContactMessageMeta> Metas { get; set; }
    }

    [TableName("ContactMessageTerm")]
    [PrimaryKey("ContactMessageId,TermId")]
    public class ContactMessageTerm
    {
        public int ContactMessageId { get; set; }
        public int TermId { get; set; }

        [ResultColumn]
        public string TaxonomyType { get; set; }

        [ResultColumn]
        public string TermName { get; set; }

        [ResultColumn]
        public string TermSlug { get; set; }
    }

    [TableName("[dbo].[ContactMessageMeta]")]
    [PrimaryKey("Id", AutoIncrement = true)]
    public partial class ContactMessageMeta : BaseMeta
    {
        public int Id { get; set; }
        public int ContactMessageId { get; set; }
    }

    public class ContactMessageSaveModel
    {
        public ContactMessage Message { get; set; }
        public List<int> TermIds { get; set; }
        public List<ContactMessageMeta> Metas { get; set; }
    }
}
