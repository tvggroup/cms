﻿using NPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TVG.Data.Domain;

namespace TVG.Data.Services
{
    public interface IContactMessageService
    {
        ContactMessage GetMessageById(int id);
        Page<ContactMessage> GetMessages(Dictionary<string, object> parameters);
        int InsertMessage(ContactMessageSaveModel model);
        int UpdateMessage(ContactMessageSaveModel model);
        void DeleteMessage(int id);

        void InsertContactMessageTerm(int messageId, List<int> termIds);
        List<ContactMessageTerm> GetTermsByContactMessageId(int messageId);

        //post meta
        List<ContactMessageMeta> GetMetasByMessageId(int messageId);
        void InsertMetas(int messageId, List<ContactMessageMeta> metas);
    }
}
