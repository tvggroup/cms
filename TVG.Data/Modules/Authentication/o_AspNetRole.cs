﻿using NPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVG.Data.Domain
{
    [TableName("[dbo].[AspNetRoles]")]
    [PrimaryKey("Id")]
    public class Role
    {
        public int Id { get; set; }        
        public string Description { get; set; }
        public string CodeName { get; set; }
        public string Name { get; set; }
        
        [Ignore]
        public List<User> Users { get; set; }
    }
}
