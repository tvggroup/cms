﻿using NPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVG.Data.Domain
{
    [TableName("dbo.PermissionCategory")]
    [PrimaryKey("Id", AutoIncrement = true)]
    public class PermissionCategory
    {
        public int Id { get; set; }
        public string CodeName { get; set; }
        public string Name { get; set; }
        public int DisplayOrder { get; set; }
    }

    [TableName("[dbo].[Permission]")]
    [PrimaryKey("Id", AutoIncrement = true)]
    public class Permission
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string CodeName { get; set; }
        public int CategoryId { get; set; }

        [ResultColumn]
        public string CategoryName { get; set; }
        [ResultColumn]
        public int DisplayOrder { get; set; }
    }

    [TableName("[dbo].[PermissionRole]")]
    [PrimaryKey("PermissionId, RoleId")]
    public class PermissionRole
    {
        public int PermissionId { get; set; }
        public int RoleId { get; set; }

        [ResultColumn]
        public string PermissionName { get; set; }
        [ResultColumn]
        public string PermissionCodeName { get; set; }
        [ResultColumn]
        public string PermissionCategory { get; set; }
        [ResultColumn]
        public string RoleName { get; set; }
    }
}
