﻿using NPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVG.Data.Domain
{
    [TableName("[dbo].[AspNetUsers]")]
    [PrimaryKey("Id")]
    public class User
    {
        public int Id { get; set; }        
        public string LastName { get; set; }       
        public string FirstName { get; set; }        
        public string Email { get; set; }       
        public bool EmailConfirmed { get; set; }        
        public string PasswordHash { get; set; }       
        public string SecurityStamp { get; set; }       
        public string PhoneNumber { get; set; }        
        public bool PhoneNumberConfirmed { get; set; }        
        public bool TwoFactorEnabled { get; set; }        
        public DateTime? LockoutEndDateUtc { get; set; }        
        public bool LockoutEnabled { get; set; }       
        public int AccessFailedCount { get; set; }        
        public string UserName { get; set; }
        public DateTime CreatedDate { get; set; }
        public int CreatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public int UpdatedBy { get; set; }
        public DateTime LastActiveDate { get; set; }

        [Ignore]
        public bool IsLock
        {
            get { return LockoutEnabled && LockoutEndDateUtc >= DateTime.UtcNow; }
        }

        [Ignore]
        public List<UserRole> UserRoles { get; set; }

        [Ignore]
        public List<Role> Roles { get; set; }
    }

    public class UserSaveModel
    {
        public User User { get; set; }
        public List<UserRole> Roles { get; set; }
    }
}
