﻿using NPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TVG.Core.Caching;
using TVG.Core.Helper;
using TVG.Core.Logging;
using TVG.Data.Domain;

namespace TVG.Data.Services
{
    public class RoleService : BaseService, IRoleService
    {
        private ILogManager logger;

        public RoleService(IDatabase database, ICacheManager cache, ILogManager logger)
        {
            this.db = database;
            this.cache = cache;
            this.logger = logger;
            this.cacheDatabaseNumber = ConstantDatas.CACHE_REDIS_DATABASE_USERS;
        }

        public Role GetRoleById(int id)
        {
            string key = CacheHelper.SetCacheName(ConstantDatas.CACHE_ROLE_BY_ID, id);
            string group = CacheHelper.SetCacheName(ConstantDatas.CACHE_ROLE_BY_ID, id);
            var obj = CacheManager.GetOrAdd<Role>(group, key, () =>
            {
                return db.SingleOrDefaultById<Role>(id);
            }, CacheHelper.CacheDurationMedium);
            return obj;
        }

        public Page<Role> GetRoles(Dictionary<string, object> parameters)
        {
            int pageIndex = ValueHelper.TryGet(parameters, "pageIndex", 1);
            int pageSize = ValueHelper.TryGet(parameters, "pageSize", int.MaxValue);
            string orderBy = ValueHelper.TryGet(parameters, "orderBy", "CodeName");

            string cacheName = CacheHelper.SetCacheName(ConstantDatas.CACHE_ROLE_ALL, pageIndex, pageSize);
            string cacheGroup = ConstantDatas.CACHE_ROLE_ALL_PATTERN;
            var obj = CacheManager.GetOrAdd<Page<Role>>(
               cacheGroup,
               cacheName,
               () =>
               {
                   Sql sql = new Sql();
                   sql.Select(@"*");
                   sql.From("AspnetRoles u");

                   sql.OrderBy(orderBy);
                   Page<Role> roles = db.Page<Role>(pageIndex, pageSize, sql);
                   return roles;
               }, CacheHelper.CacheDurationLong);
            return obj;
        }
    }
}
