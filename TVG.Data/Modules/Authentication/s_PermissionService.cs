﻿using NPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TVG.Core.Caching;
using TVG.Core.Helper;
using TVG.Data.Domain;

namespace TVG.Data.Services
{
    
    public class PermissionService : BaseService, IPermissionService
    {
        private const string PERMISSION_ACCESS_ADMIN = "AccessAdminPanel";
        public PermissionService(IDatabase db, ICacheManager cache)
        {
            this.db = db;
            this.cache = cache;
            this.cacheDatabaseNumber = ConstantDatas.CACHE_REDIS_DATABASE_USERS;
        }

        public Page<Permission> GetPermissions(Dictionary<string, object> parameters)
        {
            int pageIndex = ValueHelper.TryGet(parameters, "pageIndex", 1);
            int pageSize = ValueHelper.TryGet(parameters, "pageSize", int.MaxValue);

            string key = CacheHelper.SetCacheName(ConstantDatas.CACHE_PERMISSION_ALL, pageIndex, pageSize);
            var obj = CacheManager.GetOrAdd<Page<Permission>>(
                key,
                () => {
                    Sql sql = new Sql();
                    sql.Select("p.Id, p.CategoryId, p.CodeName, p.Name, pc.Name AS CategoryName, pc.DisplayOrder AS DisplayOrder");
                    sql.From("Permission AS p");
                    sql.InnerJoin("PermissionCategory pc").On("p.CategoryId = pc.Id");
                    sql.OrderBy("DisplayOrder");

                    Page<Permission> result = db.Page<Permission>(pageIndex, pageSize, sql);
                    return result;
                }
            );
            return obj;
        }

        public List<PermissionRole> GetPermissionRoleAccessAdmin()
        {
            var key = CacheHelper.SetCacheName(ConstantDatas.CACHE_PERMISSION_ROLE_ACCESS_ADMIN);
            var obj = CacheManager.GetOrAdd<List<PermissionRole>>(
                key,
                () => {
                    Sql sql = new Sql();
                    sql.Select("PermissionId, RoleId, p.Name AS PermissionName, p.CodeName AS PermissionCodeName, r.Name AS RoleName, pc.Name AS PermissionCategory");
                    sql.From("PermissionRole AS pr");
                    sql.InnerJoin("Permission AS p").On("pr.PermissionId = p.Id");
                    sql.InnerJoin("AspNetRoles r").On("pr.RoleId = r.Id");
                    sql.InnerJoin("PermissionCategory pc").On("p.CategoryId = pc.Id");
                    sql.Where("p.CodeName=@0", PERMISSION_ACCESS_ADMIN);
                    sql.OrderBy("pc.DisplayOrder");

                    List<PermissionRole> result = db.Fetch<PermissionRole>(sql);
                    return result;
                },
                CacheHelper.CacheDurationLong
            );
            return obj;
        }

        public List<PermissionRole> GetRolePemissions(int roleId)
        {
            var key = CacheHelper.SetCacheName(ConstantDatas.CACHE_PERMISSION_ALL_BY_ROLE_ID, roleId);
            var obj = CacheManager.GetOrAdd<List<PermissionRole>>(
                key,
                () => {
                    Sql sql = new Sql();
                    sql.Select("PermissionId, RoleId, p.Name AS PermissionName, p.CodeName AS PermissionCodeName, r.Name AS RoleName, pc.Name AS PermissionCategory");
                    sql.From("PermissionRole AS pr");
                    sql.InnerJoin("Permission AS p").On("pr.PermissionId = p.Id");
                    sql.InnerJoin("AspNetRoles r").On("pr.RoleId = r.Id");
                    sql.InnerJoin("PermissionCategory pc").On("p.CategoryId = pc.Id");
                    if (roleId > 0)
                    {
                        sql.Where("pr.RoleId = @0", roleId);
                    }
                    sql.OrderBy("pc.DisplayOrder");

                    List<PermissionRole> result = db.Fetch<PermissionRole>(sql);
                    return result;
                },
                CacheHelper.CacheDurationLong
            );
            return obj;
        }

        public List<PermissionRole> GetRolePemissions(string roleName)
        {
            var key = CacheHelper.SetCacheName(ConstantDatas.CACHE_PERMISSION_ALL_BY_ROLE_NAME, roleName);
            var obj = CacheManager.GetOrAdd<List<PermissionRole>>(
                key,
                () => {
                    Sql sql = new Sql();
                    sql.Select("PermissionId, RoleId, p.Name AS PermissionName, p.CodeName AS PermissionCodeName, r.Name AS RoleName, pc.Name AS PermissionCategory");
                    sql.From("PermissionRole AS pr");
                    sql.InnerJoin("Permission AS p").On("pr.PermissionId = p.Id");
                    sql.InnerJoin("AspNetRoles r").On("pr.RoleId = r.Id");
                    sql.InnerJoin("PermissionCategory pc").On("p.CategoryId = pc.Id");
                    if (!string.IsNullOrEmpty(roleName))
                    {
                        sql.Where("r.Name = @0", roleName);
                    }
                    sql.OrderBy("pc.DisplayOrder");

                    List<PermissionRole> result = db.Fetch<PermissionRole>(sql);
                    return result;
                },
                CacheHelper.CacheDurationLong
            );
            return obj;
        }

        public PermissionRole GetPermissionRole(int permissionid, int roleId)
        {
            string key = CacheHelper.SetCacheName(ConstantDatas.CACHE_PERMISSION_BY_PERMISSION_ID_ROLE_ID, permissionid, roleId);
            var obj = CacheManager.GetOrAdd<PermissionRole>(key, () =>
            {
                return db.SingleOrDefault<PermissionRole>("WHERE PermissionId = @0 AND RoleId = @1", permissionid, roleId);
            });
            return obj;
        }

        public int CheckPermissionRole(string permissionName, string roleName)
        {
            //co cache --> load list tu cache roi kiem tra
            var permissions = GetRolePemissions(roleName);
            var result = permissions.Count(x => string.Compare(permissionName, x.PermissionCodeName, true) == 0);
            return result;

            //khong co cache --> select theo tung role va permission
            //Sql sql = new Sql();
            //sql.Select("COUNT(*) As Number");
            //sql.From("PermissionRole AS pr");
            //sql.InnerJoin("Permission p").On("pr.PermissionId = p.Id");
            //sql.InnerJoin("AspNetRoles r").On("pr.RoleId = r.Id");
            //sql.Where("r.Name = @0", roleName);
            //sql.Where("p.CodeName = @0", permissionName);
            //var result = db.FirstOrDefault<Dictionary<string, object>>(sql.SQL, roleName, permissionName);
            //return ValueHelper.TryGet(result, "Number", 0);
        }

        public void SavePermissionRoles(List<PermissionRole> permissionRoles)
        {
            using (var transaction = db.GetTransaction())
            {
                db.Execute("TRUNCATE TABLE PermissionRole");
                db.InsertBatch<PermissionRole>(permissionRoles);
                transaction.Complete();

                CacheManager.RemoveByPattern(ConstantDatas.CACHE_PERMISSION_PATTERN);
            }
        }
    }
}
