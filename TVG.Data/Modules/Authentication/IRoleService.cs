﻿using NPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TVG.Data.Domain;

namespace TVG.Data.Services
{
    public interface IRoleService
    {
        Role GetRoleById(int id);
        Page<Role> GetRoles(Dictionary<string, object> parameters);

    }
}
