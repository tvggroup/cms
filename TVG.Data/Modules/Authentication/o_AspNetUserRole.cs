﻿using NPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVG.Data.Domain
{
    [TableName("[dbo].[AspNetUserRoles]")]
    [PrimaryKey("UserId, RoleId")]
    public class UserRole
    {
        public int UserId { get; set; }
        public int RoleId { get; set; }

        [Ignore]
        public User User { get; set; }
    }
}
