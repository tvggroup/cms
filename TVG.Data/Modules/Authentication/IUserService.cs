﻿using NPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TVG.Data.Domain;

namespace TVG.Data.Services
{
    public interface IUserService
    {
        User GetUserById(int id);
        User GetUserByUsername(string username);
        Page<User> GetUsers(Dictionary<string, object> parameters);
        int InsertUser(User user);
        int InsertUser(UserSaveModel model);
        int UpdateUser(User user);
        int UpdateUser(UserSaveModel model);
        int LockUser(User user);
        int UnlockUser(User user);

        List<UserRole> GetUserRoles(int userId);
        void InsertUserRoles(int userId, List<UserRole> userRole);
    }
}
