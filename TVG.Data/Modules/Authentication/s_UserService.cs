﻿using NPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TVG.Core.Caching;
using TVG.Core.Helper;
using TVG.Core.Logging;
using TVG.Data.Domain;

namespace TVG.Data.Services
{
    public class UserService : BaseService, IUserService
    {
        private ILogManager logger;

        public UserService(IDatabase database, ICacheManager cache, ILogManager logger)
        {
            this.db = database;
            this.cache = cache;
            this.logger = logger;
            this.cacheDatabaseNumber = ConstantDatas.CACHE_REDIS_DATABASE_USERS;
        }

        public User GetUserById(int userId)
        {
            string key = CacheHelper.SetCacheName(ConstantDatas.CACHE_USER_BY_ID, userId);
            string group = CacheHelper.SetCacheName(ConstantDatas.CACHE_USER_GROUP_BY_ID, userId);
            var obj = CacheManager.GetOrAdd<User>(group, key, () =>
            {
                return db.SingleOrDefaultById<User>(userId);
            }, CacheHelper.CacheDurationMedium);
            return obj;
        }

        public User GetUserByUsername(string username)
        {
            string key = CacheHelper.SetCacheName(ConstantDatas.CACHE_USER_BY_USERNAME, username);
            string group = CacheHelper.SetCacheName(ConstantDatas.CACHE_USER_ALL_PATTERN);
            var obj = CacheManager.GetOrAdd<User>(group, key, () =>
            {
                return db.FirstOrDefault<User>("WHERE Username=@0", username);
            }, CacheHelper.CacheDurationMedium);
            return obj;
        }

        public Page<User> GetUsers(Dictionary<string, object> parameters)
        {
            string email = ValueHelper.TryGet(parameters, "email", "");
            string username = ValueHelper.TryGet(parameters, "username", "");
            string phone = ValueHelper.TryGet(parameters, "phone", "");
            bool emailConfirmed = ValueHelper.TryGet(parameters, "emailConfirmed", false);
            string roles = ValueHelper.TryGet(parameters, "roles", "");
            string orderBy = ValueHelper.TryGet(parameters, "orderBy", "Id");
            bool isLocked = ValueHelper.TryGet(parameters, "isLocked", false);
            int pageIndex = ValueHelper.TryGet(parameters, "pageIndex", 1);
            int pageSize = ValueHelper.TryGet(parameters, "pageSize", int.MaxValue);

            string cacheName = CacheHelper.SetCacheName(ConstantDatas.CACHE_USER_ALL, roles, email, username, phone, emailConfirmed, isLocked, orderBy, pageIndex, pageSize);
            string cacheGroup = ConstantDatas.CACHE_USER_ALL_PATTERN;
            var obj = CacheManager.GetOrAdd<Page<User>>(
               cacheGroup,
               cacheName,
               () =>
               {
                   Sql sql = new Sql();
                   sql.Select(@"DISTINCT u.Id, u.LastName, u.FirstName, u.Email, u.EmailConfirmed, 
                                u.PhoneNumber, u.PhoneNumberConfirmed, u.TwoFactorEnabled, u.LockoutEndDateUtc, u.LockoutEnabled,
                                u.AccessFailedCount, u.Username, u.CreatedDate, u.CreatedBy, u.UpdatedDate, u.UpdatedBy, u.LastActiveDate");
                   sql.From("AspnetUsers u");
                   sql.LeftJoin("AspnetUserRoles ur").On("u.Id = ur.UserId");

                   if (!string.IsNullOrEmpty(roles))
                   {
                       int[] arrRoleIds = Array.ConvertAll(roles.Split(','), int.Parse);
                       if (arrRoleIds.Length > 1 || (arrRoleIds.Length == 1 && arrRoleIds[0] != 0))
                       {
                           sql.Where("ur.RoleId IN (@0) ", arrRoleIds);
                       }
                   }
                   if(!string.IsNullOrEmpty(username))
                   {
                       sql.Where("u.Username LIKE @0", "%" + username + "%");
                   }
                   if (!string.IsNullOrEmpty(email))
                   {
                       sql.Where("u.Email LIKE @0", "%" + email + "%");
                   }
                   if (!string.IsNullOrEmpty(phone))
                   {
                       sql.Where("u.PhoneNumber LIKE @0", "%" + phone + "%");
                   }
                   if (!string.IsNullOrEmpty(username))
                   {
                       sql.Where("u.Username LIKE @0", "%" + username + "%");
                   }

                   sql.OrderBy(orderBy);
                   Page<User> users = db.Page<User>(pageIndex, pageSize, sql);
                   return users;
               }, CacheHelper.CacheDurationLong);
            return obj;
        }

        public int InsertUser(User user)
        {
            db.Insert<User>(user);
            AfterInsert(user);
            return user.Id;
        }
        
        public int InsertUser(UserSaveModel model)
        {
            int id = 0;
            using (var transaction = db.GetTransaction())
            {
                db.Insert<User>(model.User);
                id = model.User.Id;

                if (id > 0)
                {
                    if (model.Roles != null && model.Roles.Count > 0)
                    {
                        foreach (var userRole in model.Roles)
                        {
                            userRole.UserId = id;
                        }
                        InsertUserRoles(id, model.Roles);
                    }

                    AfterInsert(model.User);
                }
                transaction.Complete();
            }

            return id;
        }

        public int UpdateUser(User user)
        {
            AfterUpdate(user);
            return db.Update(user);
        }

        public int UpdateUser(UserSaveModel model)
        {
            int result = 0;
            using (var transaction = db.GetTransaction())
            {
                result = db.Update(model.User);

                if (result > 0)
                {
                    InsertUserRoles(model.User.Id, model.Roles);

                    AfterUpdate(model.User);
                }
                transaction.Complete();
            }

            return result;
        }

        public int LockUser(User user)
        {
            user.LockoutEnabled = true;
            user.LockoutEndDateUtc = DateTime.UtcNow.AddYears(100);
            return UpdateUser(user);
        }

        public int UnlockUser(User user)
        {
            user.LockoutEndDateUtc = DateTime.UtcNow.AddMinutes(-1);
            return UpdateUser(user);
        }

        #region UserRoles
        public List<UserRole> GetUserRoles(int userId)
        {
            string key = CacheHelper.SetCacheName(ConstantDatas.CACHE_USER_ROLES, userId);
            string group = CacheHelper.SetCacheName(ConstantDatas.CACHE_USER_GROUP_BY_ID, userId);
            return CacheManager.GetOrAdd(group, key, () =>
            {
                return db.Fetch<UserRole>("WHERE UserId=@0", userId);
            }, CacheHelper.CacheDurationMedium);
        }

        public void InsertUserRoles(int userId, List<UserRole> userRoles)
        {
            if (userId > 0)
            {
                db.Delete<UserRole>("WHERE UserId=@0", userId);

                if (userRoles != null && userRoles.Count > 0)
                {
                    db.InsertBatch<UserRole>(userRoles);
                }
            }
        }
        #endregion

        private void AfterInsert(User user)
        {
            CacheManager.RemoveByGroup(string.Format(ConstantDatas.CACHE_USER_ALL_PATTERN));
        }

        private void AfterUpdate(User user)
        {
            CacheManager.RemoveByGroup(string.Format(ConstantDatas.CACHE_USER_ALL_PATTERN));
            CacheManager.RemoveByGroup(CacheHelper.SetCacheName(ConstantDatas.CACHE_USER_GROUP_BY_ID, user.Id));
        }
    }
}
