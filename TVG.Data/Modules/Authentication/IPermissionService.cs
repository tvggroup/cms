﻿using NPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TVG.Data.Domain;

namespace TVG.Data.Services
{
    public interface IPermissionService
    {
        Page<Permission> GetPermissions(Dictionary<string, object> parameters);
        List<PermissionRole> GetPermissionRoleAccessAdmin();
        List<PermissionRole> GetRolePemissions(int roleId);
        PermissionRole GetPermissionRole(int permissionid, int roleId);
        int CheckPermissionRole(string permissionName, string roleName);
        void SavePermissionRoles(List<PermissionRole> permissionRoles);
    }
}
