﻿using NPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TVG.Core.Caching;
using TVG.Core.Helper;
using TVG.Data.Domain;

namespace TVG.Data.Services
{
    public class MySQLBannerService : BaseService, IBannerService
    {
        public MySQLBannerService(IDatabase database, ICacheManager cache)
        {
            this.db = new Database(connectionStringName:"MySqlConnection");
            this.cache = cache;
            this.cacheDatabaseNumber = ConstantDatas.CACHE_REDIS_DATABASE_DEFAULT;
        }

        public Banner GetBannerById(int id)
        {
            string key = string.Format(ConstantDatas.CACHE_BANNER_BY_ID, id);
            var obj = CacheManager.GetOrAdd<Banner>(
                key,
                () =>
                {
                    return db.SingleOrDefaultById<Banner>(id);
                },
                CacheHelper.CacheDurationLong
            );
            return obj;
        }

        public Banner GetBannerBySlug(string slug)
        {
            string key = string.Format(ConstantDatas.CACHE_BANNER_BY_SLUG, slug);
            var obj = CacheManager.GetOrAdd<Banner>(
                ConstantDatas.CACHE_BANNER_PATTERN,
                key,
                () =>
                {
                    return db.SingleOrDefault<Banner>("WHERE Slug=@0", slug);
                },
                CacheHelper.CacheDurationLong
            );
            return obj;
        }

        public List<Banner> GetBanners(IDictionary<string, object> parameters)
        {
            var isDeleted = ValueHelper.TryGet(parameters, "isDeleted", false);
            string key = string.Format(ConstantDatas.CACHE_BANNERS, isDeleted);
            var obj = CacheManager.GetOrAdd<List<Banner>>(
                key,
                () =>
                {
                    return db.Fetch<Banner>("WHERE IsDeleted=@0", isDeleted);
                },
                CacheHelper.CacheDurationLong
            );
            return obj;
        }

        public int InsertBanner(Banner banner)
        {
            db.Insert<Banner>(banner);
            AfterInsert(banner);
            return banner.Id;
        }

        public int InsertBanner(Banner banner, List<BannerImage> images)
        {
            db.Insert<Banner>(banner);
            if (banner.Id > 0 && images != null && images.Count > 0)
            {
                foreach (var img in images)
                {
                    img.BannerId = banner.Id;
                }
                db.InsertBatch<BannerImage>(images);
            }
            AfterInsert(banner);
            return banner.Id;
        }

        public int UpdateBanner(Banner banner)
        {
            AfterUpdate(banner);
            return db.Update(banner);
        }

        public int UpdateBanner(Banner banner, List<BannerImage> images)
        {
            int result = 0;
            using (var transaction = db.GetTransaction())
            {
                var updateResult = db.Update(banner);
                if (updateResult > 0)
                {
                    db.Delete<BannerImage>("WHERE BannerId = @0", banner.Id);
                    if (images.Count > 0)
                    {
                        db.InsertBatch<BannerImage>(images);
                    }
                }
                transaction.Complete();
                AfterUpdate(banner);
                result = updateResult;
            }
            return result;
        }

        public void DeleteBanner(int id)
        {
            var banner = GetBannerById(id);
            if (banner != null)
            {
                banner.IsDeleted = true;
                db.Update(banner);
                AfterDelete(banner);
            }
        }

        public List<BannerImage> GetBannerImages(int bannerId)
        {
            return db.Fetch<BannerImage>("SELECT * FROM BannerImage WHERE BannerId = " + bannerId.ToString() + " ORDER BY DisplayOrder");
            //string key = String.Format(Constants.CACHE_BANNER_IMAGES, bannerId);
            //var obj = CacheManager.GetOrAdd<List<BannerImage>>(
            //    key,
            //    () => { return db.Fetch<BannerImage>("SELECT * FROM BannerImage WHERE BannerId = " + bannerId.ToString() + " ORDER BY DisplayOrder"); }  
            //);
            //return obj;
        }

        private void AfterInsert(Banner banner)
        {
            CacheManager.RemoveByPattern(ConstantDatas.CACHE_BANNER_PATTERN);
        }

        private void AfterUpdate(Banner banner)
        {
            CacheManager.RemoveByPattern(ConstantDatas.CACHE_BANNER_PATTERN);
        }

        private void AfterDelete(Banner banner)
        {
            CacheManager.RemoveByPattern(ConstantDatas.CACHE_BANNER_PATTERN);
        }
    }
}
