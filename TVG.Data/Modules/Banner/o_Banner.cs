﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NPoco;

namespace TVG.Data.Domain
{
    [TableName("[dbo].Banner")]
    [PrimaryKey("Id", AutoIncrement = true)]
    public class Banner
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Slug { get; set; }
        public bool IsDeleted { get; set; }
    }

    [TableName("[dbo].BannerImage")]
    [PrimaryKey("Id", AutoIncrement = true)]
    public class BannerImage
    {
        public int Id { get; set; }
        public int BannerId { get; set; }
        public string Image { get; set; }
        public string Link { get; set; }
        public int DisplayOrder { get; set; }
    }
}
