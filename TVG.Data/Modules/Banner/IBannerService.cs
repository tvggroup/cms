﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TVG.Data.Domain;

namespace TVG.Data.Services
{
    public interface IBannerService
    {
        Banner GetBannerById(int id);
        Banner GetBannerBySlug(string slug);
        List<Banner> GetBanners(IDictionary<string, object> parameters);
        int InsertBanner(Banner banner);
        int InsertBanner(Banner banner, List<BannerImage> images);
        int UpdateBanner(Banner banner);
        int UpdateBanner(Banner banner, List<BannerImage> images);
        void DeleteBanner(int id);

        List<BannerImage> GetBannerImages(int bannerId);
    }
}
