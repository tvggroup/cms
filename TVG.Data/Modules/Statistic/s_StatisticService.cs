﻿using NPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TVG.Core.Caching;
using TVG.Core.Helper;

namespace TVG.Data.Services
{
    

    public class StatisticService : IStatisticService
    {
        private IDatabase db;
        private ICacheManager cache;

        public StatisticService(IDatabase database, ICacheManager cache)
        {
            this.db = database;
            this.cache = cache;
        }

        #region Thống kê bài viết theo user
        public List<Dictionary<string, object>> GetUserLawDocumentCount(Dictionary<string, object> args)
        {
            DateTime fromDate = ValueHelper.TryGet(args, "fromDate", new DateTime(2016, 1, 1));
            DateTime toDate = ValueHelper.TryGet(args, "toDate", DateTime.Now);

            List<Dictionary<string, object>> result = new List<Dictionary<string, object>>();
            Sql sql = new Sql();
            sql.Select("COUNT(d.Id) As UserPosts, d.CreatedBy, u.Username");
            sql.From("LawDocument d");
            sql.InnerJoin("AspnetUsers u").On("d.CreatedBy =  u.Id");
            sql.Where("d.CreatedDate >= @0", fromDate);
            sql.Where("d.CreatedDate <= @0", toDate);
            sql.GroupBy("d.CreatedBy, u.Username");
            sql.OrderBy("UserPosts DESC");

            result = db.Fetch<Dictionary<string, object>>(sql);
            return result;
        }

        public List<Dictionary<string, object>> GetUserPostCount(Dictionary<string, object> args)
        {
            DateTime fromDate = ValueHelper.TryGet(args, "fromDate", new DateTime(2016, 1, 1));
            DateTime toDate = ValueHelper.TryGet(args, "toDate", DateTime.Now);

            List<Dictionary<string, object>> result = new List<Dictionary<string, object>>();
            Sql sql = new Sql();
            sql.Select("COUNT(d.Id) As UserPosts, d.CreatedBy, u.Username");
            sql.From("Posts d");
            sql.InnerJoin("AspnetUsers u").On("d.CreatedBy =  u.Id");
            sql.Where("d.CreatedDate >= @0", fromDate);
            sql.Where("d.CreatedDate <= @0", toDate);
            sql.Where("d.PostType = 'post'");
            sql.GroupBy("d.CreatedBy, u.Username");
            sql.OrderBy("UserPosts DESC");

            result = db.Fetch<Dictionary<string, object>>(sql);
            return result;
        }

        public List<Dictionary<string, object>> GetUserDocumentCount(Dictionary<string, object> args)
        {

            DateTime fromDate = ValueHelper.TryGet(args, "fromDate", new DateTime(2016, 1, 1));
            DateTime toDate = ValueHelper.TryGet(args, "toDate", DateTime.Now);

            List<Dictionary<string, object>> result = new List<Dictionary<string, object>>();
            Sql sql = new Sql();
            sql.Select("COUNT(d.Id) As UserPosts, d.CreatedBy, u.Username");
            sql.From("Document d");
            sql.InnerJoin("AspnetUsers u").On("d.CreatedBy =  u.Id");
            sql.Where("d.CreatedDate >= @0", fromDate);
            sql.Where("d.CreatedDate <= @0", toDate);
            sql.GroupBy("d.CreatedBy, u.Username");
            sql.OrderBy("UserPosts DESC");

            result = db.Fetch<Dictionary<string, object>>(sql);
            return result;
        }

        public List<Dictionary<string, object>> GetUserDocumentPostCount(Dictionary<string, object> args)
        {

            DateTime fromDate = ValueHelper.TryGet(args, "fromDate", new DateTime(2016, 1, 1));
            DateTime toDate = ValueHelper.TryGet(args, "toDate", DateTime.Now);

            List<Dictionary<string, object>> result = new List<Dictionary<string, object>>();
            Sql sql = new Sql();
            sql.Select("COUNT(d.Id) As UserPosts, d.CreatedBy, u.Username");
            sql.From("DocumentPost d");
            sql.InnerJoin("AspnetUsers u").On("d.CreatedBy =  u.Id");
            sql.Where("d.CreatedDate >= @0", fromDate);
            sql.Where("d.CreatedDate <= @0", toDate);
            sql.GroupBy("d.CreatedBy, u.Username");
            sql.OrderBy("UserPosts DESC");

            result = db.Fetch<Dictionary<string, object>>(sql);
            return result;
        }

        public List<Dictionary<string, object>> GetUserDocumentCaseCount(Dictionary<string, object> args)
        {

            DateTime fromDate = ValueHelper.TryGet(args, "fromDate", new DateTime(2016, 1, 1));
            DateTime toDate = ValueHelper.TryGet(args, "toDate", DateTime.Now);

            List<Dictionary<string, object>> result = new List<Dictionary<string, object>>();
            Sql sql = new Sql();
            sql.Select("COUNT(d.Id) As UserPosts, d.CreatedBy, u.Username");
            sql.From("DocumentCase d");
            sql.InnerJoin("AspnetUsers u").On("d.CreatedBy =  u.Id");
            sql.Where("d.CreatedDate >= @0", fromDate);
            sql.Where("d.CreatedDate <= @0", toDate);
            sql.GroupBy("d.CreatedBy, u.Username");
            sql.OrderBy("UserPosts DESC");

            result = db.Fetch<Dictionary<string, object>>(sql);
            return result;
        }
        #endregion

        #region Thống kê rà soát / tình trạng văn bản theo user
        public List<Dictionary<string, object>> GetUserLawDocumentControlTagCount(Dictionary<string, object> args)
        {
            DateTime fromDate = ValueHelper.TryGet(args, "fromDate", new DateTime(2016, 1, 1));
            DateTime toDate = ValueHelper.TryGet(args, "toDate", DateTime.Now);
            int controlTagId = ValueHelper.TryGet(args, "controlTagId", 0);

            List<Dictionary<string, object>> result = new List<Dictionary<string, object>>();
            Sql sql = new Sql();
            sql.Select("COUNT(*) As UserPosts, d.CreatedBy, u.Username");
            sql.From("LawDocumentControlTag d");
            sql.InnerJoin("AspnetUsers u").On("d.CreatedBy =  u.Id");
            sql.Where("d.CreatedDate >= @0", fromDate);
            sql.Where("d.CreatedDate <= @0", toDate);
            if(controlTagId > 0)
            {
                sql.Where("d.ControlTagId = @0", controlTagId);
            }
            sql.GroupBy("d.CreatedBy, u.Username");
            sql.OrderBy("UserPosts DESC");

            result = db.Fetch<Dictionary<string, object>>(sql);
            return result;
        }

        public List<Dictionary<string, object>> GetUserDocumentControlTagCount(Dictionary<string, object> args)
        {
            DateTime fromDate = ValueHelper.TryGet(args, "fromDate", new DateTime(2016, 1, 1));
            DateTime toDate = ValueHelper.TryGet(args, "toDate", DateTime.Now);
            int controlTagId = ValueHelper.TryGet(args, "controlTagId", 0);

            List<Dictionary<string, object>> result = new List<Dictionary<string, object>>();
            Sql sql = new Sql();
            sql.Select("COUNT(*) As UserPosts, d.CreatedBy, u.Username");
            sql.From("DocumentControlTag d");
            sql.InnerJoin("AspnetUsers u").On("d.CreatedBy =  u.Id");
            sql.Where("d.CreatedDate >= @0", fromDate);
            sql.Where("d.CreatedDate <= @0", toDate);
            if (controlTagId > 0)
            {
                sql.Where("d.ControlTagId = @0", controlTagId);
            }
            sql.GroupBy("d.CreatedBy, u.Username");
            sql.OrderBy("UserPosts DESC");

            result = db.Fetch<Dictionary<string, object>>(sql);
            return result;
        }

        public List<Dictionary<string, object>> GetUserDocumentCaseControlTagCount(Dictionary<string, object> args)
        {
            DateTime fromDate = ValueHelper.TryGet(args, "fromDate", new DateTime(2016, 1, 1));
            DateTime toDate = ValueHelper.TryGet(args, "toDate", DateTime.Now);
            int controlTagId = ValueHelper.TryGet(args, "controlTagId", 0);

            List<Dictionary<string, object>> result = new List<Dictionary<string, object>>();
            Sql sql = new Sql();
            sql.Select("COUNT(*) As UserPosts, d.CreatedBy, u.Username");
            sql.From("DocumentCaseControlTag d");
            sql.InnerJoin("AspnetUsers u").On("d.CreatedBy =  u.Id");
            sql.Where("d.CreatedDate >= @0", fromDate);
            sql.Where("d.CreatedDate <= @0", toDate);
            if (controlTagId > 0)
            {
                sql.Where("d.ControlTagId = @0", controlTagId);
            }
            sql.GroupBy("d.CreatedBy, u.Username");
            sql.OrderBy("UserPosts DESC");

            result = db.Fetch<Dictionary<string, object>>(sql);
            return result;
        }

        public List<Dictionary<string, object>> GetUserDocumentPostControlTagCount(Dictionary<string, object> args)
        {
            DateTime fromDate = ValueHelper.TryGet(args, "fromDate", new DateTime(2016, 1, 1));
            DateTime toDate = ValueHelper.TryGet(args, "toDate", DateTime.Now);
            int controlTagId = ValueHelper.TryGet(args, "controlTagId", 0);

            List<Dictionary<string, object>> result = new List<Dictionary<string, object>>();
            Sql sql = new Sql();
            sql.Select("COUNT(*) As UserPosts, d.CreatedBy, u.Username");
            sql.From("DocumentPostControlTag d");
            sql.InnerJoin("AspnetUsers u").On("d.CreatedBy =  u.Id");
            sql.Where("d.CreatedDate >= @0", fromDate);
            sql.Where("d.CreatedDate <= @0", toDate);
            if (controlTagId > 0)
            {
                sql.Where("d.ControlTagId = @0", controlTagId);
            }
            sql.GroupBy("d.CreatedBy, u.Username");
            sql.OrderBy("UserPosts DESC");

            result = db.Fetch<Dictionary<string, object>>(sql);
            return result;
        } 
        #endregion
    }
}
