﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVG.Data.Services
{
    public interface IStatisticService
    {
        List<Dictionary<string, object>> GetUserPostCount(Dictionary<string, object> args);
        List<Dictionary<string, object>> GetUserLawDocumentCount(Dictionary<string, object> args);
        List<Dictionary<string, object>> GetUserDocumentCount(Dictionary<string, object> args);
        List<Dictionary<string, object>> GetUserDocumentPostCount(Dictionary<string, object> args);
        List<Dictionary<string, object>> GetUserDocumentCaseCount(Dictionary<string, object> args);

        List<Dictionary<string, object>> GetUserLawDocumentControlTagCount(Dictionary<string, object> argrs);
        List<Dictionary<string, object>> GetUserDocumentControlTagCount(Dictionary<string, object> argrs);
        List<Dictionary<string, object>> GetUserDocumentCaseControlTagCount(Dictionary<string, object> argrs);
        List<Dictionary<string, object>> GetUserDocumentPostControlTagCount(Dictionary<string, object> argrs);
    }
}
