﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TVG.Data.Domain;

namespace TVG.Data.Services
{
    public interface IMenuService
    {
        //menu
        Menu GetMenuById(int id);
        Menu GetMenuBySlug(string slug);
        List<Menu> GetMenus(IDictionary<string, object> parameters);
        int InsertMenu(Menu item);
        int UpdateMenu(Menu item);
        void DeleteMenu(int id);

        //menu item
        MenuItem GetMenuItemById(int id);
        List<MenuItem> GetMenuItems(IDictionary<string, object> parameters);
        int InsertMenuItem(MenuItem item);
        int UpdateMenuItem(MenuItem item);
        void DeleteMenuItem(int id);
        void DeleteMenuItems(string ids);
    }
}
