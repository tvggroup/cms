﻿using NPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVG.Data.Domain
{
    [TableName("[dbo].[Menu]")]
    [PrimaryKey("Id", AutoIncrement = true)]
    public class Menu
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Slug { get; set; }
        public bool IsDeleted { get; set; }
    }

    [TableName("[dbo].[MenuItem]")]
    [PrimaryKey("Id", AutoIncrement = true)]
    public class MenuItem
    {
        public int Id { get; set; }
        public int MenuId { get; set; }
        public int ParentId { get; set; }
        public string Text { get; set; }
        public string Slug { get; set; }
        public string Url { get; set; }
        public string CustomClass { get; set; }
        public int DisplayOrder { get; set; }
        public bool Enabled { get; set; }

        [Ignore]
        public int Level { get; set; }
    }
}
