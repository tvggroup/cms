﻿using NPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TVG.Core.Caching;
using TVG.Core.Helper;
using TVG.Data.Domain;

namespace TVG.Data.Services
{
    

    public class MenuService : IMenuService
    {
        private IDatabase db;
        private ICacheManager cache;

        public MenuService(IDatabase database, ICacheManager cache)
        {
            this.db = database;
            this.cache = cache;
        }

        public Menu GetMenuById(int id)
        {
            return db.SingleOrDefaultById<Menu>(id);
        }

        public Menu GetMenuBySlug(string slug)
        {
            return db.SingleOrDefault<Menu>("WHERE Slug=@0", slug);
        }

        public List<Menu> GetMenus(IDictionary<string, object> parameters)
        {
            bool isDeleted = ValueHelper.TryGet(parameters, "isDeleted", false);
            int pageIndex = ValueHelper.TryGet(parameters, "pageIndex", 1);
            int pageSize = ValueHelper.TryGet(parameters, "pageSize", int.MaxValue);

            var list = db.Fetch<Menu>("WHERE IsDeleted=@0", isDeleted);
            return list;
        }

        public int InsertMenu(Menu item)
        {
            db.Insert<Menu>(item);
            return item.Id;
        }

        public int UpdateMenu(Menu item)
        {
            return db.Update(item);
        }

        public void DeleteMenu(int id)
        {
            var item = GetMenuById(id);
            if (item != null)
            {
                //set IsDelete=true, không xoá thực sự
                item.IsDeleted = true;
                db.Update(item);
            }
        }

        public MenuItem GetMenuItemById(int id)
        {
            return db.SingleOrDefaultById<MenuItem>(id);
        }

        public List<MenuItem> GetMenuItems(IDictionary<string, object> parameters)
        {
            int menuId = ValueHelper.TryGet(parameters, "menuId", 0);
            int parentId = ValueHelper.TryGet(parameters, "parentId", 0);
            bool getTree = ValueHelper.TryGet(parameters, "getTree", false);
            bool getEnabledOnly = ValueHelper.TryGet(parameters, "enabledOnly", false);
            List<MenuItem> list = new List<MenuItem>();
            if(!getEnabledOnly)
            {
                string sqlQuery = "SELECT * FROM MenuItem mi INNER JOIN Menu m ON mi.MenuId = m.Id WHERE MenuId=@0 AND m.IsDeleted=0 ORDER BY DisplayOrder";
                list = db.Query<MenuItem>(sqlQuery, menuId).ToList();
            }
            else
            {
                string sqlQuery = "SELECT * FROM MenuItem mi INNER JOIN Menu m ON mi.MenuId = m.Id WHERE MenuId=@0 AND m.Enabled=1 AND m.IsDeleted=0 ORDER BY DisplayOrder";
                list = db.Query<MenuItem>(sqlQuery, menuId).ToList();
            }
            BuidTree(list, parentId);
            return list;
        }

        /// <summary>
        /// Build list as a flat tree
        /// </summary>
        /// <param name="list">Original list</param>
        /// <param name="pId">Parent ID</param>
        /// <returns></returns>
        private List<MenuItem> BuidTree(List<MenuItem> list, int pId)
        {
            var tree = new List<MenuItem>();
            List<int> excludedList = new List<int>();

            var nodes = list.Where(x => x.ParentId == pId);
            MenuItem parent = null;
            if (pId != 0)
                parent = list.Where(x => x.Id == pId).SingleOrDefault();
            foreach (var n in nodes)
            {
                if (parent == null)
                {
                    n.Level = 0;
                }
                else
                {
                    n.Level = parent.Level + 1;
                }
                tree.Add(n);
                tree.AddRange(BuidTree(list, n.Id));
            }
            return tree;
        }

        public int InsertMenuItem(MenuItem item)
        {
            db.Insert(item);
            return item.Id;
        }

        public int UpdateMenuItem(MenuItem item)
        {
             return db.Update(item);           
        }

        public void DeleteMenuItem(int id)
        {
            db.Delete<MenuItem>(id);
        }

        public void DeleteMenuItems(string ids)
        {
            db.Delete<MenuItem>("WHERE Id IN (" + ids + ")");
        }
    }
}
