﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TVG.Data.Domain;

namespace TVG.Data.Services
{
    public interface IControlTagService
    {
        ControlTag GetById(int id);
        List<ControlTag> GetControlTags(IDictionary<string, object> parameters);
        int Insert(ControlTag item);
        int Update(ControlTag item);
        void Delete(int id);
    }
}
