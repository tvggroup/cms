﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NPoco;

namespace TVG.Data.Domain
{
    [TableName("Tag")]
    [PrimaryKey("Id", AutoIncrement = true)]
    public class Tag
    {
        public int Id { get; set; }
        public int ParentId { get; set; }
        public string TagType { get; set; }
        public string Name { get; set; }
        public string Slug { get; set; }
        public string MetaKeyword { get; set; }
        public string MetaDescription { get; set; }
        public bool IsDeleted { get; set; }
    }
}
