﻿using NPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TVG.Data.Domain;

namespace TVG.Data.Services
{
    public interface ITagService
    {
        Tag GetById(int id);
        Page<Tag> GetTags(IDictionary<string, object> parameters);
        Tag GetTagBySlug(string slug, string tagType = "");
        int Insert(Tag item);
        int Update(Tag item);
        void Delete(int id);
    }
}
