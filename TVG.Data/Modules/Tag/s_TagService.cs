﻿using NPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TVG.Core.Caching;
using TVG.Core.Helper;
using TVG.Data.Domain;

namespace TVG.Data.Services
{   

    public class TagService : BaseService, ITagService
    {

        public TagService(IDatabase db, ICacheManager cache)
        {
            this.db = db;
            this.cache = cache;
            this.cacheDatabaseNumber = ConstantDatas.CACHE_REDIS_DATABASE_DEFAULT;
        }

        public Tag GetById(int id)
        {
            string key = CacheHelper.SetCacheName(ConstantDatas.CACHE_TAG_BY_ID, id);
            return CacheManager.GetOrAdd<Tag>(ConstantDatas.CACHE_TAG_PATTERN, key, () =>
            {
                return db.SingleOrDefaultById<Tag>(id);
            },
            CacheHelper.CacheDurationLong);
        }

        public Tag GetTagBySlug(string slug, string tagType = "")
        {
            string key = CacheHelper.SetCacheName(ConstantDatas.CACHE_TAG_BY_SLUG, tagType, slug);
            return CacheManager.GetOrAdd<Tag>(ConstantDatas.CACHE_TAG_PATTERN, key, () =>
            {
                if(!string.IsNullOrEmpty(tagType))
                {
                    return db.FirstOrDefault<Tag>("Where Slug=@0 AND TagType=@1", slug, tagType);
                }
                else
                {
                    return db.FirstOrDefault<Tag>("Where Slug=@0", slug);
                }
                
            },
            CacheHelper.CacheDurationLong);
        }

        public Page<Tag> GetTags(IDictionary<string, object> parameters)
        {
            string type = ValueHelper.TryGet(parameters, "type", "");
            bool isDeleted = ValueHelper.TryGet(parameters, "isDeleted", false);
            int pageIndex = ValueHelper.TryGet(parameters, "pageIndex", 1);
            int pageSize = ValueHelper.TryGet(parameters, "pageSize", int.MaxValue);
            string keyword = ValueHelper.TryGet(parameters, "keyword", "");
            
            string key = CacheHelper.SetCacheName(ConstantDatas.CACHE_TAG_ALL, type, keyword, isDeleted, pageIndex, pageSize);

            var obj = CacheManager.GetOrAdd<Page<Tag>>(
                ConstantDatas.CACHE_TAXONOMY_PATTERN,
                key,
                () =>
                {
                    //string sqlQuery = "SELECT * FROM TaxonomyTerms t LEFT JOIN TaxonomyTermsEx te ON t.Id = te.Termid WHERE Taxonomy=@0 AND IsDeleted=@1 ORDER BY DisplayOrder";
                    Sql sql = new Sql();
                    sql.Select("*");
                    sql.From("Tag t");
                    if (!string.IsNullOrEmpty(keyword))
                    {
                        sql.InnerJoin(string.Format("FREETEXTTABLE(Tag, (Name), N'{0}', 1000) AS ft", keyword)).On("ft.[Key] = t.Id");
                    }
                    if (!string.IsNullOrEmpty(type))
                    {
                        sql.Where("TagType=@0", type);
                    }

                    sql.Where("IsDeleted=@0", isDeleted);
                    if (!string.IsNullOrEmpty(keyword))
                    {
                        sql.OrderBy("Rank DESC, Name");
                    }
                    else
                    {
                        sql.OrderBy("Name");
                    }

                    string sqlQuery = sql.SQL;
                    var tree = db.Page<Tag>(pageIndex, pageSize, sql);
                    return tree;
                },
                CacheHelper.CacheDurationLong
            );

            return obj;
        }

        public int Insert(Tag item)
        {
            var result = 0;
            using (var transaction = db.GetTransaction())
            {
                db.Insert(item);
                result = item.Id;
                db.CompleteTransaction();
                AfterInsert(item);
            }
            return result;
        }

        public int Update(Tag item)
        {
            int result = 0;
            result = db.Update(item);
            AfterUpdate(item);
            return result;
        }

        public void Delete(int id)
        {
            var item = GetById(id);
            if (item != null)
            {
                item.IsDeleted = true;
                db.Update(item);
                AfterDelete(item);
            }
        }

        private void AfterInsert(Tag item)
        {
            CacheManager.RemoveByGroup(ConstantDatas.CACHE_TAG_PATTERN);
        }

        private void AfterUpdate(Tag item)
        {
            CacheManager.RemoveByGroup(ConstantDatas.CACHE_TAG_PATTERN);
        }

        private void AfterDelete(Tag item)
        {
            CacheManager.RemoveByGroup(ConstantDatas.CACHE_TAG_PATTERN);
        }
    }
}
