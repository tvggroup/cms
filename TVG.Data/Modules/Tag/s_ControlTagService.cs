﻿using NPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TVG.Core.Caching;
using TVG.Data.Domain;

namespace TVG.Data.Services
{
    

    public class ControlTagService : IControlTagService
    {
        IDatabase db;
        ICacheManager cache;

        public ControlTagService(IDatabase db, ICacheManager cache)
        {
            this.db = db;
            this.cache = cache;
        }

        public ControlTag GetById(int id)
        {
            return db.SingleOrDefaultById<ControlTag>(id);
        }

        public List<ControlTag> GetControlTags(IDictionary<string, object> parameters)
        {
            return db.Fetch<ControlTag>("WHERE IsDeleted=0 ORDER BY Name");
        }

        public int Insert(ControlTag item)
        {
            db.Insert<ControlTag>(item);
            return item.Id;
        }

        public int Update(ControlTag item)
        {
            return db.Update(item);
        }

        public void Delete(int id)
        {
            var item = GetById(id);
            if (item != null)
            {
                item.IsDeleted = true;
                db.Update(item);
            }
        }
    }
}
