﻿using NPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TVG.Core.Caching;
using TVG.Core.Helper;
using TVG.Data.Domain;

namespace TVG.Data.Services
{
    public class SettingService : BaseService, ISettingService
    {
        public SettingService(IDatabase database, ICacheManager cache)
        {
            this.db = database;
            this.cache = cache;
            this.cacheDatabaseNumber = ConstantDatas.CACHE_REDIS_DATABASE_DEFAULT;
        }

        public Setting GetSettingById(int id)
        {
            string key = CacheHelper.SetCacheName(ConstantDatas.CACHE_SETTING_BY_ID, id);
            var obj = CacheManager.GetOrAdd<Setting>(
                ConstantDatas.CACHE_SETTING_PATTERN,
                key,
                () => { return db.SingleOrDefaultById<Setting>(id); },
                CacheHelper.CacheDurationLong
            );
            return obj;
        }

        public Setting GetSettingByName(string name)
        {
            string key = CacheHelper.SetCacheName(ConstantDatas.CACHE_SETTING_BY_NAME, name);
            var obj = CacheManager.GetOrAdd<Setting>(
                ConstantDatas.CACHE_SETTING_PATTERN,
                key,
                () => { return db.SingleOrDefault<Setting>("WHERE SettingName=@0", name); },
                CacheHelper.CacheDurationLong
            );
            return obj;
        }

        public List<Setting> GetSettings(string groupName = "")
        {
            string key = CacheHelper.SetCacheName(ConstantDatas.CACHE_SETTING_BY_GROUP, groupName);
            var obj = CacheManager.GetOrAdd<List<Setting>>(
                ConstantDatas.CACHE_SETTING_PATTERN,
                key,
                () =>
                {
                    string sql = "SELECT * FROM [Settings] ";
                    if (!string.IsNullOrEmpty(groupName))
                    {
                        sql += "WHERE SettingGroup=@0";
                    }

                    return db.Fetch<Setting>(sql, groupName);
                },
                CacheHelper.CacheDurationLong
            );
            return obj;
        }

        public string GetSettingValue(string name)
        {
            string value = "";
            var setting = GetSettingByName(name);
            if(setting != null)
            {
                value = ValueHelper.TryGet(setting.SettingValue, "");
            }
            return value;
        }

        public int InsertSetting(Setting opt)
        {
            db.Insert<Setting>(opt);
            AfterInsert(opt);
            return opt.Id;
        }

        public void InsertSettings(List<Setting> opts)
        {
            db.InsertBatch<Setting>(opts);
        }

        public int UpdateSetting(Setting opt)
        {
            AfterUpdate(opt);
            return db.Update(opt);
        }

        public void UpdateSettings(List<Setting> opts)
        {
            using (var transaction = db.GetTransaction())
            {
                foreach(var opt in opts)
                {
                    UpdateSetting(opt);
                }
                transaction.Complete();
            }
        }

        public void DeleteSetting(string name)
        {
            var obj = GetSettingByName(name);
            if (obj != null)
            {
                db.Delete<Setting>("WHERE SettingName=@0", name);
                AfterDelete(obj);
            }            
        }

        public void DeleteSetting(int id)
        {
            var obj = GetSettingById(id);
            if (obj != null)
            {
                db.Delete<Setting>("WHERE Id=@0", id);
                AfterDelete(obj);
            }
        }

        public void DeleteSettings(string groupName)
        {
            db.Delete<Setting>("WHERE SettingGroup=@0", groupName);
            AfterDeleteGroup(groupName);
        }

        private void AfterInsert(Setting obj)
        {
            CacheManager.RemoveByGroup(ConstantDatas.CACHE_SETTING_PATTERN);
        }

        private void AfterUpdate(Setting obj)
        {
            CacheManager.RemoveByGroup(ConstantDatas.CACHE_SETTING_PATTERN);
        }

        private void AfterDelete(Setting obj)
        {
            CacheManager.RemoveByGroup(ConstantDatas.CACHE_SETTING_PATTERN);
        }

        private void AfterDeleteGroup(string groupName)
        {
            CacheManager.RemoveByGroup(ConstantDatas.CACHE_SETTING_PATTERN);
        }

    }
}
