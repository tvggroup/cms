﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NPoco;

namespace TVG.Data.Domain
{
    [TableName("[dbo].[Settings]")]
    [PrimaryKey("Id", AutoIncrement = true)]
    public class Setting
    {
        public int Id { get; set; }
        public string SettingGroup { get; set; }
        public string SettingName { get; set; }
        public string SettingValue { get; set; }
    }
}
