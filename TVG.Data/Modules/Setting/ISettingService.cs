﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TVG.Data.Domain;

namespace TVG.Data.Services
{
    public interface ISettingService
    {
        List<Setting> GetSettings(string groupName = "");
        Setting GetSettingById(int id);
        Setting GetSettingByName(string name);
        string GetSettingValue(string name);
        int InsertSetting(Setting opt);
        void InsertSettings(List<Setting> opts);
        int UpdateSetting(Setting opt);
        void UpdateSettings(List<Setting> opts);
        void DeleteSetting(int id);
        void DeleteSetting(string name);
        void DeleteSettings(string groupName);
    }
}
