﻿using NPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TVG.Data.Services;

namespace TVG.Data.Domain
{
    [TableName("[dbo].[TaxonomyTerms]")]
    [PrimaryKey("Id", AutoIncrement = true)]
    public partial class TaxonomyTerm
    {
        public int Id { get; set; }
        public string Taxonomy { get; set; }
        public int ParentId { get; set; }
        public string Slug { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string MetaKeyword { get; set; }
        public string MetaDescription { get; set; }
        public string Image { get; set; }
        public int DisplayOrder { get; set; }
        public bool IsDeleted { get; set; }
                
        private string path;
        [Ignore]
        public string Path
        {
            get
            {
                return string.IsNullOrEmpty(path) ? Name : path;
            }
            set
            {
                path = value;
            }
        }

        [Ignore]
        public int Level { get; set; }
        
        [Ignore]
        public List<TaxonomyTerm> Children { get; set; }
        
        [Ignore]
        public TaxonomyTerm Parent { get; set; }

        [Ignore]
        public List<TaxonomyTermMeta> Metas { get; set; }
        
        [ResultColumn]
        [Reference(ReferenceType.OneToOne, ColumnName = "Id", ReferenceMemberName = "TermId")]
        public TaxonomyTermEx ExtraInfo { get; set; }
    }

    [TableName("[dbo].[TaxonomyTermsEx]")]
    [PrimaryKey("TermId", AutoIncrement = false)]
    public class TaxonomyTermEx
    {
        public int TermId { get; set; }
        public int LawDocumentCount { get; set; }
        public int DocumentCount { get; set; }
        public int DocumentCaseCount { get; set; }
        public int DocumentPostCount { get; set; }
        public int PostCount { get; set; }
    }

    [TableName("TaxonomyTermMeta")]
    [PrimaryKey("Id", AutoIncrement = true)]
    public class TaxonomyTermMeta : BaseMeta
    {
        public int Id { get; set; }
        public int TermId { get; set; }
    }
}
