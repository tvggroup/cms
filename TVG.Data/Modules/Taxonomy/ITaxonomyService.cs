﻿using NPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TVG.Data.Domain;

namespace TVG.Data.Services
{
    public interface ITaxonomyService
    {
        //Taxonomy
        Taxonomy GetTaxonomyByName(string name);
        //TaxonomyTerm
        TaxonomyTerm GetTermById(int id);
        TaxonomyTerm GetTermBySlug(string slug);
        TaxonomyTerm GetTermBySlug(string taxonomyType, string slug);
        List<TaxonomyTerm> GetTerms(string type);
        List<TaxonomyTerm> GetTerms(string type, bool isTree);
        Page<TaxonomyTerm> GetTerms(IDictionary<string, object> parameters);
        int UpdateTerm(TaxonomyTerm term);
        int InsertTerm(TaxonomyTerm term);
        void DeleteTerm(int id);

        int UpdateTermExCount(int termId, string columnName, int value);

        List<TaxonomyTermMeta> GetMetasByTermId(int termId);
        void InsertMetas(int termId, List<TaxonomyTermMeta> metas);
    }
}
