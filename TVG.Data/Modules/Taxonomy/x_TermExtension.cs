﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TVG.Data.Domain;

namespace TVG.Data.Extensions
{
    public class TermHelper
    {
        public static List<TaxonomyTermMeta> GetMetaList(string taxonomyType)
        {
            var metaList = new List<TaxonomyTermMeta>();
            switch(taxonomyType)
            {
                case "contact_tag":
                    metaList.Add(new TaxonomyTermMeta { MetaName = "color", MetaValue = "", MetaType = "color", DisplayName = "Màu sắc" });
                    break;
                case "doc_post_type":
                    metaList.Add(new TaxonomyTermMeta { MetaName = "color", MetaValue = "", MetaType = "color", DisplayName = "Màu sắc" });
                    break;

            }
            return metaList;
        }
    }
}
