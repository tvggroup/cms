﻿using NPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVG.Data.Domain
{
    [TableName("[dbo].[Taxonomy]")]
    [PrimaryKey("Id", AutoIncrement = true)]
    public class Taxonomy
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string DisplayName { get; set; }
        public bool IsHierarchy { get; set; }
        public bool HasThumb { get; set; }
    }
}
