﻿using NPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TVG.Core.Caching;
using TVG.Core.Helper;
using TVG.Data.Domain;

namespace TVG.Data.Services
{
    public class TaxonomyService : BaseService, ITaxonomyService
    {
        public TaxonomyService(IDatabase database, ICacheManager cache)
        {
            this.db = database;
            this.cache = cache;
            this.cacheDatabaseNumber = ConstantDatas.CACHE_REDIS_DATABASE_DEFAULT;
        }

        /// <summary>
        /// Get taxonomy type by name
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public Taxonomy GetTaxonomyByName(string name)
        {
            string key = CacheHelper.SetCacheName(ConstantDatas.CACHE_TAXONOMY_BY_NAME, name);
            return CacheManager.GetOrAdd<Taxonomy>(ConstantDatas.CACHE_TAXONOMY_PATTERN, key, () =>
            {
                return db.SingleOrDefault<Taxonomy>("where Name = @0", name);
            }, CacheHelper.CacheDurationLong);
        }

        /// <summary>
        /// Get term by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public TaxonomyTerm GetTermById(int id)
        {
            string key = CacheHelper.SetCacheName(ConstantDatas.CACHE_TAXONOMY_TERM_BY_ID, id);
            return CacheManager.GetOrAdd<TaxonomyTerm>(ConstantDatas.CACHE_TAXONOMY_PATTERN, key, () =>
            {
                return db.SingleOrDefaultById<TaxonomyTerm>(id);
            },
            CacheHelper.CacheDurationLong);
        }

        /// <summary>
        /// Get term by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public TaxonomyTerm GetTermBySlug(string slug)
        {
            string key = CacheHelper.SetCacheName(ConstantDatas.CACHE_TAXONOMY_TERM_BY_SLUG, slug);
            return CacheManager.GetOrAdd<TaxonomyTerm>(ConstantDatas.CACHE_TAXONOMY_PATTERN, key, () =>
            {
                return db.FirstOrDefault<TaxonomyTerm>("Where Slug=@0", slug);
            },
            CacheHelper.CacheDurationLong);
        }

        /// <summary>
        /// Get term by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public TaxonomyTerm GetTermBySlug(string taxonomyType, string slug)
        {
            string key = CacheHelper.SetCacheName(ConstantDatas.CACHE_TAXONOMY_TERM_BY_SLUG, taxonomyType, slug);
            return CacheManager.GetOrAdd<TaxonomyTerm>(ConstantDatas.CACHE_TAXONOMY_PATTERN, key, () =>
            {
                return db.FirstOrDefault<TaxonomyTerm>("Where Slug=@0 AND Taxonomy=@1", slug, taxonomyType);
            },
            CacheHelper.CacheDurationLong);
        }


        /// <summary>
        /// Get paged list of term by type
        /// </summary>
        /// <param name="type">Taxonomy type</param>
        /// <returns>Paged list</returns>
        public List<TaxonomyTerm> GetTerms(string type)
        {
            Dictionary<string, object> args = new Dictionary<string, object>() {
                { "type", type }
            };
            return GetTerms(args).Items;
        }

        /// <summary>
        /// Get paged list of term by type and build as tree or not
        /// </summary>
        /// <param name="type">Taxonomy type</param>
        /// <param name="isTree">Build tree or not</param>
        /// <returns>Paged list</returns>
        public List<TaxonomyTerm> GetTerms(string type, bool isTree)
        {
            Dictionary<string, object> args = new Dictionary<string, object>() {
                { "type", type },
                { "getTree", isTree }
            };
            return GetTerms(args).Items;
        }

        /// <summary>
        /// Get paged list of terms
        /// </summary>
        /// <param name="parameters">List of parameters</param>
        /// <returns></returns>
        public Page<TaxonomyTerm> GetTerms(IDictionary<string, object> parameters)
        {
            string type = ValueHelper.TryGet(parameters, "type", "category");
            int pId = ValueHelper.TryGet(parameters, "pId", 0);
            bool isDeleted = ValueHelper.TryGet(parameters, "isDeleted", false);
            int pageIndex = ValueHelper.TryGet(parameters, "pageIndex", 1);
            int pageSize = ValueHelper.TryGet(parameters, "pageSize", int.MaxValue);
            bool getTree = ValueHelper.TryGet(parameters, "getTree", false);
            string excluded = ValueHelper.TryGet(parameters, "excluded", "");
            string keyword = ValueHelper.TryGet(parameters, "keyword", "");

            //string sqlQuery = "SELECT * FROM TaxonomyTerms t LEFT JOIN TaxonomyTermsEx te ON t.Id = te.Termid WHERE Taxonomy=@0 AND IsDeleted=@1 ORDER BY DisplayOrder";
            Sql sql = new Sql();
            sql.Select("*");
            sql.From("TaxonomyTerms t");
            sql.LeftJoin("TaxonomyTermsEx te").On("t.Id = te.TermId");
            if (!string.IsNullOrEmpty(keyword))
            {
                sql.InnerJoin(string.Format("FREETEXTTABLE(TaxonomyTerms, (Name), N'{0}', 1000) AS ft", keyword)).On("ft.[Key] = t.Id");
            }
            sql.Where("Taxonomy=@0", type);
            sql.Where("IsDeleted=@0", isDeleted);
            if (!string.IsNullOrEmpty(keyword))
            {
                sql.OrderBy("Rank DESC, DisplayOrder");
            }
            else
            {
                sql.OrderBy("DisplayOrder");
            }

            string sqlQuery = sql.SQL;

            string key = CacheHelper.SetCacheName(ConstantDatas.CACHE_TAXONOMY_TERM_ALL, type, pId, keyword, getTree, excluded, isDeleted, pageIndex, pageSize);

            var obj = CacheManager.GetOrAdd<Page<TaxonomyTerm>>(
                ConstantDatas.CACHE_TAXONOMY_PATTERN,
                key,
                () =>
                {
                    //lấy data
                    //nếu là dạng cây (getTree = true) --> lấy hết dữ liệu và tự phân trang
                    //ngược lại --> phân trang bằng db.Page
                    //TODO: thêm cache
                    Page<TaxonomyTerm> tree;
                    if (getTree)
                    {
                        tree = new Page<TaxonomyTerm>();

                        //lấy danh sách
                        var list = db.Fetch<TaxonomyTerm>(sql);

                        //count
                        //var sqlCount = "SELECT COUNT(Id) FROM TaxonomyTerms WHERE Taxonomy=@0 AND IsDeleted=@1";
                        Sql sqlCount = new Sql();
                        sqlCount.Select("COUNT(Id)");
                        sqlCount.From("TaxonomyTerms t");
                        if (!string.IsNullOrEmpty(keyword))
                        {
                            sqlCount.InnerJoin(string.Format("FREETEXTTABLE(TaxonomyTerms, (Name), N'{0}', 1000) AS ft", keyword)).On("ft.[Key] = t.Id");
                        }
                        sqlCount.Where("Taxonomy=@0", type);
                        sqlCount.Where("IsDeleted=@0", isDeleted);
                        tree.TotalItems = db.ExecuteScalar<long>(sqlCount);

                        //tạo các giá trị bổ sung
                        tree.CurrentPage = pageIndex;
                        tree.ItemsPerPage = pageSize;
                        tree.TotalPages = tree.TotalItems / pageSize;
                        if ((tree.TotalItems % pageSize) != 0)
                            tree.TotalPages++;

                        //tạo cây
                        list = BuidTree(list, pId, excluded);

                        //lấy số item theo page
                        int offset = (pageIndex - 1) * pageSize;
                        tree.Items = list.Skip(offset).Take(pageSize).ToList();
                    }
                    else
                    {
                        tree = db.Page<TaxonomyTerm>(pageIndex, pageSize, sql);
                    }

                    return tree;
                },
                CacheHelper.CacheDurationLong
            );

            return obj;
        }

        /// <summary>
        /// Build list as a tree
        /// </summary>
        /// <param name="list">Original list</param>
        /// <param name="pId">Parent ID</param>
        /// <returns></returns>
        private List<TaxonomyTerm> BuidTree(List<TaxonomyTerm> list, int pId, string excluded)
        {
            var tree = new List<TaxonomyTerm>();
            List<int> excludedList = new List<int>();
            if (!string.IsNullOrEmpty(excluded))
            {
                excludedList = excluded.Split(',').Select(int.Parse).ToList();
            }
            var nodes = list.Where(x => x.ParentId == pId && !excludedList.Contains(x.Id));
            TaxonomyTerm parent = null;
            if (pId != 0)
                parent = list.Where(x => x.Id == pId).SingleOrDefault();
            foreach (var n in nodes)
            {
                if (parent == null)
                {
                    n.Path = n.Name;
                    n.Level = 0;
                }
                else
                {
                    n.Path = parent.Path + " / " + n.Name;
                    n.Level = parent.Level + 1;
                }
                tree.Add(n);
                tree.AddRange(BuidTree(list, n.Id, excluded));
            }
            return tree;
        }

        public int InsertTerm(TaxonomyTerm term)
        {
            var result = 0;
            using (var transaction = db.GetTransaction())
            {
                db.Insert(term);
                result = term.Id;
                var termInfo = new TaxonomyTermEx()
                {
                    TermId = result,
                    DocumentCaseCount = 0,
                    DocumentCount = 0,
                    DocumentPostCount = 0,
                    LawDocumentCount = 0,
                    PostCount = 0
                };
                db.Insert(termInfo);
                if (result > 0)
                {
                    if (term.Metas != null && term.Metas.Count > 0)
                    {
                        foreach (var meta in term.Metas)
                        {
                            meta.TermId = result;
                        }
                        InsertMetas(term.Id, term.Metas);
                    }
                }
                db.CompleteTransaction();
                AfterInsert(term);
            }
            return term.Id;
        }

        /// <summary>
        /// Update taxonomy term
        /// </summary>
        /// <param name="term"></param>
        /// <returns></returns>
        public int UpdateTerm(TaxonomyTerm term)
        {
            int result = 0;
            using (var transaction = db.GetTransaction())
            {
                result = db.Update(term);
                if (result > 0)
                {
                    InsertMetas(term.Id, term.Metas);
                }
                AfterUpdate(term);
                transaction.Complete();
            }

            return db.Update(term);
        }

        /// <summary>
        /// Delete taxonomy term
        /// </summary>
        /// <param name="id"></param>
        public void DeleteTerm(int id)
        {
            var term = GetTermById(id);
            if (term != null)
            {
                //đặt lại parent id cho các terms con
                string sql = "UPDATE TaxonomyTerms SET ParentId=0 WHERE ParentId=@0";
                db.ExecuteScalar<int>(sql, id);

                //set IsDelete=true, không xoá thực sự
                term.IsDeleted = true;
                db.Update(term);
                AfterDelete(term);
            }
        }

        public int UpdateTermExCount(int termId, string columnName, int value)
        {
            int result = 0;
            if (value == 0) return 0;
            StringBuilder sbSql = new StringBuilder("UPDATE TaxonomyTermsEx SET ");
            sbSql.Append(columnName).Append("=").Append(columnName);
            if (value > 0)
            {
                sbSql.Append("+").Append(value.ToString());
            }
            else
            {
                sbSql.Append("-").Append(Math.Abs(value).ToString());
            }
            sbSql.Append(" WHERE TermId = @0");
            result = db.ExecuteScalar<int>(sbSql.ToString(), termId);
            return result;
        }

        #region Meta

        public List<TaxonomyTermMeta> GetMetasByTermId(int termId)
        {
            string key = CacheHelper.SetCacheName(ConstantDatas.CACHE_TAXONOMY_TERM_META_BY_TERM_ID, termId);
            string group = string.Format(ConstantDatas.CACHE_TAXONOMY_PATTERN);
            return CacheManager.GetOrAdd(group, key, () =>
            {
                return db.Fetch<TaxonomyTermMeta>("WHERE TermId=@0", termId);
            }, CacheHelper.CacheDurationMedium);
        }

        public void InsertMetas(int termId, List<TaxonomyTermMeta> metas)
        {
            if (termId > 0)
            {
                db.Delete<TaxonomyTermMeta>("WHERE TermId=@0", termId);
                if (metas != null && metas.Count > 0)
                {
                    db.InsertBatch<TaxonomyTermMeta>(metas);
                }
            }
        }

        #endregion Meta

        private void AfterInsert(TaxonomyTerm term)
        {
            CacheManager.RemoveByGroup(ConstantDatas.CACHE_TAXONOMY_PATTERN);
        }

        private void AfterUpdate(TaxonomyTerm term)
        {
            CacheManager.RemoveByGroup(ConstantDatas.CACHE_TAXONOMY_PATTERN);
        }

        private void AfterDelete(TaxonomyTerm term)
        {
            CacheManager.RemoveByGroup(ConstantDatas.CACHE_TAXONOMY_PATTERN);
        }
    }
}