﻿using NPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TVG.Data.Extensions;

namespace TVG.Data.Domain
{
    [TableName("[dbo].[Posts]")]
    [PrimaryKey("Id", AutoIncrement = true)]
    public partial class Post
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Slug { get; set; }
        public string Description { get; set; }
        public string Content { get; set; }
        public string MetaKeyword { get; set; }
        public string MetaDescription { get; set; }
        public string Image { get; set; }
        public string PostType { get; set; }
        public int CreatedBy { get; set; }
        public int UpdatedBy { get; set; }
        public int PublishedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public DateTime PublishedDate { get; set; }
        public int PostStatus { get; set; }
        public bool CommentStatus { get; set; }
        public bool IsDeleted { get; set; }
        public string Note { get; set; }

        //các thông tin bổ sung từ bảng AspNetUsers
        public Dictionary<string, object> CreatedUser { get; set;}
        
        [ResultColumn]
        public int PageView { get; set; }
        [Ignore]
        public PostExt PostExt { get; set; }
        [Ignore]
        public List<PostTerm> Terms { get; set; }
        [Ignore]
        public string StatusName { get { return this.GetStatus().Name; } }
        [Ignore]
        public string StatusColor { get { return this.GetStatus().Color; } }
        [Ignore]
        public bool StatusShowInList { get { return this.GetStatus().ShowInList; } }
    }

    [TableName("PostExt")]
    [PrimaryKey("Id", AutoIncrement = false)]
    public class PostExt
    {
        public int Id { get; set; }
        public int PageView { get; set; }
    }

    [TableName("PostTag")]
    [PrimaryKey("Id", AutoIncrement = true)]
    public class PostTag
    {
        public int Id { get; set; }
        public int PostId { get; set; }
        public int TagId { get; set; }
        [ResultColumn]
        public string TagName { get; set; }
    }

    [TableName("[dbo].[PostMetas]")]
    [PrimaryKey("Id", AutoIncrement = true)]
    public partial class PostMeta
    {
        public int Id { get; set; }
        public int PostId { get; set; }
        public string MetaName { get; set; }
        public string MetaValue { get; set; }
    }

    [TableName("[dbo].[Post_Term]")]
    [PrimaryKey("PostId,TermId")]
    public partial class PostTerm
    {
        public int PostId { get; set; }
        public int TermId { get; set; }
        public int DisplayOrder { get; set; }

        [ResultColumn]
        public string TaxonomyType { get; set; }

        [ResultColumn]
        public string TermName { get; set; }

        [ResultColumn]
        public string TermSlug { get; set; }
    }

    public class PostSaveModel
    {
        public Post Post { get; set; }
        public List<PostMeta> Metas { get; set; }
        public List<PostTag> Tags { get; set; }
        public List<int> PostTermIds { get; set; }
        public List<PostTerm> Terms { get; set; }
    }
}
