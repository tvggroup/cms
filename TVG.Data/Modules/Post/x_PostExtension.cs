﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TVG.Data.Domain;

namespace TVG.Data.Extensions
{
    public static class PostExtension
    {
        public static PostStatus GetStatus(this Post post)
        {
            if (ConstantDatas.PostStatus.ContainsKey(post.PostStatus.ToString()))
            {
                return ConstantDatas.PostStatus[post.PostStatus.ToString()];
            }
            return ConstantDatas.PostStatus["-1"];
        }
    }
}
