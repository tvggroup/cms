﻿using NPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TVG.Data.Domain;

namespace TVG.Data.Services
{
    public interface IPostService
    {
        //post
        Post GetPostById(int id);
        Post GetPostBySlug(string slug);
        Page<Post> GetPosts(IDictionary<string, object> parameters);
        Page<Post> GetPostsByTag(IDictionary<string, object> parameters);
        int InsertPost(Post post);
        int InsertPost(Post post, List<int> categories);
        int InsertPost(PostSaveModel model);
        int UpdatePost(Post post);
        int UpdatePost(Post post, List<int> categories);
        int UpdatePost(PostSaveModel model);
        void DeletePost(int id);

        //post term
        List<PostTerm> GetPostTerms(Dictionary<string, object> parameters); 
        List<PostTerm> GetPostTermsByPostId(int postId);
        void InsertPostTerm(int postId, List<int> termIds);
        void InsertPostTerm(int postId, List<PostTerm> terms);

        //post tag
        List<PostTag> GetTags(int postId);
        void InsertTags(int postId, List<PostTag> tags);

        PostMeta GetMetaById(int id);
        List<PostMeta> GetMetasByPostId(int postId);
        void InsertMetas(int postId, List<PostMeta> metas);

        //Post ext
        PostExt GetPostExtById(int id);
        int UpdatePostExt(int id, string columnName, int value);
    }
}
