﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NPoco;
using TVG.Data.Domain;
using TVG.Core.Caching;
using TVG.Core.Helper;

namespace TVG.Data.Services
{
    public class PostService : BaseService, IPostService
    {
        private ITaxonomyService taxonomyService;

        public PostService(IDatabase database, ICacheManager cache, ITaxonomyService taxonomyService)
        {
            this.db = database;
            this.cache = cache;
            this.taxonomyService = taxonomyService;
            this.cacheDatabaseNumber = ConstantDatas.CACHE_REDIS_DATABASE_POSTS;
        }        

        /// <summary>
        /// Get post by id
        /// </summary>
        /// <param name="id">post id</param>
        /// <returns></returns>
        public Post GetPostById(int id)
        {
            string key = CacheHelper.SetCacheName(ConstantDatas.CACHE_POST_BY_ID, id);
            string group = CacheHelper.SetCacheName(ConstantDatas.CACHE_POST_GROUP_BY_ID, id);
            return CacheManager.GetOrAdd(group, key, () => {
                return db.SingleOrDefaultById<Post>(id);
            }, CacheHelper.CacheDurationMedium);            
        }

        /// <summary>
        /// Get post by slug
        /// </summary>
        /// <param name="id">post id</param>
        /// <returns></returns>
        public Post GetPostBySlug(string slug)
        {
            string key = CacheHelper.SetCacheName(ConstantDatas.CACHE_POST_BY_SLUG, slug);
            return CacheManager.GetOrAdd(key, () => {
                return db.FirstOrDefault<Post>("WHERE Slug = @0", slug);
            }, CacheHelper.CacheDurationMedium);            
        }

        /// <summary>
        /// Get paged list of posts
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public Page<Post> GetPosts(IDictionary<string, object> parameters)
        {
            string type = ValueHelper.TryGet(parameters, "postType", "post");
            //lấy các posts theo term ids (vd: "1,3,4,5,...")
            string termIds = ValueHelper.TryGet(parameters, "termId", "");
            int status = ValueHelper.TryGet(parameters, "status", -1);
            string keyword = ValueHelper.TryGet(parameters, "keyword", "");
            bool isDeleted = ValueHelper.TryGet(parameters, "isDeleted", false);
            int pageIndex = ValueHelper.TryGet(parameters, "pageIndex", 1);
            int pageSize = ValueHelper.TryGet(parameters, "pageSize", int.MaxValue);
            int userId = ValueHelper.TryGet(parameters, "userId", 0);
            
            string columns = @"Id, Title, Slug, Description, MetaKeyword, MetaDescription, 
                    Image, PostType, CreatedBy, UpdatedBy, PublishedBy, CreatedDate, UpdatedDate, PublishedDate, 
                    PostStatus, CommentStatus, IsDeleted";
            string orderBy = ValueHelper.TryGet(parameters, "orderBy", "Id DESC");

            string key = CacheHelper.SetCacheName(ConstantDatas.CACHE_POST_ALL, termIds, type, userId, keyword, status, isDeleted, orderBy, pageIndex, pageSize);
            string group = ConstantDatas.CACHE_POST_ALL_PATTERN;
            return CacheManager.GetOrAdd(group, key, () =>
            {
                //tạo danh sách columns cho câu truy vấn --> gắn alias p. cho các cột
                List<string> selectedCols = columns.Split(',').ToList();
                for (int i = 0; i < selectedCols.Count; i++)
                {
                    selectedCols[i] = "p." + selectedCols[i].Trim();
                }
                //thêm cột username lấy từ bảng AspNetUsers, gắn vào property CreatedUser của Post
                selectedCols.Add("u.UserName AS CreatedUser__UserName");
                selectedCols.Add("pt.DisplayOrder");
                selectedCols.Add("dx.PageView");

                Sql sql = new Sql();
                sql.Select("DISTINCT " + string.Join(",", selectedCols.ToArray()));
                sql.From("Posts p");

                sql.InnerJoin("AspNetUsers u").On("u.Id = p.CreatedBy");
                sql.LeftJoin("Post_Term pt").On("p.Id = pt.PostId");
                sql.LeftJoin("PostExt dx").On("p.Id = dx.Id");

                sql.Where("PostType=@0", type);
                sql.Where("IsDeleted=@0", isDeleted);
                if (!string.IsNullOrEmpty(termIds))
                {
                    int[] arrTermIds = Array.ConvertAll(termIds.Split(','), int.Parse);
                    if (arrTermIds.Length > 1 || (arrTermIds.Length == 1 && arrTermIds[0] != 0))
                    {
                        sql.Where("pt.TermId IN (@0) ", arrTermIds);
                    }
                }
                if (userId > 0)
                {
                    sql.Where("CreatedBy=@0", userId);
                }
                if(status >= 0)
                {
                    sql.Where("PostStatus=@0", status);
                }
                if(!string.IsNullOrEmpty(keyword))
                {
                    sql.Where("Title LIKE @0", "%" + keyword + "%");
                }

                sql.OrderBy(orderBy);
                string str = sql.SQL;
                Page<Post> posts = db.Page<Post>(pageIndex, pageSize, sql);
                return posts;
            }, CacheHelper.CacheDurationMedium);
        }

        /// <summary>
        /// Get paged list of posts
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public Page<Post> GetPostsByTag(IDictionary<string, object> parameters)
        {
            string type = ValueHelper.TryGet(parameters, "postType", "post");
            //lấy các posts theo tag ids (vd: "1,3,4,5,...")
            string tagIds = ValueHelper.TryGet(parameters, "tagIds", "");
            int status = ValueHelper.TryGet(parameters, "status", -1);
            bool isDeleted = ValueHelper.TryGet(parameters, "isDeleted", false);
            int pageIndex = ValueHelper.TryGet(parameters, "pageIndex", 1);
            int pageSize = ValueHelper.TryGet(parameters, "pageSize", int.MaxValue);

            string columns = @"Id, Title, Slug, Description, MetaKeyword, MetaDescription, 
                    Image, PostType, CreatedBy, UpdatedBy, PublishedBy, CreatedDate, UpdatedDate, PublishedDate, 
                    PostStatus, CommentStatus, IsDeleted";
            string orderBy = ValueHelper.TryGet(parameters, "orderBy", "Id DESC");

            string key = CacheHelper.SetCacheName(ConstantDatas.CACHE_POST_BY_TAG, tagIds, type, status, isDeleted, orderBy, pageIndex, pageSize);
            string group = ConstantDatas.CACHE_POST_ALL_PATTERN;
            return CacheManager.GetOrAdd(group, key, () =>
            {
                //tạo danh sách columns cho câu truy vấn --> gắn alias p. cho các cột
                List<string> selectedCols = columns.Split(',').ToList();
                for (int i = 0; i < selectedCols.Count; i++)
                {
                    selectedCols[i] = "p." + selectedCols[i].Trim();
                }
                //thêm cột username lấy từ bảng AspNetUsers, gắn vào property CreatedUser của Post
                selectedCols.Add("u.UserName AS CreatedUser__UserName");

                Sql sql = new Sql();
                sql.Select("DISTINCT " + string.Join(",", selectedCols.ToArray()));
                sql.From("Posts p");

                sql.InnerJoin("AspNetUsers u").On("u.Id = p.CreatedBy");
                sql.LeftJoin("PostTag pt").On("p.Id = pt.PostId");

                sql.Where("PostType=@0", type);
                sql.Where("IsDeleted=@0", isDeleted);
                if (!string.IsNullOrEmpty(tagIds))
                {
                    int[] arrTermIds = Array.ConvertAll(tagIds.Split(','), int.Parse);
                    if (arrTermIds.Length > 1 || (arrTermIds.Length == 1 && arrTermIds[0] != 0))
                    {
                        sql.Where("pt.TagId IN (@0) ", arrTermIds);
                    }
                }
                if (status >= 0)
                {
                    sql.Where("PostStatus=@0", status);
                }

                sql.OrderBy(orderBy);
                string str = sql.SQL;
                Page<Post> posts = db.Page<Post>(pageIndex, pageSize, sql);
                return posts;
            }, CacheHelper.CacheDurationMedium);
        }

        /// <summary>
        /// Insert a post
        /// </summary>
        /// <param name="post"></param>
        /// <returns></returns>
        public int InsertPost(Post post)
        {
            db.Insert<Post>(post);
            if(post.Id > 0)
            {
                InsertPostExt(post.Id);
            }
            AfterInsert(post);
            return post.Id;
        }

        /// <summary>
        /// Insert a post and add to terms
        /// </summary>
        /// <param name="post"></param>
        /// <param name="categories"></param>
        /// <returns></returns>
        //TO DO: insert post with other term type (categories, tags,...)
        public int InsertPost(Post post, List<int> categories)
        {
            int id = 0;
            using (var transaction = db.GetTransaction())
            {
                id = InsertPost(post);
                if(id > 0 && categories.Count > 0)
                {
                    InsertPostExt(id);
                    InsertPostTerm(id, categories);
                    AfterInsert(post);
                }
                transaction.Complete();
            }
            return id;
        }

        public int InsertPost(PostSaveModel model)
        {
            int id = 0;
            using (var transaction = db.GetTransaction())
            {
                db.Insert<Post>(model.Post);
                id = model.Post.Id;

                if (id > 0)
                {
                    InsertPostExt(id);

                    if (model.Terms != null && model.Terms.Count > 0)
                    {
                        foreach(var term in model.Terms)
                        {
                            term.PostId = id;
                        }
                        InsertPostTerm(id, model.Terms);
                    }

                    if (model.Metas != null && model.Metas.Count > 0)
                    {
                        foreach (var meta in model.Metas)
                        {
                            meta.PostId = id;
                        }
                        InsertMetas(id, model.Metas);
                    }

                    if (model.Tags != null && model.Tags.Count > 0)
                    {
                        foreach (var tag in model.Tags)
                        {
                            tag.PostId = id;
                        }
                        InsertTags(id, model.Tags);
                    }

                    AfterInsert(model.Post);
                }
                transaction.Complete();
            }

            return id;
        }

        /// <summary>
        /// Update a post
        /// </summary>
        /// <param name="post"></param>
        /// <returns></returns>
        public int UpdatePost(Post post)
        {
            AfterUpdate(post);
            return db.Update(post);
        }

        /// <summary>
        /// Update a post with terms
        /// </summary>
        /// <param name="post"></param>
        /// <param name="categories"></param>
        /// <returns></returns>
        //TO DO: update post with other term type (categories, tags,...)
        public int UpdatePost(Post post, List<int> categories)
        {
            int result = 0;
            using (var transaction = db.GetTransaction())
            {
                result = UpdatePost(post);
                if (result > 0 && categories.Count > 0)
                {
                    InsertPostTerm(post.Id, categories);
                    AfterUpdate(post);
                }
                transaction.Complete();
            }
            return result;
        }

        public int UpdatePost(PostSaveModel model)
        {
            int result = 0;
            using (var transaction = db.GetTransaction())
            {
                result = db.Update(model.Post);

                if (result > 0)
                {
                    InsertPostTerm(model.Post.Id, model.Terms);

                    InsertMetas(model.Post.Id, model.Metas);

                    InsertTags(model.Post.Id, model.Tags);

                    AfterUpdate(model.Post);
                }
                transaction.Complete();
            }

            return result;
        }

        /// <summary>
        /// Delete a post by id
        /// </summary>
        /// <param name="id"></param>
        public void DeletePost(int id)
        {
            using (var transaction = db.GetTransaction())
            {
                var post = GetPostById(id);
                if (post != null)
                {
                    //set IsDeleted = true, not actually delete
                    post.IsDeleted = true;
                    db.Update(post);

                    var postTerm = GetPostTermsByPostId(post.Id);
                    foreach(var item in postTerm)
                    {
                        taxonomyService.UpdateTermExCount(item.TermId, "PostCount", -1);
                    }

                    //delete all post - term relationship
                    //db.Delete<PostTerm>("WHERE PostId=@0", id);

                    AfterDelete(post);
                }
                
                transaction.Complete();
            }
        }

        #region Terms
        /// <summary>
        /// Insert post term 
        /// </summary>
        /// <param name="postId"></param>
        /// <param name="termIds"></param>
        public void InsertPostTerm(int postId, List<int> termIds)
        {
            //xoá hết post term relationship hiện có
            db.Delete<PostTerm>("WHERE PostId=@0", postId);

            //insert post - term mới
            List<PostTerm> postTerms = new List<PostTerm>();
            foreach (var item in termIds)
            {
                postTerms.Add(new PostTerm { PostId = postId, TermId = item });
                taxonomyService.UpdateTermExCount(item, "PostCount", 1);
            }
            //sử dụng InsertBatch thay vì làm từng câu Insert
            db.InsertBatch<PostTerm>(postTerms);
        }

        public void InsertPostTerm(int postId, List<PostTerm> terms)
        {
            //xoá hết post term relationship hiện có
            db.Delete<PostTerm>("WHERE PostId=@0", postId);

            //insert post - term mới
            foreach (var item in terms)
            {
                taxonomyService.UpdateTermExCount(item.TermId, "PostCount", 1);
            }
            //sử dụng InsertBatch thay vì làm từng câu Insert
            db.InsertBatch<PostTerm>(terms);
        }

        public List<PostTerm> GetPostTerms(Dictionary<string, object> parameters)
        {
            string postIds = ValueHelper.TryGet(parameters, "postIds", "");

            string key = CacheHelper.SetCacheName(ConstantDatas.CACHE_POST_TERMS, postIds);
            string group = ConstantDatas.CACHE_POST_ALL_PATTERN;
            return CacheManager.GetOrAdd(group, key, () => {
                Sql sql = new Sql();
                sql.Select("p.PostId, p.TermId, p.DisplayOrder, t.Slug AS TermSlug, t.Name AS TermName, t.Taxonomy AS TaxonomyType");
                sql.From("Post_Term p");
                sql.LeftJoin("TaxonomyTerms t").On("p.TermId = t.Id");
                sql.Where("t.IsDeleted=@0", 0);
                if (!string.IsNullOrEmpty(postIds))
                {
                    int[] arrPostIds = Array.ConvertAll(postIds.Split(','), int.Parse);
                    sql.Where("p.PostId IN (@0) ", arrPostIds);
                }
                return db.Fetch<PostTerm>(sql);
            }, CacheHelper.CacheDurationMedium);
        }

        /// <summary>
        /// Get post terms by post id
        /// </summary>
        /// <param name="postId"></param>
        /// <returns></returns>
        public List<PostTerm> GetPostTermsByPostId(int postId)
        {
            string key = CacheHelper.SetCacheName(ConstantDatas.CACHE_POST_TERM_BY_POST_ID, postId);
            string group = CacheHelper.SetCacheName(ConstantDatas.CACHE_POST_GROUP_BY_ID, postId);
            return CacheManager.GetOrAdd(group, key, () => {
                //return db.Fetch<PostTerm>("WHERE PostID=@0", postId);
                Sql sql = new Sql();
                sql.Select("p.PostId, p.TermId, p.DisplayOrder, t.Slug AS TermSlug, t.Name AS TermName, t.Taxonomy AS TaxonomyType");
                sql.From("Post_Term p");
                sql.LeftJoin("TaxonomyTerms t").On("p.TermId = t.Id");
                sql.Where("p.PostId = @0", postId);
                sql.Where("t.IsDeleted=@0", 0);
                
                return db.Fetch<PostTerm>(sql);
            }, CacheHelper.CacheDurationMedium);
        }
        #endregion

        #region Tags
        public List<PostTag> GetTags(int postId)
        {
            string key = CacheHelper.SetCacheName(ConstantDatas.CACHE_POST_TAGS, postId);
            string group = CacheHelper.SetCacheName(ConstantDatas.CACHE_POST_GROUP_BY_ID, postId);
            return CacheManager.GetOrAdd(group, key, () =>
            {
                string sql = @"SELECT dt.Id, dt.PostId, dt.TagId, t.Name AS TagName     
                            FROM PostTag dt 
                            INNER JOIN Tag t ON dt.TagId = t.Id
                            WHERE PostId = @0";
                return db.Fetch<PostTag>(sql, postId);
            }, CacheHelper.CacheDurationMedium);
        }

        public void InsertTags(int postId, List<PostTag> tags)
        {
            if (postId > 0)
            {
                //xoá hết tag cũ
                db.Delete<PostTag>("WHERE PostId=@0", postId);

                //insert tag mới
                if(tags != null && tags.Count > 0)
                {
                    db.InsertBatch<PostTag>(tags);
                }
            }
        }
        #endregion

        #region PostMetas
        public PostMeta GetMetaById(int id)
        {
            return db.SingleOrDefaultById<PostMeta>(id);
        }

        public List<PostMeta> GetMetasByPostId(int postId)
        {
            string key = CacheHelper.SetCacheName(ConstantDatas.CACHE_POST_METAS, postId);
            string group = CacheHelper.SetCacheName(ConstantDatas.CACHE_POST_GROUP_BY_ID, postId);
            return CacheManager.GetOrAdd(group, key, () =>
            {
                return db.Fetch<PostMeta>("WHERE PostId=@0", postId);
            }, CacheHelper.CacheDurationMedium);
            
        }

        public void InsertMetas(int postId, List<PostMeta> metas)
        {
            if (postId > 0)
            {
                db.Delete<PostMeta>("WHERE PostId=@0", postId);
                if (metas != null && metas.Count > 0)
                {
                    db.InsertBatch<PostMeta>(metas);
                }
            }
        }
        #endregion

        #region PostExt
        public PostExt GetPostExtById(int id)
        {
            string key = CacheHelper.SetCacheName(ConstantDatas.CACHE_POST_EXT_BY_ID, id);
            string group = CacheHelper.SetCacheName(ConstantDatas.CACHE_POST_GROUP_BY_ID, id);
            return CacheManager.GetOrAdd(group, key, () => {
                return db.SingleOrDefaultById<PostExt>(id);
            }, CacheHelper.CacheDurationMedium);
        }
        private void InsertPostExt(int postId)
        {
            var item = new PostExt
            {
                Id = postId,
                PageView = 0
            };
            db.Insert(item);
        }
        public int UpdatePostExt(int id, string columnName, int value)
        {
            int result = 0;
            if (value == 0) return 0;
            StringBuilder sbSql = new StringBuilder("UPDATE PostExt SET ");
            sbSql.Append(columnName).Append("=").Append(columnName);
            if (value > 0)
            {
                sbSql.Append("+").Append(value.ToString());
            }
            else
            {
                sbSql.Append("-").Append(Math.Abs(value).ToString());
            }
            sbSql.Append(" WHERE Id = @0");
            result = db.ExecuteScalar<int>(sbSql.ToString(), id);
            return result;
        }
        #endregion

        private void AfterInsert(Post post)
        {
            CacheManager.RemoveByGroup(ConstantDatas.CACHE_POST_ALL_PATTERN);
        }

        private void AfterUpdate(Post post)
        {
            CacheManager.RemoveByGroup(ConstantDatas.CACHE_POST_ALL_PATTERN);
            CacheManager.RemoveByGroup(CacheHelper.SetCacheName(ConstantDatas.CACHE_POST_GROUP_BY_ID, post.Id));
            CacheManager.Remove(CacheHelper.SetCacheName(ConstantDatas.CACHE_POST_BY_SLUG, post.Slug));
        }

        private void AfterDelete(Post post)
        {
            CacheManager.RemoveByGroup(ConstantDatas.CACHE_POST_ALL_PATTERN);
            CacheManager.RemoveByGroup(CacheHelper.SetCacheName(ConstantDatas.CACHE_POST_GROUP_BY_ID, post.Id));
            CacheManager.Remove(CacheHelper.SetCacheName(ConstantDatas.CACHE_POST_BY_SLUG, post.Slug));
        }
    }
}
