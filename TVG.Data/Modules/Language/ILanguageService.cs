﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TVG.Data.Domain;

namespace TVG.Data.Services
{
    public interface ILanguageService
    {
        List<Language> GetLanguages(Dictionary<string, object> parameters = null);
    }
}
