﻿using NPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TVG.Core.Caching;
using TVG.Core.Helper;
using TVG.Core.Logging;
using TVG.Data.Domain;

namespace TVG.Data.Services
{
    

    public class LanguageService : ILanguageService
    {
        private IDatabase db;
        private ICacheManager cache;
        private ILogManager logger;

        public LanguageService(IDatabase database, ICacheManager cache, ILogManager logger)
        {
            this.db = database;
            this.cache = cache;
            this.logger = logger;
        }

        public List<Language> GetLanguages(Dictionary<string, object> parameters = null)
        {
            bool isActive = ValueHelper.TryGet(parameters, "isActive", true);
            return db.Fetch<Language>("WHERE IsActive = @0 ORDER BY DisplayOrder", isActive);
        }
    }
}
