﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NPoco;

namespace TVG.Data.Domain
{
    [TableName("Language")]
    [PrimaryKey("Id", AutoIncrement = true)]
    public class Language
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string CodeName { get; set; }
        public bool IsActive { get; set; }
        public int DisplayOrder { get; set; }
    }
}
