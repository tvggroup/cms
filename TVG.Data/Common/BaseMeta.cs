﻿using NPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TVG.Core.Helper;

namespace TVG.Data.Domain
{
    public class BaseMeta
    {
        public string MetaName { get; set; }
        public string MetaValue { get; set; }
        [Ignore]
        public string MetaType { get; set; }
        [Ignore]
        public string DisplayName { get; set; }
    }

    public static class BaseMetaExtension
    {
        public static string GetMetaValue(this BaseMeta meta)
        {
            var result = "";
            if (meta != null)
            {
                result = meta.MetaValue.TryConvert<string>("");
            }
            return result;
        }

        public static string GetMetaValue<T>(this List<T> metas, string metaKey) where T:BaseMeta
        {
            var result = "";
            if (!string.IsNullOrEmpty(metaKey))
            {
                var meta = metas.SingleOrDefault(x => x.MetaName == metaKey);
                if (meta != null)
                {
                    result = meta.MetaValue.TryConvert<string>("");
                }
            }
            return result;
        }
    }
}
