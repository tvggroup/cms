﻿using NPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TVG.Core.Helper;

namespace TVG.Data.Extensions
{
    public static class NpocoSqlExtension
    {
        /// <summary>
        /// Build FROM clause with Full text search (add WITH(FORCESEEK))
        /// </summary>
        /// <param name="sql">Sql object</param>
        /// <param name="table">Table name</param>
        /// <param name="isSearch">Has Full text search or not</param>
        /// <returns>Sql object</returns>
        public static Sql FromWithFts(this Sql sql, string table, bool isSearch = false)
        {
            table = isSearch ? table + " WITH(FORCESEEK)" : table;
            sql.From(table);
            return sql;
        }

        /// <summary>
        /// Build where clause if condition is true
        /// </summary>
        /// <param name="sql">Sql object</param>
        /// <param name="condition">condition to build where clause</param>
        /// <param name="where">Where clause</param>
        /// <param name="args">Where clause arguments</param>
        /// <returns>Sql object</returns>
        public static Sql WhereIf(this Sql sql, bool condition, string where, params object[] args)
        {
            if(condition)
            {
                sql.Where(where, args);
            }
            return sql;
        }

        /// <summary>
        /// Build where in clause, column type INT
        /// </summary>
        /// <param name="sql">Sql object</param>
        /// <param name="whereColumn">Column name in where clause</param>
        /// <param name="inClause">IN clause, seperated by ',' (ex: "1,2,3")</param>
        /// <returns>Sql object</returns>
        public static Sql WhereInInt(this Sql sql, string whereColumn, string inClause)
        {
            if (!string.IsNullOrEmpty(inClause))
            {
                int[] arrInt = Array.ConvertAll(inClause.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries), int.Parse);
                sql.WhereIf(
                    (arrInt.Length > 1 || (arrInt.Length == 1 && arrInt[0] != 0)),
                    string.Format("{0} IN (@0) ", whereColumn),
                    arrInt
                );
            }
            return sql;
        }

        /// <summary>
        /// Build LEFT JOIN clause if condition is true
        /// </summary>
        /// <param name="sql">Sql object</param>
        /// <param name="condition">Condition to build LEFT JOIN</param>
        /// <param name="tableJoin">Table to be joined</param>
        /// <param name="onClause">ON Clause</param>
        /// <returns>Sql object</returns>
        public static Sql LeftJoinIf(this Sql sql, bool condition, string tableJoin, string onClause)
        {
            if(condition)
            {
                sql.LeftJoin(tableJoin).On(onClause);
            }
            return sql;
        }

        /// <summary>
        /// Build INNER JOIN full text search (CONTAINSTABLE, FREETEXTTABLE)
        /// </summary>
        /// <param name="sql">Sql object</param>
        /// <param name="keyword">Keyword to be search</param>
        /// <param name="table">Full text table</param>
        /// <param name="columns">Full text column</param>
        /// <param name="onClause">ON clause</param>
        /// <returns>Sql object</returns>
        public static Sql FtsJoin(this Sql sql, string keyword, string table, string columns, string onClause)
        {
            if(!string.IsNullOrEmpty(keyword))
            {
                var ftsTable = StringHelper.CreateFTSTable(keyword, table, columns);
                sql.InnerJoin(ftsTable).On(onClause);
            }
            return sql;
        }

        
    }
}
