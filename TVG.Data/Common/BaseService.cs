﻿using NPoco;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TVG.Core.Caching;
using TVG.Core.Helper;

namespace TVG.Data.Services
{
    public class BaseService
    {
        protected IDatabase db;
        protected ICacheManager cache;
        protected int cacheDatabaseNumber = 1;

        protected bool IsCacheEnabled
        {
            get
            {
                return ConfigurationManager.AppSettings["cacheEnabled"].TryConvert<bool>(false);
            }
        }

        public ICacheManager CacheManager
        {
            get
            {
                if(IsCacheEnabled)
                {
                    cache.DatabaseNumber = cacheDatabaseNumber;
                    return cache;
                } 
                else
                {
                    return new DummyCacheManager();
                }
            }
        }
    }
}
