﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVG.Data
{
    public class ConstantDatas
    {
        public const int CACHE_REDIS_DATABASE_DEFAULT = 0;
        public const int CACHE_REDIS_DATABASE_USERS = 1;
        public const int CACHE_REDIS_DATABASE_POSTS = 2;
        public const int CACHE_REDIS_DATABASE_LAW_DOCUMENTS = 3;
        public const int CACHE_REDIS_DATABASE_DOCUMENTS = 4;
        public const int CACHE_REDIS_DATABASE_DOCUMENT_POSTS = 5;
        public const int CACHE_REDIS_DATABASE_DOCUMENT_CASES = 6;
        public const int CACHE_REDIS_DATABASE_TASKS = 7;
        public const int CACHE_REDIS_DATABASE_PLACES = 8;

        public const string CACHE_TAXONOMY_PATTERN = "cache.taxonomy";
        public const string CACHE_TAXONOMY_BY_NAME = "cache.taxonomy.name-";
        public const string CACHE_TAXONOMY_TERM_BY_ID = "cache.taxonomy.term.id-";
        public const string CACHE_TAXONOMY_TERM_BY_SLUG = "cache.taxonomy.term.slug-";
        public const string CACHE_TAXONOMY_TERM_ALL = "cache.taxonomy.term.all-";
        public const string CACHE_TAXONOMY_TERM_META_BY_TERM_ID = "cache.taxonomy.term.meta.term.id-";

        public const string CACHE_TAG_PATTERN = "cache.taxonomy";
        public const string CACHE_TAG_BY_ID = "cache.taxonomy.term.id-";
        public const string CACHE_TAG_BY_SLUG = "cache.taxonomy.term.slug-";
        public const string CACHE_TAG_ALL = "cache.taxonomy.term.all-";

        public const string CACHE_POST_PATTERN = "cache.post.";
        public const string CACHE_POST_ALL_PATTERN = "cache.post.all";
        public const string CACHE_POST_ALL = "cache.post.all-";
        public const string CACHE_POST_BY_TAG = "cache.post.by.tag-";
        public const string CACHE_POST_GROUP_BY_ID = "cache.post.group.id-";
        public const string CACHE_POST_BY_ID = "cache.post.id-";
        public const string CACHE_POST_BY_SLUG = "cache.post.slug-";
        public const string CACHE_POST_TERMS = "cache.post.terms-";
        public const string CACHE_POST_TERM_BY_POST_ID = "cache.post.term.post.id-";
        public const string CACHE_POST_TAGS = "cache.post.tags-";
        public const string CACHE_POST_METAS = "cache.post.metas-";
        public const string CACHE_POST_EXT_BY_ID = "cache.post.ext.id-";

        public const string CACHE_PLACE_PATTERN = "cache.place.";
        public const string CACHE_PLACE_ALL_PATTERN = "cache.place.all";
        public const string CACHE_PLACE_ALL = "cache.place.all-";
        public const string CACHE_PLACE_BY_TAG = "cache.place.by.tag-";
        public const string CACHE_PLACE_GROUP_BY_ID = "cache.place.group.id-";
        public const string CACHE_PLACE_BY_ID = "cache.place.id-";
        public const string CACHE_PLACE_BY_SLUG = "cache.place.slug-";
        public const string CACHE_PLACE_TERMS = "cache.place.terms-";
        public const string CACHE_PLACE_TERM_BY_PLACE_ID = "cache.place.term.place.id-";
        public const string CACHE_PLACE_TAGS = "cache.place.tags-";
        public const string CACHE_PLACE_METAS = "cache.place.metas-";
        public const string CACHE_PLACE_EXT_BY_ID = "cache.place.ext.id-";

        public const string CACHE_LAW_DOCUMENT_PATTERN = "cache.lawdocument";
        public const string CACHE_LAW_DOCUMENT_GROUP_BY_ID = "cache.lawdocument.group-";
        public const string CACHE_LAW_DOCUMENT_BY_ID = "cache.lawdocument.id-";
        public const string CACHE_LAW_DOCUMENT_BY_COLUMN = "cache.law.document.column-";
        public const string CACHE_LAW_DOCUMENT_ALL_PATTERN = "cache.lawdocument.all";
        public const string CACHE_LAW_DOCUMENT_ALL = "cache.lawdocument.all-";
        public const string CACHE_LAW_DOCUMENT_META_BY_ID = "cache.lawdocument.meta.id-";
        public const string CACHE_LAW_DOCUMENT_META_BY_DOCUMENT_ID = "cache.lawdocument.meta.document.id-";
        public const string CACHE_LAW_DOCUMENT_RELATED_DOCUMENTS = "cache.lawdocument.related.documents-";
        public const string CACHE_LAW_DOCUMENT_ISSUED_PLACES = "cache.lawdocument.issued.places-";
        public const string CACHE_LAW_DOCUMENT_TAGS = "cache.lawdocument.tags-";
        public const string CACHE_LAW_DOCUMENT_CONTROL_TAGS = "cache.lawdocument.control.tags-";
        public const string CACHE_LAW_DOCUMENT_CONTROL_TAGS_BY_DOCUMENT_ID = "cache.lawdocument.control.tags.document.id-";
        public const string CACHE_LAW_DOCUMENT_ATTACHMENT_BY_ID = "cache.lawdocument.attachment.id-";
        public const string CACHE_LAW_DOCUMENT_ATTACHMENT_BY_DOCUMENT_ID = "cache.lawdocument.attachment.document.id-";
        public const string CACHE_LAW_DOCUMENT_TERM_BY_DOCUMENT_ID = "cache.lawdocument.term.document.id-";
        public const string CACHE_LAW_DOCUMENT_TERM_BY_TERM_ID = "cache.lawdocument.term.term.id-";
        public const string CACHE_LAW_DOCUMENT_NOTE_BY_DOCUMENT_ID = "cache.lawdocument.note.document.id-";
        public const string CACHE_LAW_DOCUMENT_EXT_BY_ID = "cache.lawdocument.ext.id-";

        public const string CACHE_DOCUMENT_PATTERN = "cache.document.";
        public const string CACHE_DOCUMENT_ALL_PATTERN = "cache.document.all";
        public const string CACHE_DOCUMENT_ALL = "cache.document.all-";
        public const string CACHE_DOCUMENT_GROUP_BY_ID = "cache.document.group-";
        public const string CACHE_DOCUMENT_BY_ID = "cache.document.id-";
        public const string CACHE_DOCUMENT_META_BY_DOCUMENT_ID = "cache.document.meta.document-";
        public const string CACHE_DOCUMENT_RELATED_DOCUMENTS = "cache.document.related.documents-";
        public const string CACHE_DOCUMENT_TAGS = "cache.document.tags-";
        public const string CACHE_DOCUMENT_ATTACHMENT_BY_ID = "cache.document.attachment.id-";
        public const string CACHE_DOCUMENT_ATTACHMENT_BY_DOCUMENT_ID = "cache.document.attachment.document.id-";
        public const string CACHE_DOCUMENT_TERM_BY_DOCUMENT_ID = "cache.document.term.document.id-";
        public const string CACHE_DOCUMENT_NOTE_BY_DOCUMENT_ID = "cache.document.note.document.id-";
        public const string CACHE_DOCUMENT_EXT_BY_ID = "cache.document.ext.id-";

        public const string CACHE_DOCUMENT_CASE_PATTERN = "cache.documentcase.";
        public const string CACHE_DOCUMENT_CASE_ALL_PATTERN = "cache.documentcase.all";
        public const string CACHE_DOCUMENT_CASE_ALL = "cache.documentcase.all-";
        public const string CACHE_DOCUMENT_CASE_GROUP_BY_ID = "cache.documentcase.group-";
        public const string CACHE_DOCUMENT_CASE_BY_ID = "cache.documentcase.id-";
        public const string CACHE_DOCUMENT_CASE_TERM_BY_DOCUMENT_ID = "cache.documentcase.term.document.id-";
        public const string CACHE_DOCUMENT_CASE_TAGS = "cache.documentcase.tags-";
        public const string CACHE_DOCUMENT_CASE_META_BY_DOCUMENT_ID = "cache.documentcase.meta.document.id-";
        public const string CACHE_DOCUMENT_CASE_ATTACHMENT_BY_ID = "cache.documentcase.attachment.id-";
        public const string CACHE_DOCUMENT_CASE_ATTACHMENT_BY_DOCUMENT_ID = "cache.documentcase.attachment.document.id-";
        public const string CACHE_DOCUMENT_CASE_RELATED_LAWDOCUMENT = "cache.documentcase.related.lawdocument-";
        public const string CACHE_DOCUMENT_CASE_NOTE_BY_DOCUMENT_ID = "cache.documentcase.note.document.id-";
        public const string CACHE_DOCUMENT_CASE_EXT_BY_ID = "cache.documentcase.ext.id-";

        public const string CACHE_DOCUMENT_POST_PATTERN = "cache.documentpost.";
        public const string CACHE_DOCUMENT_POST_ALL_PATTERN = "cache.documentpost.all";
        public const string CACHE_DOCUMENT_POST_ALL = "cache.documentpost.all-";
        public const string CACHE_DOCUMENT_POST_GROUP_BY_ID = "cache.documentpost.group-";
        public const string CACHE_DOCUMENT_POST_BY_ID = "cache.documentpost.id-";
        public const string CACHE_DOCUMENT_POST_TERM_BY_DOCUMENT_ID = "cache.documentpost.term.document.id-";
        public const string CACHE_DOCUMENT_POST_TAGS = "cache.documentpost.tags-";
        public const string CACHE_DOCUMENT_POST_META_BY_DOCUMENT_ID = "cache.documentpost.meta.document.id-";
        public const string CACHE_DOCUMENT_POST_ATTACHMENT_BY_ID = "cache.documentpost.attachment.id-";
        public const string CACHE_DOCUMENT_POST_ATTACHMENT_BY_DOCUMENT_ID = "cache.documentpost.attachment.document.id-";
        public const string CACHE_DOCUMENT_POST_RELATED_LAWDOCUMENT = "cache.documentpost.related.lawdocument-";
        public const string CACHE_DOCUMENT_POST_NOTE_BY_DOCUMENT_ID = "cache.documentpost.note.document.id-";
        public const string CACHE_DOCUMENT_POST_EXT_BY_ID = "cache.documentpost.ext.id-";

        public const string CACHE_SETTING_PATTERN = "cache.setting";
        public const string CACHE_SETTING_BY_GROUP = "cache.setting.group-";
        public const string CACHE_SETTING_BY_ID = "cache.setting.id-";
        public const string CACHE_SETTING_BY_NAME = "cache.setting.name-";

        public const string CACHE_BANNER_PATTERN = "cache.banner";
        public const string CACHE_BANNERS = "cache.banner.all-";//cache.banner.all-<isDeleted>
        public const string CACHE_BANNER_BY_ID = "cache.banner.id-";
        public const string CACHE_BANNER_BY_SLUG = "cache.banner.slug-";
        public const string CACHE_BANNER_IMAGES = "cache.banner.images-";//cache.banner.images-<bannerId>

        public const string CACHE_PERMISSION_PATTERN = "cache.permission";
        public const string CACHE_PERMISSION_ALL = "cache.permission.all-";//cache.permission.all-<pageIndex>;<pageSize>
        public const string CACHE_PERMISSION_ALL_BY_ROLE_ID = "cache.permission.all.roleId-";
        public const string CACHE_PERMISSION_ALL_BY_ROLE_NAME = "cache.permission.all.roleName-";
        public const string CACHE_PERMISSION_BY_PERMISSION_ID_ROLE_ID = "cache.permission.permissionId.roleId-";
        public const string CACHE_PERMISSION_ROLE_ACCESS_ADMIN = "cache.permission.role.access.admin";

        public const string CACHE_TASK_PATTERN = "cache.task.";
        public const string CACHE_TASK_ALL_PATTERN = "cache.task.alll";
        public const string CACHE_TASK_ALL = "cache.task.all-";
        public const string CACHE_TASK_GROUP_ID = "cache.task.group-";
        public const string CACHE_TASK_BY_ID = "cache.task.id-";
        public const string CACHE_TASK_LOG_BY_TASK_ID = "cache.task.log.task.id-";
        public const string CACHE_TASK_LOG_BY_ID = "cache.task.log.id-";
        public const string CACHE_TASK_ASSIGNED_USERS_BY_TASK_ID = "cache.task.assigned.users.task.id-";
        public const string CACHE_TASK_TERMS_BY_TASK_ID = "cache.task.terms.task.id-";
        public const string CACHE_TASK_COMMENT_BY_TASK_ID = "cache.task.comment.by.task.id-";
        public const string CACHE_TASK_COMMENT_GROUP_ID = "cache.task.comment.group-";
        public const string CACHE_TASK_COMMENT_BY_ID = "cache.task.comment.id-";
        public const string CACHE_TASK_COMMENT_COUNT_BY_TASK_ID = "cache.task.comment.count.by.task.id-";


        public const string CACHE_USER_PATTERN = "cache.user.";
        public const string CACHE_USER_ALL_PATTERN = "cache.user.all";
        public const string CACHE_USER_ALL = "cache.user.all-";
        public const string CACHE_USER_GROUP_BY_ID = "cache.user.group-";
        public const string CACHE_USER_BY_ID = "cache.user.id-";
        public const string CACHE_USER_BY_USERNAME = "cache.user.username-";
        public const string CACHE_USER_ROLES = "cache.user.roles-";

        public const string CACHE_ROLE_PATTERN = "cache.role.";
        public const string CACHE_ROLE_ALL_PATTERN = "cache.role.all";
        public const string CACHE_ROLE_ALL = "cache.role.all-";
        public const string CACHE_ROLE_GROUP_BY_ID = "cache.role.group-";
        public const string CACHE_ROLE_BY_ID = "cache.role.id-";

        public const int VBLQ_DUOC_HUONG_DAN = 1;
        public const int VBLQ_HUONG_DAN = 2;
        public const int VBLQ_DUOC_HOP_NHAT = 3;
        public const int VBLQ_HOP_NHAT = 4;
        public const int VBLQ_BI_SUA_DOI = 5;
        public const int VBLQ_SUA_DOI = 6;
        public const int VBLQ_BI_DINH_CHINH = 7;
        public const int VBLQ_DINH_CHINH = 8;
        public const int VBLQ_BI_THAY_THE = 9;
        public const int VBLQ_THAY_THE = 10;
        public const int VBLQ_DUOC_DAN_CHIEU = 11;
        public const int VBLQ_CAN_CU = 12;
        public const int VBLQ_NOI_DUNG = 13;

        public static DateTime MIN_DATE = new DateTime(1990, 1, 1);
        public static DateTime MAX_DATE = DateTime.Today;

        public static Dictionary<string, PostStatus> PostStatus = new Dictionary<string, PostStatus>() {
            { "-1", new PostStatus {Id = -1, Code = "khong-xac-dinh", Name = "Không xác định", Color = "#e4e4e4", ShowInList = true } },
            { "1", new PostStatus {Id = 1, Code = "da-duyet", Name = "Đã duyệt", Color = "#4caf50", ShowInList = false } },
            { "0", new PostStatus {Id = 0, Code = "cho-duyet", Name = "Chờ duyệt", Color = "#03a9f4", ShowInList = true } },
            { "2", new PostStatus {Id = 2, Code = "bi-tra-ve", Name = "Bị trả về", Color = "#F44336", ShowInList = false } },
            { "3", new PostStatus {Id = 3, Code = "nhap", Name = "Nháp", Color = "#9E9E9E", ShowInList = false } }
        };

        public static Dictionary<string, PlaceStatus> PlaceStatus = new Dictionary<string, PlaceStatus>() {
            { "-1", new PlaceStatus {Id = -1, Code = "khong-xac-dinh", Name = "Không xác định", Color = "#e4e4e4", ShowInList = true } },
            { "1", new PlaceStatus {Id = 1, Code = "da-duyet", Name = "Đã duyệt", Color = "#4caf50", ShowInList = false } },
            { "0", new PlaceStatus {Id = 0, Code = "cho-duyet", Name = "Chờ duyệt", Color = "#03a9f4", ShowInList = true } },
            { "2", new PlaceStatus {Id = 2, Code = "bi-tra-ve", Name = "Bị trả về", Color = "#F44336", ShowInList = false } },
            { "3", new PlaceStatus {Id = 3, Code = "nhap", Name = "Nháp", Color = "#9E9E9E", ShowInList = false } }
        };

    }

    public class PostStatus
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Color { get; set; }
        public bool ShowInList { get; set; }

        public const string KHONG_XAC_DINH = "khong-xac-dinh";
        public const string DA_DUYET = "da-duyet";
        public const string CHO_DUYET = "cho-duyet";
        public const string BI_TRA_VE = "bi-tra-ve";
        public const string NHAP = "nhap";

        public static Dictionary<string, PostStatus> POST_STATUS = new Dictionary<string, PostStatus>() {
            { "-1", new PostStatus {Id = -1, Code = KHONG_XAC_DINH, Name = "Không xác định", Color = "#e4e4e4", ShowInList = true } },
            { "1", new PostStatus {Id = 1, Code = DA_DUYET, Name = "Đã duyệt", Color = "#4caf50", ShowInList = false } },
            { "0", new PostStatus {Id = 0, Code = CHO_DUYET, Name = "Chờ duyệt", Color = "#03a9f4", ShowInList = true } },
            { "2", new PostStatus {Id = 2, Code = BI_TRA_VE, Name = "Bị trả về", Color = "#F44336", ShowInList = false } },
            { "3", new PostStatus {Id = 3, Code = NHAP, Name = "Nháp", Color = "#9E9E9E", ShowInList = false } }
        };

        public static PostStatus GetPostStatusByCode(string code)
        {
            var status = ConstantDatas.PostStatus.Where(x => x.Value.Code.Equals(code, StringComparison.InvariantCultureIgnoreCase))
                .Select(x => x.Value)
                .FirstOrDefault();
            return status;
            
        }

        public static int GetPostStatusIdByCode(string code)
        {
            int statusId = -1;
            var status = ConstantDatas.PostStatus.Where(x => x.Value.Code.Equals(code, StringComparison.InvariantCultureIgnoreCase))
                .Select(x => x.Value)
                .FirstOrDefault();
            if (status != null)
            {
                statusId = status.Id;
            }
            return statusId;

        }
    }

    public class PlaceStatus
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Color { get; set; }
        public bool ShowInList { get; set; }

        public const string KHONG_XAC_DINH = "khong-xac-dinh";
        public const string DA_DUYET = "da-duyet";
        public const string CHO_DUYET = "cho-duyet";
        public const string BI_TRA_VE = "bi-tra-ve";
        public const string NHAP = "nhap";

        public static Dictionary<string, PlaceStatus> PLACE_STATUS = new Dictionary<string, PlaceStatus>() {
            { "-1", new PlaceStatus {Id = -1, Code = KHONG_XAC_DINH, Name = "Không xác định", Color = "#e4e4e4", ShowInList = true } },
            { "1", new PlaceStatus {Id = 1, Code = DA_DUYET, Name = "Đã duyệt", Color = "#4caf50", ShowInList = false } },
            { "0", new PlaceStatus {Id = 0, Code = CHO_DUYET, Name = "Chờ duyệt", Color = "#03a9f4", ShowInList = true } },
            { "2", new PlaceStatus {Id = 2, Code = BI_TRA_VE, Name = "Bị trả về", Color = "#F44336", ShowInList = false } },
            { "3", new PlaceStatus {Id = 3, Code = NHAP, Name = "Nháp", Color = "#9E9E9E", ShowInList = false } }
        };

        public static PlaceStatus GetPlaceStatusByCode(string code)
        {
            var status = ConstantDatas.PlaceStatus.Where(x => x.Value.Code.Equals(code, StringComparison.InvariantCultureIgnoreCase))
                .Select(x => x.Value)
                .FirstOrDefault();
            return status;

        }

        public static int GetPlaceStatusIdByCode(string code)
        {
            int statusId = -1;
            var status = ConstantDatas.PlaceStatus.Where(x => x.Value.Code.Equals(code, StringComparison.InvariantCultureIgnoreCase))
                .Select(x => x.Value)
                .FirstOrDefault();
            if (status != null)
            {
                statusId = status.Id;
            }
            return statusId;

        }
    }


}
